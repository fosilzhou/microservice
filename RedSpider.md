# 微服务常用套件

## Eureka注册中心

## Nacos注册中心配置中心

## Zookeeper注册中心配置中心

## Consul注册中心配置中心

## apollo配置中心

## gateway网关

## zuul网关

## Soul网关

##RocketMQ消息队列

## RabbitMQ消息队列

## Kafka消息队列

## Feign服务调用

## Ribbon负载均衡

## Hystrix服务降级

## Sentinel服务降级

## Dubbo RPC服务调用

## Seata分布式事务

##  SkyWalking链路追踪

##  Zipkin链路追踪

## Mycat分库分表

## ShardingSphere分库分表
