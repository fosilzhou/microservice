package com.fosilzhou.webssh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSSHApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(WebSSHApplication.class, args);
	}

}
