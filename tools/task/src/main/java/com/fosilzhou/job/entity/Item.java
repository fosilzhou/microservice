package com.fosilzhou.job.entity;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 存放队列的元素
 */
public class Item<T> implements Delayed {

    /**
     * 到期时间（秒）
     */
    private long activeTime;

    /**
     * 业务数据
     */
    private T data;

    public Item(long activeTime, T data) {
        this.activeTime = activeTime * 1000 + System.currentTimeMillis();
        this.data = data;
    }

    public long getActiveTime() {
        return activeTime;
    }

    public T getData() {
        return data;
    }

    /**
     * 返回激活日期的剩余时间
     * @param unit
     * @return
     */
    @Override
    public long getDelay(TimeUnit unit) {
        long time = unit.convert(this.activeTime - System.currentTimeMillis(), unit);
        return time;
    }

    /**
     * 按剩余时间排序，实际计算考虑精度为纳秒数
     * @param o
     * @return
     */
    @Override
    public int compareTo(Delayed o) {
        long d = (getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS));
        if(d < 0){
            return -1;
        }else if(d == 0) {
            return 0;
        }else{
            return 1;
        }
    }
}
