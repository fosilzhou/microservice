package com.fosilzhou.job.itf;

import com.fosilzhou.job.entity.TaskResult;

/**
 * 任务实现接口
 */
public interface ITaskProcesser<T, R> {

    TaskResult<R> taskExecute(T data);
}
