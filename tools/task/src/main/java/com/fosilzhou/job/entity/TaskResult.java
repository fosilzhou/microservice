package com.fosilzhou.job.entity;

/**
 * 任务处理返回结果
 * @param <R>
 */
public class TaskResult<R> {

    /**
     * 方法执行结果
     */
    private final TaskResultType taskResultType;
    private final R returValue;
    private final String reason;

    public TaskResult(TaskResultType taskResultType, R returValue, String reason) {
        this.taskResultType = taskResultType;
        this.returValue = returValue;
        this.reason = reason;
    }

    public TaskResultType getTaskResultType() {
        return taskResultType;
    }

    public R getReturValue() {
        return returValue;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public String toString() {
        return "任务执行结果：{" +
                "方式结束类型=" + taskResultType +
                ", 任务返回结果=" + returValue +
                ", 任务失败原因='" + reason + '\'' +
                '}';
    }
}
