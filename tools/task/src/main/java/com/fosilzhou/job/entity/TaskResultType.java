package com.fosilzhou.job.entity;

/**
 * 方法执行结果类型
 */
public enum TaskResultType {
    /**
     * 方法执行完成，业务结果正确结束
     */
    SUCCESS,
    /**
     * 方法执行完成，业务结果错误结束
     */
    FAILURE,
    /**
     * 方法执行抛出异常
     */
    EXCEPTION
}
