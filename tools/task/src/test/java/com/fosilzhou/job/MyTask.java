package com.fosilzhou.job;

import com.fosilzhou.job.entity.TaskResult;
import com.fosilzhou.job.entity.TaskResultType;
import com.fosilzhou.job.itf.ITaskProcesser;

import java.util.Random;

public class MyTask implements ITaskProcesser<Integer, Integer> {
    @Override
    public TaskResult<Integer> taskExecute(Integer data) {
        Random r = new Random();
        int flag = r.nextInt(500);
        try {
            Thread.sleep(flag);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (flag <= 300) {
            // 正常处理的情况
            Integer returnValue = data.intValue() + flag;
            return new TaskResult<Integer>(TaskResultType.SUCCESS, returnValue, "success");
        } else if (flag > 301 && flag <= 400) {
            // 处理失败的情况
            return new TaskResult<Integer>(TaskResultType.FAILURE, -1, "failure");
        } else {
            // 发生异常的情况
            try {
                throw new RuntimeException("异常发生了！！");
            } catch (Exception e) {
                return new TaskResult<Integer>(TaskResultType.EXCEPTION, -1, e.getMessage());
            }
        }
    }
}
