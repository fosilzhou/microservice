package com.fosilzhou.cloud.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class RedisController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping("/redis")
    public void getUser(String key, String value) {
        if(StringUtils.isEmpty(key)){
            key = "fosilzhou";
        }
        if(StringUtils.isEmpty(value)){
            value = "心随梦飞";
        }
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        ops.set(key, value, 10, TimeUnit.SECONDS);// 10秒过期
        logger.info("保存Redis Key为："+ key);
    }
}
