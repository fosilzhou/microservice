package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisKeyInvalidApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(RedisKeyInvalidApplication.class, args);
	}

}
