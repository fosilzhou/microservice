package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <servlet>
 *     <servlet-name>baidu</servlet-name>
 *     <servlet-class>org.mitre.dsmiley.httpproxy.ProxyServlet</servlet-class>
 *     <init-param>
 *       <param-name>targetUri</param-name>
 *       <param-value>http://www.baidu.com</param-value>
 *     </init-param>
 *     <init-param>
 *       <param-name>log</param-name>
 *       <param-value>true</param-value>
 *     </init-param>
 * </servlet>
 * <servlet-mapping>
 * 	<servlet-name>baidu</servlet-name>
 * 	<url-pattern>/api/*</url-pattern>
 * </servlet-mapping>
 */
@SpringBootApplication
public class ProxyApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ProxyApplication.class, args);
	}

}
