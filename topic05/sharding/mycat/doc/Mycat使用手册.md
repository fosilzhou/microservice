# Mycat快速开始

## Mycat简介

Mycat 是一个开源的分布式数据库系统，但是由于真正的数据库需要存储引擎，而 Mycat 并没有存 储引擎，所以并不是完全意义的分布式数据库系统

MyCat是目前最流行的**基于Java语言编写的**数据库中间件，也可以理解为是数据库代理。在架构体系中是位于数据库和应用层之间的一个组件，并且对于应用层是透明的，即数据库 感受不到mycat的存在，认为是直接连接的mysql数据库（实际上是连接的mycat,mycat实现了mysql的原生协议）

MyCat是基于**阿里开源的Cobar**产品而研发，Cobar的稳定性、可靠性、优秀的架构和性能以及众多成熟的使用案例使得MyCat变得非常的强大。

mycat的三大功能：**分库分表、读写分离、主从切换**

![](assets/mycat01.png)

## Mycat部署

**编写Dockerfile**

```dockerfile
MAINTAINER fosilzhou<fosilzhou@foxmail.com>
 
LABEL name="Min Chow MyCat Image" \
    build-date="20200504"
    

ADD jdk-8u144-linux-x64.tar.gz /usr/local/
ADD Mycat-server-1.6.7.4-release-20200105164103-linux.tar.gz /usr/local/
 
ENV WORKPATH /usr/local/mycat/
WORKDIR $WORKPATH
 
ENV JAVA_HOME /usr/local/jdk1.8.0_144
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin

EXPOSE 8066
CMD /usr/local/mycat/bin/mycat console

```

```shell
# Mycat启动方式
bin目录 ./mycat console # 控制台方式启动
bin目录 ./mycat start # 后台启动

# 构建命令
docker build -f MycatDockerfile -t fosilzhou/mycat:1.0 .

# 启动容器
docker run -p 8066:8066 镜像ID

# 进入容器
docker exec -it 容器ID /bin/bash

# 拷贝文件到宿主机
docker cp 容器ID:/usr/local/mycat/conf/ /home/docker/mycat/
docker cp 容器ID:/usr/local/mycat/logs/ /home/docker/mycat/

# 启动容器
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ 镜像ID
```

## Mycat配置

Mycat的conf目录下有三个重要配置文件：

- schema.xml：定义逻辑库，表，分片节点等信息
- rule.xml：定义分片规则
- server.xml：定义用户以及系统相关变量

## Mycat连接

- 端口：8066
- 账号：root
- 密码：123456
- 逻辑库：TESTDB

![](assets/mycat02.png)

![](assets\mycat03.png)

**逻辑库，相对于Mysql这类的真实数据库而言，就是真实数据库的代理；**

**逻辑库可以对应后端多个物理数据库，逻辑库中并不保存数据；**


# MySQL主从复制

## 主从原理

主从同步过程中主服务器有一个工作线程I/O dump thread，从服务器有两个工作线程I/O thread和SQL thread。

主库把外界接收的SQL请求记录到自己的binlog日志中，从库的I/O thread去请求主库的binlog日志，并将binlog日志写到中继日志中，然后从库重做中继日志的SQL语句。主库通过I/O dump thread给从库I/O thread传送binlog日志

![](assets/mycat04.png)
![](assets/mycat05.png)

## 主从部署

```shell
# 启动容器
docker run -p 3306:3306 -d -e MYSQL_ROOT_PASSWORD=123456 镜像ID

docker exec -it 容器ID /bin/bash

# 拷贝文件到宿主机
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/master/
docker cp 容器ID:/var/log/ /home/docker/master/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/slave/
docker cp 容器ID:/var/log/ /home/docker/slave/

# 启动容器
docker run -p 3306:3306 --name master -d -v /home/docker/master/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/master/log/:/var/log -e MYSQL_ROOT_PASSWORD=123456 镜像ID
docker run -p 3307:3306 --name slave -d -v /home/docker/slave/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/slave/log/:/var/log -e MYSQL_ROOT_PASSWORD=123456 镜像ID
```

## 配置固定IP

```shell
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3306:3306 --name master -d -v /home/docker/master/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/master/log/:/var/log --net mycatnet --ip 172.20.0.2 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3307:3306 --name slave -d -v /home/docker/slave/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/slave/log/:/var/log --net mycatnet --ip 172.20.0.3 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

## 现在启动两个Mysql容器，并且固定分配IP为172.20.0.2和172.20.0.3
## 注意：这里必须用172.20.0.2开始分配，因为172.20.0.1是网关；

#查看容器IP地址信息
docker inspect 容器id

# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```

## 主从配置

主库配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=2
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=order_db
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
```

主库命令

(Navicat工具：选择数据库弹出菜单-->命令列界面，执行以下命令)

```shell
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      154 | order_db     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set

mysql> CREATE USER 'slave1'@'172.20.0.3' IDENTIFIED BY '123456';
Query OK, 0 rows affected

mysql> GRANT REPLICATION SLAVE ON *.* TO 'slave1'@'172.20.0.3';
Query OK, 0 rows affected

mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected

mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      761 | order_db     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set
```

从库配置

```mysql
[mysqld]
server-id=3
relay-log=mysql-relay
```

从库命令

(Navicat工具：选择数据库弹出菜单-->命令列界面，执行以下命令)

```shell
mysql> CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave1', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=761;
Query OK, 0 rows affected

mysql> START SLAVE;
Query OK, 0 rows affected

mysql> SHOW SLAVE STATUS;
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Slave_IO_State                   | Master_Host | Master_User | Master_Port | Connect_Retry | Master_Log_File  | Read_Master_Log_Pos | Relay_Log_File     | Relay_Log_Pos | Relay_Master_Log_File | Slave_IO_Running | Slave_SQL_Running | Replicate_Do_DB | Replicate_Ignore_DB | Replicate_Do_Table | Replicate_Ignore_Table | Replicate_Wild_Do_Table | Replicate_Wild_Ignore_Table | Last_Errno | Last_Error | Skip_Counter | Exec_Master_Log_Pos | Relay_Log_Space | Until_Condition | Until_Log_File | Until_Log_Pos | Master_SSL_Allowed | Master_SSL_CA_File | Master_SSL_CA_Path | Master_SSL_Cert | Master_SSL_Cipher | Master_SSL_Key | Seconds_Behind_Master | Master_SSL_Verify_Server_Cert | Last_IO_Errno | Last_IO_Error | Last_SQL_Errno | Last_SQL_Error | Replicate_Ignore_Server_Ids | Master_Server_Id | Master_UUID                          | Master_Info_File           | SQL_Delay | SQL_Remaining_Delay | Slave_SQL_Running_State                                | Master_Retry_Count | Master_Bind | Last_IO_Error_Timestamp | Last_SQL_Error_Timestamp | Master_SSL_Crl | Master_SSL_Crlpath | Retrieved_Gtid_Set | Executed_Gtid_Set | Auto_Position | Replicate_Rewrite_DB | Channel_Name | Master_TLS_Version |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Waiting for master to send event | 172.20.0.2  | slave1      |        3306 |            60 | mysql-bin.000001 |                 761 | mysql-relay.000002 |           320 | mysql-bin.000001      | Yes              | Yes               |                 |                     |                    |                        |                         |                             |          0 |            |            0 |                 761 |             523 | None            |                |             0 | No                 |                    |                    |                 |                   |                |                     0 | No                            |             0 |               |              0 |                |                             |                2 | 8460233e-6cdb-11ea-86de-0242ac140002 | /var/lib/mysql/master.info |         0 | NULL                | Slave has read all relay log; waiting for more updates |              86400 |             |                         |                          |                |                    |                    |                   |             0 |                      |              |                    |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
1 row in set
```



**binlog_format详解**

mysql复制主要有三种方式：基于SQL语句的复制(statement-based replication, SBR)，基于行的复制(row-based replication, RBR)，混合模式复制(mixed-based replication, MBR)。对应的，binlog的格式也有三种：STATEMENT，ROW，MIXED。

① STATEMENT模式（SBR）
每一条会修改数据的sql语句会记录到binlog中。优点是并不需要记录每一条sql语句和每一行的数据变化，减少了binlog日志量，节约IO，提高性能。缺点是在某些情况下会导致master-slave中的数据不一致(如sleep()函数， last_insert_id()，以及user-defined functions(udf)等会出现问题)

② ROW模式（RBR）
不记录每条sql语句的上下文信息，仅需记录哪条数据被修改了，修改成什么样了。而且不会出现某些特定情况下的存储过程、或function、或trigger的调用和触发无法被正确复制的问题。缺点是会产生大量的日志，尤其是alter table的时候会让日志暴涨。

③ MIXED模式（MBR）
以上两种模式的混合使用，一般的复制使用STATEMENT模式保存binlog，对于STATEMENT模式无法复制的操作使用ROW模式保存binlog，MySQL会根据执行的SQL语句选择日志保存方式。

## 主从测试

```sql
CREATE DATABASE order_db;
```
## 常用命令
```shell
## 启动从库（START SLAVE）
同时启动I/O 线程和SQL线程。
I/O线程从主库读取binlog，并存储到relaylog中继日志文件中。
SQL线程读取中继日志，解析后，在从库重放。

## 停止从库（STOP SLAVE）
完成停止I/O线程和SQL线程的操作

## 查询从库服务器状态（SHOW SLAVE STATUS）

## 查询主库服务器状态（SHOW MASTER STATUS）

## 重置主库（RESET MASTER）
删除所有index file 中记录的所有binlog 文件，将日志索引文件清空，创建一个新的日志文件。
这个命令通常仅仅用于第一次用于搭建主从关系的时的主库
```

## 重做主从

```shell
## 1、停止从库
STOP SLAVE;

## 2、调整配置，删除相关库、表等对象
# 将主库的 binlog_format 值由 MIXED 改为 STATEMENT 

## 3、重置主库
RESET MASTER;

## 4、查看binlog位置
SHOW MASTER STATUS;

## 5、调整binlog同步位置
CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave1', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=761;

## 6、启动从库
START SLAVE;
```

# Mycat读写分离

## 单主单从读写分离

### 读写分离简介


上一章讲到MySql主从复制，通过Mycat，能够实现读写分离，即master主服务器实现写操作(insert,update,delete等)，salve从服务器实现读操作(select等)；主服务器一旦有写入操作，从服务器通过读取binlog，来实现数据同步；Mycat也时时发送心跳包来检测mysql服务器是否可用；

![](./assets/mycat10.png)

**部署规划**

| 类型      | 分配IP     |
| --------- | ---------- |
| MySQL主库 | 172.20.0.2 |
| MySQL备库 | 172.20.0.3 |
| Mycat     | 172.20.0.4 |

### MySQL部署

#### 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3306:3306 --name master -d -v /home/docker/master/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.2 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3307:3306 --name slave -d -v /home/docker/slave/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.3 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```

#### 主库配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=2
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=order_db
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
```

#### 备库配置

```mysql
[mysqld]
server-id=3
relay-log=mysql-relay
```

#### 主库命令

```shell
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      154 | order_db     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set

mysql> CREATE USER 'slave1'@'172.20.0.3' IDENTIFIED BY '123456';
Query OK, 0 rows affected

mysql> GRANT REPLICATION SLAVE ON *.* TO 'slave1'@'172.20.0.3';
Query OK, 0 rows affected

mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected

mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      761 | order_db     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set
```

#### 备库命令

```shell
mysql> CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave1', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=761;
Query OK, 0 rows affected

mysql> START SLAVE;
Query OK, 0 rows affected

mysql> SHOW SLAVE STATUS;
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Slave_IO_State                   | Master_Host | Master_User | Master_Port | Connect_Retry | Master_Log_File  | Read_Master_Log_Pos | Relay_Log_File     | Relay_Log_Pos | Relay_Master_Log_File | Slave_IO_Running | Slave_SQL_Running | Replicate_Do_DB | Replicate_Ignore_DB | Replicate_Do_Table | Replicate_Ignore_Table | Replicate_Wild_Do_Table | Replicate_Wild_Ignore_Table | Last_Errno | Last_Error | Skip_Counter | Exec_Master_Log_Pos | Relay_Log_Space | Until_Condition | Until_Log_File | Until_Log_Pos | Master_SSL_Allowed | Master_SSL_CA_File | Master_SSL_CA_Path | Master_SSL_Cert | Master_SSL_Cipher | Master_SSL_Key | Seconds_Behind_Master | Master_SSL_Verify_Server_Cert | Last_IO_Errno | Last_IO_Error | Last_SQL_Errno | Last_SQL_Error | Replicate_Ignore_Server_Ids | Master_Server_Id | Master_UUID                          | Master_Info_File           | SQL_Delay | SQL_Remaining_Delay | Slave_SQL_Running_State                                | Master_Retry_Count | Master_Bind | Last_IO_Error_Timestamp | Last_SQL_Error_Timestamp | Master_SSL_Crl | Master_SSL_Crlpath | Retrieved_Gtid_Set | Executed_Gtid_Set | Auto_Position | Replicate_Rewrite_DB | Channel_Name | Master_TLS_Version |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Waiting for master to send event | 172.20.0.2  | slave1      |        3306 |            60 | mysql-bin.000001 |                 761 | mysql-relay.000002 |           320 | mysql-bin.000001      | Yes              | Yes               |                 |                     |                    |                        |                         |                             |          0 |            |            0 |                 761 |             523 | None            |                |             0 | No                 |                    |                    |                 |                   |                |                     0 | No                            |             0 |               |              0 |                |                             |                2 | 8460233e-6cdb-11ea-86de-0242ac140002 | /var/lib/mysql/master.info |         0 | NULL                | Slave has read all relay log; waiting for more updates |              86400 |             |                         |                          |                |                    |                    |                   |             0 |                      |              |                    |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
1 row in set
```

### Mycat部署

![](assets/mycat06.png)



#### 配置文件

**schema.xml**

```xml
<?xml version="1.0"?>
<!DOCTYPE mycat:schema SYSTEM "schema.dtd">
<!--
	主配置标签
-->
<mycat:schema xmlns:mycat="http://io.mycat/">
	<!--
		逻辑库配置标签
		name: 逻辑库名
		sqlMaxLimit: 限制每次查询数据所返回的最大行数(server.xml中的limit是整个mycat系统的默认值，这里则是当前逻辑库的默认值，默认先看schema.xml的限制数)
		checkSQLschema: 检查发给Mycat的SQL是否包含库名
	-->
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000" dataNode="dn1">
		<!--
			逻辑表配置标签
			name: 逻辑表名，配置多个表名用逗号分隔
			splitTableNames: 多个表名将此属性设置为true
			primaryKey: 物理表的真实主键
			dataNode: 表数据存储的实际物理节点名称(多个用逗号隔开，顺序一旦定义就不能修改，否则会出现混乱)
			rule: 在rule.xml中的<tableRule>中定义
		-->
		<!--
		<table name="travelrecord,address" dataNode="dn1,dn2,dn3" rule="auto-sharding-long" splitTableNames ="true"/>
		-->
	</schema>
	<!--
		dataNode数据节点配置标签
		name: 数据节点名称
		dataHost: 数据库所在主机组的名字
		database: 定义物理数据库名
	-->
	<dataNode name="dn1" dataHost="localhost1" database="order_db" />
	<!--
		dataHost数据库主机配置标签
		name: 数据库服务器的名称
		maxCon: Mycat连接mysql的最大连接数
		minCon: Mycat连接mysql的最小连接数
		balance: Mycat读写分离、读的负载均衡的工作方式
				0:不开启读写分离机制
				1:全部的readHost与stand by writeHost参与select语句的负载均衡(双主双从使用该配置)
				2:所有的readHost与writeHost都参与select语句的负载均衡
				3:所有的readHost参与select语句的负载均衡(单主单从使用该配置)（1.4版本新增）
		writeType: 
				0:所有写操作都是由writeHost标签配置的第一个写主机来执行，只有当第一个挂掉之后，写请求才会到第二个上（切换记录在配置文件dnindex.properties中）
				1:随机发送到写请求的writeHost上定义的写主机执行（1.5以后版本废弃）
		dbType: 使用的数据库类型（mysql、mongodb等）
		dbDriver: 所使用数据库的驱动程序
				natice：mysql原生的通讯协议
		switchType: 写数据库如何进行高可用切换
				 1: 当每一个writeHost不可访问的时候都会切换到第二个writeHost写服务器
				-1: 关闭自动切换功能
				 2: 基于MySQL主从同步的状态决定是否切换，心跳语句为 show slave status
				 3: 基于MySQL galary cluster 的切换机制（适合集群）（1.4.1版本新增）
		slaveThreshold: 配置真实MySQL与MyCat的心跳间隔
	-->
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<!--
			select user()是返回当前连接mysql的用户
		-->
		<heartbeat>select user()</heartbeat>
		<!--
			writeHost通常定义master服务器，readHost通常定义slave服务器。readHost必须镶嵌在writeHost内
			host: MySQL实例名称
			url: MySQL数据库连接的IP地址和端口
			user: MySQL数据库用户名
			password: MySQL数据库密码(不支持加密)
		-->
		<writeHost host="hostM1" url="172.20.0.2:3306" user="root" password="123456">
			<readHost host="hostS1" url="172.20.0.3:3316" user="root" password="123456"/>
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

#### 启动命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```



## 双主双从读写分离

### 读写分离简介

上一章我们搭建了Mycat实现一主一从读写分离，现在来搭建一个双主双从高可用的读写分离，抗风险能力强，企业级架构必须是双主双从；

![](assets/mycat07.png)

部署规划

| 类型       | 分配IP     |
| ---------- | ---------- |
| Mycat      | 172.20.0.4 |
| MySQL主库1 | 172.20.0.2 |
| MySQL备库1 | 172.20.0.3 |
| MySQL主库2 | 172.20.0.5 |
| MySQL备库2 | 172.20.0.6 |

M1 S1 主从复制  M2  S2 主从复制；  M1,M2 互为主从复制 实现数据同步；无论哪个挂掉，Mycat可以自动切换，依然系统可用； 

### MySQL部署

#### 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 拷贝文件到宿主机
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/master1/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/slave1/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/master2/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/slave2/
# 在容器中通过（--net mycatnet --ip 172.20.0.2）指定IP地址
docker run -p 3302:3306 --name master1 -d -v /home/docker/master1/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.2 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3303:3306 --name slave1 -d -v /home/docker/slave1/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.3 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3305:3306 --name master2 -d -v /home/docker/master2/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.5 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3306:3306 --name slave2 -d -v /home/docker/slave2/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.6 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm mycatnet
# 删除所有容器
docker rm -f $(docker ps -qa)
```

#### 配置文件

##### 主库M1配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=2
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=order_db
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
# 在作为从数据库的时候，有写入操作也要更新二进制日志文件
log-slave-updates
#表示自增长字段每次递增的量，指自增字段的起始值，其默认值是1，取值范围是1 .. 65535
auto-increment-increment=2 
# 表示自增长字段从哪个数开始，指字段一次递增多少，他的取值范围是1 .. 65535
auto-increment-offset=1
```

##### 主库M2配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=5
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=order_db
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
# 在作为从数据库的时候，有写入操作也要更新二进制日志文件
log-slave-updates
#表示自增长字段每次递增的量，指自增字段的起始值，其默认值是1，取值范围是1 .. 65535
auto-increment-increment=2 
# 表示自增长字段从哪个数开始，指字段一次递增多少，他的取值范围是1 .. 65535
auto-increment-offset=2
```

##### 备库S1配置

```mysql
[mysqld]
server-id=3
relay-log=mysql-relay
```


##### 备库S2配置

```mysql
[mysqld]
server-id=6
relay-log=mysql-relay
```

#### MySQL执行命令

```shell
#### 配置M1与M2,S1主从复制
## M1执行命令
CREATE USER 'slave'@'172.20.0.%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'slave'@'172.20.0.%';
FLUSH PRIVILEGES;
RESET MASTER;
SHOW MASTER STATUS;

## S1执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;

## M2执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;


#### 配置M2与M1,S2主从复制
## M2执行命令
CREATE USER 'slave'@'172.20.0.%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'slave'@'172.20.0.%';
FLUSH PRIVILEGES;
RESET MASTER;
SHOW MASTER STATUS;

## S2执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.5', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;

## M1执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.5', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;
```

#### MySQL测试脚本

```mysql

CREATE TABLE `t_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

## 先M1执行两次观察结果
## 再M2执行两次观察结果
INSERT INTO t_test (hostname) VALUES (@@hostname);
```



### Mycat部署

![](assets/mycat06.png)



#### 配置文件

**schema.xml**

```xml
<?xml version="1.0"?>
<!DOCTYPE mycat:schema SYSTEM "schema.dtd">
<!--
	主配置标签
-->
<mycat:schema xmlns:mycat="http://io.mycat/">
	<!--
		逻辑库配置标签
		name: 逻辑库名
		sqlMaxLimit: 限制每次查询数据所返回的最大行数(server.xml中的limit是整个mycat系统的默认值，这里则是当前逻辑库的默认值，默认先看schema.xml的限制数)
		checkSQLschema: 检查发给Mycat的SQL是否包含库名
	-->
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000" dataNode="dn1">
		<!--
			逻辑表配置标签
			name: 逻辑表名，配置多个表名用逗号分隔
			splitTableNames: 多个表名将此属性设置为true
			primaryKey: 物理表的真实主键
			dataNode: 表数据存储的实际物理节点名称(多个用逗号隔开，顺序一旦定义就不能修改，否则会出现混乱)
			rule: 在rule.xml中的<tableRule>中定义
		-->
		<!--
		<table name="travelrecord,address" dataNode="dn1,dn2,dn3" rule="auto-sharding-long" splitTableNames ="true"/>
		-->
	</schema>
	<!--
		dataNode数据节点配置标签
		name: 数据节点名称
		dataHost: 数据库所在主机组的名字
		database: 定义物理数据库名
	-->
	<dataNode name="dn1" dataHost="localhost1" database="order_db" />
	<!--
		dataHost数据库主机配置标签
		name: 数据库服务器的名称
		maxCon: Mycat连接mysql的最大连接数
		minCon: Mycat连接mysql的最小连接数
		balance: Mycat读写分离、读的负载均衡的工作方式
				0:不开启读写分离机制
				1:全部的readHost与stand by writeHost参与select语句的负载均衡(双主双从使用该配置)
				2:所有的readHost与writeHost都参与select语句的负载均衡
				3:所有的readHost参与select语句的负载均衡(单主单从使用该配置)（1.4版本新增）
		writeType: 
				0:所有写操作都是由writeHost标签配置的第一个写主机来执行，只有当第一个挂掉之后，写请求才会到第二个上（切换记录在配置文件dnindex.properties中）
				1:随机发送到写请求的writeHost上定义的写主机执行（1.5以后版本废弃）
		dbType: 使用的数据库类型（mysql、mongodb等）
		dbDriver: 所使用数据库的驱动程序
				natice：mysql原生的通讯协议
		switchType: 写数据库如何进行高可用切换
				 1: 当每一个writeHost不可访问的时候都会切换到第二个writeHost写服务器
				-1: 关闭自动切换功能
				 2: 基于MySQL主从同步的状态决定是否切换，心跳语句为 show slave status
				 3: 基于MySQL galary cluster 的切换机制（适合集群）（1.4.1版本新增）
		slaveThreshold: 配置真实MySQL与MyCat的心跳间隔
	-->
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="1" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<!--
			select user()是返回当前连接mysql的用户
		-->
		<heartbeat>select user()</heartbeat>
		<!--
			writeHost通常定义master服务器，readHost通常定义slave服务器。readHost必须镶嵌在writeHost内
			host: MySQL实例名称
			url: MySQL数据库连接的IP地址和端口
			user: MySQL数据库用户名
			password: MySQL数据库密码(不支持加密)
		-->
		<writeHost host="hostM1" url="172.20.0.2:3306" user="root" password="123456">
			<readHost host="hostS1" url="172.20.0.3:3316" user="root" password="123456"/>
		</writeHost>
		<writeHost host="hostM2" url="172.20.0.5:3306" user="root" password="123456">
			<readHost host="hostS2" url="172.20.0.6:3316" user="root" password="123456"/>
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

#### 启动命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

#### 测试脚本

```mysql
## M1、S1、M2、S2、Mycat启动完，执行测试脚本
## M1自增ID为奇数增长，M2自增ID为偶数增长
## 观察自增ID是否为奇数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
## 停止M1数据服务，执行测试脚本
## 观察自增ID是否改为偶数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
## 开启M1数据服务，执行测试脚本
## 观察自增ID是否仍为偶数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
## 停止M2数据服务，执行测试脚本
## 观察自增ID是否改为奇数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
```


# Mycat垂直分库

## 垂直分库简介

![](./assets/mycat08.png)

单个数据库负载是有限的，在应对高并发时候，达到极限时候，容易崩掉，以及当某个表数据量很大的时候，执行速度缓慢，影响应用用户体验；

这时候，mycat提供了对数据库表垂直的库拆分，不同的业务模块的表，可以放不同的数据库，通过mycat，能屏蔽拆分细节，也就是连接mycat操作，我们依然可以看做是一个库；

同时了mycat支持了把单个数据量巨大的表依据某个字段规则拆分成多个表分散存到多个数据库当中去，来减少单表压力；

在设计时，通常把同一个业务模块的表放同一个库，**依据业务模块来垂直分库；** 

垂直分库原则：

**1、根据业务模块来划分库，不搞跨库join操作，避坑；**

**2、公共表，比如数据字典表，系统属性表等，采用全局表；**

**3、有些核心表，比如用户表，部门表，权限表等，业务模块偶尔用到的时候，可以通过API方式查询，无需划分到具体业务模块里去；**

![](assets/mycat09.png)

**部署规划**

| 类型     | 分配IP | 角色      | 分配表               |
| --------- | ---------- | ------------- | ------------------ |
| Mycat     | 172.20.0.4 |  |  |
| MySQL用户库 | 172.20.0.7 | user_db  | t_user             |
| MySQL订单库 | 172.20.0.8 | order_db | t_order、t_product |

##  MySQL部署


### 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3307:3306 --name user_db -d --net mycatnet --ip 172.20.0.7 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3308:3306 --name order_db -d --net mycatnet --ip 172.20.0.8 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```



##  Mycat部署

###  配置文件

**schema.xml**

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2" />
		<table name="t_product" primaryKey="id" dataNode="dn2" />
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="user_db" />
	<dataNode name="dn2" dataHost="localhost2" database="order_db" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```



### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql

-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 商品表t_product
CREATE TABLE `t_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `p_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `u_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 数据字典表t_datadic:
CREATE TABLE `t_datadic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(20) DEFAULT NULL COMMENT '数据字典名称',
  `value` varchar(20) DEFAULT NULL COMMENT '数据字典值',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert  into `t_user`(`id`,`username`,`password`) values (1,'java1234','123456'),(2,'jack','123'),(3,'marry','456');

insert  into `t_product`(`id`,`name`,`price`,`description`,`stock`) values (1,'耐克鞋2020款001A','998','dfs',50),(2,'阿迪达斯休闲衣服2020Add','388','dfs',20);

insert  into `t_order`(`id`,`p_id`,`num`,`u_id`,`order_date`) values (1,1,1,2,'2020-03-16 22:10:25'),(2,2,3,3,'2020-03-16 22:10:34');

select * from t_order o left join t_product p on o.p_id=p.id
```


# Mycat水平分表

## 水平分表简介

![](./assets/mycat08.png)

可以根据业务来垂直划分库，来减少服务器负载，但是假如单个表的数据量很大的时候，比如订单表，上了千万数据量，就查询就歇菜了，

这时候我们需要进行水平分表，把单个表的数据根据分表策略分到多个数据库，来减少单表压力，这就是水平分表；

## 分片规则

- 分片枚举：sharding-by-intfile

- 主键范围约定：auto-sharding-long（此分片适用于，提前规划好分片字段某个范围属于哪个分片）

- 一致性hash：sharding-by-murmur

- 字符串hash解析：sharding-by-stringhash

- 按日期（天）分片：sharding-by-date

- 按单月小时拆分：sharding-by-hour

- 自然月分片：sharding-by-month

- 取模：mod-long（此规则为对分片字段求摸运算）

- 取模范围约束：sharding-by-pattern（此种规则是取模运算与范围约束的结合，主要为了后续数据迁移做准备，即可以自主决定取模后数据的节点分布）

**部署规划**

| 类型     | 分配IP | 角色      | 分配表               |
| --------- | ---------- | ------------- | ------------------ |
| Mycat     | 172.20.0.4 |  |  |
| MySQL用户库 | 172.20.0.7 | user_db  | t_user             |
| MySQL订单库 | 172.20.0.8 | order_db | t_order、t_product |
| MySQL订单库 | 172.20.0.9 | order_db | t_order |

#  MySQL部署


## 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3307:3306 --name user_db -d -v /home/docker/mysql/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.7 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3308:3306 --name order_db01 -d -v /home/docker/mysql/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.8 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3309:3306 --name order_db02 -d -v /home/docker/mysql/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.9 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```


##  取模分片

###  配置文件

**schema.xml**

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order-rule"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="user_db" />
	<dataNode name="dn2" dataHost="localhost2" database="order_db" />
	<dataNode name="dn3" dataHost="localhost3" database="order_db" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

**rule.xml**

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="order-rule">
       <rule>
          <columns>user_id</columns>
          <algorithm>mod-long</algorithm>
       </rule>
	</tableRule>
  
	<!-- 取模算法函数 -->
	<function name="mod-long" class="io.mycat.route.function.PartitionByMod">
		<!-- count：分片节点数 -->
		<property name="count">2</property>
	</function>
</mycat:rule>
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 172.20.0.7建 user_db 数据库；
-- 172.20.0.8 和172.20.0.9 建 order_db 数据库
-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `product_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (1,1,1,1001,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (2,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (3,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (4,1,1,1004,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (5,1,1,1005,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (6,1,1,1005,now());
```

## ER表分片

### 配置文件

**schema.xml**

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order-rule">
            <!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="user_db" />
	<dataNode name="dn2" dataHost="localhost2" database="order_db" />
	<dataNode name="dn3" dataHost="localhost3" database="order_db" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 172.20.0.7建 user_db 数据库；
-- 172.20.0.8 和172.20.0.9 建 order_db 数据库
CREATE DATABASE IF NOT EXISTS user_db DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
CREATE DATABASE IF NOT EXISTS order_db DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `product_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 订单详情表t_order_detail：
CREATE TABLE `t_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `detail` varchar(200) DEFAULT NULL COMMENT '订单详情',
  `order_id` int(11) DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (1,1,1,1001,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (2,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (3,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (4,1,1,1004,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (5,1,1,1005,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (6,1,1,1005,now());

insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (1,1,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (2,2,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (3,3,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (4,4,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (5,5,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (6,6,'心随梦飞');

select * from T_ORDER;
select * from T_ORDER_DETAIL;
select * from T_ORDER inner o join T_ORDER_DETAIL d on o.id=d.order_id;
```

### 测试异常
异常信息：`ERROR 1064 (HY000): can't find (root) parent sharding node for sql:INSERT INTO t_order_detail...`
出现以上异常原因有两种情况：
原因一：此类ER表的插入操作不能作为一个事务进行数据提交，如果父子表在一个事务中进行提交，显然在事务没有提交前子表是无法查询附表的数据的，因此就无法确定分片节点，如果是ER关系的表在插入数据时不能再同一个事务中提交数据，需要分开提交
原因二：表名字大小写的问题导致。在my.cnf配置文件，在[mysqld]节点下加上如下配置`lower_case_table_names = 1`

## 全局表分片

### 配置文件

**schema.xml**

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order-rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="user_db" />
	<dataNode name="dn2" dataHost="localhost2" database="order_db" />
	<dataNode name="dn3" dataHost="localhost3" database="order_db" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```



### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 172.20.0.7建 user_db 数据库；
-- 172.20.0.8 和172.20.0.9 建 order_db 数据库
-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `product_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 订单详情表t_order_detail：
CREATE TABLE `t_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `detail` varchar(200) DEFAULT NULL COMMENT '订单详情',
  `order_id` int(11) DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 数据字典表t_datadic:
CREATE TABLE `t_datadic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(20) DEFAULT NULL COMMENT '数据字典名称',
  `value` varchar(20) DEFAULT NULL COMMENT '数据字典值',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (1,1,1,1001,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (2,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (3,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (4,1,1,1004,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (5,1,1,1005,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (6,1,1,1005,now());

insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (1,1,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (2,2,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (3,3,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (4,4,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (5,5,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (6,6,'心随梦飞');

select * from T_ORDER;
select * from T_ORDER_DETAIL;
select * from T_ORDER inner o join T_ORDER_DETAIL d on o.id=d.order_id;

insert into `T_DATADIC`(`id`,`name`,`value`,`remark`) values (1,'gender','男','性别');
insert into `T_DATADIC`(`id`,`name`,`value`,`remark`) values (2,'gender','女','性别');
```



## 枚举分片

### 配置文件

**schema.xml**

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<!-- 取模分表 -->
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order—rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
		<!-- 枚举分表 -->
		<table name="t_product" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_intfile"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="user_db" />
	<dataNode name="dn2" dataHost="localhost2" database="order_db" />
	<dataNode name="dn3" dataHost="localhost3" database="order_db" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

**rule.xml**

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="sharding_by_intfile">
		<rule>
			<!-- 分片字段 -->
			<columns>region</columns>
			<!-- 分片算法函数 -->
			<algorithm>hash-int</algorithm>
		</rule>
	</tableRule>
  
	<!-- 枚举分表算法函数 -->
	<function name="hash-int" class="io.mycat.route.function.PartitionByFileMap">
		<!-- 标识配置文件名称 -->
		<property name="mapFile">partition-hash-int.txt</property>
		<!-- 分片字段类型：0为int类型，非0为字符串 -->
		<property name="type">1</property>
		<!-- 默认节点：小于0表示不设置默认节点，大于等于0设置默认节点（设置了默认节点如果遇到不能识别的枚举值，就会路由到默认节点；如果没有设置默认节点，则会报错） -->
		<property name="defaultNode">0</property>
	</function>
</mycat:rule>
```

**partition-hash-int.txt**

```
## 区域编码=节点编号
36=0
44=1
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 商品表t_product
CREATE TABLE `t_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  `region` varchar(4) DEFAULT NULL COMMENT '生产地',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert into `T_PRODUCT`(`id`,`name`,`price`,`description`,`stock`,`region`) values (1,'余干辣椒', 6.25,'余干辣椒皮嫩爽口非常辣',100,'36');
insert into `T_PRODUCT`(`id`,`name`,`price`,`description`,`stock`,`region`) values (2,'龙眼',7.85,' 营养丰富,有壮阳益气、补益心脾、养血安神、润肤美容等多种功效,可治疗贫血、心悸、失眠、健忘、神经衰弱及病后、产后身体虚弱等症',230,'44');
insert into `T_PRODUCT`(`id`,`name`,`price`,`description`,`stock`,`region`) values (3,'荔枝',9.0,'果肉产鲜时半透明凝脂状，味香美，但不耐储藏',300,'44');
```



## 范围分片

### 配置文件

**schema.xml**

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<!-- 取模分表 -->
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order—rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
		<!-- 枚举分表 -->
		<table name="t_product" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_intfile"/>
		<!-- 范围分表 -->
		<table name="t_payment" primaryKey="id" dataNode="dn2,dn3" rule="auto_sharding_long"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="user_db" />
	<dataNode name="dn2" dataHost="localhost2" database="order_db" />
	<dataNode name="dn3" dataHost="localhost3" database="order_db" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

**rule.xml**

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="auto_sharding_long">
		<rule>
			<!-- 分片字段 -->
			<columns>order_id</columns>
			<!-- 分片算法函数 -->
			<algorithm>rang-long</algorithm>
		</rule>
	</tableRule>
  
	<!-- 范围分表算法函数 -->
	<function name="rang-long" class="io.mycat.route.function.AutoPartitionByLong">
		<!-- 标识配置文件名称 -->
		<property name="mapFile">autopartition-long.txt</property>
		<!-- 默认节点：小于0表示不设置默认节点，大于等于0设置默认节点（设置了默认节点如果遇到不能识别的枚举值，就会路由到默认节点；如果没有设置默认节点，则会报错） -->
		<property name="defaultNode">0</property>
	</function>
</mycat:rule>
```

**autopartition-long.txt**

```
1-100=0
101-200=1
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 支付表 t_payment
CREATE TABLE `t_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `order_id` int(11) DEFAULT NULL COMMENT '订单编号',
  `status` int(4) DEFAULT NULL COMMENT '支付状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (1, 27, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (2, 121, 0);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (3, 725, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (4, 99, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (5, 100, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (6, 101, 1);
```


## 日期分片

### 配置文件

**schema.xml**

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<!-- 取模分表 -->
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order—rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
		<!-- 枚举分表 -->
		<table name="t_product" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_intfile"/>
		<!-- 范围分表 -->
		<table name="t_payment" primaryKey="id" dataNode="dn2,dn3" rule="auto_sharding_long"/>
		<!-- 日期分表 -->
		<table name="t_login_log" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_date"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="user_db" />
	<dataNode name="dn2" dataHost="localhost2" database="order_db" />
	<dataNode name="dn3" dataHost="localhost3" database="order_db" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

**rule.xml**

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="sharding_by_date">
		<rule>
			<!-- 分片字段 -->
			<columns>login_date</columns>
			<!-- 分片算法函数 -->
			<algorithm>partbyday</algorithm>
		</rule>
	</tableRule>
  
	<!-- 按天分表算法函数 -->
	<function name="partbyday" class="io.mycat.route.function.PartitionByDate">
		<!-- 日期格式 -->
		<property name="dateFormat">yyyy-MM-dd</property>
		<!-- 开始日期 -->
		<property name="sBeginDate">2020-04-01</property>
		<!-- 结束日期：代表数据达到结束日期后循环从开始分片插入 -->
		<property name="sEndDate">2014-04-06</property>
		<!-- 分区天数：默认从开始日期算起，分隔3天为一个分区 -->
		<property name="sPartionDay">3</property>
	</function>
</mycat:rule>
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 登录日志表 t_login_log
CREATE TABLE `t_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `login_date` date DEFAULT NULL COMMENT '登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (1, 19880627, '2020-04-01');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (2, 19880725, '2020-04-02');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (3, 20180121, '2020-04-03');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (4, 19880627, '2020-04-04');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (5, 19880725, '2020-04-05');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (6, 20180121, '2020-04-06');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (7, 19880627, '2020-04-07');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (8, 19880725, '2020-04-08');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (9, 20180121, '2020-04-09');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (10, 19880627, '2020-04-10');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (11, 19880725, '2020-04-11');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (12, 20180121, '2020-04-12');
```



## 全局自增ID

第一步：导入dbseq.sql脚本到`dn2`库

第二步：修改sequence_db_conf.properties文件 

```properties
#sequence stored in datanode
GLOBAL=dn2
COMPANY=dn2
CUSTOMER=dn2
ORDERS=dn2
```

第三步：修改server.xml配置文件的 sequnceHandlerType配置 ，将值改为`1`

- 0：本地方式
- 1：数据库方式
- 2：时间戳方式

第四步：重启Mycat

第五步：测试

```sql
DELETE FROM T_ORDER;

insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1001,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1002,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1002,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1004,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1005,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1005,now());
```


