# Sharding-JDBC

## 水平分表

### 数据库脚本

```sql
CREATE DATABASE IF NOT EXISTS user_db DEFAULT CHARACTER SET = utf8mb4;
Use user_db;
CREATE TABLE `t_user_1` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_user_2` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### 配置文件

```properties
# shardingjdbc分片策略
# 配置数据源，给数据源起名称
spring.shardingsphere.datasource.names=m0

# 一个实体类对应两张表，覆盖
spring.main.allow-bean-definition-overriding=true

#配置数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m0.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m0.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m0.url=jdbc:mysql://192.168.135.136:3306/user_db?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m0.username=root
spring.shardingsphere.datasource.m0.password=123456

#指定t_user表分布情况，配置表在哪个数据库里面，表名称都是什么  m1.course_1 , m1.course_2
spring.shardingsphere.sharding.tables.t_user.actual-data-nodes=m0.t_user_$->{1..2}

# 指定t_user表里面主键id 生成策略  SNOWFLAKE
spring.shardingsphere.sharding.tables.t_user.key-generator.column=id
spring.shardingsphere.sharding.tables.t_user.key-generator.type=SNOWFLAKE

# 指定分片策略  约定id值偶数添加到t_user_1表，如果id是奇数添加到t_user_2表
spring.shardingsphere.sharding.tables.t_user.table-strategy.inline.sharding-column=id
spring.shardingsphere.sharding.tables.t_user.table-strategy.inline.algorithm-expression=t_user_$->{id % 2 + 1}

# 打开sql输出日志
spring.shardingsphere.props.sql.show=true
```

### 测试代码

```java
package com.fosilzhou.shardingjdbc.entity;
@Data
@TableName(value = "t_user")  //指定对应表
public class User {
    private Long id;
    private String username;
    private String password;
}

package com.fosilzhou.shardingjdbc.mapper;
@Repository
public interface UserMapper extends BaseMapper<User> {
}


package com.fosilzhou.shardingjdbc;
@SpringBootApplication
@MapperScan("com.fosilzhou.shardingjdbc.mapper")
public class ShardingJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcApplication.class, args);
    }

}

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void addUser() {
        for(int i=0;i<10;i++) {
            User user = new User();
            user.setUsername("fosilzhou"+i);
            user.setPassword("123456");
            userMapper.insert(user);
        }
    }
    @Test
    public void findUser() {
        QueryWrapper<User>  wrapper = new QueryWrapper<User>();
        wrapper.eq("id",1290484912125722626L);
        User user = userMapper.selectOne(wrapper);
        System.out.println(user);
    }
}
```



## 水平分库分表

### 数据库脚本

```sql
CREATE DATABASE IF NOT EXISTS product_db01 DEFAULT CHARACTER SET = utf8mb4;
Use product_db01;
CREATE TABLE `t_product_1` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_product_2` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE DATABASE IF NOT EXISTS product_db02 DEFAULT CHARACTER SET = utf8mb4;
Use product_db02;
CREATE TABLE `t_product_1` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `t_product_2` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### 配置文件

```properties
# shardingjdbc分片策略
# 配置数据源，给数据源起名称,
# 水平分库分表，配置两个数据源
spring.shardingsphere.datasource.names=m1,m2

# 一个实体类对应两张表，覆盖
spring.main.allow-bean-definition-overriding=true

#配置第一个数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m1.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m1.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m1.url=jdbc:mysql://192.168.135.136:3306/order_db01?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m1.username=root
spring.shardingsphere.datasource.m1.password=123456

#配置第二个数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m2.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m2.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m2.url=jdbc:mysql://192.168.135.136:3306/order_db02?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m2.username=root
spring.shardingsphere.datasource.m2.password=123456

#指定数据库分布情况，数据库里面表分布情况
# m1  m2    t_product_1 t_product_2
spring.shardingsphere.sharding.tables.t_product.actual-data-nodes=m$->{1..2}.t_product_$->{1..2}

# 指定t_product表里面主键id 生成策略  SNOWFLAKE
spring.shardingsphere.sharding.tables.t_product.key-generator.column=id
spring.shardingsphere.sharding.tables.t_product.key-generator.type=SNOWFLAKE

# 指定表分片策略  约定id值偶数添加到t_product_1表，如果id是奇数添加到t_product_2表
spring.shardingsphere.sharding.tables.t_product.table-strategy.inline.sharding-column=id
spring.shardingsphere.sharding.tables.t_product.table-strategy.inline.algorithm-expression=t_product_$->{id % 2 + 1}

# 指定数据库分片策略 约定u_id是偶数添加m1，是奇数添加m2
spring.shardingsphere.sharding.tables.t_product.database-strategy.inline.sharding-column=user_id
spring.shardingsphere.sharding.tables.t_product.database-strategy.inline.algorithm-expression=m$->{user_id % 2 + 1}

# 打开sql输出日志
spring.shardingsphere.props.sql.show=true
```

### 测试代码

```java
package com.fosilzhou.shardingjdbc.entity;
@Data
@TableName("t_product")
public class Product {
    private Long id;
    private String name;
    private BigDecimal price;
    private String description;
    private int stock;
    private Long userId;
}

package com.fosilzhou.shardingjdbc.mapper;
@Repository
public interface ProductMapper extends BaseMapper<Product> {

}

package com.fosilzhou.shardingjdbc;
@SpringBootApplication
@MapperScan("com.fosilzhou.shardingjdbc.mapper")
public class ShardingJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcApplication.class, args);
    }

}


@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    @Autowired
    private ProductMapper productMapper;

    private Long[] userIds = new Long[]{1290484907772035074L,1290484912381575170L,1290484912243163137L,1290484912297689089L};
    @Test
    public void addProduct() {
        for(int i=1;i<30;i++) {
            Product product = new Product();
            product.setName("MI 9 SE");
            product.setDescription("MI 9 SE RAM:6GB ROM:64GB");
            product.setPrice(new BigDecimal(1399.00));
            product.setStock(i*10);
            product.setUserId(userIds[i % userIds.length]);
            productMapper.insert(product);
        }
    }

    @Test
    public void findProductByName() {
        // 查询四次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_2
        // Actual SQL: m2 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m2 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_2
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.likeLeft("name","MI");
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }

    /**
     * 根据用户Id查询商品信息，跨表不跨库查询
     */
    @Test
    public void findProductByUserId() {
        // 查询两次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_2
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.eq("user_id",1290484912381575170L);
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }

    @Test
    public void findProductById() {
        // 查询两次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m2 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.eq("id",1290493850707623938L);
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }

    @Test
    public void findProductByIdAndUserId() {
        // 查询一次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.eq("user_id", 1290484912381575170L);
        wrapper.eq("id",1290493850707623938L);
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }
}
```

## 垂直分库

### 数据库脚本

同`水平分表`、`水平分库分表`


### 配置文件

```properties
# shardingjdbc分片策略
# 配置数据源，给数据源起名称
spring.shardingsphere.datasource.names=m0,m1,m2

# 一个实体类对应两张表，覆盖
spring.main.allow-bean-definition-overriding=true

#配置数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m0.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m0.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m0.url=jdbc:mysql://192.168.135.136:3306/user_db?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m0.username=root
spring.shardingsphere.datasource.m0.password=123456

#配置第一个数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m1.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m1.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m1.url=jdbc:mysql://192.168.135.136:3306/order_db01?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m1.username=root
spring.shardingsphere.datasource.m1.password=123456

#配置第二个数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m2.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m2.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m2.url=jdbc:mysql://192.168.135.136:3306/order_db02?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m2.username=root
spring.shardingsphere.datasource.m2.password=123456

#指定t_user表分布情况，配置表在哪个数据库里面，表名称都是什么  m1.course_1 , m1.course_2
spring.shardingsphere.sharding.tables.t_user.actual-data-nodes=m0.t_user_$->{1..2}

# 指定t_user表里面主键id 生成策略  SNOWFLAKE
spring.shardingsphere.sharding.tables.t_user.key-generator.column=id
spring.shardingsphere.sharding.tables.t_user.key-generator.type=SNOWFLAKE

# 指定分片策略  约定id值偶数添加到t_user_1表，如果id是奇数添加到t_user_2表
spring.shardingsphere.sharding.tables.t_user.table-strategy.inline.sharding-column=id
spring.shardingsphere.sharding.tables.t_user.table-strategy.inline.algorithm-expression=t_user_$->{id % 2 + 1}


#指定数据库分布情况，数据库里面表分布情况
# m1  m2    t_product_1 t_product_2
spring.shardingsphere.sharding.tables.t_product.actual-data-nodes=m$->{1..2}.t_product_$->{1..2}

# 指定t_product表里面主键id 生成策略  SNOWFLAKE
spring.shardingsphere.sharding.tables.t_product.key-generator.column=id
spring.shardingsphere.sharding.tables.t_product.key-generator.type=SNOWFLAKE

# 指定表分片策略  约定id值偶数添加到t_product_1表，如果id是奇数添加到t_product_2表
spring.shardingsphere.sharding.tables.t_product.table-strategy.inline.sharding-column=id
spring.shardingsphere.sharding.tables.t_product.table-strategy.inline.algorithm-expression=t_product_$->{id % 2 + 1}

# 指定数据库分片策略 约定u_id是偶数添加m1，是奇数添加m2
spring.shardingsphere.sharding.tables.t_product.database-strategy.inline.sharding-column=user_id
spring.shardingsphere.sharding.tables.t_product.database-strategy.inline.algorithm-expression=m$->{user_id % 2 + 1}

# 打开sql输出日志
spring.shardingsphere.props.sql.show=true
```

### 测试代码

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;


    @Test
    public void test() {
        User user = new User();
        user.setUsername("fosilzhou");
        user.setPassword("123456");
        userMapper.insert(user);
        user = userMapper.selectOne(new QueryWrapper<User>().eq("username","fosilzhou"));
        Product product = new Product();
        product.setName("MI 9 SE");
        product.setDescription("MI 9 SE RAM:6GB ROM:64GB");
        product.setPrice(new BigDecimal(1399.00));
        product.setStock(999);
        product.setUserId(user.getId());
        productMapper.insert(product);
        product = productMapper.selectOne(new QueryWrapper<Product>().eq("user_id", user.getId()));
        System.err.println(product.getId());
    }
}
```

## 公共表

### 数据库脚本

```sql
CREATE TABLE `t_datadic` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(20) DEFAULT NULL COMMENT '数据字典名称',
  `value` varchar(20) DEFAULT NULL COMMENT '数据字典值',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

### 配置文件

```properties
# shardingjdbc分片策略
# 配置数据源，给数据源起名称
spring.shardingsphere.datasource.names=m0,m1,m2

# 一个实体类对应两张表，覆盖
spring.main.allow-bean-definition-overriding=true

#配置数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m0.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m0.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m0.url=jdbc:mysql://192.168.135.136:3306/user_db?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m0.username=root
spring.shardingsphere.datasource.m0.password=123456

#配置第一个数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m1.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m1.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m1.url=jdbc:mysql://192.168.135.136:3306/order_db01?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m1.username=root
spring.shardingsphere.datasource.m1.password=123456

#配置第二个数据源具体内容，包含连接池，驱动，地址，用户名和密码
spring.shardingsphere.datasource.m2.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.m2.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.m2.url=jdbc:mysql://192.168.135.136:3306/order_db02?serverTimezone=GMT%2B8
spring.shardingsphere.datasource.m2.username=root
spring.shardingsphere.datasource.m2.password=123456
# 配置公共表
spring.shardingsphere.sharding.broadcast-tables=t_datadic
spring.shardingsphere.sharding.tables.t_udict.key-generator.column=id
spring.shardingsphere.sharding.tables.t_udict.key-generator.type=SNOWFLAKE

# 打开sql输出日志
spring.shardingsphere.props.sql.show=true
```

### 测试代码

```java
package com.fosilzhou.shardingjdbc.entity;
@Data
@TableName(value = "t_datadic")
public class Dict {
    private Long id;
    private String name;
    private String value;
    private String remark;
}

package com.fosilzhou.shardingjdbc.mapper;
@Repository
public interface DictMapper extends BaseMapper<Dict> {
}

package com.fosilzhou.shardingjdbc;
@SpringBootApplication
@MapperScan("com.fosilzhou.shardingjdbc.mapper")
public class ShardingJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcApplication.class, args);
    }

}

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    @Autowired
    private DictMapper dictMapper;

    @Test
    public void AddDict() {
        Dict dict = new Dict();
        dict.setName("gender");
        dict.setValue("男");
        dict.setRemark("性别");
        dictMapper.insert(dict);
        dict = new Dict();
        dict.setName("gender");
        dict.setValue("女");
        dict.setRemark("性别");
        dictMapper.insert(dict);
    }

    @Test
    public void findDict() {
        QueryWrapper<Dict>  wrapper = new QueryWrapper<Dict>();
        wrapper.eq("name", "gender");
        List<Dict> dictList = dictMapper.selectList(wrapper);
        System.out.println(dictList);
    }
}
```



## 读写分离

### 数据库脚本



### 配置文件



### 测试代码



# Sharding-Proxy



## 分库分表



## 读写分离