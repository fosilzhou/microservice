package com.fosilzhou.shardingjdbc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fosilzhou.shardingjdbc.entity.Product;
import com.fosilzhou.shardingjdbc.entity.User;
import com.fosilzhou.shardingjdbc.mapper.ProductMapper;
import com.fosilzhou.shardingjdbc.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * 根据用户Id进行水平分表
 *
 * 对应application01.properties配置文件
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class VerticalApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;


    @Test
    public void test() {
        User user = new User();
        user.setUsername("fosilzhou");
        user.setPassword("123456");
        userMapper.insert(user);
        user = userMapper.selectOne(new QueryWrapper<User>().eq("username","fosilzhou"));
        Product product = new Product();
        product.setName("MI 9 SE");
        product.setDescription("MI 9 SE RAM:6GB ROM:64GB");
        product.setPrice(new BigDecimal(1399.00));
        product.setStock(999);
        product.setUserId(user.getId());
        productMapper.insert(product);
        product = productMapper.selectOne(new QueryWrapper<Product>().eq("user_id", user.getId()));
        System.err.println(product.getId());
    }
}
