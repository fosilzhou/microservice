package com.fosilzhou.shardingjdbc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fosilzhou.shardingjdbc.entity.Dict;
import com.fosilzhou.shardingjdbc.entity.Product;
import com.fosilzhou.shardingjdbc.entity.User;
import com.fosilzhou.shardingjdbc.mapper.DictMapper;
import com.fosilzhou.shardingjdbc.mapper.ProductMapper;
import com.fosilzhou.shardingjdbc.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

/**
 * 根据用户Id进行水平分表
 *
 * 对应application01.properties配置文件
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonApplicationTests {

    @Autowired
    private DictMapper dictMapper;

    @Test
    public void AddDict() {
        Dict dict = new Dict();
        dict.setName("gender");
        dict.setValue("男");
        dict.setRemark("性别");
        dictMapper.insert(dict);
        dict = new Dict();
        dict.setName("gender");
        dict.setValue("女");
        dict.setRemark("性别");
        dictMapper.insert(dict);
    }

    @Test
    public void findDict() {
        QueryWrapper<Dict>  wrapper = new QueryWrapper<Dict>();
        wrapper.eq("name", "gender");
        List<Dict> dictList = dictMapper.selectList(wrapper);
        System.out.println(dictList);
    }
}
