package com.fosilzhou.shardingjdbc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fosilzhou.shardingjdbc.entity.Product;
import com.fosilzhou.shardingjdbc.mapper.ProductMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

/**
 * 根据用户Id水平分库，商品Id水平分表
 *
 * 对应application02.properties配置文件
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductApplicationTests {

    @Autowired
    private ProductMapper productMapper;

    private Long[] userIds = new Long[]{1290484907772035074L,1290484912381575170L,1290484912243163137L,1290484912297689089L};
    @Test
    public void addProduct() {
        for(int i=1;i<30;i++) {
            Product product = new Product();
            product.setName("MI 9 SE");
            product.setDescription("MI 9 SE RAM:6GB ROM:64GB");
            product.setPrice(new BigDecimal(1399.00));
            product.setStock(i*10);
            product.setUserId(userIds[i % userIds.length]);
            productMapper.insert(product);
        }
    }

    @Test
    public void findProductByName() {
        // 查询四次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_2
        // Actual SQL: m2 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m2 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_2
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.likeLeft("name","MI");
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }

    /**
     * 根据用户Id查询商品信息，跨表不跨库查询
     */
    @Test
    public void findProductByUserId() {
        // 查询两次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_2
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.eq("user_id",1290484912381575170L);
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }

    @Test
    public void findProductById() {
        // 查询两次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        // Actual SQL: m2 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.eq("id",1290493850707623938L);
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }

    @Test
    public void findProductByIdAndUserId() {
        // 查询一次
        // Actual SQL: m1 ::: SELECT  id,name,price,description,stock,user_id  FROM t_product_1
        QueryWrapper<Product>  wrapper = new QueryWrapper<Product>();
        wrapper.eq("user_id", 1290484912381575170L);
        wrapper.eq("id",1290493850707623938L);
        List<Product> productList = productMapper.selectList(wrapper);
        System.out.println(productList);
    }
}
