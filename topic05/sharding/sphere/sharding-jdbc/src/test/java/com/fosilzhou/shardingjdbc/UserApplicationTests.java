package com.fosilzhou.shardingjdbc;

import com.fosilzhou.shardingjdbc.entity.Order;
import com.fosilzhou.shardingjdbc.entity.Product;
import com.fosilzhou.shardingjdbc.entity.User;
import com.fosilzhou.shardingjdbc.mapper.ProductMapper;
import com.fosilzhou.shardingjdbc.mapper.DictMapper;
import com.fosilzhou.shardingjdbc.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * 实现user和product的垂直分库
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void addUser() {
        for(int i=0;i<10;i++) {
            User user = new User();
            user.setUsername("fosilzhou"+i);
            user.setPassword("123456");
            userMapper.insert(user);
        }
    }
    @Test
    public void findUser() {
        QueryWrapper<User>  wrapper = new QueryWrapper<User>();
        wrapper.eq("id",1290484912125722626L);
        User user = userMapper.selectOne(wrapper);
        System.out.println(user);
    }
}
