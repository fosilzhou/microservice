package com.fosilzhou.shardingjdbc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fosilzhou.shardingjdbc.entity.Order;
import com.fosilzhou.shardingjdbc.entity.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper extends BaseMapper<Order> {

}
