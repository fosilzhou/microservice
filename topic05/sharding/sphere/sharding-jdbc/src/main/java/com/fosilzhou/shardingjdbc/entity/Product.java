package com.fosilzhou.shardingjdbc.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("t_product")
public class Product {
    private Long id;
    private String name;
    private BigDecimal price;
    private String description;
    private int stock;
    private Long userId;
}
