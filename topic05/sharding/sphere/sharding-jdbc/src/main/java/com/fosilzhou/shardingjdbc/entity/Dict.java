package com.fosilzhou.shardingjdbc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "t_datadic")
public class Dict {
    private Long id;
    private String name;
    private String value;
    private String remark;
}
