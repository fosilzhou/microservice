package com.fosilzhou.shardingjdbc.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_order")
public class Order {
    private Long id;
    private Long pId;
    private Long uId;
    private Integer num;
    private String orderDate;
}
