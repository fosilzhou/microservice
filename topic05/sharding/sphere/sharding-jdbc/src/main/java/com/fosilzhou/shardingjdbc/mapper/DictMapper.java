package com.fosilzhou.shardingjdbc.mapper;

import com.fosilzhou.shardingjdbc.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface DictMapper extends BaseMapper<Dict> {
}
