package com.fosilzhou.cloud.service.impl;


import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hmily.annotation.Hmily;
import org.dromara.hmily.core.concurrent.threadlocal.HmilyTransactionContextLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Override
    @Hmily(confirmMethod = "commit", cancelMethod = "rollback")
    public void updateAccountBalance(String name, Double amount){
        String globalTransId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("try 开始执行，txId:{}",globalTransId);
    }

    @Transactional
    public void commit(String name, Double amount){
        String globalTransId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("confirm 开始执行，txId:{}",globalTransId);
        if(accountMapper.isConfirm(globalTransId) > 0){
            log.info("【confirm幂等处理】 confirm已经执行，无需重复执行，txId:{}", globalTransId);
            return;
        }

        accountMapper.addAccountBalance(name, amount);

        accountMapper.addConfirm(globalTransId);
    }

    public void rollback(String name, Double amoun){
        String globalTransId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("cancel 开始执行，txId:{}",globalTransId);
    }
}
