package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    @Autowired
    AccountService accountService;

    @PostMapping(value = "/weixin/transfer")
    public Boolean transfer(@RequestParam("name") String name, @RequestParam("amount") Double amount) {
        try {
            accountService.updateAccountBalance(name, amount);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
