package com.fosilzhou.cloud.service;

import org.dromara.hmily.annotation.Hmily;

public interface AccountService {

    void updateAccountBalance(String name, Double amount);
}
