package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableFeignClients
@EnableAspectJAutoProxy
@EnableDiscoveryClient
@ComponentScan({"com.fosilzhou.cloud","org.dromara.hmily"})
public class WeixinApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(WeixinApplication.class, args);
	}

}
