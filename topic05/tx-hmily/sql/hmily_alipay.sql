/*
Navicat MySQL Data Transfer

Source Server         : trade
Source Server Version : 50730
Source Host           : 192.168.135.136:3306
Source Database       : hmily_alipay

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-07-27 14:04:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account_info
-- ----------------------------
DROP TABLE IF EXISTS `account_info`;
CREATE TABLE `account_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `truename` varchar(255) DEFAULT NULL COMMENT '姓名',
  `username` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `balance` double DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of account_info
-- ----------------------------
INSERT INTO `account_info` VALUES ('1', '心随梦飞', 'fosilzhou', null, '123456', '90');

-- ----------------------------
-- Table structure for local_cancel_log
-- ----------------------------
DROP TABLE IF EXISTS `local_cancel_log`;
CREATE TABLE `local_cancel_log` (
  `tx_id` varchar(255) NOT NULL COMMENT '事务Id',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of local_cancel_log
-- ----------------------------

-- ----------------------------
-- Table structure for local_confirm_log
-- ----------------------------
DROP TABLE IF EXISTS `local_confirm_log`;
CREATE TABLE `local_confirm_log` (
  `tx_id` varchar(255) NOT NULL COMMENT '事务Id',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of local_confirm_log
-- ----------------------------

-- ----------------------------
-- Table structure for local_mq_log
-- ----------------------------
DROP TABLE IF EXISTS `local_mq_log`;
CREATE TABLE `local_mq_log` (
  `tx_id` varchar(255) NOT NULL COMMENT '事务Id',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of local_mq_log
-- ----------------------------

-- ----------------------------
-- Table structure for local_try_log
-- ----------------------------
DROP TABLE IF EXISTS `local_try_log`;
CREATE TABLE `local_try_log` (
  `tx_id` varchar(255) NOT NULL COMMENT '事务Id',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of local_try_log
-- ----------------------------
INSERT INTO `local_try_log` VALUES ('1286999197197713408', '2020-07-25 12:18:26');
INSERT INTO `local_try_log` VALUES ('1287000085651963904', '2020-07-25 12:21:52');
INSERT INTO `local_try_log` VALUES ('1287000340904722432', '2020-07-25 12:22:54');
INSERT INTO `local_try_log` VALUES ('1287001822254129152', '2020-07-25 12:29:02');
INSERT INTO `local_try_log` VALUES ('1287004178357211136', '2020-07-25 12:39:35');
