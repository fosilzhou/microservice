package com.fosilzhou.cloud.service;

import org.dromara.hmily.annotation.Hmily;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(value = "tx-tcc-hmily-cloud-weixin")
public interface WeixinAccountService {

    @PostMapping(value = "/weixin/transfer")
    @Hmily
    public Boolean transfer(@RequestParam("name") String name, @RequestParam("amount") Double amount);
}
