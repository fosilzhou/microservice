package com.fosilzhou.cloud.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.dromara.hmily.common.config.HmilyDbConfig;
import org.dromara.hmily.core.bootstrap.HmilyTransactionBootstrap;
import org.dromara.hmily.core.service.HmilyInitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class DataSourceProxyConfig {

    @Autowired
    private Environment env;

    @Bean
    public HmilyTransactionBootstrap hmilyTransactionBootstrap(HmilyInitService hmilyInitService) throws Exception {
        HmilyTransactionBootstrap bootstrap = new HmilyTransactionBootstrap(hmilyInitService);
        bootstrap.setSerializer(env.getProperty("org.dromara.hmily.serializer"));
        bootstrap.setRecoverDelayTime(Integer.parseInt(env.getProperty("org.dromara.hmily.recoverDelayTime")));
        bootstrap.setRetryMax(Integer.parseInt(env.getProperty("org.dromara.hmily.retryMax")));
        bootstrap.setScheduledDelay(Integer.parseInt(env.getProperty("org.dromara.hmily.scheduledDelay")));
        bootstrap.setScheduledThreadMax(Integer.parseInt(env.getProperty("org.dromara.hmily.scheduledThreadMax")));
        bootstrap.setRepositorySupport(env.getProperty("org.dromara.hmily.repositorySupport"));
        bootstrap.setStarted(Boolean.parseBoolean(env.getProperty("org.dromara.hmily.started")));
        HmilyDbConfig config = new HmilyDbConfig();
        config.setDriverClassName(env.getProperty("org.dromara.hmily.hmilyDbConfig.driverClassName"));
        config.setUrl(env.getProperty("org.dromara.hmily.hmilyDbConfig.url"));
        config.setUsername(env.getProperty("org.dromara.hmily.hmilyDbConfig.username"));
        config.setPassword(env.getProperty("org.dromara.hmily.hmilyDbConfig.password"));
        bootstrap.setHmilyDbConfig(config);
        return bootstrap;
    }
}
