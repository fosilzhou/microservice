package com.fosilzhou.cloud.service.impl;


import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AlipayAccountService;
import com.fosilzhou.cloud.service.WeixinAccountService;
import lombok.extern.slf4j.Slf4j;
import org.dromara.hmily.annotation.Hmily;
import org.dromara.hmily.core.concurrent.threadlocal.HmilyTransactionContextLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Slf4j
public class AlipayAccountServiceImpl implements AlipayAccountService {

    @Autowired
    AccountMapper accountMapper;

    @Resource
    WeixinAccountService weixinAccountService;

    /**
     * 使用@Hmily标记的方法就是try方法，并在注解中指定confirm和cancel方法
     */
    @Hmily(confirmMethod = "confirmAccountBalance", cancelMethod = "cancelAccountBalance")
    @Transactional
    @Override
    public void tryAccountBalance(String sourceName, String targetName, Double amount){
        String globalTransId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("try 开始执行，txId:{}",globalTransId);
        if(accountMapper.isTry(globalTransId)>0){
            // 幂等性判断：判断local_try_log表中是否有try日志记录，如果有，则不再执行
            log.info("【try幂等处理】 try已经执行，无需重复执行，txId:{}", globalTransId);
            return;
        }

        if(accountMapper.isConfirm(globalTransId)>0 || accountMapper.isCancel(globalTransId)>0){
            log.info("【try悬挂处理】 Confirm或Cancel已经执行，不允许执行，txId:{}", globalTransId);
            return;
        }

        if(accountMapper.subtractAccountBalance(sourceName, amount) == 0){
            log.info("扣減金额失败，txId:{}",globalTransId);
            throw new RuntimeException("扣減金额失败！");
        }
        // 插入try执行记录，用于幂等性校验
        accountMapper.addTry(globalTransId);

        if(!weixinAccountService.transfer(targetName, amount)){
            log.info("远程调用微信转账失败，txId:{}",globalTransId);
            throw new RuntimeException("远程调用微信转账失败！");
        }
    }

    /**
     * 提交阶段
     */
    public void confirmAccountBalance(String sourceName, String targetName, Double amount){
        String globalTransId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("confirm 开始执行，txId:{}",globalTransId);
    }

    /**
     * 回滚阶段
     */
    @Transactional
    public void cancelAccountBalance(String sourceName, String targetName, Double amount){
        String globalTransId = HmilyTransactionContextLocal.getInstance().get().getTransId();
        log.info("cancel 开始执行，txId:{}",globalTransId);
        if(accountMapper.isCancel(globalTransId) > 0){
            log.info("【cancel幂等处理】 cancel已经执行，无需重复执行，txId:{}", globalTransId);
            return;
        }
        if(accountMapper.isTry(globalTransId) == 0){
            log.info("【cancel空回滚处理】 如果try没有执行，cancel不允许执行，txId:{}", globalTransId);
            return;
        }

        // 回滚金额
        accountMapper.addAccountBalance(sourceName, amount);

        // 插入cancel执行记录，用于幂等性校验
        accountMapper.addCancel(globalTransId);
    }
}
