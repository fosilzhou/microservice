package com.fosilzhou.cloud.mapper;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface AccountMapper {

    @Update("UPDATE account_info SET balance = balance - #{amount} WHERE truename=#{name} and balance > #{amount}")
    int subtractAccountBalance(@Param("name")String name, @Param("amount") Double amount);

    @Update("UPDATE account_info set balance = balance + #{amount} WHERE truename=#{name}")
    int addAccountBalance(@Param("name")String name, @Param("amount") Double amount);

    @Insert("INSERT INTO local_try_log VALUES (#{txId},now());")
    int addTry(String localTradeNo);
    @Insert("INSERT INTO local_confirm_log VALUES (#{txId},now());")
    int addConfirm(String localTradeNo);
    @Insert("INSERT INTO local_cancel_log VALUES (#{txId},now());")
    int addCancel(String localTradeNo);

    @Select("SELECT count(1) FROM local_try_log WHERE tx_id=#{txId}")
    int isTry(String localTradeNo);
    @Select("SELECT count(1) FROM local_confirm_log WHERE tx_id=#{txId}")
    int isConfirm(String localTradeNo);
    @Select("SELECT count(1) FROM local_cancel_log WHERE tx_id=#{txId}")
    int isCancel(String localTradeNo);
}