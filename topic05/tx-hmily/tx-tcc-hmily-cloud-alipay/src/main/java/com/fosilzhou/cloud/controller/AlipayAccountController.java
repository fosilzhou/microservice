package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.service.AlipayAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付宝账号向微信账号转账
 */
@RestController
public class AlipayAccountController {

    @Autowired
    AlipayAccountService accountService;

    @GetMapping(value = "/alipay/transfer")
    public void transfer(@RequestParam("sourceName") String sourceName, @RequestParam("targetName") String targetName, @RequestParam("amount") Double amount) {
        accountService.tryAccountBalance(sourceName, targetName, amount);
    }
}
