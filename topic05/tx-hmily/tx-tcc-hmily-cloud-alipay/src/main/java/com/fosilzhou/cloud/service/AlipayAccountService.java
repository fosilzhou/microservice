package com.fosilzhou.cloud.service;

public interface AlipayAccountService {

    void tryAccountBalance(String sourceName, String targetName, Double amount);
}
