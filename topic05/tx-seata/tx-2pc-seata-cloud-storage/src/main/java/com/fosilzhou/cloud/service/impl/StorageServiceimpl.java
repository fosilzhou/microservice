package com.fosilzhou.cloud.service.impl;

import com.fosilzhou.cloud.entity.Storage;
import com.fosilzhou.cloud.entity.StorageExample;
import com.fosilzhou.cloud.mapper.StorageMapper;
import com.fosilzhou.cloud.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class StorageServiceimpl implements StorageService {

    @Resource
    private StorageMapper storageMapper;


    @Override
    public void reduceStorage(Long productId, Integer count) {
        StorageExample example = new StorageExample();
        StorageExample.Criteria criteria = example.createCriteria();
        criteria.andProductIdEqualTo(productId);
        List<Storage> list = storageMapper.selectByExample(example);
        if(list != null && list.size() == 1){
            // update t_storage set used = used + #{count},residue = residue -#{count} where product_id = #{productId}
            Storage storage = list.get(0);
            storage.setUsed(storage.getUsed() + count);
            storage.setResidue(storage.getResidue() - count);
            storageMapper.updateByExampleSelective(storage, example);
        }
    }
}
