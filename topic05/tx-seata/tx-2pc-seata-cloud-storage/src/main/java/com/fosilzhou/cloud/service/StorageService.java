package com.fosilzhou.cloud.service;

public interface StorageService {

    void reduceStorage(Long productId,Integer count);
}
