package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.service.StorageService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/storage")
public class StorageController {

    @Resource
    private StorageService storageService;

    @PostMapping(value = "/reduceStorage")
    public R reduceStorage(@RequestParam("productId") Long productId, @RequestParam("count") Integer count) {
        storageService.reduceStorage(productId, count);
        return new R(200,"库存扣减成功");
    }
}
