package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(value = "tx-2pc-seata-cloud-storage")
public interface StorageService {

    @PostMapping(value = "/storage/reduceStorage")
    public R reduceStorage(@RequestParam("productId") Long productId, @RequestParam("count") Integer count);
}
