package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@Component
@FeignClient(value = "tx-2pc-seata-cloud-account")
public interface AccountService {

    @PostMapping(value = "/account/reduceMoneyPaid")
    public R reduceMoneyPaid(@RequestParam("userId") Long userId, @RequestParam("money")BigDecimal money);
}
