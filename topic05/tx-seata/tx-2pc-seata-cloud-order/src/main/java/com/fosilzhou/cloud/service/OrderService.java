package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.Order;

public interface OrderService {

    void create(Order order);
}
