package com.fosilzhou.cloud.service.impl;


import com.fosilzhou.cloud.entity.Order;
import com.fosilzhou.cloud.entity.OrderExample;
import com.fosilzhou.cloud.mapper.OrderMapper;
import com.fosilzhou.cloud.service.AccountService;
import com.fosilzhou.cloud.service.OrderService;
import com.fosilzhou.cloud.service.StorageService;
import com.fosilzhou.cloud.util.IdGeneratorSnowflake;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;
    @Resource
    private StorageService storageService;
    @Resource
    private AccountService accountService;
    @Autowired
    private IdGeneratorSnowflake idGenerator;

    @GlobalTransactional(name = "fosilzhou-create-order",rollbackFor = Exception.class)
    @Override
    public void create(Order order) {

        generator();

        log.info("-------->创建新订单");
        order.setStatus(0);
        orderMapper.insertSelective(order);

        log.info("--------订单微服务开始调用库存服务,执行扣减金额库存");
        storageService.reduceStorage(order.getProductId(),order.getCount());

        log.info("-------订单微服务开始调用账户服务，执行扣减金额");
        accountService.reduceMoneyPaid(order.getUserId(),order.getMoney());

        log.info("-------修改订单状态");
        OrderExample example = new OrderExample();
        example.createCriteria().andUserIdEqualTo(order.getUserId());
        Order o = new Order();
        o.setStatus(1);
        orderMapper.updateByExampleSelective(o, example);
    }

    public void generator() {
        ExecutorService threadPool = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 100; i++) {
            threadPool.submit(()->{
                System.out.println(idGenerator.snowflakeId());
            });
        }
        threadPool.shutdown();
    }
}
