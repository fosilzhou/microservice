package com.fosilzhou.cloud.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ID生成规则硬性要求：
 * 1、全局唯一：不能出现重复的ID号，既然是唯一标识，这是最基本的要求
 * 2、趋势递增：在MySQL InnoDB引擎中使用的是聚集索引，由于多数RDBMS使用B-tree的数据结构来存储索引数据，在主键的选择上面我们应该尽量使用有序的主键保证写入性能。
 * 3、单调递增：保证下一个ID一定大于上一个ID，例如事务版本号、IM增量消息、排序等特殊需求。
 * 4、信息安全：如果ID是连续的，恶意用户的扒取工作就非常容易做了，直接按照顺序下载指定URL即可；所以在一些应用场景下，会需要ID无规则、不规则。
 * 5、含时间戳
 * 性能要求：
 * 1、平均延迟和TP999延迟都要尽可能低
 * 2、可用性5个9
 * 3、高QPS
 *
 * 通用解决方案
 * 1、UUID
 * 2、数据库自增
 * +-------------+--------------+------+-----+-------------------+-----------------------------+
 * | Field       | Type         | Null | Key | Default           | Extra                       |
 * +-------------+--------------+------+-----+-------------------+-----------------------------+
 * | biz_tag     | varchar(128) | NO   | PRI |                   |  业务标识                    |
 * | max_id      | bigint(20)   | NO   |     | 1                 |  当前使用的ID                |
 * | step        | int(11)      | NO   |     | NULL              |  ID步长值                   |
 * | desc        | varchar(256) | YES  |     | NULL              |                             |
 * | update_time | timestamp    | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
 * +-------------+--------------+------+-----+-------------------+-----------------------------+
 * 3、基于Redis生成全局ID策略
 * 4、雪花算法
 *
 * 雪花算法
 * 优点：
 * 1、毫秒数在高位，自增序列在低位，整个ID都是趋势递增
 * 2、不依赖第三方系统或中间件，稳定性更高，生成ID的性能也非常高
 * 3、可以根据自身业务特性分配bit位，非常灵活
 * 缺点：
 * 1、依赖机器时钟，如果机器时钟回拨，会导致重复ID生成
 * 2、在单机上是递增的，但是在分布式环境，由于每台机器上时钟不可能完全同步，会出现不是全局递增的情况
 * 针对上述两个缺点：
 *  1、百度开源的分布式唯一ID生成器UidGenerator
 *  2、美团点评分布式ID生成系统Leaf
 */
@Slf4j
@Component
public class IdGeneratorSnowflake {

    private long workerId = 0;
    private long datacenterId = 1;
    private Snowflake snowflake = IdUtil.createSnowflake(workerId, datacenterId);

    @PostConstruct
    public void init(){
        try {
            workerId = NetUtil.ipv4ToLong(NetUtil.getLocalhostStr());
            log.info("当前机器的workerId：{}", workerId);
        }catch (Exception e){
            e.printStackTrace();
            log.warn("当前机器的workerId获取失败",e);
            workerId = NetUtil.getLocalhostStr().hashCode();
        }
    }

    public synchronized long snowflakeId() {
        return snowflake.nextId();
    }

    public synchronized long snowflakeId(long workerId, long datacenterId) {
        Snowflake snowflake = IdUtil.createSnowflake(workerId, datacenterId);
        return snowflake.nextId();
    }

    public static void main(String[] args) {
        System.out.println(new IdGeneratorSnowflake().snowflakeId());
    }
}
