package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.Order;
import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("/order/create")
    public R create(Order order) {
        orderService.create(order);
        return new R(200,"订单创建成功！");
    }
}
