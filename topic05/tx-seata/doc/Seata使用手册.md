# Seata部署

TC 需要进行全局事务和分支事务的记录，所以需要对应的**存储**。目前，TC 有三种存储模式( `store.mode` ) 

- file 模式：适合**单机**模式，全局事务会话信息在**内存**中读写，并持久化本地文件 `root.data`，性能较高。
- db 模式：适合**集群**模式，全局事务会话信息通过 **db** 共享，相对性能差点。
- redis 模式：适合**集群**模式，全局事务会话信息通过 **redis** 共享，相对性能较好。

## 部署单机 TC Server

![单机 TC Server](assets/11.png) 

```shell
## 下载
wget https://github.com/seata/seata/releases/download/v1.3.0/seata-server-1.3.0.tar.gz
## 解压
tar -zxvf seata-server-1.3.0.tar.gz
## 启动
nohup sh bin/seata-server.sh &
```



## 部署集群 TC Server

![集群 TC Server](assets/21.png)

### 下载安装包 

```shell
## 下载
wget https://github.com/seata/seata/releases/download/v1.3.0/seata-server-1.3.0.tar.gz
## 解压
tar -zxvf seata-server-1.3.0.tar.gz
## 添加MySQL驱动包
cd seata/lib/jdbc/ && cp mysql-connector-java-5.1.35.jar ../
```

### 初始化数据库

[数据库脚本](https://github.com/seata/seata/blob/develop/script/server/db/mysql.sql)

```mysql
-- -------------------------------- The script used when storeMode is 'db' --------------------------------
-- the table to store GlobalSession data
CREATE TABLE IF NOT EXISTS `global_table`
(
    `xid`                       VARCHAR(128) NOT NULL,
    `transaction_id`            BIGINT,
    `status`                    TINYINT      NOT NULL,
    `application_id`            VARCHAR(32),
    `transaction_service_group` VARCHAR(32),
    `transaction_name`          VARCHAR(128),
    `timeout`                   INT,
    `begin_time`                BIGINT,
    `application_data`          VARCHAR(2000),
    `gmt_create`                DATETIME,
    `gmt_modified`              DATETIME,
    PRIMARY KEY (`xid`),
    KEY `idx_gmt_modified_status` (`gmt_modified`, `status`),
    KEY `idx_transaction_id` (`transaction_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- the table to store BranchSession data
CREATE TABLE IF NOT EXISTS `branch_table`
(
    `branch_id`         BIGINT       NOT NULL,
    `xid`               VARCHAR(128) NOT NULL,
    `transaction_id`    BIGINT,
    `resource_group_id` VARCHAR(32),
    `resource_id`       VARCHAR(256),
    `branch_type`       VARCHAR(8),
    `status`            TINYINT,
    `client_id`         VARCHAR(64),
    `application_data`  VARCHAR(2000),
    `gmt_create`        DATETIME(6),
    `gmt_modified`      DATETIME(6),
    PRIMARY KEY (`branch_id`),
    KEY `idx_xid` (`xid`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- the table to store lock data
CREATE TABLE IF NOT EXISTS `lock_table`
(
    `row_key`        VARCHAR(128) NOT NULL,
    `xid`            VARCHAR(96),
    `transaction_id` BIGINT,
    `branch_id`      BIGINT       NOT NULL,
    `resource_id`    VARCHAR(256),
    `table_name`     VARCHAR(32),
    `pk`             VARCHAR(36),
    `gmt_create`     DATETIME,
    `gmt_modified`   DATETIME,
    PRIMARY KEY (`row_key`),
    KEY `idx_branch_id` (`branch_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
```

### 调整存储配置

**修改conf/file.conf**

![1597112503948](assets/1597112503948.png)

### 设置注册中心

**修改conf/registry.conf** ，设置使用nacos注册中心

> Nacos部署请参考【Nacos使用手册.md】

![1597112694149](assets/1597112694149.png)

### 启动 TC Server

```shell
nohup sh bin/seata-server.sh -p 19090 -n 1 &
nohup sh bin/seata-server.sh -p 29090 -n 2 &
nohup sh bin/seata-server.sh -p 39090 -n 3 &
```

- `-p`：Seata TC Server 监听的端口。
- `-n`：Server node。在多个 TC Server 时，需区分各自节点，用于生成不同区间的 transactionId 事务编号，以免冲突。

![1597113320154](assets/1597113320154.png)

# 接入框架情况

## AT 模式

### 接入Dubbo



### 接入Spring Boot



### 接入Spring Cloud



## TCC模式

示例：<https://github.com/seata/seata-samples/tree/master/tcc> 

## Saga模式

示例：<https://github.com/seata/seata-samples/tree/master/saga> 