package com.fosilzhou.cloud.service;

import java.math.BigDecimal;

public interface AccountService {

    /**
     * 扣减支付金额
     * @param userId
     * @param money
     */
    void reduceMoneyPaid(Long userId, BigDecimal money);
}
