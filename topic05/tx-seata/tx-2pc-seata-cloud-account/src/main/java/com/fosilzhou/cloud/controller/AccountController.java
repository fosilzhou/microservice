package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.service.AccountService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

@RestController
@RequestMapping(value = "account")
public class AccountController {

    @Resource
    private AccountService accountService;

    @PostMapping(value = "reduceMoneyPaid")
    public R reduceMoneyPaid(@RequestParam("userId") Long userId, @RequestParam("money")BigDecimal money) {
        accountService.reduceMoneyPaid(userId, money);
        return new R(200,"账户余额扣减成功");
    }
}
