package com.fosilzhou.cloud.service.impl;

import com.fosilzhou.cloud.entity.Account;
import com.fosilzhou.cloud.entity.AccountExample;
import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountMapper accountMapper;

    @Override
    public void reduceMoneyPaid(Long userId, BigDecimal money) {
        AccountExample example = new AccountExample();
        AccountExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        List<Account> list = accountMapper.selectByExample(example);
        if(list != null && list.size() == 1){
            // update t_account set used = used + #{money},residue = residue - #{money} where user_id=#{userId}
            Account account = list.get(0);
            account.setUsed(account.getUsed().add(money));
            account.setResidue(account.getResidue().subtract(money));
            accountMapper.updateByExampleSelective(account, example);
        }
    }
}
