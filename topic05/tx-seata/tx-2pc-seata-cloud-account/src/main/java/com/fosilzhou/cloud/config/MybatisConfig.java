package com.fosilzhou.cloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.fosilzhou.cloud.mapper"})
public class MybatisConfig {
}
