package com.fosilzhou.cloud.mapper;

import com.fosilzhou.cloud.entity.AccountPay;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface AccountMapper {

    @Insert("INSERT INTO t_recharge (id, account, amount, status) VALUES (#{txId}, #{account}, #{amount}, #{status})")
    int insertAccountPay(@Param("txId")long txId, @Param("account")String account, @Param("amount") Double amount, @Param("status") int status);

    @Select("SELECT id, account, amount, status FROM t_recharge WHERE id=#{txId}")
    AccountPay findByTxId(@Param("txId")long txId);
}