package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.AccountPay;

public interface AccountService {

    /**
     * 充值接口
     * @param accountPay
     * @return
     */
    AccountPay insertAccountPay(AccountPay accountPay);

    /**
     * 查询充值结果
     * @param txId
     * @return
     */
    AccountPay getAccountPay(long txId);
}
