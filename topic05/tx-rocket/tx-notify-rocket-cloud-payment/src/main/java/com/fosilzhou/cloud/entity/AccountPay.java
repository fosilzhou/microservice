package com.fosilzhou.cloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountPay implements Serializable {
    private long id;
    private String account;
    private double amount;
    private int status;
}
