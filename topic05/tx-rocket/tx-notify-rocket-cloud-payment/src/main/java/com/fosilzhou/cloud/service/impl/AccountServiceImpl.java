package com.fosilzhou.cloud.service.impl;


import com.fosilzhou.cloud.entity.AccountPay;
import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    RocketMQTemplate rocketMQTemplate;

    /**
     * 充值方需要接收充值通知信息，获取充值结果信息，如果无法从消息通知获取到充值结果；可以查询充值结果接口获取
     * @param accountPay
     * @return
     */
    @Override
    public AccountPay insertAccountPay(AccountPay accountPay) {

        int success = accountMapper.insertAccountPay(accountPay.getId(), accountPay.getAccount(), accountPay.getAmount(), 0);
        if(success > 0){
            accountPay.setStatus(0);
            // 发送充值成功通知，使用普通消息发送通知
            String destination= "topic-payment-notify";
            rocketMQTemplate.convertAndSend(destination, accountPay);
            return accountPay;
        }
        return null;
    }

    /**
     * 查询充值记录，接收通知方需要调用此方法查询充值结果
     * @param txId
     * @return
     */
    @Override
    public AccountPay getAccountPay(long txId) {
        return accountMapper.findByTxId(txId);
    }
}
