package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.AccountPay;
import com.fosilzhou.cloud.service.AccountService;
import com.fosilzhou.cloud.util.IdGeneratorSnowflake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    @Autowired
    private IdGeneratorSnowflake idGenerator;

    @Autowired
    AccountService accountService;

    @GetMapping(value = "/payment")
    public void payment(AccountPay accountPay) {
        accountPay.setId(idGenerator.snowflakeId());
        accountService.insertAccountPay(accountPay);
    }

    @GetMapping(value = "/result/{txId}")
    public AccountPay result(@PathVariable("txId") long txId) {
        return accountService.getAccountPay(txId);
    }
}
