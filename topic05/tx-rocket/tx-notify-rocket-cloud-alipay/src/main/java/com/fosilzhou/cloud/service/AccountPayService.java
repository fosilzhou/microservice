package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.AccountPay;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "tx-notify-rocket-cloud-payment")
public interface AccountPayService {

    @GetMapping(value = "/result/{txId}")
    public AccountPay result(@PathVariable("txId") long txId);
}
