package com.fosilzhou.cloud.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.entity.AccountPay;
import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountPayService;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    AccountPayService accountPayService;

    @Override
    @Transactional
    public void updateAccountBalance(AccountChangeEvent accountChangeEvent) {
        if(accountMapper.isTx(accountChangeEvent.getTxId()) > 0){
            // 幂等性校验
            return;
        }
        accountMapper.subtractAccountBalance(accountChangeEvent.getAccount(), accountChangeEvent.getAmount());

        accountMapper.addTx(accountChangeEvent.getTxId());
    }

    @Override
    public AccountPay queryPaymentResult(long txId) {
        log.info("主动请求充值系统查询充值结果...");
        AccountPay accountPay = accountPayService.result(txId);

        if(accountPay.getStatus() == 0) {
            AccountChangeEvent accountChangeEvent = new AccountChangeEvent();
            accountChangeEvent.setAccount(accountPay.getAccount());
            accountChangeEvent.setAmount(accountPay.getAmount());
            accountChangeEvent.setTxId(accountPay.getId());
            updateAccountBalance(accountChangeEvent);
        }

        return accountPay;
    }
}
