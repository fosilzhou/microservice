package com.fosilzhou.cloud.message;

import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.entity.AccountPay;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RocketMQMessageListener(topic = "topic-payment-notify", consumerGroup = "fosilzhou-consumer-alipay-notify")
public class NotifyMessageListener implements RocketMQListener<AccountPay> {

    @Autowired
    AccountService accountService;

    @Override
    public void onMessage(AccountPay message) {
        log.info("通过消息通知方式获取充值结果...");
        if(message.getStatus() == 0) {
            AccountChangeEvent accountChangeEvent = new AccountChangeEvent();
            accountChangeEvent.setAccount(message.getAccount());
            accountChangeEvent.setAmount(message.getAmount());
            accountChangeEvent.setTxId(message.getId());
            accountService.updateAccountBalance(accountChangeEvent);
        }
    }
}
