package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.entity.AccountPay;
import com.fosilzhou.cloud.service.AccountService;
import com.fosilzhou.cloud.util.IdGeneratorSnowflake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付宝账号向微信账号转账
 */
@RestController
public class AccountController {

    @Autowired
    AccountService accountService;

    /**
     * 主动查询充值结果
     * @param txId
     * @return
     */
    @GetMapping(value = "/payment/result/{txId}")
    public AccountPay paymentResult(@PathVariable("txId") long txId) {
        return accountService.queryPaymentResult(txId);
    }
}
