package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.entity.AccountPay;

public interface AccountService {

    /**
     * 更新账户余额
     * @param accountChangeEvent
     */
    void updateAccountBalance(AccountChangeEvent accountChangeEvent);

    /**
     * 查询充值结果（远程调用）
     */
    AccountPay queryPaymentResult(long txId);
}
