package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.AccountChangeEvent;

public interface AccountService {

    /**
     * 向RocketMQ发送转账消息
     * @param accountChangeEvent
     */
    void sendUpdateAccountBalance(AccountChangeEvent accountChangeEvent);

    /**
     * 扣减金额
     * @param accountChangeEvent
     */
    void doUpdateAccountBalance(AccountChangeEvent accountChangeEvent);
}
