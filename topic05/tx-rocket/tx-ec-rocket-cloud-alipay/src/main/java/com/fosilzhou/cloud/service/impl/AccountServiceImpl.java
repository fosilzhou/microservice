package com.fosilzhou.cloud.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    RocketMQTemplate rocketMQTemplate;

    @Override
    public void sendUpdateAccountBalance(AccountChangeEvent accountChangeEvent) {
        log.info("发送事务消息");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accountChange", accountChangeEvent);
        Message<String> message = MessageBuilder.withPayload(jsonObject.toJSONString()).build();
        String producerGroup = "fosilzhou-producer-alipay-tx";
        String destination = "topic-tx";
        rocketMQTemplate.sendMessageInTransaction(producerGroup, destination, message, null);
    }

    @Override
    public void doUpdateAccountBalance(AccountChangeEvent accountChangeEvent) {
        log.info("更新支付宝本地账户：账号：{}, 金额：{}",accountChangeEvent.getSourceName(), accountChangeEvent.getAmount());
        if(accountMapper.isTx(accountChangeEvent.getTxId()) > 0) {
            return;
        }
        accountMapper.subtractAccountBalance(accountChangeEvent.getSourceName(), accountChangeEvent.getAmount());

        accountMapper.addTx(accountChangeEvent.getTxId());
    }
}
