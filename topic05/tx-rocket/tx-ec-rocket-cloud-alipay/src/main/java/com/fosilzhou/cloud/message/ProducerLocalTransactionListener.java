package com.fosilzhou.cloud.message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
@RocketMQTransactionListener(txProducerGroup = "fosilzhou-producer-alipay-tx")
public class ProducerLocalTransactionListener implements RocketMQLocalTransactionListener {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountMapper accountMapper;
    /**
     * 事务消息发送后回调方法，当消息发送给MQ成功后此方法被回调
     * @param msg
     * @param arg
     * @return
     */
    @Override
    @Transactional
    public RocketMQLocalTransactionState executeLocalTransaction(Message msg, Object arg) {
        log.info("事务消息发送后回调方法");
        try {
            String msgStr = new String((byte[]) msg.getPayload());
            JSONObject jsonObject = JSON.parseObject(msgStr);
            String jsonStr = jsonObject.getString("accountChange");
            AccountChangeEvent accountChangeEvent = JSON.parseObject(jsonStr, AccountChangeEvent.class);
            accountService.doUpdateAccountBalance(accountChangeEvent);
            //当返回COMMIT时，自动向MQ发送commit消息，MQ将消息状态改为可消费状态
            return RocketMQLocalTransactionState.COMMIT;
        }catch (Exception e){
            return RocketMQLocalTransactionState.ROLLBACK;
        }
    }

    /**
     * 事务状态回查，查询是否扣减金额
     * @param msg
     * @return
     */
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message msg) {
        log.info("事务状态回查");
        String msgStr = new String((byte[]) msg.getPayload());
        JSONObject jsonObject = JSON.parseObject(msgStr);
        String jsonStr = jsonObject.getString("accountChange");
        AccountChangeEvent accountChangeEvent = JSON.parseObject(jsonStr, AccountChangeEvent.class);
        // 事务ID
        if(accountMapper.isTx(accountChangeEvent.getTxId()) > 0){
            return RocketMQLocalTransactionState.COMMIT;
        }else {
            return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}
