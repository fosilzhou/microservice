package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.service.AccountService;
import com.fosilzhou.cloud.util.IdGeneratorSnowflake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付宝账号向微信账号转账
 * 测试请求：http://localhost:18080/alipay/transfer?sourceName=fosilzhou&targetName=hjh1988&amount=10
 */
@RestController
public class AccountController {

    @Autowired
    private IdGeneratorSnowflake idGenerator;

    @Autowired
    AccountService accountService;

    @GetMapping(value = "/alipay/transfer")
    public void transfer(@RequestParam("sourceName") String sourceName, @RequestParam("targetName") String targetName, @RequestParam("amount") Double amount) {
        AccountChangeEvent accountChangeEvent = new AccountChangeEvent();
        accountChangeEvent.setAmount(amount);
        accountChangeEvent.setSourceName(sourceName);
        accountChangeEvent.setTargetName(targetName);
        accountChangeEvent.setTxId(idGenerator.snowflakeId()+"");
        accountService.sendUpdateAccountBalance(accountChangeEvent);
    }
}
