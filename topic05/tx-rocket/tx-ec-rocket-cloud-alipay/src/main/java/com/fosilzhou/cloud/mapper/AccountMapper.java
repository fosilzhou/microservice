package com.fosilzhou.cloud.mapper;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface AccountMapper {

    @Update("UPDATE account_info SET balance = balance - #{amount} WHERE truename=#{name} and balance > #{amount}")
    int subtractAccountBalance(@Param("name")String name, @Param("amount") Double amount);

    @Update("UPDATE account_info set balance = balance + #{amount} WHERE truename=#{name}")
    int addAccountBalance(@Param("name")String name, @Param("amount") Double amount);

    @Insert("INSERT INTO local_mq_log VALUES (#{txId},now());")
    int addTx(String localTradeNo);

    @Select("SELECT count(1) FROM local_mq_log WHERE tx_id=#{txId}")
    int isTx(String localTradeNo);
}