package com.fosilzhou.cloud.message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
@RocketMQMessageListener(consumerGroup = "fosilzhou-producer-weixin-tx", topic = "topic-tx")
public class ConsumerRocketMQListener implements RocketMQListener<String> {

    @Autowired
    AccountService accountService;

    @Override
    public void onMessage(String message) {
        JSONObject jsonObject = JSON.parseObject(message);
        String jsonStr = jsonObject.getString("accountChange");
        AccountChangeEvent accountChangeEvent = JSON.parseObject(jsonStr, AccountChangeEvent.class);

        accountService.addAccountBalance(accountChangeEvent);
    }
}
