package com.fosilzhou.cloud.service.impl;


import com.fosilzhou.cloud.entity.AccountChangeEvent;
import com.fosilzhou.cloud.mapper.AccountMapper;
import com.fosilzhou.cloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Override
    public void addAccountBalance(AccountChangeEvent accountChangeEvent) {
        log.info("更新微信本地账户：账号：{}, 金额：{}",accountChangeEvent.getTargetName(), accountChangeEvent.getAmount());
        if(accountMapper.isTx(accountChangeEvent.getTxId()) > 0){
            // 处理幂等性
            return;
        }

        accountMapper.addAccountBalance(accountChangeEvent.getTargetName(), accountChangeEvent.getAmount());

        accountMapper.addTx(accountChangeEvent.getTxId());
    }
}
