package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.AccountChangeEvent;

public interface AccountService {

    void addAccountBalance(AccountChangeEvent accountChangeEvent);
}
