## 本地运行

先安装gitbook

```bash
npm install gitbook-cli -g
```

然后安装gitbook插件

```bash
gitbook install
```

运行服务

```bash
gitbook serve .
```

然后就可以在本地 [http://localhost:4000](http://localhost:4000)访问了。

## 仓库代码

```
├─topic00
│  ├─juc
│  └─jvm
├─topic01
│  ├─consul
│  │  ├─consul-consumer
│  │  ├─consul-provider
│  │  └─doc
│  ├─eureka
│  │  ├─doc
│  │  ├─eureka-consumer
│  │  ├─eureka-provider
│  │  ├─eureka-server-cluster18761
│  │  ├─eureka-server-cluster28761
│  │  ├─eureka-server-cluster38761
│  │  ├─eureka-server-security
│  │  └─eureka-server-standalone
│  ├─nacos
│  │  ├─doc
│  │  ├─nacos-consumer
│  │  └─nacos-provider
│  └─zookeeper
│      ├─doc
│      ├─zookeeper-consumer
│      └─zookeeper-provider
├─topic02
│  ├─apollo-config
│  │  ├─apollo-config-consumer
│  │  ├─apollo-config-provider
│  │  └─doc
│  ├─consul-config
│  │  ├─consul-config-consumer
│  │  ├─consul-config-provider
│  │  └─doc
│  ├─nacos-config
│  │  ├─doc
│  │  ├─nacos-config-consumer
│  │  └─nacos-config-provider
│  └─zookeeper-config
│      ├─doc
│      ├─zookeeper-config-consumer
│      └─zookeeper-config-provider
├─topic03
│  ├─gateway
│  │  ├─gateway-custom-gateway-filter
│  │  ├─gateway-custom-global-filter
│  │  ├─gateway-demo
│  │  ├─gateway-fuse-hystrix
│  │  ├─gateway-fuse-sentinel
│  │  ├─gateway-rate-limiter
│  │  ├─gateway-route-config-apollo
│  │  ├─gateway-route-config-nacos
│  │  └─gateway-route-registry
│  └─zuul
│      ├─zuul-custom-filter
│      ├─zuul-demo
│      ├─zuul-fuse-hystrix
│      ├─zuul-fuse-sentinel
│      ├─zuul-route-config-apollo
│      ├─zuul-route-config-nacos
│      └─zuul-route-registry
├─topic04
│  ├─kafka
│  │  ├─spring-boot-kafka
│  │  ├─spring-cloud-bus-kafka
│  │  └─spring-cloud-stream-kafka
│  │      ├─doc
│  │      ├─stream-kafka-consumer-ack
│  │      ├─stream-kafka-consumer-batch
│  │      ├─stream-kafka-consumer-broadcast
│  │      ├─stream-kafka-consumer-concurrency
│  │      ├─stream-kafka-consumer-demo
│  │      ├─stream-kafka-consumer-filter
│  │      ├─stream-kafka-consumer-orderly
│  │      ├─stream-kafka-consumer-retry
│  │      ├─stream-kafka-consumer-transaction
│  │      ├─stream-kafka-producer-ack
│  │      ├─stream-kafka-producer-batch
│  │      ├─stream-kafka-producer-broadcast
│  │      ├─stream-kafka-producer-concurrency
│  │      ├─stream-kafka-producer-demo
│  │      ├─stream-kafka-producer-filter
│  │      ├─stream-kafka-producer-orderly
│  │      ├─stream-kafka-producer-retry
│  │      └─stream-kafka-producer-transaction
│  ├─rabbitmq
│  │  ├─spring-boot-rabbitmq
│  │  │  ├─doc
│  │  │  ├─spring-boot-rabbitmq-batch
│  │  │  ├─spring-boot-rabbitmq-delay
│  │  │  ├─spring-boot-rabbitmq-demo
│  │  │  └─spring-boot-rabbitmq-retry01
│  │  ├─spring-cloud-bus-rabbitmq
│  │  └─spring-cloud-stream-rabbitmq
│  │      ├─doc
│  │      ├─stream-rabbitmq-consumer-ack
│  │      ├─stream-rabbitmq-consumer-batch
│  │      ├─stream-rabbitmq-consumer-broadcast
│  │      ├─stream-rabbitmq-consumer-concurrency
│  │      ├─stream-rabbitmq-consumer-confirm
│  │      ├─stream-rabbitmq-consumer-delay
│  │      ├─stream-rabbitmq-consumer-demo
│  │      ├─stream-rabbitmq-consumer-filter
│  │      ├─stream-rabbitmq-consumer-orderly
│  │      ├─stream-rabbitmq-consumer-retry
│  │      ├─stream-rabbitmq-consumer-transaction
│  │      ├─stream-rabbitmq-producer-ack
│  │      ├─stream-rabbitmq-producer-batch
│  │      ├─stream-rabbitmq-producer-broadcast
│  │      ├─stream-rabbitmq-producer-concurrency
│  │      ├─stream-rabbitmq-producer-confirm
│  │      ├─stream-rabbitmq-producer-delay
│  │      ├─stream-rabbitmq-producer-demo
│  │      ├─stream-rabbitmq-producer-filter
│  │      ├─stream-rabbitmq-producer-orderly
│  │      ├─stream-rabbitmq-producer-retry
│  │      └─stream-rabbitmq-producer-transaction
│  └─rocketmq
│      ├─native-rocketmq
│      │  ├─doc
│      ├─spring-boot-rocketmq
│      │  ├─doc
│      ├─spring-cloud-bus-rocketmq
│      │  ├─bus-rocketmq-listener
│      │  ├─bus-rocketmq-publisher
│      └─spring-cloud-stream-rocketmq
│          ├─doc
│          ├─stream-rocketmq-consumer-actuator
│          ├─stream-rocketmq-consumer-broadcast
│          ├─stream-rocketmq-consumer-delay
│          ├─stream-rocketmq-consumer-demo
│          ├─stream-rocketmq-consumer-filter
│          ├─stream-rocketmq-consumer-orderly
│          ├─stream-rocketmq-consumer-retry
│          ├─stream-rocketmq-consumer-transaction
│          ├─stream-rocketmq-producer-actuator
│          ├─stream-rocketmq-producer-broadcast
│          ├─stream-rocketmq-producer-delay
│          ├─stream-rocketmq-producer-demo
│          ├─stream-rocketmq-producer-filter
│          ├─stream-rocketmq-producer-orderly
│          ├─stream-rocketmq-producer-retry
│          └─stream-rocketmq-producer-transaction
├─topic05
│  ├─elastic-job
│  ├─sharding
│  │  ├─mycat
│  │  └─sphere
│  │      ├─doc
│  │      ├─sharding-jdbc
│  │      └─sharding-proxy
│  ├─transaction
│  ├─tx-hmily
│  │  ├─tx-tcc-hmily-cloud-alipay
│  │  └─tx-tcc-hmily-cloud-weixin
│  ├─tx-rocket
│  │  ├─tx-ec-rocket-cloud-alipay
│  │  ├─tx-ec-rocket-cloud-weixin
│  │  ├─tx-notify-rocket-cloud-alipay
│  │  └─tx-notify-rocket-cloud-payment
│  └─tx-seata
│      ├─tx-2pc-seata-cloud-account
│      ├─tx-2pc-seata-cloud-order
│      └─tx-2pc-seata-cloud-storage
├─topic06
│  ├─dubbo-rpc
│  │  ├─alibaba-dubbo-nacos
│  │  ├─alibaba-dubbo-nacos-fegin
│  │  ├─alibaba-dubbo-nacos-seata
│  │  ├─alibaba-dubbo-nacos-sentinel
│  │  ├─alibaba-dubbo-nacos-validation
│  │  ├─dubbo-nacos
│  │  ├─dubbo-nacos-seata
│  │  ├─dubbo-zookeeper
│  │  ├─dubbo-zookeeper-exception
│  │  ├─dubbo-zookeeper-sentinel
│  │  ├─dubbo-zookeeper-validation
│  │  └─dubbo-zookeeper-xml
│  ├─netty-rpc
│  │  ├─netty-rpc-client
│  │  ├─netty-rpc-common
│  │  └─netty-rpc-server
│  └─servicecomb
│      ├─servicecomb-rest
│      │  ├─servicecomb-rest-api
│      │  ├─servicecomb-rest-consumer
│      │  └─servicecomb-rest-provider
│      └─servicecomb-rpc
│          ├─servicecomb-rpc-api
│          ├─servicecomb-rpc-consumer
│          └─servicecomb-rpc-provider
├─topic07
│  ├─spring-websocket
│  └─tomcat-websocket
├─topic08
└─topic09
    └─skywalking
        ├─skywalking-elasticsearch4jest
        ├─skywalking-mongo
        ├─skywalking-mysql
        ├─skywalking-redis
        └─skywalking-springmvc
```

## 本书简介

### 使用手册

[MySQL使用手册]

[Nacos使用手册](./topic01/nacos/doc/Nacos使用手册.md)

[Seata使用手册](./topic05/tx-seata/doc/Seata使用手册.md)

[Sentinel使用手册]

[Apollo使用手册](./topic02/apollo-config/doc/Apollo使用手册.md)

[Mycat使用手册](./topic05/sharding/mycat/doc/Mycat使用手册.md)

[ShardingSphere使用手册](./topic05/sharding/sphere/doc/ShardingSphere使用手册.md)

### SpringCloud专栏



### SpringCloudAlibaba专栏



### 消息队列专栏



### 分布式事务专栏

#### Seata分布式事务

AT模式+多数据源

AT模式+远程调用

AT模式+Dubbo

AT模式+SpringCloudAlibaba+Dubbo

AT模式+SpringCloudAlibaba+Fegin

### RPC调用专栏

#### Dubbo