package com.fosilzhou.cloud.service;

import com.fosilzhou.cloud.entity.User;

public interface UserService {

	int create(User user);
	
	User getById(Integer id);
}
