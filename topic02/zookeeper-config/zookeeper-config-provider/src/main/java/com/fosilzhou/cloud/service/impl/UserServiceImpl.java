package com.fosilzhou.cloud.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fosilzhou.cloud.entity.User;
import com.fosilzhou.cloud.mapper.UserMapper;
import com.fosilzhou.cloud.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;

	@Override
	public int create(User user) {
		return userMapper.insertSelective(user);
	}

	@Override
	public User getById(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}
	
	
}
