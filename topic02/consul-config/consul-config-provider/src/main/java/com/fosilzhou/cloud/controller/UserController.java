package com.fosilzhou.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;
import com.fosilzhou.cloud.service.UserService;

@RestController
@RefreshScope  //支持动态刷新功能
public class UserController {

	@Value("${fosilzhou.name}")
	private String name;
	@Autowired
	private UserService userService;
	
	@PostMapping("/user/create")
	public R<User> create(@RequestBody User user) {
		int result = userService.create(user);
		if(result > 0) {
			return new R<User>(200, "操作成功！");
		}else {
			return new R<User>(500, "操作失败！");
		}
	}
	
	@GetMapping("/user/{id}")
	public R<User> getById(@PathVariable("id")Integer id) {
		User user = userService.getById(id);
		if(user != null) {
			return new R<User>(200, "查询成功！", user);
		}else {
			return new R<User>(500, "没有记录！"+ name);
		}
	}
}
