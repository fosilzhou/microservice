#!/bin/sh

UAT_URL="jdbc:mysql://10.10.0.228:3306/ApolloConfigDBUat?characterEncoding=utf8"
UAT_USER="root"
UAT_PWD="123456"
VERSION="1.6.1"

echo "启动用户测试环境配置中心..."

java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${UAT_URL} -Dspring.datasource.username=${UAT_USER} -Dspring.datasource.password=${UAT_PWD} -Dlogging.file=./logs/apollo-configservice-uat.log -Dserver.port=38080 -jar apollo-configservice-${VERSION}.jar &
java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${UAT_URL} -Dspring.datasource.username=${UAT_USER} -Dspring.datasource.password=${UAT_PWD} -Dlogging.file=./logs/apollo-adminservice-uat.log -Dserver.port=38081 -jar apollo-adminservice-${VERSION}.jar &
