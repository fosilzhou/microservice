#!/bin/sh

SERVER_PORT=8080
PORTAL_URL="jdbc:mysql://10.10.0.228:3306/ApolloPortalDB?characterEncoding=utf8"
PORTAL_USER="root"
PORTAL_PWD="123456"
VERSION="1.6.1"
SERVICE_NAME="apollo-portal"
## 开发环境(配置configservice的端口)
DEV_META="http://localhost:18080/"
## 软件测试环境(功能验证环境)
FAT_META="http://localhost:28080/"
## 用户测试环境(预生产环境)
UAT_META="http://localhost:38080/"
## 生产环境
PRO_META="http://localhost:48080/"

if [ "Z${DEV_META}" != "Z" ] && [ "Z${PRO_META}" != "Z" ] && [ "Z${FAT_META}" != "Z" ] && [ "Z${UAT_META}" != "Z" ];then
  echo "dev,fat,uat,pro"
  java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Ddev_meta=${DEV_META} -Dpro_meta=${PRO_META} -Dfat_meta=${FAT_META} -Duat_meta=${UAT_META} -Dserver.port=${SERVER_PORT} -Dspring.datasource.url=${PORTAL_URL} -Dspring.datasource.username=${PORTAL_USER} -Dspring.datasource.password=${PORTAL_PWD} -jar ${SERVICE_NAME}-${VERSION}.jar &
elif [ "Z${DEV_META}" != "Z" ] && [ "Z${PRO_META}" != "Z" ] && [ "Z${FAT_META}" != "Z" ];then
  echo "dev,fat,pro"
  java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Ddev_meta=${DEV_META} -Dpro_meta=${PRO_META} -Dfat_meta=${FAT_META} -Dserver.port=${SERVER_PORT} -Dspring.datasource.url=${PORTAL_URL} -Dspring.datasource.username=${PORTAL_USER} -Dspring.datasource.password=${PORTAL_PWD} -jar ${SERVICE_NAME}-${VERSION}.jar &
elif [ "Z${DEV_META}" != "Z" ] && [ "Z${FAT_META}" != "Z" ];then
  echo "dev,fat"
  java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Ddev_meta=${DEV_META} -Dfat_meta=${FAT_META} -Dserver.port=${SERVER_PORT} -Dspring.datasource.url=${PORTAL_URL} -Dspring.datasource.username=${PORTAL_USER} -Dspring.datasource.password=${PORTAL_PWD} -jar ${SERVICE_NAME}-${VERSION}.jar &
else
  echo "dev"
  java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Ddev_meta=${DEV_META} -Dserver.port=${SERVER_PORT} -Dspring.datasource.url=${PORTAL_URL} -Dspring.datasource.username=${PORTAL_USER} -Dspring.datasource.password=${PORTAL_PWD} -jar ${SERVICE_NAME}-${VERSION}.jar &
fi
