echo

set PORTAL_URL="jdbc:mysql://192.168.135.136:3306/ApolloPortalDB?characterEncoding=utf8"
set DEV_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBDev?characterEncoding=utf8"
set FAT_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBFat?characterEncoding=utf8"
set UAT_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBUat?characterEncoding=utf8"
set PRO_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBPro?characterEncoding=utf8"
set DEV_META="http://localhost:18080/"
set FAT_META="http://localhost:28080/"
set UAT_META="http://localhost:38080/"
set PRO_META="http://localhost:48080/"
set DB_USER="root"
set DB_PWD="123456"
set VERSION="1.6.1"

rem 开发环境
start "ConfigServiceDev" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=18080 -Dspring.datasource.url=%DEV_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-dev.log -jar .\apollo-configservice-%VERSION%.jar
start "AdminServiceDev" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=18081 -Dspring.datasource.url=%DEV_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-dev.log -jar .\apollo-adminservice-%VERSION%.jar

rem 测试环境
start "ConfigServiceFat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=28080 -Dspring.datasource.url=%FAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-fat.log -jar .\apollo-configservice-%VERSION%.jar
start "AdminServiceFat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=28081 -Dspring.datasource.url=%FAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-fat.log -jar .\apollo-adminservice-%VERSION%.jar

rem 仿真环境
rem start "ConfigServiceUat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=38080 -Dspring.datasource.url=%UAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-uat.log -jar .\apollo-configservice-%VERSION%.jar
rem start "AdminServiceUat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=38081 -Dspring.datasource.url=%UAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-uat.log -jar .\apollo-adminservice-%VERSION%.jar

rem 生产环境
rem start "ConfigServicePro" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=48080 -Dspring.datasource.url=%PRO_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-pro.log -jar .\apollo-configservice-%VERSION%.jar
rem start "AdminServicePro" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=48081 -Dspring.datasource.url=%PRO_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-pro.log -jar .\apollo-adminservice-%VERSION%.jar

rem 管理端
start "ApolloPortal" java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Dserver.port=8080 -Ddev_meta=%DEV_META% -Dfat_meta=%FAT_META% -Dspring.datasource.url=%PORTAL_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-portal.log -jar .\apollo-portal-%VERSION%.jar
rem start "ApolloPortal" java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Dserver.port=8080 -Ddev_meta=%DEV_META% -Dfat_meta=%FAT_META% -Duat_meta=%UAT_META% -Dpro_meta=%PRO_META% -Dspring.datasource.url=%PORTAL_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-portal.log -jar .\apollo-portal-%VERSION%.jar
