#!/bin/sh

PRO_URL="jdbc:mysql://10.10.0.228:3306/ApolloConfigDBPro?characterEncoding=utf8"
PRO_USER="root"
PRO_PWD="123456"
VERSION="1.6.1"

echo "启动生产环境配置中心..."

java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${PRO_URL} -Dspring.datasource.username=${PRO_USER} -Dspring.datasource.password=${PRO_PWD} -Dlogging.file=./logs/apollo-configservice-prod.log -Dserver.port=48080 -jar apollo-configservice-${VERSION}.jar &
java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${PRO_URL} -Dspring.datasource.username=${PRO_USER} -Dspring.datasource.password=${PRO_PWD} -Dlogging.file=./logs/apollo-adminservice-prod.log -Dserver.port=48081 -jar apollo-adminservice-${VERSION}.jar &
