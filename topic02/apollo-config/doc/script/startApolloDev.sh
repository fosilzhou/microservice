#!/bin/sh

DEV_URL="jdbc:mysql://10.10.0.228:3306/ApolloConfigDBDev?characterEncoding=utf8"
DEV_USER="root"
DEV_PWD="123456"
VERSION="1.6.1"

echo "启动开发环境配置中心..."

java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${DEV_URL} -Dspring.datasource.username=${DEV_USER} -Dspring.datasource.password=${DEV_PWD} -Dlogging.file=./logs/apollo-configservice-dev.log -Dserver.port=18080 -jar apollo-configservice-${VERSION}.jar &
java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${DEV_URL} -Dspring.datasource.username=${DEV_USER} -Dspring.datasource.password=${DEV_PWD} -Dlogging.file=./logs/apollo-adminservice-dev.log -Dserver.port=18081 -jar apollo-adminservice-${VERSION}.jar &
