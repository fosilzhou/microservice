#!/bin/sh

FAT_URL="jdbc:mysql://10.10.0.228:3306/ApolloConfigDBFat?characterEncoding=utf8"
FAT_USER="root"
FAT_PWD="123456"
VERSION="1.6.1"

echo "启动功能测试环境配置中心..."

java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${FAT_URL} -Dspring.datasource.username=${FAT_USER} -Dspring.datasource.password=${FAT_PWD} -Dlogging.file=./logs/apollo-configservice-fat.log -Dserver.port=28080 -jar apollo-configservice-${VERSION}.jar &
java -Xms256m -Xmx256m -Dapollo_profile=github -Dspring.datasource.url=${FAT_URL} -Dspring.datasource.username=${FAT_USER} -Dspring.datasource.password=${FAT_PWD} -Dlogging.file=./logs/apollo-adminservice-fat.log -Dserver.port=28081 -jar apollo-adminservice-${VERSION}.jar &
