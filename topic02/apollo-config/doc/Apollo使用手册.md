# Apollo安装部署

![](assets/Apollo部署方案.png)

## 部署DEV环境

### 导入脚本

[开发环境数据库脚本：apolloconfigdb_dev.sql](./script/sql/apolloconfigdb_dev.sql)

[portal数据库脚本：apolloportaldb.sql](./script/sql/apolloportaldb.sql)

### 修改Eureka地址

```sql
USE ApolloConfigDBDev;
UPDATE ServerConfig SET `Value`='http://localhost:18080/eureka/' WHERE `Key` = 'eureka.service.url';
```

### 配置启动参数

```shell
echo

set PORTAL_URL="jdbc:mysql://localhost:3306/ApolloPortalDB?characterEncoding=utf8"
set DEV_URL="jdbc:mysql://localhost:3306/ApolloConfigDBDev?characterEncoding=utf8"
set DEV_META="http://localhost:18080/"
set DB_USER="root"
set DB_PWD="123456"
set VERSION="1.6.1"

rem 开发环境
start "ConfigServiceDev" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=18080 -Dspring.datasource.url=%DEV_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-dev.log -jar .\apollo-configservice-%VERSION%.jar
start "AdminServiceDev" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=18081 -Dspring.datasource.url=%DEV_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-dev.log -jar .\apollo-adminservice-%VERSION%.jar

rem 管理端
start "ApolloPortal" java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Dserver.port=8080 -Ddev_meta=%DEV_META% -Dspring.datasource.url=%PORTAL_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-portal.log -jar .\apollo-portal-%VERSION%.jar

```

### 调整系统参数

管理员工具 -> 系统参数

![1596302770963](assets/1596302770963.png)


## 部署FAT环境

### 导入脚本

[测试环境数据库脚本：apolloconfigdb_fat.sql](./script/sql/apolloconfigdb_fat.sql)

### 修改Eureka地址

```sql
USE ApolloConfigDBFat;
UPDATE ServerConfig SET `Value`='http://localhost:28080/eureka/' WHERE `Key` = 'eureka.service.url';

```

### 配置启动参数

```shell

set FAT_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBFat?characterEncoding=utf8"
set FAT_META="http://localhost:28080/"

rem 测试环境
start "ConfigServiceFat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=28080 -Dspring.datasource.url=%FAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-fat.log -jar .\apollo-configservice-%VERSION%.jar
start "AdminServiceFat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=28081 -Dspring.datasource.url=%FAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-fat.log -jar .\apollo-adminservice-%VERSION%.jar

rem 管理端
start "ApolloPortal" java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Dserver.port=8080 -Ddev_meta=%DEV_META% -Dfat_meta=%FAT_META% -Dspring.datasource.url=%PORTAL_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-portal.log -jar .\apollo-portal-%VERSION%.jar

```

### 调整系统参数

管理员工具 -> 系统参数

![1596302770963](assets/1596302770963.png)

## 部署UAT环境

### 导入脚本

[仿真环境数据库脚本：apolloconfigdb_uat.sql](./script/sql/apolloconfigdb_uat.sql)

### 修改Eureka地址

```sql
USE ApolloConfigDBUat;
UPDATE ServerConfig SET `Value`='http://localhost:38080/eureka/' WHERE `Key` = 'eureka.service.url';
```

### 配置启动参数

```shell

set UAT_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBUat?characterEncoding=utf8"
set UAT_META="http://localhost:38080/"

rem 仿真环境
rem start "ConfigServiceUat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=38080 -Dspring.datasource.url=%UAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-uat.log -jar .\apollo-configservice-%VERSION%.jar
rem start "AdminServiceUat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=38081 -Dspring.datasource.url=%UAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-uat.log -jar .\apollo-adminservice-%VERSION%.jar


rem 管理端
start "ApolloPortal" java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Dserver.port=8080 -Ddev_meta=%DEV_META% -Dfat_meta=%FAT_META% -Duat_meta=%UAT_META% -Dspring.datasource.url=%PORTAL_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-portal.log -jar .\apollo-portal-%VERSION%.jar

```

### 调整系统参数

管理员工具 -> 系统参数

![1596302770963](assets/1596302770963.png)

## 部署PRO环境

### 导入脚本

[生产环境数据库脚本：apolloconfigdb_pro.sql](./script/sql/apolloconfigdb_pro.sql)

### 修改Eureka地址

```sql
USE ApolloConfigDBPro;
UPDATE ServerConfig SET `Value`='http://localhost:48080/eureka/' WHERE `Key` = 'eureka.service.url';
```

### 配置启动参数

```shell

set PRO_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBPro?characterEncoding=utf8"
set PRO_META="http://localhost:48080/"

rem 生产环境
rem start "ConfigServicePro" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=48080 -Dspring.datasource.url=%PRO_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-pro.log -jar .\apollo-configservice-%VERSION%.jar
rem start "AdminServicePro" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=48081 -Dspring.datasource.url=%PRO_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-pro.log -jar .\apollo-adminservice-%VERSION%.jar


rem 管理端
start "ApolloPortal" java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Dserver.port=8080 -Ddev_meta=%DEV_META% -Dfat_meta=%FAT_META% -Duat_meta=%UAT_META% -Dpro_meta=%PRO_META%  -Dspring.datasource.url=%PORTAL_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-portal.log -jar .\apollo-portal-%VERSION%.jar

```

### 调整系统参数

管理员工具 -> 系统参数

![1596302770963](assets/1596302770963.png)

#Apollo实现原理

## 模块职责

- ConfigService：提供配置的读取、推送等功能，服务对象是Apollo客户端
- AdminService：提供配置的修改、发布等功能，服务对象是ApolloPortal管理端
- MetaServer：用于封装Eureka的服务发现接口
- Eureka：提供服务注册和发现，为了简单起见，Eureka、ConfigService和MetaServer是在一个JVM进程中部署，ConfigService和AdminService都是多实例、无状态部署，所以需要将自己注册到Eureka中并保持心跳
- Client：通过域名访问MetaServer获取ConfigService服务列表，而后通过IP:PORT访问服务，同时在Client则做负载均衡和错误重试
- Portal：通过域名访问MetaServer获取AdminService服务列表，而后通过IP:PORT访问服务，同时在Portal则做负载均衡和错误重试

## 核心概念

- application(应用)：就是实际使用配置的应用，Apollo客户端在运行是需要知道当前应用，从而可以获取对应的配置。 `关键字：appId`
- environment(环境)：Apollo客户端在运行是需要知道挡圈应用属于哪个环境，从而可以获取对应的配置。 `关键字：env`
- cluster(集群)：一个应用下不同的分组，比如典型分组是按数据中心分组（上海机房、北京机房、广东机房等）。  `关键字：cluster`
- namespace(命名空间)：一个应用下不同配置分组，可以简单的把namespace比作为配置文件，不同类型的配置存放在不同的配置文件中。比如：数据库配置文件、RPC配置文件等 `关键字：namespaces`

![1596294138100](assets/1596294138100.png)



# Apollo操作指南



## 部门管理

管理员工具 -> 系统参数

![1596289234340](assets/1596289234340.png)

## 用户管理

管理员工具 -> 用户管理

![1596289633557](assets/1596289633557.png)

## 项目管理

### 创建项目

![1596289861347](assets/1596289861347.png)

### 删除项目

管理员工具 -> 删除应用、集群、AppNamespace 

![1596292495742](assets/1596292495742.png)

### 修改项目

![1596293273927](assets/1596293273927.png)

## 配置管理

![1596294015284](assets/1596294015284.png)



## Namespace管理

### 创建namespace

![1596297089520](assets/1596297089520.png)

![1596296961267](assets/1596296961267.png)

### 删除namespace

管理员工具 -> 删除应用、集群、AppNamespace

![1596297178220](assets/1596297178220.png)

## 集群管理

### 创建集群

![1596300767305](assets/1596300767305.png)

### 同步配置

将default集群中的mysql配置同步到China集群中

![1596300956730](assets/1596300956730.png)

![1596301158622](assets/1596301158622.png)

![1596301198238](assets/1596301198238.png)

## 环境配置

### 创建数据库

[开发环境数据库脚本：apolloconfigdb_dev.sql](./script/sql/apolloconfigdb_dev.sql)

[测试环境数据库脚本：apolloconfigdb_fat.sql](./script/sql/apolloconfigdb_fat.sql)

[仿真环境数据库脚本：apolloconfigdb_uat.sql](./script/sql/apolloconfigdb_uat.sql)

[生产环境数据库脚本：apolloconfigdb_pro.sql](./script/sql/apolloconfigdb_pro.sql)

### 修改Eureka地址

```sql
USE ApolloConfigDBDev;
UPDATE ServerConfig SET `Value`='http://localhost:18080/eureka/' WHERE `Key` = 'eureka.service.url';

USE ApolloConfigDBFat;
UPDATE ServerConfig SET `Value`='http://localhost:28080/eureka/' WHERE `Key` = 'eureka.service.url';

USE ApolloConfigDBUat;
UPDATE ServerConfig SET `Value`='http://localhost:38080/eureka/' WHERE `Key` = 'eureka.service.url';

USE ApolloConfigDBPro;
UPDATE ServerConfig SET `Value`='http://localhost:48080/eureka/' WHERE `Key` = 'eureka.service.url';
```

### 配置启动参数

```shell
echo

set PORTAL_URL="jdbc:mysql://192.168.135.136:3306/ApolloPortalDB?characterEncoding=utf8"
set DEV_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBDev?characterEncoding=utf8"
set FAT_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBFat?characterEncoding=utf8"
set UAT_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBUat?characterEncoding=utf8"
set PRO_URL="jdbc:mysql://192.168.135.136:3306/ApolloConfigDBPro?characterEncoding=utf8"
set DEV_META="http://localhost:18080/"
set FAT_META="http://localhost:28080/"
set UAT_META="http://localhost:38080/"
set PRO_META="http://localhost:48080/"
set DB_USER="root"
set DB_PWD="123456"
set VERSION="1.6.1"

rem 开发环境
start "ConfigServiceDev" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=18080 -Dspring.datasource.url=%DEV_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-dev.log -jar .\apollo-configservice-%VERSION%.jar
start "AdminServiceDev" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=18081 -Dspring.datasource.url=%DEV_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-dev.log -jar .\apollo-adminservice-%VERSION%.jar

rem 测试环境
start "ConfigServiceFat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=28080 -Dspring.datasource.url=%FAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-fat.log -jar .\apollo-configservice-%VERSION%.jar
start "AdminServiceFat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=28081 -Dspring.datasource.url=%FAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-fat.log -jar .\apollo-adminservice-%VERSION%.jar

rem 仿真环境
rem start "ConfigServiceUat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=38080 -Dspring.datasource.url=%UAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-uat.log -jar .\apollo-configservice-%VERSION%.jar
rem start "AdminServiceUat" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=38081 -Dspring.datasource.url=%UAT_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-uat.log -jar .\apollo-adminservice-%VERSION%.jar

rem 生产环境
rem start "ConfigServicePro" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=48080 -Dspring.datasource.url=%PRO_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-configservice-pro.log -jar .\apollo-configservice-%VERSION%.jar
rem start "AdminServicePro" java -Xms256m -Xmx256m -Dapollo_profile=github -Dserver.port=48081 -Dspring.datasource.url=%PRO_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-adminservice-pro.log -jar .\apollo-adminservice-%VERSION%.jar

rem 管理端
## -Ddev_meta=%DEV_META% 开发环境
## -Dfat_meta=%FAT_META% 测试环境
## -Duat_meta=%UAT_META% 仿真环境
## -Dpro_meta=%PRO_META% 生产环境
start "ApolloPortal" java -Xms256m -Xmx256m -Dapollo_profile=github,auth -Dserver.port=8080 -Ddev_meta=%DEV_META% -Dfat_meta=%FAT_META% -Duat_meta=%UAT_META% -Dpro_meta=%PRO_META% -Dspring.datasource.url=%PORTAL_URL% -Dspring.datasource.username=%DB_USER% -Dspring.datasource.password=%DB_PWD% -Dlogging.file=.\logs\apollo-portal.log -jar .\apollo-portal-%VERSION%.jar
```


### 调整Portal配置

管理员工具 -> 系统参数

![1596302770963](assets/1596302770963.png)

**重启Portal服务**



## 灰度发布