package com.fosilzhou.netty.controller;

import com.fosilzhou.netty.client.NettyClient;
import com.fosilzhou.netty.codec.Invocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private NettyClient nettyClient;

    @PostMapping("/send")
    public String sendMessage(String type, String message) {
        // 创建 Invocation 对象
        Invocation invocation = new Invocation(type, message);
        // 发送消息
        nettyClient.send(invocation);
        return "ok";
    }

}
