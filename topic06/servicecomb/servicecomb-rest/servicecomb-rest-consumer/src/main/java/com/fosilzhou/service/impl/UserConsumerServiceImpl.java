package com.fosilzhou.service.impl;

import com.fosilzhou.api.UserService;
import com.fosilzhou.entity.User;
import org.apache.servicecomb.provider.springmvc.reference.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserConsumerServiceImpl implements UserService {

    private final RestTemplate restTemplate = RestTemplateBuilder.create();

    @Override
    public User getById(Integer id) {
        String provideName = "servicecomb-rest-provider";
        return restTemplate.getForObject("cse://"+provideName+"/user/1", User.class);
    }
}
