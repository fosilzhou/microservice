package com.fosilzhou.service.impl;

import com.fosilzhou.api.UserService;
import com.fosilzhou.entity.User;
import com.fosilzhou.mapper.UserMapper;
import org.apache.servicecomb.provider.rest.common.RestSchema;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;


@RestSchema(schemaId = "servicecomb-rest-provider-user")
@RequestMapping("/user")
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;

	@Override
	@GetMapping("/{id}")
	public User getById(@PathVariable("id") Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}
}
