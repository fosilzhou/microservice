package com.fosilzhou.service.impl;

import com.fosilzhou.api.UserService;
import com.fosilzhou.entity.User;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Component;

@Component
public class UserConsumerServiceImpl implements UserService {

    // 从注册中心中查找
    // microserviceName：来自于服务提供者的${APPLICATION_ID}:${service_description.name}
    // schemaId：来自于服务提供者的RpcSchema.schemaId
    @RpcReference(microserviceName = "start.servicecomb.io:servicecomb-rpc-provider", schemaId = "servicecomb-rpc-provider-user")
    private UserService userService;

    @Override
    public User getById(Integer id) {
        return userService.getById(id);
    }
}
