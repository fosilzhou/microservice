package com.fosilzhou.api;

import com.fosilzhou.entity.User;

public interface UserService {

	User getById(Integer id);
}
