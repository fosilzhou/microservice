package com.fosilzhou.rpc;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ImportResource("classpath:sofarpc.xml")
public class ConsumerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ConsumerApplication.class, args);
	}

	@Component
	public class UserServiceTest implements CommandLineRunner {

		private final Logger logger = LoggerFactory.getLogger(getClass());

		@Autowired
		private UserService userService;

		@Override
		public void run(String... args) throws Exception {
			User user = userService.getById(1);
			logger.info("[run][发起一次 Dubbo RPC 请求，获得用户名为({})", user.getName());
		}

	}
}
