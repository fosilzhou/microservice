package com.fosilzhou.rpc.service.impl;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import com.fosilzhou.rpc.mapper.UserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;

	@Override
	public User getById(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}
	
	
}
