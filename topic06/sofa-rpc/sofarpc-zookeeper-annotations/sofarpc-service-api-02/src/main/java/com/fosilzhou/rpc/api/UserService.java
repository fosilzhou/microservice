package com.fosilzhou.rpc.api;

import com.fosilzhou.rpc.entity.User;

public interface UserService {

	User getById(Integer id);
}
