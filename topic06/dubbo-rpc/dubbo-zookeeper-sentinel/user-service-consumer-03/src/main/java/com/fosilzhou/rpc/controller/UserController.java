package com.fosilzhou.rpc.controller;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Reference(version = "${dubbo.consumer.UserService.version}")
    private UserService userService;

    @RequestMapping("/user")
    public User get(@RequestParam("id") Integer id) {
        return userService.getById(id);
    }
}
