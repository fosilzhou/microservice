package com.fosilzhou.rpc.api;

import com.fosilzhou.rpc.entity.User;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;

public interface UserService {

	int save(User user) throws ConstraintViolationException;

	User getById(@NotNull(message = "用户编号不能为空")Integer id) throws ConstraintViolationException;
}
