package com.fosilzhou.rpc.constant;

/**
 * 异常码定义规范
 * 第一段：1位，表示异常类型
 *  1 - 业务级别异常
 *  2 - 系统级别异常
 * 第二段：3位，系统类型
 *  001 - 用户系统
 *  002 - 商品系统
 *  003 - 订单系统
 *  004 - 支付系统
 *  .....
 * 第三段：3位，业务模块
 *  不限制规则
 *  001 - 认证模块
 *  002 - 用户模块
 * 第四段：3位，错误码
 *  不限制规则，每个模块递增即可
 *
 */
public enum ServiceExceptionEnum {

    SUCCESS(200,"请求成功"),
    SYS_ERROR(500, "系统异常"),
    INVALID_REQUEST_PARAM_ERROR(2001001001, "请求参数不能为空");

    /**
     * 错误码
     */
    private int code;
    /**
     * 错误提示
     */
    private String message;

    ServiceExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
