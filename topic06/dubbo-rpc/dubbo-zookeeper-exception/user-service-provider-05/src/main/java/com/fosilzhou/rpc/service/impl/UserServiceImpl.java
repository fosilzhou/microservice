package com.fosilzhou.rpc.service.impl;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import com.fosilzhou.rpc.mapper.UserMapper;
import org.apache.dubbo.config.annotation.Service;


@Service(version = "${dubbo.provider.UserService.version}")
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;

	@Override
	public int save(User user) throws ConstraintViolationException {
		return userMapper.insert(user);
	}

	@Override
	public User getById(Integer id) throws ConstraintViolationException {
		return userMapper.selectByPrimaryKey(id);
	}
	
	
}
