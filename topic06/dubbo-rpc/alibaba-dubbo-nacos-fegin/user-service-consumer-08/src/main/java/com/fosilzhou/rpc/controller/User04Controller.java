package com.fosilzhou.rpc.controller;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

/**
 * 服务调用方式四：Dubbo （推荐使用）
 *
 */
@RestController
@RequestMapping("/user04")
public class User04Controller {

    @Reference(version = "1.0.0", protocol = "dubbo")
    private UserService userService;

    @GetMapping("/{id}")
    public User get(@PathVariable("id") Integer id) {
        return userService.getById(id);
    }

    @GetMapping("/save")
    public Integer save(@RequestParam("id") Integer id) {
        User user = new User();
        user.setId(id);
        user.setName("心随梦飞"+id);
        return userService.save(user);
    }

}
