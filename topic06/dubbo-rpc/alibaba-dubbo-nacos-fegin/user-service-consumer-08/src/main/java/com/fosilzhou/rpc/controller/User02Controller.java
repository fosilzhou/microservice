package com.fosilzhou.rpc.controller;

import com.fosilzhou.rpc.dto.User;
import com.fosilzhou.rpc.feign.User02FeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 服务调用方式二：Feign + Ribbon （未验证通过？？？）
 *
 * 关联代码：User02FeignClient
 */
@RestController
@RequestMapping("/user02")
public class User02Controller {

    @Autowired
    private User02FeignClient userFeignClient;

    @GetMapping("/{id}")
    public User get(@PathVariable("id") Integer id) {
        return userFeignClient.getById(id);
    }

    @GetMapping("/save")
    public Integer save(@RequestParam("id") Integer id) {
        User user = new User();
        user.setId(id);
        user.setName("心随梦飞"+id);
        return userFeignClient.save(user);
    }

}
