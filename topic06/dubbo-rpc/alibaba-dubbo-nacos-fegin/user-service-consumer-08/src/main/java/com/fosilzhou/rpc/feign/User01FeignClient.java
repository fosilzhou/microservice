package com.fosilzhou.rpc.feign;

import com.alibaba.cloud.dubbo.annotation.DubboTransported;
import com.fosilzhou.rpc.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "user-service-provider-08")
@DubboTransported(protocol = "dubbo")
// @DubboTransported(protocol = "rest")
public interface User01FeignClient {

    @PostMapping("/save")
    Integer save(@RequestBody User user);

    @GetMapping("/{id}")
    User getById(@PathVariable("id")Integer id);
}
