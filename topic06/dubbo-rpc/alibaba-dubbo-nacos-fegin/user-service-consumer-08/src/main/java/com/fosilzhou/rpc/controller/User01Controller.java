package com.fosilzhou.rpc.controller;

import com.fosilzhou.rpc.entity.User;
import com.fosilzhou.rpc.feign.User01FeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 服务调用方式一：Feign + Dubbo
 *
 * 关联代码：User01FeignClient
 */
@RestController
@RequestMapping("/user01")
public class User01Controller {

    @Autowired
    private User01FeignClient userFeignClient;

    @GetMapping("/{id}")
    public User get(@PathVariable("id") Integer id) {
        return userFeignClient.getById(id);
    }

    @GetMapping("/save")
    public Integer save(@RequestParam("id") Integer id) {
        User user = new User();
        user.setId(id);
        user.setName("心随梦飞"+id);
        return userFeignClient.save(user);
    }

}
