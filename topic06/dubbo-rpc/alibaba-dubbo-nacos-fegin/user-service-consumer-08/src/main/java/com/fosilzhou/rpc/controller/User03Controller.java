package com.fosilzhou.rpc.controller;

import com.alibaba.fastjson.JSON;
import com.fosilzhou.rpc.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * 服务调用方式三：RestTemplate + Dubbo
 *
 * 关联代码：RestTemplateConfig
 */
@RestController
@RequestMapping("/user03")
public class User03Controller {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/{id}")
    public User get(@PathVariable("id") Integer id) {
        String url = String.format("http://%s/user/%d", "user-service-provider-08", id);
        return restTemplate.getForObject(url, User.class);
    }

    @GetMapping("/save")
    public Integer save(@RequestParam("id") Integer id) {
        User user = new User();
        user.setId(id);
        user.setName("心随梦飞"+id);
        // 请求头
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // 请求体
        String body = JSON.toJSONString(user);
        // 创建 HttpEntity 对象
        HttpEntity<String> entity = new HttpEntity<>(body, headers);
        // 执行请求
        String url = String.format("http://%s/user/save", "user-service-provider-08");
        return restTemplate.postForObject(url, entity, Integer.class);
    }

}
