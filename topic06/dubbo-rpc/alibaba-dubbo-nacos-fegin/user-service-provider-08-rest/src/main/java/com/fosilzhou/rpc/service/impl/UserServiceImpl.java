package com.fosilzhou.rpc.service.impl;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import com.fosilzhou.rpc.mapper.UserMapper;
import org.apache.dubbo.config.annotation.Service;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@Path("/user")
@Service(protocol = {"dubbo", "rest"}, version = "1.0.0")
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;

	@Override
	@POST
	@Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	public Integer save(User user) {
		System.err.println("REST模式接口服务");
		user.setName(user.getName()+":REST");
		return userMapper.insert(user);
	}

	@Override
	@GET
	@Path("/{id}")
	@Produces(APPLICATION_JSON_VALUE)
	public User getById(@PathParam("id") Integer id) {
		System.err.println("REST模式接口服务");
		return userMapper.selectByPrimaryKey(id);
	}

}
