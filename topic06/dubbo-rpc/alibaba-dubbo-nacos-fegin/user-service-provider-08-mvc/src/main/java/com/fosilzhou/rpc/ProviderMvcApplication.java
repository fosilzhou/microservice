package com.fosilzhou.rpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProviderMvcApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ProviderMvcApplication.class, args);
	}

}
