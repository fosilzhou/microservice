package com.fosilzhou.rpc.service.impl;

import javax.annotation.Resource;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import com.fosilzhou.rpc.mapper.UserMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
@Service(protocol = "dubbo", version = "1.0.0")
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;

	@Override
	@PostMapping("/save")
	public Integer save(@RequestBody User user) {
		System.err.println("SpringMvc模式接口服务");
		user.setName(user.getName()+":MVC");
		return userMapper.insert(user);
	}

	@Override
	@GetMapping("/{id}")
	public User getById(@PathVariable("id") Integer id) {
		System.err.println("SpringMvc模式接口服务");
		return userMapper.selectByPrimaryKey(id);
	}
	
	
}
