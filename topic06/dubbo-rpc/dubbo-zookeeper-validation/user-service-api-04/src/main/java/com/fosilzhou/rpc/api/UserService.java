package com.fosilzhou.rpc.api;

import com.fosilzhou.rpc.entity.User;

public interface UserService {

	int save(User user);

	User getById(Integer id);
}
