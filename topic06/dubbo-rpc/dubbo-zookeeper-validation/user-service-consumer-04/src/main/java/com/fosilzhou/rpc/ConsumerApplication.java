package com.fosilzhou.rpc;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class ConsumerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ConsumerApplication.class, args);
	}

	@Component
	public class UserServiceTest implements CommandLineRunner {

		private final Logger logger = LoggerFactory.getLogger(getClass());

		@Reference(version = "${dubbo.consumer.UserService.version}")
		private UserService userService;

		@Override
		public void run(String... args) throws Exception {
			User user = userService.getById(1);
			logger.info("[run][发起一次 Dubbo RPC 请求，获得用户名为({})", user.getName());
			try {
				userService.save(user);
			}catch (Exception e){
				//Failed to validate service: com.fosilzhou.rpc.api.UserService, method: save, cause: [ConstraintViolationImpl{interpolatedMessage='账号不能为空', propertyPath=username, rootBeanClass=class com.fosilzhou.rpc.entity.User, messageTemplate='账号不能为空'}]
				System.err.println(e.getMessage());
			}
		}

	}
}
