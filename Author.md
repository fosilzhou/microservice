# 作者介绍


## 心随梦飞
Java高级开发工程师，拥有国内外分布式架构高并发项目开发经验，主要从事微服务架构下API设计与开发。对于敏捷实践与持续集成/交付有丰富经验。拥抱开源，善于钻研技术难点。热爱与人沟通，有丰富的培训团队和新人经验。 
