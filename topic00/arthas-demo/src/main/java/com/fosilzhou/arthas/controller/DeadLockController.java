package com.fosilzhou.arthas.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 模拟线程死锁
 */
@RestController
public class DeadLockController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 模拟线程阻塞
     */
    @RequestMapping("/deadLock")
    public void deadThread() {
        /** 创建资源 */
        Object resourceA = new Object();
        Object resourceB = new Object();
        // 创建线程
        Thread threadA = new Thread(() -> {
            synchronized (resourceA) {
                logger.info(Thread.currentThread() + "获得资源A并锁定资源A");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                logger.info(Thread.currentThread() + "等待获取资源B");
                synchronized (resourceB) {
                    logger.info(Thread.currentThread() + "获得资源B");
                }
            }
        });

        Thread threadB = new Thread(() -> {
            synchronized (resourceB) {
                logger.info(Thread.currentThread() + " 获得资源B并锁定资源B");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                logger.info(Thread.currentThread() + "等待获取资源A");
                synchronized (resourceA) {
                    logger.info(Thread.currentThread() + "获得资源A");
                }
            }
        });
        threadA.start();
        threadB.start();
    }
}
