package com.fosilzhou.arthas.controller;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class MemoryController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * -Xms10M -Xmx10M -XX:MaxDirectMemorySize=5M
     */
    @RequestMapping("/gcOverhead")
    public void gcOverhead() {
        int count = 0;
        try {
            List<String> list = new ArrayList<>();
            while (true) {
                count++;
                logger.info("****** gcOverhead ******"+count);
                list.add(String.valueOf("fosilzhou" + count).intern());
            }
        }catch (Throwable e){
            logger.error("****** gcOverhead ******"+count);
            e.printStackTrace();
        }
    }

    /**
     * -Xms10M -Xmx10M
     */
    @RequestMapping("/heapSpace")
    public void heapSpace() {
        int count = 0;
        try {
            while (true) {
                count ++;
                logger.info("****** heapSpace ******"+count);
                byte[] data = new byte[512 * 1024 * 1024];
                TimeUnit.MILLISECONDS.sleep(100);
            }
        }catch (Throwable e){
            logger.error("****** heapSpace ******"+count);
            e.printStackTrace();
        }
    }

    /**
     * -XX:MetaspaceSize=512M -XX:MaxMetaspaceSize=512M
     */
    @RequestMapping("/metaspace")
    public void metaspace() {
        int count = 0;
        try{
            while (true){
                count ++;
                logger.info("****** metaspace ******"+count);
                Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(OOMMetaspace.class);
                enhancer.setUseCache(false);
                enhancer.setCallback(new MethodInterceptor() {
                    @Override
                    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                        return methodProxy.invokeSuper(o, null);
                    }
                });
                enhancer.create();
            }
        }catch (Throwable e){
            logger.error("第【"+count+"】此发生异常");
            e.printStackTrace();
        }
    }

    @RequestMapping("/unableCreateNewThread")
    public void unableCreateNewThread(){
        for (int i = 1; ; i++) {
            logger.info("****** unableCreateNewThread ****** i = " + i);
            new Thread(()->{
                try {
                    TimeUnit.HOURS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();
        }
    }

    @RequestMapping("/stackOverflowError")
    public void stackOverflowError(){
        stackOverflowErrorDemo(1);
    }

    /**
     * -Xms10M -Xmx10M -XX:MaxDirectMemorySize=5M
     */
    @RequestMapping("/directBufferMemory")
    public void directBufferMemory(){
        int count = 0;
        try {
            List<String> list = new ArrayList<>();
            while (true) {
                count++;
                logger.info("****** directBufferMemory ******"+count);
                ByteBuffer byteBuffer = ByteBuffer.allocateDirect(50 * 1024 * 1024);
            }
        }catch (Throwable e){
            logger.error("****** directBufferMemory ******"+count);
            e.printStackTrace();
        }
    }

    private void stackOverflowErrorDemo(int count) {
        // Exception in thread "main" java.lang.StackOverflowError
        logger.info("****** stackOverflowError ****** count="+count);
        count ++;
        stackOverflowErrorDemo(count);
    }
    static class OOMMetaspace{
    }
}
