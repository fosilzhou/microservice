package com.fosilzhou.arthas.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private Logger logger = LoggerFactory.getLogger(getClass());
    
    public void get(Integer uid) {
        check(uid);
        service(uid);
        redis(uid);
        mysql(uid);
    }

    public boolean check(Integer uid) {
        if (uid == null || uid < 10) {
            logger.error("uid不正确，uid:{}", uid);
            throw new RuntimeException("uid不正确");
        }
        return true;
    }

    public void service(Integer uid) {
        int count = 0;
        for (int i = 0; i < 10; i++) {
            count += i;
        }
        logger.info("service  end {}", count);
    }

    public void redis(Integer uid) {
        int count = 0;
        for (int i = 0; i < 10000; i++) {
            count += i;
        }
        logger.info("redis  end {}", count);
    }

    public void mysql(Integer uid) {
        int count = 0;
        for (int i = 0; i < 10000000; i++) {
            count += i;
        }
        logger.info("mysql  end {}", count);
    }
}
