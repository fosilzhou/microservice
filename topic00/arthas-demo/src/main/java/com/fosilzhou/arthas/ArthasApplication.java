package com.fosilzhou.arthas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArthasApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ArthasApplication.class, args);
    }
}
