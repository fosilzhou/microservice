package com.fosilzhou.arthas.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 模拟极高CPU消耗
 */
@RestController
public class CpuHighController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private static ExecutorService executorService = Executors.newFixedThreadPool(1);

    /**
     * 极度消耗CPU的线程
     */
    @RequestMapping("/cpuHigh")
    public void cpuHigh() {
        Thread thread = new Thread(() -> {
            while (true) {
                logger.info("cpu start 100%");
            }
        });
        // 添加到线程
        executorService.submit(thread);
    }


    /**
     * 普通消耗CPU的线程
     */
    @RequestMapping("/cpuNormal")
    public void cpuNormal() {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (true) {
                    logger.info("cpu start");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
