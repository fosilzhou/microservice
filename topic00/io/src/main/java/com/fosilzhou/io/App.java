package com.fosilzhou.io;

public interface App {

    void start();

    void stop();

    void await();
}
