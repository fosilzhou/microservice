package com.fosilzhou.io.bio.file;

import com.fosilzhou.io.FileCopyRunner;

import java.io.*;

/**
 * 使用缓冲区的流的拷贝
 */
public class BufferStreamCopy implements FileCopyRunner {
    @Override
    public void copyFile(File source, File target) {
        InputStream fin = null;
        OutputStream fout = null;
        try {
            fin = new FileInputStream(source);
            fout = new FileOutputStream(target);
            //创建缓冲区
            byte[] buffer = new byte[1024];
            int result;
            while ((result = fin.read(buffer)) != -1) {
                //result这里表示从中读出来的具体字节数
                //虽然缓冲区中能缓存1024，但是我们读取的时候不一定就有这么多字节
                //所以我们使用result做下面的参数
                fout.write(buffer, 0, result);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(fin);
            close(fout);
        }
    }

    private static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
