package com.fosilzhou.io.decorator;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

/**
 * 具体装饰角色
 */
public class ConcreteDecorator02 extends Decorator {

    public ConcreteDecorator02(Component component) {
        super(component);
    }

    @Override
    public String invoke(String json) {
        json = this.prefixInvoke(json);
        json = super.invoke(json);
        json = this.suffixInvoke(json);
        return json;
    }

    /**
     * 请求之前添加附加功能
     * @param json
     * @return
     */
    private String prefixInvoke(String json) {
        JSONArray jsonArray = JSON.parseArray(json);
        jsonArray.add("用户密码校验功能");
        return jsonArray.toJSONString();
    }

    /**
     * 请求之后添加附加功能
     * @param json
     * @return
     */
    private String suffixInvoke(String json) {
        return json;
    }
}
