package com.fosilzhou.io;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbsApp extends Logger implements App {
    protected boolean run = false;
    private final AtomicBoolean state = new AtomicBoolean(false);

    protected abstract void run();

    public final void start() {
        if (run || state.get())
            return;

        run = true;
        new Thread() {
            public void run() {
                try {
                    info("启动");
                    state.compareAndSet(false, true);
                    AbsApp.this.run();
                } finally {
                    state.compareAndSet(true, false);
                    info("已停止");
                }
            }
        }.start();
    }

    protected void subStop() {

    }

    public final void stop() {
        if (!run)
            return;

        if (state.get())
            info("停止开始!");
        run = false;
        subStop();
    }

    public final void await() {
        if (run)
            return;

        while (state.get()) {
            info("等待停止中……");

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
