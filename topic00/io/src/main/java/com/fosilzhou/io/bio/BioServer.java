package com.fosilzhou.io.bio;

import com.fosilzhou.io.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class BioServer extends Server {
    private List<Service> services = new ArrayList<Service>();
    private AtomicInteger nextServiceNameNum = new AtomicInteger(1);

    protected void run(){
        ServerSocket acceptor = null;
        try {
            acceptor = new ServerSocket();
            acceptor.bind(new InetSocketAddress("localhost", 8080));
            acceptor.setSoTimeout(1000);

            while(run){
                info("接收客户端连接中……");
                Socket socket = null;
                try {
                    socket = acceptor.accept();
                } catch (SocketTimeoutException e) {
                    continue ;
                } catch (IOException e) {
                    error(e.getMessage());
                    // e.printStackTrace();
                    socket.close();
                    continue;
                }

                if(!run)
                    break;

                // 注：这里不使用线程池，使用笨重的方法。
                info("接收到一个客户端连接");
                Service service = new Service(socket);
                services.add(service);
                service.start();
            }
        } catch (IOException e) {
            error(e.getMessage());
            // e.printStackTrace();
        } finally {
            if(acceptor == null)
                return ;

            for(Service service : services)
                service.stop();
            for(Service service : services)
                service.await();
            services.clear();

            try {
                acceptor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class Service extends BioSocketApp {
        private final String name;

        public Service(Socket socket) {
            super(socket);
            name = BioServer.this.getName() + "/" + "服务" + nextServiceNameNum.getAndIncrement();
        }

        public final String getName(){
            return name;
        }

        @Override
        public void run() {
            try {
                socket.setSoTimeout(1000);

                while(run) {
                    String request = receive("客户端请求");
                    if(request != null) {
                        if(request.equals("disconnect"))
                            break;

                        if(request.startsWith("error")) {
                            if(send("响应", "请求格式不正确！"))
                                continue;
                            break;
                        }

                        info("接收到客户端请求：" + request);
                        String response = service(request);
                        if(response != null && !send("响应", response))
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                info("与客户端断开连接!");
            }
        }
    }
}

