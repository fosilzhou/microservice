package com.fosilzhou.io.nio;

import com.fosilzhou.io.Session;

public class NioSession extends Session {
    private String contents = null;
    private int id;

    public NioSession() {
    }

    public NioSession(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}

