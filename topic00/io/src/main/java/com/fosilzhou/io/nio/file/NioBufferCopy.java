package com.fosilzhou.io.nio.file;

import com.fosilzhou.io.FileCopyRunner;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 使用带有缓冲区的channel复制 nio
 */
public class NioBufferCopy implements FileCopyRunner {
    @Override
    public void copyFile(File source, File target) {
        FileChannel fin = null;
        FileChannel fout = null;

        try {
            fin = new FileInputStream(source).getChannel();
            fout = new FileOutputStream(target).getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

            while (fin.read(byteBuffer) != -1) {
                byteBuffer.flip();//转变为读模式
                while (byteBuffer.hasRemaining()) {
                    fout.write(byteBuffer);
                }
                byteBuffer.clear();//转变为写模式
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(fin);
            close(fout);
        }
    }

    private static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
