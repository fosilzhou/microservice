package com.fosilzhou.io.nio;

import com.fosilzhou.io.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class NioServer extends Server {
    private Selector acceptorSelector = null;
    private AtomicInteger nextServiceNameNum = new AtomicInteger(1);

    protected void run() {
        ServerSocketChannel acceptor = null;
        Service service = null;
        try {
            try {
                acceptor = ServerSocketChannel.open();
                acceptor.bind(new InetSocketAddress("localhost", 8080));
                acceptor.configureBlocking(false);

                service = new Service();
                service.init();
                service.start();

                acceptorSelector = Selector.open();
                acceptor.register(acceptorSelector, SelectionKey.OP_ACCEPT);
            } catch (IOException e1) {
                e1.printStackTrace();
                info("启动失败!");
                return;
            }

            while (run) {
                info("正在接收客户端连接中……");
                try {
                    if (acceptorSelector.select(1000) > 0) {
                        Iterator<SelectionKey> keys = acceptorSelector.selectedKeys().iterator();
                        while (keys.hasNext()) {
                            SelectionKey key = keys.next();
                            if (key.isAcceptable()) {
                                SocketChannel socket = acceptor.accept();
                                // 这里有可能获取的socket为空
                                // if(socket != null){
                                info("接收到一个客户端连接!");
                                service.register(socket);
                                // }
                            }
                            keys.remove();
                        }
                    }
                } catch (IOException e) {
                    error(e.getMessage());
                    e.printStackTrace();
                }
            }

        } finally {
            if (acceptor != null) {
                if (acceptorSelector != null) {
                    try {
                        acceptorSelector.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                acceptorSelector = null;

                if (service != null) {
                    service.stop();
                    service.await();
                }

                try {
                    acceptor.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            info("已停止!");
        }
    }

    protected void subStop() {
        Selector selector = this.acceptorSelector;
        if (selector != null) {
            selector.wakeup();
        }
    }

    private class Service extends NioSocketApp {
        private Selector selector;
        private Object blockLock = null;
        private int serviceId = 0;

        public String getName() {
            return NioServer.this.getName() + "/" + "服务" + (serviceId > 0 ? serviceId : "");
        }

        public void init() throws IOException {
            if (selector == null)
                selector = Selector.open();
        }

        @Override
        protected void run() {
            try {
                Object blockLock = null;

                while (run) {
                    try {
                        if (selector.select() > 0) {
                            Iterator<SelectionKey> keys = selector.selectedKeys().iterator();
                            while (keys.hasNext()) {
                                SelectionKey key = keys.next();
                                keys.remove();
                                NioSession session = (NioSession) key.attachment();
                                serviceId = session.getId();
                                if (key.isReadable()) {
                                    String request = receive("客户端请求", key);
                                    if (request == null)
                                        continue;
                                    // 不会发生！NIO的读取数据都是就绪，也就是说总会读取到数据。
                                    if (request.equals("disconnect")) {
                                        closeSocket(key);
                                        continue;
                                    }
                                    if (request.startsWith("error")) {
                                        session.setContents("请求格式不正确！");
                                    } else {
                                        String response = service(request);
                                        if (response == null) {
                                            closeSocket(key);
                                            continue;
                                        }
                                        session.setContents(response);
                                    }
                                    key.interestOps(SelectionKey.OP_WRITE);
                                }
                                if (key.isWritable()) {
                                    if (!send("响应", key)) {
                                        closeSocket(key);
                                        continue;
                                    }
                                    key.interestOps(SelectionKey.OP_READ);
                                }
                                serviceId = 0;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (this.blockLock != null) {
                        blockLock = this.blockLock;
                        if (blockLock != null) {
                            synchronized (blockLock) {
                                // 不操作，这里同步，为了让register方法有机会同步块。
                            }
                        }
                    }
                }
            } finally {
                if (selector != null) {
                    for (SelectionKey key : selector.keys()) {
                        NioSession session = (NioSession) key.attachment();
                        serviceId = session.getId();
                        closeSocket(key);
                        serviceId = 0;
                    }

                    try {
                        selector.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    selector = null;
                }
            }
        }

        public void register(SocketChannel socket) {
            try {
                blockLock = socket.blockingLock();
                synchronized (blockLock) {
                    socket.configureBlocking(false);
                    // 这里要唤醒，因为register和selector是互斥锁方法。
                    selector.wakeup();
                    SelectionKey key = socket.register(selector, SelectionKey.OP_READ);
                    key.attach(new NioSession(nextServiceNameNum.getAndIncrement()));
                    blockLock = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        protected void subStop() {
            Selector selector = this.selector;
            if (selector != null) {
                selector.wakeup();
            }
        }
    }
}

