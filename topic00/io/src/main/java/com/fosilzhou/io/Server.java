package com.fosilzhou.io;

public abstract class Server extends AbsApp {
    public String getName(){
        return "服务器：";
    }

    /**
     * 处理请求，并返回需要发送的响应信息
     */
    public abstract String service(String request);
}
