package com.fosilzhou.io;

public interface Client extends App {
    /**
     * 接收响应，并返回需要发送的请求信息
     */
    String running(String response);
}
