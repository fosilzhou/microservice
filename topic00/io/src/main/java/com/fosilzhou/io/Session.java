package com.fosilzhou.io;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class Session {
    public static final int RECEIVE_MAX_LEN = 100;
    private ByteBuffer receiveContents = ByteBuffer.allocate(RECEIVE_MAX_LEN);
    private boolean receiveOutMaxLen = false;

    public final String onReceive(ByteBuffer buffer){
        while(buffer.hasRemaining()){
            byte b = buffer.get();
            if(b == 0){
                if(!receiveOutMaxLen){
                    receiveContents.flip();
                    String result = new String(receiveContents.array(), 0, receiveContents.limit(), Charset.defaultCharset());
                    receiveContents.clear();
                    return result;
                } else {
                    return "error.outLen";
                }
            } else if(receiveOutMaxLen) {
                // 处理接收数据的长度超出限制的情况，这里处理方式为一直接收，无时间限制。
            } else if(receiveContents.hasRemaining()) {
                receiveContents.put(b);
            } else {
                receiveOutMaxLen = true;
                receiveContents.clear();
            }
        }

        return null;
    }
}
