package com.fosilzhou.io.nio.file;

import com.fosilzhou.io.FileCopyRunner;

import java.io.*;
import java.nio.channels.FileChannel;

/**
 * 使用没有缓冲区的channel复制文件
 */
public class NioTransferCopy implements FileCopyRunner {
    @Override
    public void copyFile(File source, File target) {
        FileChannel fin = null;
        FileChannel fout = null;

        try {
            fin = new FileInputStream(source).getChannel();
            fout = new FileOutputStream(target).getChannel();

            long transferred = 0L;
            long size = fin.size();
            while (transferred != size) {
                //如果拷贝的大小没有达到源文件的大小就要一直拷贝
                transferred += fin.transferTo(0, size, fout);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(fin);
            close(fout);
        }
    }

    private static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
