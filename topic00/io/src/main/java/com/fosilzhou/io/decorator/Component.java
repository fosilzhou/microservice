package com.fosilzhou.io.decorator;

/**
 * 抽象构件角色
 */
public interface Component {

    String invoke(String json);
}
