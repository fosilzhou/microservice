package com.fosilzhou.io;

public abstract class Logger {
    public String getName() {
        return toString();
    }

    public final void info(String message) {
        System.out.println(getName() + " -> " + message);
    }

    public final void error(String message) {
        System.err.println(getName() + " -> " + message);
    }
}
