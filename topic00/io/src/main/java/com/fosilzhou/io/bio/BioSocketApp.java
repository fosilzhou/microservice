package com.fosilzhou.io.bio;

import com.fosilzhou.io.AbsApp;
import com.fosilzhou.io.Session;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

public abstract class BioSocketApp extends AbsApp {
    public BioSocketApp(){
        this(null);
    }

    public BioSocketApp(Socket socket) {
        this.socket = socket;
    }

    protected Socket socket = null;
    private ByteBuffer buffer = ByteBuffer.allocate(Session.RECEIVE_MAX_LEN);
    private Session session = new Session();

    protected String receive(String target){
        info("正在接收" + target + "……");
        // 解析 <-> 读取
        while(run){
            // 解析
            buffer.flip();
            try {
                String contents = session.onReceive(buffer);
                if(contents != null)
                    return contents;
            } finally {
                buffer.compact();
            }

            while(run) {
                int count = -1;
                while(run){
                    try {
                        count = socket.getInputStream().read(buffer.array());
                        break;
                    } catch (IOException e) {
                        error(e.getMessage());
                        // e.printStackTrace();
                    }
                }

                if(count > 0 ){
                    buffer.position(buffer.position() + count);
                    break ;
                } if(count == -1){
                    return "disconnect";
                }
            }
        }
        return null;
    }

    protected final boolean send(String target, String response){
        info("发送" + target + "：" + response + "!");
        ByteBuffer buffer = ByteBuffer.wrap((response + (char)0).getBytes());

        info("正在发送" + target + "……");
        try {
            socket.getOutputStream().write(buffer.array(), buffer.position(), buffer.limit());
        } catch (IOException e) {
            info(e.getMessage());
            // e.printStackTrace();
            return run;
        }

        info("发送" + target + "成功!");
        return run;
    }
}

