package com.fosilzhou.io.decorator;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import java.util.HashMap;
import java.util.Map;

/**
 * 具体构件角色
 */
public class ConcreteComponent implements Component {
    @Override
    public String invoke(String json) {
        Map<String, Object> user  = new HashMap<>();
        user.put("name", "胡金红");
        JSONArray jsonArray = JSON.parseArray(json);
        jsonArray.add(user);
        System.out.println(user.get("name"));
        return jsonArray.toJSONString();
    }
}
