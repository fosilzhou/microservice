package com.fosilzhou.io.bio;

import com.fosilzhou.io.Client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public abstract class BioClient extends BioSocketApp implements Client {
    public BioClient() {
    }

    private boolean connected = false;

    public String getName(){
        return "Bio客户端";
    }

    public void run(){
        try {
            // 连接 -> 运行
            while(connect() && running()){
                // 关闭连接后，将重新连接
                closeConnect();
            }
        } finally {
            // 退出运行时，关闭连接
            closeConnect();
        }
    }

    /**
     * 连接，并返回是否继续运行。
     */
    private boolean connect(){
        if(socket != null)
            throw new RuntimeException("socket not closed!");

        info("正在连接服务器中……");
        InetSocketAddress address = new InetSocketAddress("localhost", 8080);
        while(run){
            try {
                socket = new Socket();
                socket.connect(address);
                socket.setSoTimeout(1000);
                connected = true;
                info("连接服务器成功!");
                return true;
            } catch (IOException e) {
                error(e.getMessage());
                // e.printStackTrace();
                closeConnect();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * 关闭连接
     */
    private void closeConnect(){
        if(socket != null){
            if(connected) {
                connected = false;
                info("与服务器断开连接!");
            }

            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }
    }

    /**
     * 运行，并返回是否重新连接。
     */
    private boolean running(){
        String response = null;
        // 循环请求
        while(run){
            // 处理响应，获取请求
            String request = running(response);
            // 退出运行
            if(request == null)
                break ;
            // 清空响应
            response = null;
            // 发送请求
            if(send("请求", request)){
                // 接收响应
                response = receive("服务器响应");
                if(response != null){
                    if(response.equals("disconnect"))
                        break;
                    if(!response.startsWith("error"))
                        info("接收到服务器响应：" + response);
                    else
                        info("接收服务器响应时，发生错误：" + response);
                } else {
                    response = "无回应!";
                }
            }
        }
        return run;
    }
}
