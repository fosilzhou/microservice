package com.fosilzhou.io;

import com.fosilzhou.io.bio.file.BufferStreamCopy;
import com.fosilzhou.io.bio.file.NoBufferStreamCopy;
import com.fosilzhou.io.nio.file.NioBufferCopy;
import com.fosilzhou.io.nio.file.NioTransferCopy;

import java.io.File;

public class FileCopyDemo {

    public static void main(String[] args) {
        File source = new File("D:/AdobePhotoshopCC2018.zip");
        File target = new File("D:/p1.zip");
        File target2 = new File("D:/p2.zip");
        File target3 = new File("D:/p3.zip");
        File target4 = new File("D:/p4.zip");

        new Thread(() -> new NoBufferStreamCopy().copyFile(source,target)).start();
        new Thread(() -> new BufferStreamCopy().copyFile(source,target2)).start();
        new Thread(() -> new NioBufferCopy().copyFile(source,target3)).start();
        new Thread(() -> new NioTransferCopy().copyFile(source,target4)).start();
    }

}



