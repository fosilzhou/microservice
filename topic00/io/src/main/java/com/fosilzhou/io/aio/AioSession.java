package com.fosilzhou.io.aio;

import com.fosilzhou.io.Session;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Future;

public class AioSession extends Session {
    private int id;
    private ByteBuffer buffer = ByteBuffer.allocate(AioSession.RECEIVE_MAX_LEN);
    private AsynchronousSocketChannel socket;
    private Future<Integer> future;
    private boolean read = true;

    public AioSession(AsynchronousSocketChannel socket) {
        this.socket = socket;
    }

    public AioSession(int id, AsynchronousSocketChannel socket) {
        this.id = id;
        this.socket = socket;
    }

    public int getId(){
        return id;
    }

    public ByteBuffer getBuffer(){
        return buffer;
    }

    public AsynchronousSocketChannel getSocket(){
        return socket;
    }

    public Future<Integer> getFuture(){
        return future;
    }

    public void setFuture(Future<Integer> future, boolean read){
        this.future = future;
        this.read = read;
    }

    public boolean isRead() {
        return read;
    }
}

