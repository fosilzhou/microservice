package com.fosilzhou.io.aio;

import com.fosilzhou.io.Client;
import com.fosilzhou.io.Server;

public class AIOTest {
    public static void main(String[] args) throws InterruptedException {
        new AIOTest().test();
    }

    public void test() throws InterruptedException{
        final Server server = new AioServer(){
            @Override
            public String service(String request) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(request == null) // system error?
                    return "？？？";
                if(request.contains("你好"))
                    return "你好!你是？";
                if(request.contains("我是"))
                    return "哈哈！我记得了！";
                return "不想跟你说话";
            }
        };

        final Client client1 = new AioClient(){
            public String getName(){
                return "客户端A";
            }

            @Override
            public String running(String response) {
                if(response == null)
                    return "你好！";
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(response.contains("你是"))
                    return "============我是客户端A！你不记得了吗？===========";
                if(response.contains("我记得"))
                    return "记得就好！上次说好要请我吃饭的，什么时候……";
                return "不想跟你说话！";
            }
        };

        final Client client2 = new AioClient(){
            public String getName(){
                return "客户端B";
            }

            @Override
            public String running(String response) {
                if(response == null)
                    return "你好！";
                try {
                    Thread.sleep(700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(response.contains("你是"))
                    return "==========我是客户端B！你不记得了吗？==========";
                if(response.contains("我记得"))
                    return "出门忘带钱包了，能不能先错我点？";
                return "不想跟你说话！";
            }
        };

        // 模拟App
        server.start();
        client1.start();
        client2.start();

        // 2秒后停止运行
        Thread.sleep(2000);
        client1.stop();
        // 3秒后停止运行
        Thread.sleep(3000);
        client2.stop();
        server.stop();

        client1.await();
        client2.await();
        server.await();
    }

}