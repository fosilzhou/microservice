package com.fosilzhou.io.nio;

import com.fosilzhou.io.Client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public abstract class NioClient extends NioSocketApp implements Client {
    public NioClient() {
    }

    private Selector selector = null;
    private SocketChannel socket = null;
    private boolean connected = false;

    public String getName() {
        return "Nio客户端";
    }

    public void run() {
        try {
            try {
                selector = Selector.open();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            // 连接 -> 运行
            while (connect() && running()) {
                // 关闭连接后，将重新连接
                closeConnect();
            }
        } finally {
            // 退出运行时，关闭连接
            closeConnect();

            if (selector != null) {
                try {
                    selector.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean connect() {
        if (socket != null)
            throw new RuntimeException("connect is not closed");

        info("正在连接服务器中……");
        while (run) {
            try {
                socket = SocketChannel.open();
                socket.configureBlocking(false);
                socket.connect(new InetSocketAddress("localhost", 8080));
                socket.register(selector, SelectionKey.OP_CONNECT);
                return run;
            } catch (IOException e) {
                e.printStackTrace();
                closeConnect();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private void closeConnect() {
        if (socket != null) {
            if (connected) {
                connected = false;
                info("与服务器断开连接!");
            }

            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }
    }

    private boolean running() {
        while (run) {
            try {
                if (selector.select() > 0) {
                    Iterator<SelectionKey> keys = selector.selectedKeys().iterator();
                    while (keys.hasNext()) {
                        SelectionKey key = keys.next();
                        keys.remove();
                        if (key.isConnectable()) {
                            socket.finishConnect();
                            key.attach(new NioSession());
                            key.interestOps(SelectionKey.OP_WRITE);
                            info("连接服务器成功!");
                        }

                        NioSession session = (NioSession) key.attachment();
                        if (key.isWritable()) {
                            String request = running(session.getContents());
                            session.setContents(request);
                            if (!send("响应", key)) {
                                closeSocket(key);
                                continue;
                            }
                            key.interestOps(SelectionKey.OP_READ);
                        }
                        if (key.isReadable()) {
                            String response = receive("服务器响应", key);
                            if (response == null)
                                continue;
                            // 不会发生！NIO的读取数据都是就绪，也就是说总会读取到数据。
                            if (response.equals("disconnect")) {
                                closeSocket(key);
                                continue;
                            }
                            if (response.startsWith("error")) {
                                info("接收服务器响应时，发生错误：" + response);
                            } else
                                info("接收到服务器响应：" + response);
                            key.interestOps(SelectionKey.OP_WRITE);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    protected void subStop() {
        Selector selector = this.selector;
        if (selector != null) {
            selector.wakeup();
        }
    }
}

