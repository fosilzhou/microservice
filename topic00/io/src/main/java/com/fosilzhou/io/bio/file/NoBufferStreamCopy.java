package com.fosilzhou.io.bio.file;

import com.fosilzhou.io.FileCopyRunner;

import java.io.*;

/**
 * 不使用任何缓冲的留的拷贝
 */
public class NoBufferStreamCopy implements FileCopyRunner {
    @Override
    public void copyFile(File source, File target) {
        InputStream fin = null;
        OutputStream fout = null;
        try {
            fin = new FileInputStream(source);
            fout = new FileOutputStream(target);
            int result;
            while ((result = fin.read()) != -1) {
                fout.write(result);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(fin);
            close(fout);
        }
    }

    private static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
