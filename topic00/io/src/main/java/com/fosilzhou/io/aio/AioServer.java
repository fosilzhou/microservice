package com.fosilzhou.io.aio;

import com.fosilzhou.io.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AioServer extends Server {
    private AtomicInteger nextServiceNameNum = new AtomicInteger(1);

    protected void run(){
        Service service = new Service();
        AsynchronousServerSocketChannel acceptor = null;
        try {
            acceptor = AsynchronousServerSocketChannel.open();
            acceptor.bind(new InetSocketAddress("localhost", 8080));

            service = new Service();
            service.start();

            while(run){
                Future<AsynchronousSocketChannel> future = null;
                try {
                    while(run){
                        info("正在接收客户端连接……");
                        if(future == null)
                            future = acceptor.accept();

                        if(future.isDone()){
                            info("接收到一个客户端的连接！");
                            AsynchronousSocketChannel socket = null;
                            try {
                                socket = future.get();
                                service.add(socket);
                            } catch (InterruptedException | ExecutionException e) {
                                e.printStackTrace();
                                socket.close();
                            }
                            future = null;
                            continue ;
                        }

                        if(future.isCancelled()){
                            info("接收客户端连接已中止！");
                            future = null;
                            continue ;
                        }

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } finally {
                    if(future != null && !future.cancel(false)){
                        while(!(future.isDone() || future.isCancelled())){
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if(acceptor != null){
                if(service != null){
                    service.stop();
                    service.await();
                    service.clear();
                }

                try {
                    acceptor.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class Service extends AioSocketApp {
        private BlockingQueue<AsynchronousSocketChannel> sockets = new ArrayBlockingQueue<AsynchronousSocketChannel>(100);
        private Set<AioSession> sessions = new HashSet<AioSession>();
        private int serviceId = 0;

        public String getName(){
            return AioServer.this.getName() + "/" + "服务" + (serviceId > 0 ? serviceId : "");
        }

        @Override
        protected void run() {
            AsynchronousSocketChannel socket = null;
            while(run){
                while(run && !sockets.isEmpty()) {
                    try {
                        socket = sockets.poll(1, TimeUnit.MINUTES);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        continue;
                    }
                    if(socket == null)
                        continue;
                    AioSession session = new AioSession(nextServiceNameNum.getAndIncrement(), socket);
                    sessions.add(session);
                    receive(session);
                }

                if(sessions.size() > 0){
                    Iterator<AioSession> sessions = new ArrayList<AioSession>(this.sessions).iterator();
                    while(run && sessions.hasNext()){
                        AioSession session = sessions.next();
                        serviceId = session.getId();
                        if(session.isRead())
                            receive(session);
                        else
                            send(session);
                        serviceId = 0;
                    }
                }
            };
        }

        private void receive(AioSession session){
            receive(session, false);
        }

        private void receive(AioSession session, boolean reset){
            if(reset)
                session.setFuture(null, true);

            Future<Integer> future = session.getFuture();
            if(future == null || future.isDone()){
                String request = receive("客户端请求", session);
                if(request == null)
                    return ;
                if(request.equals("disconnect")){
                    closeSocket(session);
                    return ;
                }
                String response = null;
                if(request.startsWith("error")) {
                    response = "请求格式不正确！";
                } else {
                    info("接收到客户端请求：" + request);
                    response = service(request);
                    if(response == null){
                        closeSocket(session);
                        return ;
                    }
                }
                if(!send("响应", response, session))
                    closeSocket(session);
            } else if(future.isCancelled()){
                closeSocket(session);
                info("接收客户端请求已中止！");
            }
        }

        private void send(AioSession session){
            Future<Integer> future = session.getFuture();
            if(future.isDone()){
                info("发送响应成功！");
                // 重置future
                receive(session, true);
            } else if(future.isCancelled()){
                closeSocket(session);
                info("发送响应已中止！");
            }
        }

        protected void closeSocket(AioSession session) {
            Future<Integer> future = session.getFuture();
            if(future != null && !future.cancel(false)){
                while(!(future.isDone() || future.isCancelled())){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                session.setFuture(null, true);
            }

            try {
                session.getSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            sessions.remove(session);
            info("与客户端断开连接！");
        }

        public void add(AsynchronousSocketChannel socket) throws InterruptedException {
            sockets.put(socket);
        }

        public void clear(){
            while(!sockets.isEmpty()){
                AsynchronousSocketChannel socket = null;
                try {
                    socket = sockets.poll(1, TimeUnit.MINUTES);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(socket != null){
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(sessions.size() > 0){
                Iterator<AioSession> sessions = new ArrayList<AioSession>(this.sessions).iterator();
                while(sessions.hasNext()){
                    AioSession session = sessions.next();
                    closeSocket(session);
                }
            }
        }
    }
}

