package com.fosilzhou.io.nio;

import com.fosilzhou.io.AbsApp;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public abstract class NioSocketApp extends AbsApp {
    private ByteBuffer buffer = ByteBuffer.allocate(NioSession.RECEIVE_MAX_LEN);

    protected final String receive(String target, SelectionKey key) {
        SocketChannel socket = (SocketChannel) key.channel();
        NioSession session = (NioSession) key.attachment();

        int count = -1;
        try {
            count = socket.read(buffer);
        } catch (IOException e) {
            error(e.getMessage());
            // e.printStackTrace();
        }
        if (count == -1) {
            return "disconnect";
        }

        if (count > 0) {
            buffer.flip();
            try {
                String contents = session.onReceive(buffer);
                if (contents != null) {
                    session.setContents(contents);
                    return contents;
                }
            } finally {
                buffer.compact();
            }
        }
        return null;
    }

    protected final boolean send(String target, SelectionKey key) {
        SocketChannel socket = (SocketChannel) key.channel();
        NioSession session = (NioSession) key.attachment();
        String response = session.getContents();

        info("发送" + target + "：" + response + "!");
        ByteBuffer buffer = ByteBuffer.wrap((response + (char) 0).getBytes());

        info("正在发送" + target + "……");
        try {
            socket.write(buffer);
        } catch (IOException e) {
            info(e.getMessage());
            return run;
        }

        info("发送" + target + "成功!");
        return run;
    }

    protected void closeSocket(SelectionKey key) {
        try {
            key.channel().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        info("与客户端断开连接！");
    }
}

