package com.fosilzhou.io.aio;

import com.fosilzhou.io.AbsApp;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public abstract class AioSocketApp extends AbsApp {

    protected final String receive(String target, AioSession session){
        AsynchronousSocketChannel socket = session.getSocket();
        ByteBuffer buffer = session.getBuffer();

        Future<Integer> future = session.getFuture();
        int count = -1;
        if(future != null){
            try {
                count = future.get();
            } catch (InterruptedException | ExecutionException e) {
                error(e.getMessage());
                // e.printStackTrace();
            }
            if(count == -1){
                return "disconnect";
            }
        }

        if(future == null || count > 0){
            if(count > 0){
                buffer.flip();
                try {
                    String contents = session.onReceive(buffer);
                    if(contents != null)
                        return contents;
                } finally {
                    buffer.compact();
                }
            }
            future = socket.read(buffer);
            session.setFuture(future, true);
        }
        return null;
    }

    protected final boolean send(String target, String contents, AioSession session){
        AsynchronousSocketChannel socket = session.getSocket();

        info("发送" + target + "：" + contents + "!");
        ByteBuffer buffer = ByteBuffer.wrap((contents + (char)0).getBytes());

        info("正在发送" + target + "……");
        session.setFuture(socket.write(buffer), false);

        return run;
    }
}

