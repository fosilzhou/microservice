package com.fosilzhou.io.aio;

import com.fosilzhou.io.Client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Future;

public abstract class AioClient extends AioSocketApp implements Client {
    public AioClient() {
    }

    public String getName(){
        return "Aio客户端";
    }

    AsynchronousSocketChannel socket = null;
    AioSession session = null;

    public void run(){
        try {
            while(connect() && running()){
                closeConnect();
            }
        } finally {
            closeConnect();
        }
    }

    private boolean connect(){
        while(run){
            Future<Void> future = null;
            try {
                try {
                    socket = AsynchronousSocketChannel.open();
                } catch (IOException e) {
                    e.printStackTrace();
                    closeConnect();
                    return false;
                }
                future = socket.connect(new InetSocketAddress("localhost", 8080));
                while(run){
                    if(future.isDone()){
                        session = new AioSession(socket);
                        info("连接服务器成功！");
                        return true;
                    }

                    if(future.isCancelled()){
                        info("连接服务器终止！");
                    }
                }

            } finally {
                if(future != null && !future.cancel(false)){
                    while(!(future.isCancelled() || future.isDone())){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    private void closeConnect(){
        if(session != null){
            Future<Integer> future = session.getFuture();
            if(future != null){
                while(!(future.isCancelled() || future.isDone())){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            session = null;
        }

        if(socket != null){
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket = null;
        }
    }

    private boolean running(){
        String response = null;
        String request = running(response);
        // 循环请求
        while(run){
            // 退出运行
            if(request == null)
                break ;
            // 清空响应
            response = null;
            // 发送请求
            if(send("请求", request, session)){
                Future<Integer> future = session.getFuture();
                while(run){
                    if(future.isDone()){
                        info("发送请求成功！");
                        break;
                    }
                    if(future.isCancelled()){
                        info("发送请求已中止！");
                        return run;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                // 接收响应
                future = null;
                while(run){
                    if(future == null || future.isDone()){
                        response = receive("服务器响应", session);
                        if(response == null) {
                            future = session.getFuture();
                            continue ;
                        }
                        if(response.equals("disconnect"))
                            return run;
                        if(response.startsWith("error")) {
                            request = "请求格式不正确！";
                        } else {
                            info("接收到服务器响应：" + response);
                            request = running(response);
                        }
                        break ;
                    } else if(future.isCancelled()) {
                        info("接收客户端响应已中止！");
                        return true;
                    }
                }
            }
        }
        return run;
    }
}