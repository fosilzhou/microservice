package com.fosilzhou.io;

import java.io.File;

public interface FileCopyRunner{
    void copyFile(File source, File target);
}
