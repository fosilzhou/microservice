package com.fosilzhou.io.decorator;

public class Client {

    public static void main(String[] args) {
        String json = "[]";
        Component component = new ConcreteDecorator02(new ConcreteDecorator01(new ConcreteComponent()));
        System.out.println(component.invoke(json));
    }
}
