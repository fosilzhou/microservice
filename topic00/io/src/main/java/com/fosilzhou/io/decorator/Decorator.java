package com.fosilzhou.io.decorator;

/**
 * 装饰角色
 */
public class Decorator implements Component {

    private Component component;

    public Decorator(Component component){
        this.component = component;
    }

    @Override
    public String invoke(String json) {
        return component.invoke(json);
    }
}
