package com.fosilzhou.nio;

import java.nio.ByteBuffer;

/**
 * sliceBuffer与buffer是共享相同的底层数组
 */
public class NioTest06 {

    public static void main(String[] args) throws Exception {
        ByteBuffer buffer = ByteBuffer.allocate(64);

        for (int i = 0; i < buffer.capacity(); i++) {
            buffer.put((byte) i);
        }
        
        buffer.position(2);
        buffer.limit(6);

        ByteBuffer sliceBuffer = buffer.slice();
        for (int i = 0; i < sliceBuffer.capacity(); i++) {
            byte b = sliceBuffer.get(i);
            b *= 2;
            sliceBuffer.put(i, b);
        }

        buffer.position(0);
        buffer.limit(buffer.capacity());

        while (buffer.hasRemaining()){
            System.out.println(buffer.get());
        }
    }
}
