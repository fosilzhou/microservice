package com.fosilzhou.nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 传统IO转换为NIO 拷贝文件
 */
public class NioTest04 {

    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream("input.txt");
        FileOutputStream fos = new FileOutputStream("output.txt");
        FileChannel inputChannel = fis.getChannel();
        FileChannel outputChannel = fos.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        while (true){
            buffer.clear();// 读文件前清空缓存区
            int read = inputChannel.read(buffer);
            System.out.println("read="+read);
            if(read == -1){
                break;
            }
            buffer.flip();//写文件归零下标
            outputChannel.write(buffer);
        }
        inputChannel.close();
        outputChannel.close();

    }
}
