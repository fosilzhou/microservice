package com.fosilzhou.nio;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

public class NioTest11 {

    public static void main(String[] args) throws Exception {
        int[] ports = new int[5];
        for (int i = 0; i < 5; i++) {
            ports[i] = 5000+i;
        }

        Selector selector = Selector.open();
        for (int port: ports) {
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            ServerSocket serverSocket = serverSocketChannel.socket();
            InetSocketAddress address = new InetSocketAddress(port);
            serverSocket.bind(address);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("监听端口："+ port);
        }
        
        while (selector.select() > 0){
            int number = selector.select();
            System.out.println("连接数："+number);
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            System.out.println("selectionKeys:"+selectionKeys);
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()){
                SelectionKey selectionKey = iterator.next();
                iterator.remove();
                if(selectionKey.isAcceptable()){
                    acceptHandler(selectionKey, selector);
                }else if(selectionKey.isReadable()){
                    readHandler(selectionKey, selector);
                }
            }
        }
    }

    private static void readHandler(SelectionKey selectionKey, Selector selector) throws Exception {
        SocketChannel socketChannel = (SocketChannel)selectionKey.channel();
        int bytesRead = 0;
        while (true){
            ByteBuffer byteBuffer = ByteBuffer.allocate(512);
            byteBuffer.clear();
            int read = socketChannel.read(byteBuffer);
            if(read <= 0){
                break;
            }
            byteBuffer.flip();
            socketChannel.write(byteBuffer);
            bytesRead += read;
        }
        System.out.println("读取："+bytesRead+"，来自于："+socketChannel);
    }

    private static void acceptHandler(SelectionKey selectionKey, Selector selector) throws Exception {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel)selectionKey.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);
        socketChannel.register(selector, SelectionKey.OP_READ);
        System.out.println("获取客户端连接："+socketChannel);
    }
}
