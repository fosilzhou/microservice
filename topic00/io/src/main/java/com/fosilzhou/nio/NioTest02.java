package com.fosilzhou.nio;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 传统IO转换为NIO 读取文件信息
 */
public class NioTest02 {

    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream("NioTest02.txt");
        FileChannel fileChannel = fis.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(512);
        fileChannel.read(byteBuffer);
        byteBuffer.flip();
        while (byteBuffer.remaining() > 0){
            byte b = byteBuffer.get();
            System.out.println((char)b);
        }
        fis.close();
    }
}
