java.io中最为核心的一个概念是流（Stream），面向流编程。Java中一个流要么是输入流，要么是输出流。不可能同时既是输入流也是输出流（InputStream和OutputStream是抽象类，而不是接口）
java.nio中拥有3个核心概念：Selector，Channel和Buffer。在java.nio中，面向块（block）或缓存区（buffer）编程的。
java.nio底层实现：
Buffer:
    Buffer的底层实现是个数组；数据的读，写都是通过Buffer来实现
    Buffer还提供了对数据结构化访问方式，并且可以追踪到系统的读写过程
    java中的8中原生数据类型都有各自对应的Buffer类型
    IntBuffer  -->  int
    LongBuffer  -->  long
    ByteBuffer  -->  byte
    CharBuffer  -->  char
    DoubleBuffer  -->  double
    FloatBuffer  -->  float
    ShortBuffer  -->  short
Channel:
   Channel指的是可以写入数据或从中读取数据的对象，类似于java.io中的Stream
   所有数据的读写都是通过Buffer来进行的，永远不会出现直接向Channel写入数据的情况，或者是直接从Channel读取数据情况
   与Stream不同的是，Channel是双向的，Channel打开后则可以进行读取、写入或读写。
   由于Channel是双向的，因此它能更好的反映出底层操作系统的真实情况；在Linux系统中，底层操作系统的通道就是双向的。