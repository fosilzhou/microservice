package com.fosilzhou.nio;

import java.nio.*;
import java.security.SecureRandom;

public class NioTest01 {

    public static void main(String[] args) {
        IntBuffer buffer = IntBuffer.allocate(10);
        for (int i = 0; i < 5; i++) {
            int randomNumber = new SecureRandom().nextInt(20);
            buffer.put(randomNumber);
        }

        System.out.println("before flip limit："+buffer.limit());
        buffer.flip();
        System.out.println("after flip limit："+buffer.limit());

        while (buffer.hasRemaining()){
            System.out.println("position："+buffer.position());
            System.out.println("limit："+buffer.limit());
            System.out.println("capacity："+buffer.capacity());
            buffer.get();
        }
    }
}
