package com.fosilzhou.nio;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 传统IO转换为NIO 写入文件信息
 */
public class NioTest03 {

    public static void main(String[] args) throws Exception {
        FileOutputStream fos = new FileOutputStream("NioTest03.txt");
        FileChannel fileChannel = fos.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(512);

        byte[] message = "hello fosilzhou.".getBytes();
        for (int i = 0; i < message.length; i++) {
            byteBuffer.put(message[i]);
        }
        byteBuffer.flip();

        fileChannel.write(byteBuffer);
        fos.close();
    }
}
