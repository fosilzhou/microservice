package com.fosilzhou.nio;

import java.nio.ByteBuffer;

/**
 * 适合一些自定义协议的数据格式
 */
public class NioTest05 {

    public static void main(String[] args) throws Exception {
        ByteBuffer buffer = ByteBuffer.allocate(64);

        buffer.putChar('周');
        buffer.putChar('路');
        buffer.putChar('敏');
        buffer.putInt(31);
        buffer.putLong(19880121L);
        buffer.putShort((short) 0);
        buffer.putFloat(17347.25F);

        buffer.flip();

        System.out.println(buffer.getChar()+buffer.getChar()+buffer.getChar());
        System.out.println(buffer.getInt());
        System.out.println(buffer.getLong());
        System.out.println(buffer.getShort());
        System.out.println(buffer.getFloat());
    }
}
