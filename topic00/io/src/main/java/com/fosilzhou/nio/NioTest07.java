package com.fosilzhou.nio;

import java.nio.ByteBuffer;

/**
 * readOnlyBuffer与buffer是共享相同的底层数组
 */
public class NioTest07 {

    public static void main(String[] args) throws Exception {
        ByteBuffer buffer = ByteBuffer.allocate(64);
        System.out.println(buffer.getClass());// 读写Buffer类 java.nio.HeapByteBuffer

        for (int i = 0; i < buffer.capacity(); i++) {
            buffer.put((byte) i);
        }

        ByteBuffer readOnlyBuffer = buffer.asReadOnlyBuffer();
        System.out.println(readOnlyBuffer.getClass());// 只读Buffer类 java.nio.HeapByteBufferR
    }
}
