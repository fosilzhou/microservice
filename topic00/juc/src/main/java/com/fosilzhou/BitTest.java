package com.fosilzhou;

/**
 * 位运算
 */
public class BitTest {

    public static void main(String[] args) {
        rightMove();
    }

    /**
     * 二进制右移，获取移出的值是1还是0
     */
    private static void rightMove() {
        int num = 38;
        System.out.println(Integer.toBinaryString(num));
        for (int i = 0; i < 6; i++) {
            System.out.println("【"+Integer.toBinaryString(num)+"】["+Integer.toBinaryString(num >> i)+"]第"+(i+1)+"位：" + (num >> i & 0x01));
        }
    }
}
