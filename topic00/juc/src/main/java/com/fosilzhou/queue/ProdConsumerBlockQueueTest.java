package com.fosilzhou.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 使用阻塞队列实现生产者消费者模式
 */
public class ProdConsumerBlockQueueTest {
    public static void main(String[] args) throws Exception {
        Resource resource = new Resource(new ArrayBlockingQueue<>(10));
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t生产线程启动");
            try {
                resource.producer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "生产线程").start();

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t消费线程启动");
            try {
                resource.consumer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "消费线程").start();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("时间到,停止活动");
        resource.stop();
    }
}


class Resource {
    /**
     * 默认开启 进行生产消费的交互
     */
    private volatile boolean isStart = true;
    /**
     * 默认值是0
     */
    private AtomicInteger atomicInteger = new AtomicInteger();

    private BlockingQueue<String> blockingQueue = null;

    public Resource(BlockingQueue<String> blockingQueue) {
        this.blockingQueue = blockingQueue;
        System.out.println(blockingQueue.getClass().getName());
    }

    public void producer() throws Exception {
        String data = null;
        boolean returnValue;
        while (isStart) {
            data = atomicInteger.incrementAndGet() + "";
            returnValue = blockingQueue.offer(data, 2L, TimeUnit.SECONDS);
            if (returnValue) {
                System.out.println(Thread.currentThread().getName() + "\t 插入队列数据" + data + "成功");
            } else {
                System.out.println(Thread.currentThread().getName() + "\t 插入队列数据" + data + "失败");
            }
            TimeUnit.SECONDS.sleep(1);
        }
        System.out.println(Thread.currentThread().getName() + "\t 停止 表示 isStart" + isStart);
    }

    public void consumer() throws Exception {
        String result = null;
        while (isStart) {
            result = blockingQueue.poll(2L, TimeUnit.SECONDS);
            if (null == result || "".equals(result)) {
                isStart = false;
                System.out.println(Thread.currentThread().getName() + "\t" + "超过2m没有取到 消费退出");
                System.out.println();
                System.out.println();
                return;
            }
            System.out.println(Thread.currentThread().getName() + "消费队列" + result + "成功");

        }
    }

    public void stop() {
        isStart = false;
    }
}