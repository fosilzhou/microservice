package com.fosilzhou;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 集合不安全的问题
 *
 * 1、故障现象
 *      java.util.ConcurrentModificationException
 * 2、导致原因
 *      并发抢夺修改导致数据不一致异常；
 *      当一个线程正在写入数据时，另外一个线程过来抢夺写入数据，就会导致并发修改异常。
 * 3、解决方案
 *      * new Vector<>();
 *      * Collections.synchronizedList(new ArrayList<>());、Collections.synchronizedSet(new HashSet<>());、Collections.synchronizedMap(new HashMap<>());
 *      * new CopyOnWriteArrayList<>();、new CopyOnWriteArraySet<>();、new ConcurrentHashMap<K,V>();
 *
 * 4、优化建议
 *      写实复制CopyOnWrite
 *
 * 写时复制机制（借鉴读写分离思路实现）
 * CopyOnWrite 容器即写时复制的容器：
 * 往容器添加元素的时候,不直接往当前容器object[]添加，而是先将当前容器object[]进行复制出一个新的object[] newElements
 * 然后向新容器object[] newElements 里面添加元素 添加元素后，再将原容器的引用指向新的容器 setArray(newElements);
 * 这样的好处是可以对copyOnWrite容器进行并发的读，而不需要加锁；因为当前容器不会添加任何容器。所以copyOnwrite容器也是一种读写分离的思想，读和写不同的容器。
 *      public boolean add(E e) {
 *         final ReentrantLock lock = this.lock;
 *         lock.lock();
 *         try {
 *             Object[] elements = getArray();
 *             int len = elements.length;
 *             Object[] newElements = Arrays.copyOf(elements, len + 1);
 *             newElements[len] = e;
 *             setArray(newElements);
 *             return true;
 *         } finally {
 *             lock.unlock();
 *         }
 *     }
 */
public class ContainerNotSafeTest {

    public static void main(String[] args) {
//        Map<Integer, String> map = new HashMap<>();//线程不安全
//        Map<Integer, String> map = Collections.synchronizedMap(new HashMap<>());// 解决方案一
        Map<Integer, String> map = new ConcurrentHashMap<>();// 解决方案二
        for (int i = 0; i < 30; i++) {
            int t = i;
            new Thread(()->{
                map.put(t, "");
                System.out.println(map);
            }).start();
        }
    }

    private static void setNotSafe() {
        // HashSet底层是HashMap<key,new Object()> 因为HashMap中的key是不能重复的
//        Set<Integer> set = new HashSet<>();//线程不安全
//        Set<Integer> set = Collections.synchronizedSet(new HashSet<>());// 解决方案一
        // CopyOnWriteArraySet底层是CopyOnWriteArrayList 添加逻辑：indexOf(e, snapshot, 0, snapshot.length) >= 0 ? false : addIfAbsent(e, snapshot);
        Set<Integer> set = new CopyOnWriteArraySet<>();// 解决方案二
        for (int i = 0; i < 30; i++) {
            int t = i;
            new Thread(()->{
                set.add(t);
                System.out.println(set);
            }).start();
        }
    }

    private static void listNotSafe() {
//        List<Integer> list = new ArrayList<>();//线程不安全
//        List<Integer> list = new Vector<>();// 解决方案一
//        List<Integer> list = Collections.synchronizedList(new ArrayList<>());// 解决方案二
        List<Integer> list = new CopyOnWriteArrayList<>();//解决方案三

        for (int i = 0; i < 30; i++) {
            int t = i;
            new Thread(()->{
                list.add(t);
                System.out.println(list);
            }).start();
        }
    }
}
