package com.fosilzhou.atomic;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * ABA问题演示2
 */
public class AtomicReferenceTest {

//    private final static int A = 1000;
//    private final static int B = 1001;
    private final static int A = 100;
    private final static int B = 101;

    private static AtomicReference<Integer> atomicReference=new AtomicReference<>(A);

    public static void main(String[] args) {

        System.out.println("===演示ABA问题的产生===");
        new Thread(()->{
            System.out.println(atomicReference.compareAndSet(A, B) +"\t"+atomicReference.get());
            System.out.println(atomicReference.compareAndSet(B, A) +"\t"+atomicReference.get());
        },"t1").start();

        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(atomicReference.compareAndSet(A, 2020) +"\t"+atomicReference.get());
        },"t2").start();

        try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

    }
}