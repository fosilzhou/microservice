package com.fosilzhou.atomic;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * ABA问题解决方案
 */
public class AtomicStampedReferenceTest {

    private final static int A = 100;
    private final static int B = 101;

    private static AtomicStampedReference<Integer> stampedReference=new AtomicStampedReference<>(A,1);

    public static void main(String[] args) {
        System.out.println("===演示ABA问题的解决===");

        new Thread(()->{
            int stamp = stampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t 初始化版本号："+stamp+"\t值是："+stampedReference.getReference());
            //暂停1秒钟t3线程
            try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

            stampedReference.compareAndSet(A,B,stampedReference.getStamp(),stampedReference.getStamp() + 1);
            System.out.println(Thread.currentThread().getName()+"\t 第1次修改版本号："+stampedReference.getStamp()+"\t值是："+stampedReference.getReference());
            stampedReference.compareAndSet(B,A,stampedReference.getStamp(),stampedReference.getStamp()+1);
            System.out.println(Thread.currentThread().getName()+"\t 第2次修改版本号："+stampedReference.getStamp()+"\t值是："+stampedReference.getReference());
        },"t3").start();

        new Thread(()->{
            int stamp = stampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t 初始化版本号："+stamp+"\t值是："+stampedReference.getReference());
            //保证线程3完成1次ABA
            try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
            boolean result = stampedReference.compareAndSet(A, 2020, stamp, stamp + 1);
            System.out.println(Thread.currentThread().getName()+"\t 第1次修改版本号："+stampedReference.getStamp()+"\t 是否修改成功："+result+"\t最新的值："+stampedReference.getReference());
        },"t4").start();
    }
}
