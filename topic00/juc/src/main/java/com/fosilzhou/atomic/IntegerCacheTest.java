package com.fosilzhou.atomic;

public class IntegerCacheTest {

    public static void main(String[] args) {
        Integer n1 = 127;
        Integer n2 = 127;
        Integer n3 = 128;
        Integer n4 = 128;
        // 通过new Integer方式赋值不会使用IntegerCache
        Integer n5 = new Integer(127);
        Integer n6 = new Integer(127);

        // 默认配置测试结果
        System.out.println(n1 == n2);// true
        System.out.println(n3 == n4);// fasle
        System.out.println(n4 == n5);// fasle
        System.out.println(n5 == n6);// fasle

        // 从源码中可以看到最小边界是-128，最大边界可以通过-XX:AutoBoxCacheMax进行配置（默认是127）
        // 设置最大边界为：`-XX:AutoBoxCacheMax=512` 测试结果
        Integer n7 = 512;
        Integer n8 = 512;
        Integer n9 = 513;
        Integer n10 = 513;
        System.out.println(n7 == n8);// true
        System.out.println(n9 == n10);// true
    }
}
