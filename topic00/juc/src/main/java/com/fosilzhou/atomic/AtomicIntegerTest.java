package com.fosilzhou.atomic;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * ABA问题演示1
 */
public class AtomicIntegerTest {

    private final static int A = 100;
    private final static int B = 101;
    public static AtomicInteger atomicReference = new AtomicInteger(A);

    public static void main(String[] args) {

        new Thread(()->{
            System.out.println(atomicReference.compareAndSet(A, B) +"\t"+atomicReference.get());
            System.out.println(atomicReference.compareAndSet(B, A) +"\t"+atomicReference.get());
        },"t1").start();

        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(atomicReference.compareAndSet(A, 2020) +"\t"+atomicReference.get());
        },"t2").start();
    }
}
