package com.fosilzhou.locks.demo;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockDemo02 {

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();//加锁
                try {
                    System.out.println(Thread.currentThread().getName()+"开始干活...");
                    TimeUnit.SECONDS.sleep(6);
                    System.out.println(Thread.currentThread().getName()+"干活结束...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    lock.unlock();//解锁
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {

                while(true){
                    if(lock.tryLock()){
                        System.out.println(Thread.currentThread().getName()+"尝试获取锁成功，开始干活...");
                        System.out.println(Thread.currentThread().getName()+"干活结束，释放锁...");
                        lock.unlock();
                        break;
                    }else{
                        System.out.println(Thread.currentThread().getName()+"没有获取到锁，等待片刻继续尝试获取锁...");
                        try {
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        t1.start();
        t2.start();
    }
}
