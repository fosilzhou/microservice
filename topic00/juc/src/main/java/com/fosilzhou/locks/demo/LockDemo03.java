package com.fosilzhou.locks.demo;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockDemo03 {

    public static void main(String[] args) {
        final Lock lock = new ReentrantLock();

        final Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();//加锁
                try {
                    System.out.println(Thread.currentThread().getName()+"开始干活...");
                    TimeUnit.SECONDS.sleep(10);
                    System.out.println(Thread.currentThread().getName()+"干活结束...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    lock.unlock();//解锁
                }
            }
        });


        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    lock.lockInterruptibly();

                    System.out.println(Thread.currentThread().getName()+"获取到锁，可以干活了...");

                    lock.unlock();
                } catch (InterruptedException e) {
//                    e.printStackTrace();
                    System.out.println("interrupt ... ");
                }

            }
        });

        t1.start();
        t2.start();


        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("interrupt");
        t2.interrupt();
    }
}
