package com.fosilzhou.locks.demo;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockDemo01 {

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                lock.lock();//加锁
                try {
                    System.out.println(Thread.currentThread().getName()+"开始干活...");
                    TimeUnit.SECONDS.sleep(6);
                    System.out.println(Thread.currentThread().getName()+"干活结束...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    lock.unlock();//解锁
                }
            }
        });

        t.start();
        System.out.println("线程启动...");
        System.out.println(Thread.currentThread().getName()+"加锁");
        lock.lock();
        System.out.println(Thread.currentThread().getName()+"加锁干活");
        lock.unlock();
        System.out.println(Thread.currentThread().getName()+"解锁");
    }
}
