package com.fosilzhou.pool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

public class TimingThreadPool extends ThreadPoolExecutor {

    private final ThreadLocal<Long> startTime = new ThreadLocal<Long>();
    private final Logger log = Logger.getAnonymousLogger();
    private final AtomicLong numTasks = new AtomicLong();
    private final AtomicLong totalTime = new AtomicLong();

    public TimingThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    /**
     * 在执行任务的线程执行之前调用
     * 可以添加日志、计时、监视或者统计信息收集的功能
     *
     * @param t 将要运行任务的线程
     * @param r 将要执行的任务
     */
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        log.info(String.format("线程 %s: 开始 %s", t, r));
        startTime.set(System.nanoTime());
    }

    /**
     * 在执行任务的线程执行之后调用
     * 无论任务线程是正常返回，还是抛出一个异常而返回，都会被调用
     *
     * 如果任务在完成后抛出Error，那么就不会调用afterExecute
     * 如果beforeExecute抛出一个RuntimeException，那么任务将不被执行，并且afterExecute也不会被调用
     *
     * @param r 执行完成的任务
     * @param t
     */
    protected void afterExecute(Runnable r, Throwable t) {
        try {
            long endTime = System.nanoTime();
            long taskTime = endTime - startTime.get();
            numTasks.incrementAndGet();
            totalTime.addAndGet(taskTime);
            log.info(String.format("线程 %s: 结束，异常信息 %s, 执行耗时=%dns", r, t, taskTime));
        } finally {
            super.afterExecute(r, t);
        }
    }

    /**
     * 在线程池完成关闭时调用
     * 在所有任务都已经完成并且所有工作者线程也已经关闭后，
     * 可以用来释放Executor在其生命周期里分配的各种资源，
     * 还可以执行发送通知、记录日志或者手机finalize统计等操作
     */
    protected void terminated() {
        try {
            log.info(String.format("全部结束: 平均耗时=%dns", totalTime.get() / numTasks.get()));
        } finally {
            super.terminated();
        }
    }
}
