package com.fosilzhou.pool;

import java.util.Random;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TimingThreadPoolDemo {

    public static void main(String[] args) throws InterruptedException {
        ThreadPoolExecutor exec = new TimingThreadPool(0, 10, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
        for (int i = 0; i < 10; i++) {
            int time = i;
            exec.execute(new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"：加入线程池...");
                try {
//                    int time = new Random().nextInt(10);
                    TimeUnit.SECONDS.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }));
            TimeUnit.SECONDS.sleep(1);
        }
        exec.shutdown();
    }
}

