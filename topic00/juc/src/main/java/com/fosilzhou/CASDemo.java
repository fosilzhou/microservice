package com.fosilzhou;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CAS：Compare And Set 比较并交换
 */
public class CASDemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(5);
        System.out.println(atomicInteger.compareAndSet(5, 1988)+"\t current"+atomicInteger.get());
        System.out.println(atomicInteger.compareAndSet(5, 1018)+"\t current"+atomicInteger.get());
    }
}
