package com.fosilzhou;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

class Data{

    //    int number = 0;
    volatile int number = 0;
    volatile AtomicInteger atomicInteger = new AtomicInteger();

    public void add60(){
        this.number = 60;
    }

    public /*synchronized*/ void addPlusPlus(){
        this.number ++;
    }

    public void addAtomic(){
        atomicInteger.getAndIncrement();
    }
}

/**
 *
 * 2、验证volatile不保证原子性
 * 2.1、解决保证原子性方案
 *  方案一：synchronized
 *  方案二：AtomicInteger
 */
public class VolatileDemo {
    public static void main(String[] args) {
        Data data = new Data();
        for (int i = 0; i < 40; i++) {
            new Thread(()->{
                for (int j = 0; j < 1000; j++) {
                    data.addPlusPlus();
                    data.addAtomic();
                }
            },String.valueOf(i)).start();
        }

        // 等待线程都计算完成后，再调用main线程取得最终结果
        while (Thread.activeCount()>2){//默认有两个线程：GC线程和main线程
            Thread.yield();
        }
        // 如果每次打印的结果都是4万时，表示volatile保证原子性，否则不保证原子性
        System.out.println(Thread.currentThread().getName()+"\t 最终值："+data.number);
        System.out.println(Thread.currentThread().getName()+"\t 最终值："+data.atomicInteger);
    }

    /**
     * volatile可以保证可见性，及时通知其他线程，主物理内存的值已经被修改。
     */
    private static void seeOkByVolatile() {
        Data data = new Data();
        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"\t 进入线程...");
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            data.add60();
            System.out.println(Thread.currentThread().getName()+"\t 更新number的值："+data.number);
        }).start();

        while (data.number==0){
            // 直到number的值不等于零时，结束循环
        }
        System.out.println(Thread.currentThread().getName()+"\t 获取到number的值："+data.number);
    }
}
