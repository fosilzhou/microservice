package com.fosilzhou.qrcode;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

//import sun.misc.BASE64Encoder; 

public class QrcodeUtils {

	/**
	 * 二维码宽度
	 */
	private static final int QRCODE_WIDTH = 120;
	/**
	 * 二维码高度
	 */
	private static final int QRCODE_HEIGHT = 120;
	
	/**
	 * LOGO宽度
	 */
	private static final int LOGO_WIDTH = 30;
	/**
	 * LOGO高度
	 */
	private static final int LOGO_HEIGHT = 30;
	
	/**
	 * 解析二维码
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public static String decodeQrcode(File file) throws Exception{
		BufferedImage image = ImageIO.read(file);
		BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
        hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
        Result result = new MultiFormatReader().decode(bitmap, hints);
        System.out.println("二维码解析结果：" + result.toString());
        System.out.println("二维码的格式：" + result.getBarcodeFormat());
        System.out.println("二维码的文本内容：" + result.getText());
		return result.getText();
	}

	/**
	 * 生成二维码并返回图片的Base64字符串
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public static String encodeQrcodeBase64(String content) throws Exception {
		BufferedImage image = encodeQrcode(content);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, "png", out);
		byte[] bytes = out.toByteArray();
		// BASE64Encoder encoder = new BASE64Encoder();
		// return encoder.encodeBuffer(bytes).trim();
		return Base64.encodeBase64String(bytes);
	}
	
	/**
	 * 生成二维码并返回图片的二进制数组
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public static byte[] encodeQrcodeBinary(String content) throws Exception {
		BufferedImage image = encodeQrcode(content);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, "png", out);
		return out.toByteArray();
	}
	
	/**
	 * 生成二维码并返回图片文件对象
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public static File encodeQrcodeFile(String content, String destPath) throws Exception {
		BufferedImage image = encodeQrcode(content);
		File file = new File(destPath);
		ImageIO.write(image, "png", file);
		return file;
	}
	
	/**
	 * 生成二维码并返回输出流
	 * @param content
	 * @param response
	 * @throws Exception
	 */
	public static void encodeQrcodeResponse(String content, HttpServletResponse response) throws Exception {
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/png");
		BufferedImage image = encodeQrcode(content);
		ServletOutputStream output = response.getOutputStream();
		ImageIO.write(image, "png", output);
	}
	
	/**
	 * 生成带LOGO二维码并返回图片的Base64字符串
	 * @param content
	 * @param logoPath
	 * @return
	 * @throws Exception
	 */
	public static String encodeQrcodeBase64(String content, String logoPath) throws Exception {
		BufferedImage image = encodeQrcode(content);
		insertLogo(image, new File(logoPath), true);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, "png", out);
		byte[] bytes = out.toByteArray();
		return Base64.encodeBase64String(bytes);
	}
	
	/**
	 * 生成带LOGO二维码并返回图片的二进制数组
	 * @param content
	 * @param logoPath
	 * @return
	 * @throws Exception
	 */
	public static byte[] encodeQrcodeBinary(String content, String logoPath) throws Exception {
		BufferedImage image = encodeQrcode(content);
		insertLogo(image, new File(logoPath), true);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, "png", out);
		return out.toByteArray();
	}
	
	/**
	 * 生成带LOGO二维码并返回图片文件对象
	 * @param content
	 * @param logoPath
	 * @return
	 * @throws Exception
	 */
	public static File encodeQrcodeFile(String content, String logoPath, String destPath) throws Exception {
		BufferedImage image = encodeQrcode(content);
		insertLogo(image, new File(logoPath), true);
		File file = new File(destPath);
		ImageIO.write(image, "png", file);
		return file;
	}
	
	/**
	 * 生成带LOGO二维码并返回输出流
	 * @param content
	 * @param logoPath
	 * @param response
	 * @throws Exception
	 */
	public static void encodeQrcodeResponse(String content, String logoPath, HttpServletResponse response) throws Exception {
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/png");
		BufferedImage image = encodeQrcode(content);
		insertLogo(image, new File(logoPath), true);
		ServletOutputStream output = response.getOutputStream();
		ImageIO.write(image, "png", output);
	}

	/**
	 * 生成二维码
	 * @param content
	 * @return
	 * @throws Exception
	 */
	private static BufferedImage encodeQrcode(String content) throws Exception {
		Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		hints.put(EncodeHintType.MARGIN, 1);
		BitMatrix bitMatrix = null;
		try {
			bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QRCODE_WIDTH, QRCODE_HEIGHT, hints);
		} catch (WriterException e) {
			e.printStackTrace();
		}
		int width = bitMatrix.getWidth();
		int height = bitMatrix.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
			}
		}

		return image;
	}
	
	/**
	 * 为二维码添加LOGO图标
	 * @param image 二维码对象
	 * @param logoFile LOGO图片文件对象
	 * @param compress 是否压缩LOGO图标
	 * @throws IOException
	 */
	private static void insertLogo(BufferedImage image, File logoFile, boolean compress) throws IOException{
		Image logo = ImageIO.read(logoFile);
		int width = logo.getWidth(null);
		int height = logo.getHeight(null);
		if(compress) {
			width = width > LOGO_WIDTH ? LOGO_HEIGHT : width;
			height = height > LOGO_HEIGHT ? LOGO_HEIGHT : height;
			Image compressLogo = logo.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics g = tag.getGraphics();
			g.drawImage(compressLogo, 0, 0, null);
			g.dispose();
			logo = compressLogo;
		}
		
		Graphics2D graph = image.createGraphics();
		int x = (QRCODE_WIDTH - width) / 2;
		int y = (QRCODE_HEIGHT - height) / 2;
		graph.drawImage(logo, x, y, width, height, null);
		Shape shape = new RoundRectangle2D.Float(x, y, width, height, 6, 6);
		graph.setStroke(new BasicStroke(3f));
		graph.draw(shape);
		graph.dispose();
	}

	public static void main(String[] args) throws Exception {
		System.err.println("data:image/jpg;base64," + encodeQrcodeBase64("https://www.cnblogs.com/jpfss/p/9518327.html", "logo.jpg"));
		System.err.println(encodeQrcodeFile("https://www.cnblogs.com/jpfss/p/9518327.html", "qrcode.png").getAbsolutePath());
		System.err.println(encodeQrcodeFile("https://www.cnblogs.com/jpfss/p/9518327.html", "logo.jpg", "qrcode.png").getAbsolutePath());
		System.err.println(decodeQrcode(new File("qrcode.png")));
	}
	
//	@RequestMapping(path = "/createQRCode",method = RequestMethod.POST)
//    @ResponseBody
//    public void createQRCode(String data, Integer height ,Integer width, String logo, HttpServletResponse response) throws Exception {
//		encodeQrcodeResponse(data, logo, response);
//	}
}
