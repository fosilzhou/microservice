package com.fosilzhou.tree;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class RBTreeTest {

	@Test
	public void test1() {
		RBTree<Integer, String> tree = new RBTree<Integer, String>();
		for(int i = 10; i< 20; i++) {
			tree.add(i, "#"+i);
			//tree.printTreeLevel();
		}
		
		tree.remove(17);
        tree.printTreeLevel();
 
        tree.remove(14);
        tree.printTreeLevel();
	}
	
	@Test
	public void test2() throws Exception {
		RBTree<Integer, String> tree = new RBTree<Integer, String>();
		List<Integer> list = new ArrayList<>();
        //随机插入
        for (int i = 0; i < 20; i++) {
 
            int ran = (int)(Math.random() * 100);
            System.out.println("新插入结点：" + ran);
            tree.add(ran, "" + i);
            tree.printTreeLevel();
 
            list.add(ran);
 
        }
 
        for (Integer integer : list){
            System.out.print(integer.intValue() + " ");
        }
	}
	
	
	@Test
	public void test3() throws Exception {
	        //55 94 0 8 75 5 82 78 43 47 75 33
	        RBTree<Integer, String> tree = new RBTree<Integer, String>();
	 
	        tree.add(55, "#55");
	        tree.add(94, "#94");
	        tree.add(0, "#0");
	        tree.add(8, "#8");
	        tree.add(75, "#75");
	 
	 
	        tree.printTreeLevel();
	 
//	        tree.remove(82);
//	        tree.remove(43);
	 
//	        tree.printTreeLevel();
	 
//	        tree.remove(43);
//	        tree.printTreeLevel();
	 
	        System.out.println(tree.get(55));

	}
	
	@Test
	public void testName() throws Exception {
		RBTree<String, String> tree = new RBTree<String, String>();
		tree.add("zhoulumin", "周路敏");
		tree.add("zhoushuchen", "周书辰");
		tree.add("zhouximin", "周细敏");
		tree.add("hujinhong", "胡金红");
		tree.add("hujinhong", "胡锦洪");
		tree.add("huyuhong", "胡钰洪");
		tree.printTreeLevel();
		System.out.println(tree.get("hujinhong"));
	}

}
