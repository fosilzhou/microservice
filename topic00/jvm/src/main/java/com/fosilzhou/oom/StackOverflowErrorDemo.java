package com.fosilzhou.oom;

/**
 * 递归调用引发栈溢出
 */
public class StackOverflowErrorDemo {

    public static void main(String[] args) {
        stackOverflowError();
    }

    private static void stackOverflowError() {
        // Exception in thread "main" java.lang.StackOverflowError
        stackOverflowError();
    }
}
