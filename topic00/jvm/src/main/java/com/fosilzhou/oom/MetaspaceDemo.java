package com.fosilzhou.oom;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 配置参数
 * -XX:MetaspaceSize=10M -XX:MaxMetaspaceSize=10M
 *
 * Java8及之后版本使用Metaspace来替代永久代（老年代）
 * Metaspace是方法区在HotSpot中的实现，他与永久代最大的区别在于：Metaspace并不在虚拟机内存中，而是使用本地内存
 * 也即在java8中，classes metadata(the virtual machines internal presentation of java class)，被存储在叫Metaspace的native memory
 *
 * 永久代在java8后被元空间Metaspace取代了，存放以下信息：
 * 虚拟机加载的类信息
 * 常量池
 * 静态变量
 * 即时编译后的代码
 *
 * 模拟Metaspace空间溢出，让程序不断生成类往元空间中灌，类占据的空间总会超过Metaspace指定的内存大小
 */
public class MetaspaceDemo {
    static class OOMMetaspace{
    }
    public static void main(String[] args) {
        int count = 0;
        try{
            while (true){
                count ++;
                Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(OOMMetaspace.class);
                enhancer.setUseCache(false);
                enhancer.setCallback(new MethodInterceptor() {
                    @Override
                    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                        return methodProxy.invokeSuper(o, args);
                    }
                });
                enhancer.create();
            }
        }catch (Throwable e){
            System.out.println("第【"+count+"】此发生异常");
            e.printStackTrace();
        }
    }
}
