package com.fosilzhou.oom;

import java.util.ArrayList;
import java.util.List;

/**
 * -Xms10M -Xmx10M -XX:MaxDirectMemorySize=5M
 *
 * GC回收时间过长时会抛出OutOfMemoryError：过长时间的定义是超过98%的时间用来GC并且GC回收不到2%的堆内存
 * 连续多次GC都只回收不到2%的极端情况下才会抛出OutOfMemoryError
 * 假如不抛出OutOfMemoryError错误就会导致GC清理的少量内存很快就会被应用程序再次填满，迫使GC再次执行。
 * 这样就会形成恶性循环，CPU使用率飙升直到100%，而GC却没有任何效果
 */
public class GCOverheadDemo {

    public static void main(String[] args) {
        // java.lang.OutOfMemoryError: GC overhead limit exceeded
        int count = 0;
        try {
            List<String> list = new ArrayList<>();
            while (true) {
                count++;
                list.add(String.valueOf("fosilzhou" + count).intern());
            }
        }catch (Throwable e){
            System.out.println("***********"+count);
            e.printStackTrace();
        }
    }
}
