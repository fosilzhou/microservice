package com.fosilzhou.oom;

/**
 * 堆内存不够（-Xms10M -Xmx10M）
 */
public class JavaHeapSpaceDemo {

    public static void main(String[] args) {
        // 申明15M的大对象
        // Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
        byte[] data = new byte[15 * 1024 * 1024];
    }
}
