package com.fosilzhou.oom;

import java.nio.ByteBuffer;

/**
 * 配置参数：
 *  -Xms10M -Xmx10M -XX:MaxDirectMemorySize=5M
 * 故障现象：
 *  Exception in thread "main" java.lang.OutOfMemoryError: Direct buffer memory
 * 导致原因：
 *  写NIO程序经常使用ByteBuffer来读取或写入数据，这是一种基于通道（Channel）与缓冲区（Buffer）的I/O方式，
 * 可以使用Native函数库直接分配堆外内存，然后通过一个储存在Java堆里面的DirectByteBuffer对象作为这块内存的引用进行操作。
 * 这样能在一些场景中显著提供性能，因为避免了在Java堆和Native堆中来回复制数据。
 *
 * ByteBuffer.allocate(capacity);这种方式是分配JVM 堆内存，属于GC管理管辖范围，由于需要拷贝所以速度相对较慢
 * ByteBuffer.allocateDirect(capacity); 这种方式是分配OS 本地内存，不属于GC管理管辖范围，由于不需要内存拷贝，所以速度相对较快
 * 但是如果不断分配本地内存，堆内存很少使用，那么JVM 就不会执行GC，DirectByteBuffer对象就不会被回收，
 * 这时候堆内存充足，但是本地内存可能已经使用完了，再次尝试分配本地内存就会出现异常，那程序就直接崩溃了
 */
public class DirectBufferMemoryDemo {

    public static void main(String[] args) {
        System.out.println("配置maxDirectMemory："+(sun.misc.VM.maxDirectMemory()/(double)1024/1024) +"MB");
        // 设置本地内存大小为5M，实际使用6M
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(6 * 1024 * 1024);
    }
}
