package com.fosilzhou.oom;

import java.util.concurrent.TimeUnit;

/**
 * 高并发请求服务器时，经常会出现如下异常：java.lang.OutOfMemoryError: unable to create new native thread
 * 准确的将该native thread异常与对应的平台有关
 *
 * 导致原因：
 * 1、你的应用创建了太多线程，一个应用进程创建多个线程，导致超过系统承载极限
 * 2、你的服务器并不允许你的应用程序创建这么多线程，Linux系统默认允许单个进程可以创建的线程数是1024个
 * 当应用创建超过限制线程数就会报java.lang.OutOfMemeoryError: unable to create new native thread
 *
 * 解决办法：
 * 1、想办法降低你应用创建的线程数，分析应用是否真的需要创建爱你这么多线程，如果不是，调整代码将线程数降到最低
 * 2、对有的应用，确实需要创建很多线程，远超过Linux系统的默认限制的线程数，可以通过修改Linux服务器配置，来扩大Linux默认配置
 *
 *
 * 演示操作：
 * （使用fosilzhou用户执行以下命令）
 * javac -d . UnableCreateNewThreadDemo.java
 * java com.fosilzhou.oom.UnableCreateNewThreadDemo
 *
 * 使用root用户调整系统线程数配置
 * ulimit -u
 * vi /etc/security/limits.d/20-nproc.conf
 * *          soft    nproc     4096      <=== 其他用户创建线程数限制为4096
 * root       soft    nproc     unlimited <=== root用户创建线程数不限制
 * fosilzhou  soft    nproc     1024  <==单独配置fosilzhou用户创建线程数限制
 *
 */
public class UnableCreateNewThreadDemo {
    public static void main(String[] args) {
        for (int i = 1; ; i++) {
            System.out.println("****************** i = " + i);
            new Thread(()->{
                try {
                    TimeUnit.HOURS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();
        }
    }
}
