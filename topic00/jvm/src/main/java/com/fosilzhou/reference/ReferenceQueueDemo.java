package com.fosilzhou.reference;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * 引用队列
 * 软引用、弱引用、虚引用在GC回收之前都会放在引用队列里面
 */
public class ReferenceQueueDemo {

    public static void main(String[] args) {
        Object obj = new Object();
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
        WeakReference<Object> weakReference = new WeakReference<>(obj, referenceQueue);
        System.out.println("--------------触发GC之前---------------");
        System.out.println(obj);
        System.out.println(weakReference.get());
        System.out.println(referenceQueue.poll());
        System.out.println("--------------触发GC之后---------------");
        obj = null;
        System.gc();
        System.out.println(obj);
        System.out.println(weakReference.get());
        // 软引用、弱引用、虚引用在GC回收之前都会放在引用队列里面暂时保存下
        System.out.println(referenceQueue.poll());
    }
}
