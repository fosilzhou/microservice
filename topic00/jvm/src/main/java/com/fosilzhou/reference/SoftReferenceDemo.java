package com.fosilzhou.reference;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

/**
 * 软引用（内存充足时不回收，内存不足时回收）
 *
 */
public class SoftReferenceDemo {

    /**
     * -Xms5M -Xmx5M -XX:+PrintGCDetails
     * @param args
     */
    public static void main(String[] args) {
        Object obj = new Object();
        SoftReference<Object> weakReference = new SoftReference<>(obj);
        System.err.println("--------------内存充足时GC之前---------------");
        System.out.println(obj);
        System.out.println(weakReference.get());
        System.err.println("--------------内存充足时GC之后---------------");
        obj = null;
        System.gc();
        System.out.println(obj);
        System.out.println(weakReference.get());
        System.err.println("--------------内存不足时GC之后---------------");
        try{
            byte[] data = new byte[6 * 1024 * 1024];
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            System.out.println(obj);
            System.out.println(weakReference.get());
        }

    }
}
