package com.fosilzhou.reference;

import java.util.HashMap;
import java.util.WeakHashMap;

/**
 */
public class WeakHashMapDemo {

    public static void main(String[] args) {

        System.out.println("--------------HashMap--------------");
        hashmap();
        System.out.println("--------------WeakHashMap--------------");
        weakhashmap();

    }

    private static void weakhashmap(){
        WeakHashMap<Integer, String> map = new WeakHashMap<>();
        Integer key = new Integer(1988);
        String value = new String("fosilzhou");
        map.put(key, value);
        System.out.println(map);

        key = null;
        value = null;
        System.out.println(map);

        System.gc();
        System.out.println(map);
    }

    private static void hashmap(){
        HashMap<Integer, String> map = new HashMap<>();
        Integer key = new Integer(1988);
        String value = new String("fosilzhou");
        map.put(key, value);
        System.out.println(map);

        key = null;
        value = null;
        System.out.println(map);

        System.gc();
        System.out.println(map);
    }
}
