package com.fosilzhou.reference;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

/**
 *
 */
public class PhantomReferenceDemo {

    public static void main(String[] args) {
        Object obj = new Object();
        ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
        PhantomReference<Object> phantomReference = new PhantomReference<>(obj, referenceQueue);
        System.out.println("--------------触发GC之前---------------");
        System.out.println(obj);
        System.out.println(phantomReference.get());
        System.out.println(referenceQueue.poll());
        System.out.println("--------------触发GC之后---------------");
        obj = null;
        System.gc();
        System.out.println(obj);
        System.out.println(phantomReference.get());
        // 软引用、弱引用、虚引用在GC回收之前都会放在引用队列里面
        System.out.println(referenceQueue.poll());
    }
}
