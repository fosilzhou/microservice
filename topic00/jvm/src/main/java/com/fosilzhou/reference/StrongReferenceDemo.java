package com.fosilzhou.reference;

import java.lang.ref.WeakReference;

/**
 * 强引用（引发OOM都不会垃圾回收）
 */
public class StrongReferenceDemo {

    public static void main(String[] args) {
        Object obj = new Object();
        WeakReference<Object> weakReference = new WeakReference<>(obj);
        System.out.println("--------------GC之前---------------");
        System.out.println(obj);
        System.out.println(weakReference.get());
        System.out.println("--------------GC之后---------------");
        obj = null;
        System.gc();
        System.out.println(obj);
        System.out.println(weakReference.get());
    }
}
