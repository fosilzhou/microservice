package com.fosilzhou.reference;

import java.lang.ref.WeakReference;

/**
 * 弱引用（只要触发GC就回收）
 *
 * MyBatis的缓存中大量使用了软引用和软引用
 *
 * 高速缓存使用软引用或弱引用实现
 * 使用软引用或弱引用实现本地图片缓存（Android图片加载案例）
 * Map<String, SoftReference<Bitmap>> imageCacheMap = new HashMap<String, SoftReference<Bitmap>>();
 * Map<String, WeakReference<Bitmap>> imageCacheMap = new HashMap<String, WeakReference<Bitmap>>();
 */
public class WeakReferenceDemo {

    public static void main(String[] args) {
        Object obj = new Object();
        WeakReference<Object> weakReference = new WeakReference<>(obj);
        System.out.println("--------------触发GC之前---------------");
        System.out.println(obj);
        System.out.println(weakReference.get());
        System.out.println("--------------触发GC之后---------------");
        obj = null;
        System.gc();
        System.out.println(obj);
        System.out.println(weakReference.get());
    }
}
