package com.fosilzhou.gc;

import java.util.Random;
import java.util.UUID;

/**
 * GC 组合配置方式：
 * 配置新生代GC会自动配置老年代GC；同理配置老年代GC也会自动配置新生代GC
 *
 * 组合一：
 * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseSerialGC  （DefNew + Tenured）
 *
 * 组合二：
 * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseParNewGC  （ParNew + Tenured）
 *
 * 警告：Java HotSpot(TM) 64-Bit Server VM warning: Using the ParNew young collector with the Serial old collector is deprecated and will likely be removed in a future release
 *
 * 组合三：
 * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseParallelGC    （PSYoungGen + ParOldGen）
 *
 * 组合四：
 * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseParallelOldGC     （PSYoungGen + ParOldGen）
 *
 * 组合五：
 * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseConcMarkSweepGC   （par new generation + concurrent mark-sweep generation）
 *
 * [GC (CMS Initial Mark) [1 CMS-initial-mark: 5426K(6848K)] 标记GC Root可以直达的对象
 * [CMS-concurrent-mark-start] 从第一步标记对象出发，并发第标记可达对象
 * [GC (CMS Final Remark)  重新进行标记，修正concurrent-mark期间由于用户程序运行而导致对象关系间的变化及新创建对象
 * [CMS-concurrent-sweep-start] 并行的进行无用对象回收
 *
 * 组合六：
 * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseG1GC
 *
 *
 * 同时配置新生代GC和老年代GC
 * -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseParNewGC -XX:+UseConcMarkSweepGC
 *
 *
 */
public class GCDemo {

    public static void main(String[] args) {
        try {
            String str = "fosilzhou";
            while (true) {
                str += str + new Random().nextInt(999999999)+new Random().nextInt(888888888)+new Random().nextInt(777777777);
                str.intern();
            }
        }catch (Throwable e){
            e.printStackTrace();
        }
    }
}
