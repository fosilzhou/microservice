package com.fosilzhou.cloud.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Component
public class AuthZuulFilter extends ZuulFilter {

    /**
     * 外部请求 Header - token 认证令牌
     */
    private static final String DEFAULT_TOKEN_HEADER_NAME = "token";
    /**
     * 转发请求 Header - userId 用户编号
     */
    private static final String DEFAULT_HEADER_NAME = "user-id";

    /**
     * token 和 userId 的映射
     */
    private static Map<String, Integer> TOKENS = new HashMap<String, Integer>();

    static {
        TOKENS.put("fosilzhou", 1);
    }

    public String filterType() {
        // FilterConstants.PRE_TYPE --> 前置类型：在route之前调用；使用场景：身份认证、记录请求日志、服务容错、服务限流...
        // FilterConstants.ROUTE_TYPE --> 路由类型：路由请求到后端 URL 或服务实例上；使用场景：使用 Apache HttpClient 请求后端 URL、或使用 Netflix Ribbon 请求服务实例...
        // FilterConstants.POST_TYPE --> 后置类型：1. 发生异常时，在 error 之后调用、2. 正常执行时，最后调用；使用场景：收集监控信息、响应结果给客户端...
        // FilterConstants.ERROR_TYPE --> 错误类型：处理请求时发生的错误时被调用。
        return FilterConstants.PRE_TYPE; // 前置过滤器
    }

    public int filterOrder() {
        return 0;
    }

    public boolean shouldFilter() {
        return true; // 需要过滤
    }

    public Object run() throws ZuulException {
        // 获取当前请求上下文
        RequestContext ctx = RequestContext.getCurrentContext();
        // 获得 token
        HttpServletRequest request = ctx.getRequest();
        String token = request.getHeader(DEFAULT_TOKEN_HEADER_NAME);

        // 如果没有 token，则不进行认证。因为可能是无需认证的 API 接口
        if (!StringUtils.hasText(token)) {
            return null;
        }

        // 进行认证
        Integer userId = TOKENS.get(token);

        // 通过 token 获取不到 userId，说明认证不通过
        if (userId == null) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value()); // 响应 401 状态码
            return null;
        }

        // 认证通过，将 userId 添加到 Header 中
        ctx.getZuulRequestHeaders().put(DEFAULT_HEADER_NAME, String.valueOf(userId));
        return null;
    }

}