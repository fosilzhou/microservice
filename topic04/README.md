# 消息队列专栏

## Spring Boot

|                  | RocketMQ | RabbitMQ | Kafka |
| ---------------- | -------- | -------- | ----- |
| 普通消息         |          | √ |       |
| 批量发送消息         |          | √ |       |
| 批量消费消息         |          | √ |       |
| 定时消息         |          | √ |       |
| 消费重试         |          | √ |       |
| 广播消息         |          | √ |       |
| 集群消息         |          | √ |       |
| 并发消息         |          | √ |       |
| 顺序消息         |          | √ |       |
| 事务消息         |          | √ |       |
| 确认消息(消费者) |          | √ |       |
| 确认消息(生产者) |          | √ |       |
| RPC调用          |          | √ |       |
| 消费异常处理器           |          | √ |       |
| 消息转换器 |          | √ |       |

## Spring Cloud

### Spring Cloud Stream

|                  | RocketMQ                                                     | RabbitMQ                                                     | Kafka                                                        |
| ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 普通消息         | √ [消费者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-consumer-demo) & [生产者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-producer-demo) | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-demo) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-demo) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-demo) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-demo) |
| 延迟消息         | √ [消费者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-consumer-delay) & [生产者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-producer-delay) | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-delay) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-delay) | ×                                                            |
| 消费重试         | √ [消费者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-consumer-retry) & [生产者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-producer-retry) | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-retry) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-retry) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-retry) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-retry) |
| 广播消息         | √ [消费者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-consumer-broadcast) & [生产者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-producer-broadcast) | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-broadcast) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-broadcast) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-broadcast) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-broadcast) |
| 并发消息         | ×                                                            | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-concurrency) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-concurrency) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-concurrency) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-concurrency) |
| 顺序消息         | √ [消费者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-consumer-orderly) & [生产者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-producer-orderly) | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-orderly) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-orderly) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-orderly) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-orderly) |
| 过滤消息         | √ [消费者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-consumer-filter) & [生产者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-producer-filter) | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-filter) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-filter) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-filter) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-filter) |
| 事务消息         | √ [消费者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-consumer-transaction) & [生产者](rocketmq/spring-cloud-stream-rocketmq/stream-rocketmq-producer-transaction) | ? [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-transaction) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-transaction) | ? [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-transaction) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-transaction) |
| 确认消息(消费者) | ×                                                            | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-ack) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-ack) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-ack) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-ack) |
| 确认消息(生产者) | ×                                                            | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-confirm) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-confirm) | ×                                                            |
| 批量消息         | ×                                                            | √ [消费者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-consumer-batch) & [生产者](rabbitmq/spring-cloud-stream-rabbitmq/stream-rabbitmq-producer-batch) | √ [消费者](kafka/spring-cloud-stream-kafka/stream-kafka-consumer-batch) & [生产者](kafka/spring-cloud-stream-kafka/stream-kafka-producer-batch) |

√：表示MQ本身支持的功能

×：表示MQ本身不支持并且Stream也不支持的功能

?：表示不支持事务消息回查机制

### Spring Cloud Bus




