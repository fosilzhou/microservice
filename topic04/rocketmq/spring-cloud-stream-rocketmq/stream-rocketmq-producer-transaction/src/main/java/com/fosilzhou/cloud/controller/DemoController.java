package com.fosilzhou.cloud.controller;

import com.alibaba.fastjson.JSON;
import com.fosilzhou.cloud.message.DemoMessage;
import com.fosilzhou.cloud.source.DemoSource;
import org.apache.rocketmq.common.message.MessageConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/transaction")
public class DemoController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DemoSource demoSource;

    /**
     * 发送普通消息
     * @return
     */
    @GetMapping("/send")
    public boolean sendTransaction() {
        // 创建 Message
        DemoMessage message = new DemoMessage();
        message.setId(new Random().nextInt());
        Args args = new Args();
        args.setArg1(1);
        args.setArg2("fosilzhou");
        // 创建 Spring Message 对象
        Message<DemoMessage> springMessage = MessageBuilder.withPayload(message)
                .setHeader("args", JSON.toJSONString(args))
                .build();
        // 发送消息
        boolean sendResult = demoSource.output().send(springMessage);
        logger.info("[sendTransaction][发送消息完成, 结果 = {}]", sendResult);
        return sendResult;
    }

    public static class Args{
        private Integer arg1;
        private String arg2;

        public Integer getArg1() {
            return arg1;
        }

        public void setArg1(Integer arg1) {
            this.arg1 = arg1;
        }

        public String getArg2() {
            return arg2;
        }

        public void setArg2(String arg2) {
            this.arg2 = arg2;
        }
    }
}
