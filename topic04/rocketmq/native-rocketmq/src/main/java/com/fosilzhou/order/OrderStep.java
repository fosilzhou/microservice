package com.fosilzhou.order;

import java.util.ArrayList;
import java.util.List;

public class OrderStep {

    private int orderId;
    private String desc;

    public static List<OrderStep> build(){
        List<OrderStep> list = new ArrayList<>();
        list.add(new OrderStep(1001, "订单创建"));
        list.add(new OrderStep(1002, "订单创建"));
        list.add(new OrderStep(1003, "订单创建"));
        list.add(new OrderStep(1001, "订单付款"));
        list.add(new OrderStep(1002, "订单付款"));
        list.add(new OrderStep(1003, "订单付款"));
        list.add(new OrderStep(1001, "订单推送"));
        list.add(new OrderStep(1002, "订单推送"));
        list.add(new OrderStep(1003, "订单推送"));
        list.add(new OrderStep(1001, "订单完成"));
        list.add(new OrderStep(1002, "订单完成"));
        list.add(new OrderStep(1003, "订单完成"));
        return list;
    }

    public OrderStep() {
    }

    public OrderStep(int orderId, String desc) {
        this.orderId = orderId;
        this.desc = desc;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "订单流程{" +
                "订单号=" + orderId +
                ", 订单流程描述='" + desc + '\'' +
                '}';
    }
}
