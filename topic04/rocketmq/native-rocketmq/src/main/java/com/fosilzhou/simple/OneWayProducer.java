package com.fosilzhou.simple;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.concurrent.TimeUnit;

/**
 * 发送同步消息（适用于不关心结果成功与否，如：应用日志）
 */
public class OneWayProducer {

    public static void main(String[] args) throws Exception {
        // 1、创建消息生产者producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("fosilzhou");
        // 2、指定Nameserver地址
        producer.setNamesrvAddr("10.10.0.220:9876;10.10.0.222:9876");
        // 3、启动生产者producer
        producer.start();
        for (int i = 0; i < 100; i++) {
            String topic = "simple";
            String tag = "oneway";
            String content = "心随梦飞"+i;
            // 4、创建消息对象，指定主题Topic、标签Tag和消息体
            /**
             * 参数一：消息主题Topic
             * 参数二：消息标签Tag
             * 参数三：消息体
             */
            Message message = new Message(topic, tag, content.getBytes());
            // 5、发送消息
            producer.sendOneway(message);

            TimeUnit.SECONDS.sleep(1);
        }

        // 6、关闭生产者
        producer.shutdown();
    }
}
