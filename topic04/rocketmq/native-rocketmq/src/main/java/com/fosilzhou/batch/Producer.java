package com.fosilzhou.batch;

import com.fosilzhou.MqConstant;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 发送延迟消息
 */
public class Producer {

    public static void main(String[] args) throws Exception {
        // 1、创建消息生产者producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer(MqConstant.GROUP);
        // 2、指定Nameserver地址
        producer.setNamesrvAddr(MqConstant.HOST_ADDR);
        // 3、启动生产者producer
        producer.start();

        List<Message> msgs = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            String topic = MqConstant.BATCH_TOPIC;
            String tag = MqConstant.BATCH_TAG;
            String content = "心随梦飞"+i;
            // 4、创建消息对象，指定主题Topic、标签Tag和消息体
            /**
             * 参数一：消息主题Topic
             * 参数二：消息标签Tag
             * 参数三：消息体
             */
            Message message = new Message(topic, tag, content.getBytes());

            msgs.add(message);
        }

        // 5、批量发送消息
        ListSplitter splitter = new ListSplitter(msgs);
        while (splitter.hasNext()){
            List<Message> item = splitter.next();
            SendResult result = producer.send(item);
            System.out.println("发送结果："+result);
        }

        TimeUnit.SECONDS.sleep(1);

        // 6、关闭生产者
        producer.shutdown();
    }
}
