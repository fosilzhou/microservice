package com.fosilzhou.simple;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.concurrent.TimeUnit;

/**
 * 发送异步消息（适用于对并发性能要求很高，如：站内消息）
 */
public class AsyncProducer {

    public static void main(String[] args) throws Exception {
        // 1、创建消息生产者producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("fosilzhou");
        // 2、指定Nameserver地址
        producer.setNamesrvAddr("10.10.0.220:9876;10.10.0.222:9876");
        // 3、启动生产者producer
        producer.start();
        for (int i = 0; i < 100; i++) {
            String topic = "simple";
            String tag = "async";
            String content = "心随梦飞"+i;
            // 4、创建消息对象，指定主题Topic、标签Tag和消息体
            /**
             * 参数一：消息主题Topic
             * 参数二：消息标签Tag
             * 参数三：消息体
             */
            Message message = new Message(topic, tag, content.getBytes());
            // 5、发送消息
            producer.send(message, new SendCallback() {
                @Override
                public void onSuccess(SendResult result) {
                    SendStatus status = result.getSendStatus();
                    String msgId = result.getMsgId();
                    String offsetMsgId = result.getOffsetMsgId();
                    MessageQueue messageQueue = result.getMessageQueue();
                    long queueOffset = result.getQueueOffset();
                    String transactionId = result.getTransactionId();
                    String regionId = result.getRegionId();
                    System.out.println("发送结果："+result+"\n发送状态："+status+"\n消息Id"+msgId+"事务Id"+transactionId);
                }

                @Override
                public void onException(Throwable e) {

                }
            });

            TimeUnit.SECONDS.sleep(1);
        }

        // 6、关闭生产者
        producer.shutdown();
    }
}
