package com.fosilzhou;

public class MqConstant {
    public static final String HOST_ADDR = "10.10.0.164:9876";
    public static final String GROUP = "fosilzhou";

    public static final String ORDER_TOPIC = "OrderTopic";
    public static final String ORDER_TAG = "order";

    public static final String DELAY_TOPIC = "DelayTopic";
    public static final String DELAY_TAG = "delay";

    public static final String BATCH_TOPIC = "BatchTopic";
    public static final String BATCH_TAG = "Batch";

    public static final String TRANSACTION_TOPIC = "TransactionTopic";
}
