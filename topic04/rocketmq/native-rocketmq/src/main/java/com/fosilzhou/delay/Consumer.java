package com.fosilzhou.delay;

import com.fosilzhou.MqConstant;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;

/**
 * 发送延迟消息
 *
 */
public class Consumer {
    public static void main(String[] args) throws Exception {
        // 1、创建消费者Consumer，指定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(MqConstant.GROUP);
//        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer("fosilzhou");
        // 2、指定Nameserver地址
        consumer.setNamesrvAddr(MqConstant.HOST_ADDR);
        // 3、订阅主题和标签
        String topic = MqConstant.DELAY_TOPIC;
        String tag = "*";//* 表示消费所有Tag的消息
        consumer.subscribe(topic, tag);

        // 4、设置回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {

            //接收消息內容
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt message : msgs) {
                    System.out.println("消息ID["+message.getMsgId()+"]延迟时间"+ (System.currentTimeMillis()-message.getStoreTimestamp()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        // 5、启动消费者
        consumer.start();
    }
}
