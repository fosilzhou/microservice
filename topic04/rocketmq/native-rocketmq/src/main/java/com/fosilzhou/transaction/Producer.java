package com.fosilzhou.transaction;

import com.fosilzhou.MqConstant;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.*;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.concurrent.TimeUnit;

/**
 * 发送事务消息
 */
public class Producer {

    public static void main(String[] args) throws Exception {
        // 1、创建消息生产者producer，并指定生产者组名
        TransactionMQProducer producer = new TransactionMQProducer(MqConstant.GROUP);
        // 2、指定Nameserver地址
        producer.setNamesrvAddr(MqConstant.HOST_ADDR);

        producer.setTransactionListener(new TransactionListener() {
            /**
             * 执行本地事务
             * @param msg Half(prepare) message
             * @param arg Custom business parameter
             * @return
             */
            @Override
            public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                if(StringUtils.equals("TAG_A", msg.getTags())){
                    return LocalTransactionState.COMMIT_MESSAGE;
                }else if(StringUtils.equals("TAG_B", msg.getTags())){
                    return LocalTransactionState.ROLLBACK_MESSAGE;
                }else if(StringUtils.equals("TAG_C", msg.getTags())){
                    return LocalTransactionState.UNKNOW;
                }else {
                    return LocalTransactionState.UNKNOW;
                }
            }

            /**
             * MQ消息进行事务状态回查
             * @param msg Check message
             * @return
             */
            @Override
            public LocalTransactionState checkLocalTransaction(MessageExt msg) {
                System.out.println("消息事务状态回查"+msg.getTags());
                return LocalTransactionState.COMMIT_MESSAGE;
            }
        });
        // 3、启动生产者producer
        producer.start();

        String[] tags = new String[]{"TAG_A","TAG_B","TAG_C"};
        for (int i = 0; i < 3; i++) {
            String topic = MqConstant.TRANSACTION_TOPIC;
            String content = "心随梦飞"+i;
            // 4、创建消息对象，指定主题Topic、标签Tag和消息体
            /**
             * 参数一：消息主题Topic
             * 参数二：消息标签Tag
             * 参数三：消息体
             */
            Message message = new Message(topic, tags[i], content.getBytes());

            // 5、发送消息
            SendResult result = producer.sendMessageInTransaction(message, null);

            System.out.println("发送结果："+result);

            TimeUnit.SECONDS.sleep(1);
        }
    }
}
