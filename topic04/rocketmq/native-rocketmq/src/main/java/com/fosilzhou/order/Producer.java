package com.fosilzhou.order;

import com.fosilzhou.MqConstant;
import org.apache.rocketmq.client.producer.*;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 顺序消息发送者
 */
public class Producer {

    public static void main(String[] args) throws Exception {
        // 1、创建消息生产者producer，并指定生产者组名
        DefaultMQProducer producer = new DefaultMQProducer(MqConstant.GROUP);
        // 2、指定Nameserver地址
        producer.setNamesrvAddr(MqConstant.HOST_ADDR);
        // 3、启动生产者producer
        producer.start();

        List<OrderStep> orderStepList = OrderStep.build();
        for (OrderStep orderStep: orderStepList) {
            // 4、创建消息对象，指定主题Topic、标签Tag和消息体
            String topic = MqConstant.ORDER_TOPIC;
            String tag = MqConstant.ORDER_TAG;
            String content = orderStep.getDesc()+"["+orderStep.getOrderId()+"]";
            Message message = new Message(topic, tag, content.getBytes());
            /**
             * 参数一：消息体
             * 参数二：消息队列选择器
             * 参数三：选择队列业务标识（订单ID）
             */
            SendResult result = producer.send(message, new MessageQueueSelector() {
                /**
                 *
                 * @param mqs 消息集合
                 * @param msg 消息体
                 * @param arg 业务标识参数
                 * @return
                 */
                @Override
                public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                    int orderId = (int) arg;
                    int index = orderId % mqs.size();
                    return mqs.get(index);
                }
            }, orderStep.getOrderId());

            System.out.println("发送结果："+result);
        }

        // 6、关闭生产者
        producer.shutdown();
    }
}
