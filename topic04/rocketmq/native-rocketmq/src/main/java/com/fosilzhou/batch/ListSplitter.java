package com.fosilzhou.batch;

import org.apache.rocketmq.common.message.Message;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ListSplitter implements Iterator<List<Message>> {

    private final int LIMIT_SZIE = 1024 * 1024 *4;
    private List<Message> messages;
    private int currIndex;

    public ListSplitter(List<Message> messages){
        this.messages = messages;
    }

    @Override
    public boolean hasNext() {
        return currIndex < messages.size();
    }

    @Override
    public List<Message> next() {
        int nextIndex = currIndex;
        int totalSize = 0;
        for (;nextIndex<messages.size();nextIndex++) {
            Message message = messages.get(nextIndex);
            int tmpSize = message.getTopic().length()+message.getBody().length;
            Map<String, String> properties = message.getProperties();
            for(Map.Entry<String, String> entry : properties.entrySet()){
                tmpSize += entry.getKey().length()+entry.getValue().length();
            }
            tmpSize += 20;//增加日志开销20字节
            if(tmpSize > LIMIT_SZIE){
                //单个消息超过最大限制，忽略该消息，否则会阻塞分裂进程
                if(nextIndex - currIndex == 0){
                    //假如下一个子列表没有元素，则添加这个子列表然后退出循环，否则只是退出循环
                    nextIndex ++;
                }
                break;
            }
            if(tmpSize + totalSize > LIMIT_SZIE){
                break;
            }else {
                totalSize+=tmpSize;
            }

        }
        List<Message> subList = messages.subList(currIndex, nextIndex);
        currIndex = nextIndex;
        return subList;
    }
}
