package com.fosilzhou.order;

import com.fosilzhou.MqConstant;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.*;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;

/**
 * 负载均衡模式：多个消费者共同消费队列消息，每个消费只会消费一次
 * 广播模式：每个消息都会被多个消费者消费一次
 *
 */
public class Consumer {
    public static void main(String[] args) throws Exception {
        // 1、创建消费者Consumer，指定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(MqConstant.GROUP);
//        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer("fosilzhou");
        // 2、指定Nameserver地址
        consumer.setNamesrvAddr(MqConstant.HOST_ADDR);
        // 3、订阅主题和标签
        String topic = MqConstant.ORDER_TOPIC;
        String tag = "*";
        consumer.subscribe(topic, tag);

        //设置消费模式：负载均衡模式(默认)、广播模式
        consumer.setMessageModel(MessageModel.CLUSTERING);

        // 4、设置回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerOrderly() {

            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
                for (MessageExt message: msgs) {
                    System.out.println("线程名称【"+Thread.currentThread().getName()+"】接收消息内容："+ new String(message.getBody()));
                }
                return ConsumeOrderlyStatus.SUCCESS;
            }
        });
        // 5、启动消费者
        consumer.start();
    }
}
