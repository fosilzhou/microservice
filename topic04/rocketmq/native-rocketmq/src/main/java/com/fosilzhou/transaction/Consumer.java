package com.fosilzhou.transaction;

import com.fosilzhou.MqConstant;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 * 接收事务消息
 *
 */
public class Consumer {
    public static void main(String[] args) throws Exception {
        // 1、创建消费者Consumer，指定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(MqConstant.GROUP);
//        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer("fosilzhou");
        // 2、指定Nameserver地址
        consumer.setNamesrvAddr(MqConstant.HOST_ADDR);
        // 3、订阅主题和标签
        String topic = MqConstant.TRANSACTION_TOPIC;
        String tag = "*";//* 表示消费所有Tag的消息
        consumer.subscribe(topic, tag);

        // 4、设置回调函数，处理消息
        consumer.registerMessageListener(new MessageListenerConcurrently() {

            //接收消息內容
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt message : msgs) {
                    System.out.println("线程名称【"+Thread.currentThread().getName()+"】消息体："+message+"\n"+ new String(message.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        // 5、启动消费者
        consumer.start();
    }
}
