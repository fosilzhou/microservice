package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Demo01：普通消息
 * Demo02：批量发送
 * Demo03：定时消息
 * Demo04：消费重试
 * Demo05：广播消息
 * Demo06：顺序消息
 * Demo07：事务消息
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

}
