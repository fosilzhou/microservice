package com.fosilzhou.cloud.message;

public class Demo07Message {

    public static final String TOPIC = "DEMO_07";

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
