package com.fosilzhou.cloud.producer;

import com.fosilzhou.cloud.message.Demo01Message;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 发送单条消息
 */
@Component
public class Demo01Producer {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 发送同步消息
     * @param id
     * @return
     */
    public SendResult syncSend(Integer id){
        Demo01Message message = new Demo01Message();
        message.setId(id);
        return rocketMQTemplate.syncSend(Demo01Message.TOPIC, message);
    }

    /**
     * 发送异步消息
     * @param id
     * @param callback
     */
    public void asyncSend(Integer id, SendCallback callback){
        Demo01Message message = new Demo01Message();
        message.setId(id);
        rocketMQTemplate.asyncSend(Demo01Message.TOPIC, message, callback);
    }

    /**
     * 仅发送一次消息
     * @param id
     */
    public void onewaySend(Integer id){
        Demo01Message message = new Demo01Message();
        message.setId(id);
        rocketMQTemplate.sendOneWay(Demo01Message.TOPIC, message);
    }
}
