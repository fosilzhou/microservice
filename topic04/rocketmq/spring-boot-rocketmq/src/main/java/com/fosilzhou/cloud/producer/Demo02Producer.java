package com.fosilzhou.cloud.producer;

import com.fosilzhou.cloud.message.Demo02Message;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 批量发送消息
 */
@Component
public class Demo02Producer {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 发送同步消息
     * @param ids
     * @return
     */
    public SendResult sendBatch(Collection<Integer> ids){
        List<Message> messages = new ArrayList<>(ids.size());
        for (Integer id: ids) {
            Demo02Message message = new Demo02Message();
            message.setId(id);
            messages.add(MessageBuilder.withPayload(message).build());
        }
        return rocketMQTemplate.syncSend(Demo02Message.TOPIC, messages, 30 * 1000L);
    }

}
