package com.fosilzhou.cloud.consumer;

import com.fosilzhou.cloud.message.Demo01Message;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
        topic = Demo01Message.TOPIC,
        consumerGroup = "demo01-A-rocketmq-group-" + Demo01Message.TOPIC
)
public class Demo01AConsumer implements RocketMQListener<Demo01Message> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onMessage(Demo01Message message) {
        logger.info("[onMessage-A][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }
}
