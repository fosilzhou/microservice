package com.fosilzhou.cloud;

import com.fosilzhou.cloud.producer.Demo03Producer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class Demo03ProducerTest {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo03Producer producer;

    @Test
    public void testSyncSendDelay() throws InterruptedException {
        int id = (int) (System.currentTimeMillis() / 1000);
        SendResult result = producer.syncSendDelay(id, 3); // 延迟级别 3 ，即 10 秒后消费
        logger.info("[testSyncSendDelay][发送编号：[{}] 发送结果：[{}]]", id, result);

        // 阻塞等待，保证消费
        new CountDownLatch(1).await();
    }

    @Test
    public void testASyncSendDelay() throws InterruptedException {
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.asyncSendDelay(id, 3, new SendCallback() {
            @Override
            public void onSuccess(SendResult result) {
                logger.info("[testASyncSendDelay][发送编号：[{}] 发送成功，结果为：[{}]]", id, result);
            }
            @Override
            public void onException(Throwable e) {
                logger.info("[testASyncSendDelay][发送编号：[{}] 发送异常]]", id, e);
            }
        }); // 延迟级别 3 ，即 10 秒后消费

        // 阻塞等待，保证消费
        new CountDownLatch(1).await();
    }
}
