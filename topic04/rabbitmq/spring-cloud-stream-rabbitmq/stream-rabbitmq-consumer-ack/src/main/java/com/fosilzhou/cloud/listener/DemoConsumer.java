package com.fosilzhou.cloud.listener;

import com.fosilzhou.cloud.message.DemoMessage;
import com.fosilzhou.cloud.sink.DemoSink;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class DemoConsumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private AtomicInteger index = new AtomicInteger();

    @StreamListener(DemoSink.DEMO_INPUT)
    public void onMessage(@Payload DemoMessage message, @Header(AmqpHeaders.CHANNEL) Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) throws IOException {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        int idx = index.incrementAndGet();
        if(idx % 4 == 0){
            // ack 确认消息
            // 第二个参数 multiple ，用于批量确认消息，为了减少网络流量，手动确认可以被批处。
            // 1. 当 multiple 为 true 时，则可以一次性确认 deliveryTag 小于等于传入值的所有消息
            // 2. 当 multiple 为 false 时，则只确认当前 deliveryTag 对应的消息
            channel.basicAck(deliveryTag, false);
            logger.info("[onMessage]【确认消息】[线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        }else if(idx % 4 == 1){
            // nack 否认消息
            channel.basicNack(deliveryTag, false, true);
            logger.info("[onMessage]【否认消息】[线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        }else if(idx % 4 == 2) {
            // reject 拒绝消息
            channel.basicReject(deliveryTag, false);
            logger.info("[onMessage]【拒绝消息】[线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        }else {
            // 参考：https://www.jianshu.com/p/2c5eebfd0e95
            // 如果服务忘记Ack消息，RabbitMQ将不会再发送消息给它，因为RabbitMQ认为该服务的处理能力有限
        }
    }
}
