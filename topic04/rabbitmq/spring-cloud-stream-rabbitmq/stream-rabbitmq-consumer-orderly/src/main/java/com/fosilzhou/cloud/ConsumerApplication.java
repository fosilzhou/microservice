package com.fosilzhou.cloud;

import com.fosilzhou.cloud.sink.DemoSink;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * 启动参数指定消费的分区
 * CONSUMER_INSTANCE_INDEX=0
 * CONSUMER_INSTANCE_INDEX=1
 */
@SpringBootApplication
@EnableBinding(DemoSink.class)
public class ConsumerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ConsumerApplication.class, args);
	}

}
