package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.message.DemoMessage;
import com.fosilzhou.cloud.source.DemoSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/transaction")
public class DemoController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DemoSource demoSource;

    /**
     * 发送普通消息
     * @return
     */
    @GetMapping("/send")
    @Transactional
    public boolean sendTransaction() {
        // 创建 Message
        DemoMessage message = new DemoMessage();
        message.setId(new Random().nextInt());
        // 创建 Spring Message 对象
        Message<DemoMessage> springMessage = MessageBuilder.withPayload(message).build();
        // 发送消息
        boolean sendResult = demoSource.output().send(springMessage);
        logger.info("[sendTransaction][发送消息完成, 结果 = {}]", sendResult);

        try {
            // 在此处停顿10秒，用来判断Kafka事务是否生效
            // 如果同步消息发送成功后，Consumer立即消费到该消息，说明Kafka事务未生效
            // 如果同步消息发送成功后，Consumer是10秒之后才消费，说明Kafka事务生效
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return sendResult;
    }

}
