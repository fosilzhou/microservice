package com.fosilzhou.cloud.sink;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface DemoSink {

    String INPUT = "retry-input";

    @Input(INPUT)
    SubscribableChannel input();
}
