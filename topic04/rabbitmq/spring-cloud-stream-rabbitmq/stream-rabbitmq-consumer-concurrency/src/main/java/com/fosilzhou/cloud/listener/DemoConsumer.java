package com.fosilzhou.cloud.listener;

import com.fosilzhou.cloud.message.DemoMessage;
import com.fosilzhou.cloud.sink.DemoSink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class DemoConsumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @StreamListener(DemoSink.DEMO_INPUT)
    public void onMessage(@Payload DemoMessage message) {
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }
}
