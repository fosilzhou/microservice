package com.fosilzhou.cloud.listener;

import com.fosilzhou.cloud.sink.DemoSink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class DemoConsumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @StreamListener(value = DemoSink.DEMO_INPUT, condition = "headers['TAGS'] == 'alipay'")
    public void onMessage(Message<?> message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }
}
