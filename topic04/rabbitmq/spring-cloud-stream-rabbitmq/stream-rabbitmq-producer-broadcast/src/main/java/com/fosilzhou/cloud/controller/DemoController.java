package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.message.DemoMessage;
import com.fosilzhou.cloud.source.DemoSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/broadcast")
public class DemoController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DemoSource demoSource;

    /**
     * 发送普通消息
     * @return
     */
    @GetMapping("/send")
    public boolean sendDemo() {
        // 创建 Message
        DemoMessage message = new DemoMessage();
        message.setId(new Random().nextInt());
        // 创建 Spring Message 对象
        Message<DemoMessage> springMessage = MessageBuilder.withPayload(message).build();
        // 发送消息
        boolean sendResult = demoSource.output().send(springMessage);
        logger.info("[sendDemo][发送消息完成, 结果 = {}]", sendResult);
        return sendResult;
    }

}
