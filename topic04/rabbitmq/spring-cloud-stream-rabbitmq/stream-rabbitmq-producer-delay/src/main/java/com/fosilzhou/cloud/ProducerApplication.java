package com.fosilzhou.cloud;

import com.fosilzhou.cloud.source.DemoSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * 安装延迟插件
 * wget https://github.com/rabbitmq/rabbitmq-delayed-message-exchange/releases/download/v3.8.0/rabbitmq_delayed_message_exchange-3.8.0.ez
 * docker cp rabbitmq_delayed_message_exchange-3.8.0.ez  rabbitmq:/opt/rabbitmq/plugins
 * docker exec -it rabbitmq /bin/sh
 * cd /opt/rabbitmq/plugins
 * rabbitmq-plugins enable rabbitmq_delayed_message_exchange
 */
@SpringBootApplication
@EnableBinding(DemoSource.class)
public class ProducerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ProducerApplication.class, args);
	}

}
