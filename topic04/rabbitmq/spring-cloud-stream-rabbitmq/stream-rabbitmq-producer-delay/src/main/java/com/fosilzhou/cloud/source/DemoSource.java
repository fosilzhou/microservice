package com.fosilzhou.cloud.source;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface DemoSource {

    @Output("delay-output")
    MessageChannel output();
}
