package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo09Message;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Demo09DirectExchangeConfiguration {

    @Bean
    public Queue demo09Queue() {
        return new Queue(Demo09Message.QUEUE, // Queue 名字
                true, // durable: 是否持久化
                false, // exclusive: 是否排它
                false); // autoDelete: 是否自动删除
    }

    @Bean
    public DirectExchange demo09Exchange() {
        return new DirectExchange(Demo09Message.EXCHANGE,
                true,  // durable: 是否持久化
                false);  // exclusive: 是否排它
    }

    @Bean
    public Binding demo09Binding() {
        return BindingBuilder.bind(demo09Queue()).to(demo09Exchange()).with(Demo09Message.ROUTING_KEY);
    }
}
