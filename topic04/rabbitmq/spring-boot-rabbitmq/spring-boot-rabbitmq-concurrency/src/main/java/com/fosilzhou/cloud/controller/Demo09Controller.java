package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo09Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo09")
public class Demo09Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo09Producer producer;

    @GetMapping("/syncSend")
    public void syncSend(){
        for (int i = 0; i < 10; i++) {
            int id = (int) (System.currentTimeMillis() / 1000);
            producer.syncSend(id);
            logger.info("[syncSend][发送编号：[{}] 发送成功]", id);
        }

    }

}
