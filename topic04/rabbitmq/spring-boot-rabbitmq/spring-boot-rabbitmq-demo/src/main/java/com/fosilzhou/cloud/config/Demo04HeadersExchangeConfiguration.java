package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo04Message;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.HeadersExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Demo04HeadersExchangeConfiguration {

    @Bean
    public Queue demo04Queue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo04Message.QUEUE, true, false, false);
    }

    @Bean
    public HeadersExchange demo04Exchange() {
        /**
         * 创建交换器 TopicExchange Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new HeadersExchange(Demo04Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding demo04Binding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo04Message.QUEUE
         * @param DirectExchange Demo04Message.EXCHANGE
         * @param RoutingKey Demo04Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo04Queue()).to(demo04Exchange()).where(Demo04Message.HEADER_KEY).matches(Demo04Message.HEADER_VALUE); // 配置 Headers 匹配
    }
}
