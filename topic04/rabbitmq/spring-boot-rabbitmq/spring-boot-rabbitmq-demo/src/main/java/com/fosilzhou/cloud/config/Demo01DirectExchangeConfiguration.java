package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo01Message;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息订阅模型：定向订阅模式
 */
@Configuration
public class Demo01DirectExchangeConfiguration {

    @Bean
    public Queue demo01Queue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo01Message.QUEUE, true, false, false);
    }

    @Bean
    public DirectExchange demo01Exchange() {
        /**
         * 创建交换器 Direct Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new DirectExchange(Demo01Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding demo01Binding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo01Message.QUEUE
         * @param DirectExchange Demo01Message.EXCHANGE
         * @param RoutingKey Demo01Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo01Queue()).to(demo01Exchange()).with(Demo01Message.ROUTING_KEY);
    }
}
