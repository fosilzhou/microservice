package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo01Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo01")
public class Demo01Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo01Producer producer;

    @GetMapping("/syncSend")
    public void syncSend(){
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSend(id);
        logger.info("[syncSend][发送编号：[{}] 发送成功]", id);

    }

    @GetMapping("/syncSendDefault")
    public void syncSendDefault(){
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSendDefault(id);
        logger.info("[syncSendDefault][发送编号：[{}] 发送成功]", id);

    }

    @GetMapping("asyncSend")
    public void asyncSend() {
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.asyncSend(id).addCallback(new ListenableFutureCallback<Void>() {

            @Override
            public void onFailure(Throwable e) {
                logger.info("[asyncSend][发送编号：[{}] 发送异常]]", id, e);
            }

            @Override
            public void onSuccess(Void aVoid) {
                logger.info("[asyncSend][发送编号：[{}] 发送成功，发送成功]", id);
            }

        });
        logger.info("[asyncSend][发送编号：[{}] 调用完成]", id);
    }
}
