package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo03Message;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息订阅模型：广播订阅模式
 */
@Configuration
public class Demo03FanoutExchangeConfiguration {

    @Bean
    public Queue demo03AQueue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo03Message.QUEUE_A, true, false, false);
    }
    @Bean
    public Queue demo03BQueue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo03Message.QUEUE_B, true, false, false);
    }

    @Bean
    public FanoutExchange demo03Exchange() {
        /**
         * 创建交换器 Fanout Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new FanoutExchange(Demo03Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding demo03ABinding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo03Message.QUEUE
         * @param DirectExchange Demo03Message.EXCHANGE
         * @param RoutingKey Demo03Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo03AQueue()).to(demo03Exchange());
    }
    @Bean
    public Binding demo03BBinding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo03Message.QUEUE
         * @param DirectExchange Demo03Message.EXCHANGE
         * @param RoutingKey Demo03Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo03BQueue()).to(demo03Exchange());
    }
}
