package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo01Producer;
import com.fosilzhou.cloud.producer.Demo03Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo03")
public class Demo03Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo03Producer producer;

    @GetMapping("/syncSend")
    public void syncSend() {
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSend(id);
        logger.info("[syncSend][发送编号：[{}] 发送成功]", id);

    }

}
