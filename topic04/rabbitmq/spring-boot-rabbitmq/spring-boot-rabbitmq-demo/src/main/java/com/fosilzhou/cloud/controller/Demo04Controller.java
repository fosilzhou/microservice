package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.message.Demo04Message;
import com.fosilzhou.cloud.producer.Demo03Producer;
import com.fosilzhou.cloud.producer.Demo04Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo04")
public class Demo04Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo04Producer producer;

    @GetMapping("/syncSend")
    public void syncSend() {
        String[] headerValues = new String[]{Demo04Message.HEADER_VALUE, "MinChow"};
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSend(id, headerValues[id % 2]);
        logger.info("[syncSend][发送编号：[{}-->{}] 发送成功]", id, headerValues[id % 2]);
    }
}
