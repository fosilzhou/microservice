package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo02Message;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息订阅模型：通配符订阅模式
 */
@Configuration
public class Demo02TopicExchangeConfiguration {
    @Bean
    public Queue demo02Queue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo02Message.QUEUE, true, false, false);
    }

    @Bean
    public TopicExchange demo02Exchange() {
        /**
         * 创建交换器 TopicExchange Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new TopicExchange(Demo02Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding demo02Binding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo02Message.QUEUE
         * @param DirectExchange Demo02Message.EXCHANGE
         * @param RoutingKey Demo02Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo02Queue()).to(demo02Exchange()).with(Demo02Message.ROUTING_KEY);
    }
}
