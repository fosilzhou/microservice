package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo02Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo02")
public class Demo02Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo02Producer producer;

    @GetMapping("/syncSend")
    public void syncSend() {
        String[] routingKeys = new String[]{"alipay.fosil.zhou","wxpay.min.chow"};
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSend(id, routingKeys[id%2]);
        logger.info("[syncSend][发送编号：[{}-->{}] 发送成功]", id, routingKeys[id%2]);
    }

}
