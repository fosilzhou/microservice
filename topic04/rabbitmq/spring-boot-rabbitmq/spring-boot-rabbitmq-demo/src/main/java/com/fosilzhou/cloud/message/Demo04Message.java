package com.fosilzhou.cloud.message;

import java.io.Serializable;

public class Demo04Message implements Serializable {

    public static final String QUEUE = "QUEUE_DEMO_04";

    public static final String EXCHANGE = "EXCHANGE_DEMO_04";

    public static final String HEADER_KEY = "name";
    public static final String HEADER_VALUE = "fosilzhou";

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
