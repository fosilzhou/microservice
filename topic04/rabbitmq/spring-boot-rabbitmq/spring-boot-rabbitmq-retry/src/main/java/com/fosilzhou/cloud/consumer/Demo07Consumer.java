package com.fosilzhou.cloud.consumer;

import com.fosilzhou.cloud.message.Demo07Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = Demo07Message.QUEUE)
public class Demo07Consumer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /*@RabbitHandler
    public void onMessage(Demo07Message message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        // 注意：此处抛出一个 RuntimeException 异常，模拟消费失败
        throw new RuntimeException("故意抛出一个异常");
    }*/

    @RabbitHandler(isDefault = true)
    public void onMessage(org.springframework.amqp.core.Message message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        // 注意：此处抛出一个 RuntimeException 异常，模拟消费失败
        throw new RuntimeException("故意抛出一个异常");
    }
}
