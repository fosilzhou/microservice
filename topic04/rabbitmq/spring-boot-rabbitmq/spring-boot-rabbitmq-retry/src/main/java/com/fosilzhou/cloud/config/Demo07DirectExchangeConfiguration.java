package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo07Message;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息订阅模型：定向订阅模式
 */
@Configuration
public class Demo07DirectExchangeConfiguration {

    @Bean
    public Queue demo07Queue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return QueueBuilder.durable(Demo07Message.QUEUE)// durable: 是否持久化
                .exclusive()// exclusive: 是否排它
                .autoDelete()// autoDelete: 是否自动删除
                .deadLetterExchange(Demo07Message.EXCHANGE)
                .deadLetterRoutingKey(Demo07Message.DEAD_ROUTING_KEY)
                .build();
    }

    @Bean
    public Queue demo07DeadQueue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo07Message.DEAD_QUEUE, true, false, false);
    }

    @Bean
    public DirectExchange demo07Exchange() {
        /**
         * 创建交换器 Direct Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new DirectExchange(Demo07Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding demo07Binding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo07Message.QUEUE
         * @param DirectExchange Demo07Message.EXCHANGE
         * @param RoutingKey Demo07Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo07Queue()).to(demo07Exchange()).with(Demo07Message.ROUTING_KEY);
    }

    @Bean
    public Binding demo07DeadBinding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo07Message.DEAD_QUEUE
         * @param DirectExchange Demo07Message.EXCHANGE
         * @param RoutingKey Demo07Message.DEAD_ROUTING_KEY
         */
        return BindingBuilder.bind(demo07DeadQueue()).to(demo07Exchange()).with(Demo07Message.DEAD_ROUTING_KEY);
    }
}
