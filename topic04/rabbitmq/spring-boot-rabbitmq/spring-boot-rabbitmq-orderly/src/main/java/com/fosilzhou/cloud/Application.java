package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 问题①：如果消息仅仅投递到一个Queue中，并采用单个Consumer串行消费，在监听的Queue每秒消息量比较大的时候，会出现消费不及时，导致消息挤压问题
 * 	方案一：在Producer端，将Queue拆分多个子Queue，假设原来Queue是QUEUE_USER，那么就拆分成QUEUE_USE_00至QUEUE_USE_${N-1}，这样就拆分为N个队列，然后基于消息的用户编号取余，路由到对应的子队列中
 * 	方案二：在Consumer端，将Queue拉取到的消息，将相关的消息发到相同的线程中消费。例如：Queue是QUEUE_USER，创建N个线程大小为1的ExecutorService数组，然后基于消息的用户编号取余，提交到对应的ExecutorService中的单个线程来执行
 * 	以上两个方案并不冲突，可以结合使用
 * 问题②：启动相同Consumer的多个进程，会导致相同Queue的消息被分配到多个Consumer进行消费，破坏Consumer严格顺序消费
 * 	方案一：引入Zookeeper来协调，动态设置多个进程中相同的Consumer开关，保证有且仅有一个Consumer开启对同一个Queue的消费
 * 	方案二：引入Zookeeper来协调，动态设置多个进程中相同的Consumer消费的Queue的分配，保证有且仅有一个Consumer开启对同一个Queue的消费（仅适用于【问题①】的【方案一】）
 */
@SpringBootApplication
@EnableAsync // 开启异步
public class Application {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}
}
