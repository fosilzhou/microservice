package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo10Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo10")
public class Demo10Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo10Producer producer;

    @GetMapping("/syncSend")
    public void syncSend(){
        for (int i = 1; i < 6; i++) {
            for (int id = 1; id < 6; id++) {
                producer.syncSend(id);
                logger.info("[syncSend][发送编号：[{}] 发送成功]", id);
            }
        }
    }

}
