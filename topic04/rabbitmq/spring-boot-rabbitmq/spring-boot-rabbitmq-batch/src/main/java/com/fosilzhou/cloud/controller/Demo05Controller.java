package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo05Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo05")
public class Demo05Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo05Producer producer;

    @GetMapping("/syncSend")
    public void syncSend() throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            // 同步发送消息
            int id = (int) (System.currentTimeMillis() / 1000);
            producer.syncSend(id);
            // 故意每条消息之间，隔离 10 秒
            logger.info("[syncSend][发送编号：[{}] 发送成功]", id);
            Thread.sleep(10 * 1000L);
        }
    }
}
