package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo05Message;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.batch.BatchingStrategy;
import org.springframework.amqp.rabbit.batch.SimpleBatchingStrategy;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.BatchingRabbitTemplate;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

/**
 * 批量消息
 * 消息订阅模型：定向订阅模式
 */
@Configuration
public class Demo05BatchDirectExchangeConfiguration {
    @Bean
    public Queue demo05Queue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo05Message.QUEUE, true, false, false);
    }

    @Bean
    public DirectExchange demo05Exchange() {
        /**
         * 创建交换器 Direct Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new DirectExchange(Demo05Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding demo05Binding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo05Message.QUEUE
         * @param DirectExchange Demo05Message.EXCHANGE
         * @param RoutingKey Demo05Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo05Queue()).to(demo05Exchange()).with(Demo05Message.ROUTING_KEY);
    }

    @Bean
    public BatchingRabbitTemplate batchRabbitTemplate(ConnectionFactory connectionFactory) {
        // 创建 BatchingStrategy 对象，代表批量策略
        int batchSize = 16384; // 超过收集的消息数量的最大条数。
        int bufferLimit = 33554432; // 每次批量发送消息的最大内存
        int timeout = 30000; // 超过收集的时间的最大等待时长，单位：毫秒
        BatchingStrategy batchingStrategy = new SimpleBatchingStrategy(batchSize, bufferLimit, timeout);

        // 创建 TaskScheduler 对象，用于实现超时发送的定时器
        TaskScheduler taskScheduler = new ConcurrentTaskScheduler();

        // 创建 BatchingRabbitTemplate 对象
        BatchingRabbitTemplate batchTemplate = new BatchingRabbitTemplate(batchingStrategy, taskScheduler);
        batchTemplate.setConnectionFactory(connectionFactory);
        return batchTemplate;
    }

    @Bean(name = "consumerBatchContainerFactory")
    public SimpleRabbitListenerContainerFactory consumerBatchContainerFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer, ConnectionFactory connectionFactory) {
        // 创建 SimpleRabbitListenerContainerFactory 对象
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        // 额外添加批量消费的属性
        factory.setBatchListener(true);
        return factory;
    }
}
