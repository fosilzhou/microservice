package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo06Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CountDownLatch;

@RestController
@RequestMapping("/demo06")
public class Demo06Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Demo06Producer producer;

    /**
     * 测试地址：http://localhost:28080/demo06/syncSend?n=15
     * 发送小于10条消息时，测试Demo06Consumer获取数量为batchSize=10消息，因为小于10条，所以会一直等，等到超时后才会批量消费
     * 发送等于10条消息时，测试Demo06Consumer获取数量为batchSize=10消息，满足批量消息数直接批量消费消息，不用等
     * 发送大于10条消息时，测试Demo06Consumer获取数量为batchSize=10消息，会采用（n % 10 + 1）分批次批量消费
     * @param n
     */
    @GetMapping("/syncSend")
    public void syncSend(int n) {
        for (int i = 0; i < n; i++) {
            // 同步发送消息
            int id = (int) (System.currentTimeMillis() / 1000);
            producer.syncSend(id);
            logger.info("[syncSend][发送编号：[{}] 发送成功]", id);
        }
    }

}
