package com.fosilzhou.cloud.message;

import java.io.Serializable;

public class Demo11Message implements Serializable {

    public static final String QUEUE = "QUEUE_DEMO_11";

    public static final String EXCHANGE = "EXCHANGE_DEMO_11";

    public static final String ROUTING_KEY = "ROUTING_KEY_11";

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
