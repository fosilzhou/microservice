package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo11Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/demo11")
public class Demo11Controller {

    @Autowired
    private Demo11Producer producer;

    @GetMapping("/syncSend")
    public void syncSend(){
        int id = (int) (System.currentTimeMillis() / 1000);
        producer.syncSend(id);

    }

}
