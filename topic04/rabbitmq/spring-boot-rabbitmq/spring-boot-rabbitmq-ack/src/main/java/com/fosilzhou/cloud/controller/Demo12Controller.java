package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.Demo12Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/demo12")
public class Demo12Controller {

    @Autowired
    private Demo12Producer producer;

    @GetMapping("/syncSend")
    public void syncSend(){
        for (int i = 0; i < 2; i++) {
            int id = (int) (System.currentTimeMillis() / 1000);
            producer.syncSend(id + i);
        }
    }

}
