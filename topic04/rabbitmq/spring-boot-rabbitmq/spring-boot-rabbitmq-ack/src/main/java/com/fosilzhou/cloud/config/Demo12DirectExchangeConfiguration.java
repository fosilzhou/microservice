package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo12Message;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.transaction.RabbitTransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
public class Demo12DirectExchangeConfiguration {

    @Bean
    public Queue Demo12Queue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo12Message.QUEUE, true, false, false);
    }

    @Bean
    public DirectExchange Demo12Exchange() {
        /**
         * 创建交换器 Direct Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new DirectExchange(Demo12Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding Demo12Binding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo12Message.QUEUE
         * @param DirectExchange Demo12Message.EXCHANGE
         * @param RoutingKey Demo12Message.ROUTING_KEY
         */
        return BindingBuilder.bind(Demo12Queue()).to(Demo12Exchange()).with(Demo12Message.ROUTING_KEY);
    }
}
