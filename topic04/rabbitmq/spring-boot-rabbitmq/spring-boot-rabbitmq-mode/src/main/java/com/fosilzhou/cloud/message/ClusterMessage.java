package com.fosilzhou.cloud.message;

import java.io.Serializable;

/**
 * 集群消息消费示例
 */
public class ClusterMessage implements Serializable {

    public static final String QUEUE = "QUEUE_CLUSTERING";

    public static final String EXCHANGE = "EXCHANGE_CLUSTERING";

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
