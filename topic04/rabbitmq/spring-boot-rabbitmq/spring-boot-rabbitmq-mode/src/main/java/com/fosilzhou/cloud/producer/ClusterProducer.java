package com.fosilzhou.cloud.producer;

import com.fosilzhou.cloud.message.ClusterMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClusterProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void syncSend(Integer id) {
        // 创建 Demo07Message 消息
        ClusterMessage message = new ClusterMessage();
        message.setId(id);
        // 同步发送消息
        rabbitTemplate.convertAndSend(ClusterMessage.EXCHANGE, null, message);
    }
}
