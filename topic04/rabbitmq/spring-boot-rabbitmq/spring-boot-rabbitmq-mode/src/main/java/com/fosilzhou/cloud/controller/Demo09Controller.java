package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.BroadcastProducer;
import com.fosilzhou.cloud.producer.ClusterProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo09")
public class Demo09Controller {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ClusterProducer clusterProducer;

    @Autowired
    private BroadcastProducer broadcastProducer;

    @GetMapping("/syncSendCluster")
    public void syncSendCluster(){
        int id = (int) (System.currentTimeMillis() / 1000);
        clusterProducer.syncSend(id);
        logger.info("[syncSend][发送编号：[{}] 发送成功]", id);
    }

    @GetMapping("/syncSendBroadcast")
    public void syncSendBroadcast(){
        int id = (int) (System.currentTimeMillis() / 1000);
        broadcastProducer.syncSend(id);
        logger.info("[syncSend][发送编号：[{}] 发送成功]", id);
    }

}
