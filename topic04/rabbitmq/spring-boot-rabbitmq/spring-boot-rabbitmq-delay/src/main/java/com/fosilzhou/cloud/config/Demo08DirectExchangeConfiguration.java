package com.fosilzhou.cloud.config;

import com.fosilzhou.cloud.message.Demo08Message;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息订阅模型：定向订阅模式
 */
@Configuration
public class Demo08DirectExchangeConfiguration {

    @Bean
    public Queue demo08Queue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return QueueBuilder.durable(Demo08Message.QUEUE)// durable: 是否持久化
                .exclusive()// exclusive: 是否排它
                .autoDelete()// autoDelete: 是否自动删除
                .ttl(10 * 1000) // 设置队列里的默认过期时间为 10 秒
                .deadLetterExchange(Demo08Message.EXCHANGE)
                .deadLetterRoutingKey(Demo08Message.DELAY_ROUTING_KEY)
                .build();
    }

    @Bean
    public Queue demo08DeadQueue() {
        /**
         * 创建队列 Queue
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new Queue(Demo08Message.DELAY_QUEUE, true, false, false);
    }

    @Bean
    public DirectExchange demo08Exchange() {
        /**
         * 创建交换器 Direct Exchange
         * @param name Queue 名字
         * @param durable 是否持久化
         * @param exclusive 是否排它
         * @param autoDelete 是否自动删除
         */
        return new DirectExchange(Demo08Message.EXCHANGE, true, false);
    }

    @Bean
    public Binding demo08Binding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo08Message.QUEUE
         * @param DirectExchange Demo08Message.EXCHANGE
         * @param RoutingKey Demo08Message.ROUTING_KEY
         */
        return BindingBuilder.bind(demo08Queue()).to(demo08Exchange()).with(Demo08Message.ROUTING_KEY);
    }

    @Bean
    public Binding demo08DeadBinding() {
        /**
         * 创建绑定器 Binding
         * @param Queue Demo08Message.DEAD_QUEUE
         * @param DirectExchange Demo08Message.EXCHANGE
         * @param RoutingKey Demo08Message.DEAD_ROUTING_KEY
         */
        return BindingBuilder.bind(demo08DeadQueue()).to(demo08Exchange()).with(Demo08Message.DELAY_ROUTING_KEY);
    }
}
