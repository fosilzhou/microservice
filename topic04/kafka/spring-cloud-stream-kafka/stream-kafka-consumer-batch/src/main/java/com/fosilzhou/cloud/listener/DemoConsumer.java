package com.fosilzhou.cloud.listener;

import com.fosilzhou.cloud.message.DemoMessage;
import com.fosilzhou.cloud.sink.DemoSink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DemoConsumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    /*@StreamListener(DemoSink.DEMO_INPUT)
    public void onMessage(@Payload DemoMessage message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }*/

    @StreamListener(DemoSink.DEMO_INPUT)
    public void onMessage(@Payload List<DemoMessage> messages) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), messages);
    }
}
