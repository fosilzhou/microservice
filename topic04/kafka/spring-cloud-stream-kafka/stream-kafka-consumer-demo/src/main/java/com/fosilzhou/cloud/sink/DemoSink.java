package com.fosilzhou.cloud.sink;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface DemoSink {

    String DEMO_INPUT = "demo-input";

    @Input(DEMO_INPUT)
    SubscribableChannel input();
}
