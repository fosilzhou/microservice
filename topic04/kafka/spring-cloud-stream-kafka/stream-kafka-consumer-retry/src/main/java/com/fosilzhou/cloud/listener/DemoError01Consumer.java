package com.fosilzhou.cloud.listener;

import com.fosilzhou.cloud.message.DemoMessage;
import com.fosilzhou.cloud.sink.DemoSink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class DemoError01Consumer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @StreamListener(DemoSink.INPUT)
    public void onMessage(@Payload DemoMessage message) {
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
        // 注意：此处抛出一个 RuntimeException 异常，模拟消费失败
        throw new RuntimeException("故意抛出一个异常");
    }
}
