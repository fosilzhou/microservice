package com.fosilzhou.cloud;

import com.fosilzhou.cloud.source.DemoSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * docker exec -it kafka-standalone /bin/sh
 * # 情况一，如果 `DEMO-TOPIC-01` Topic 未创建，则进行创建：
 * $ bin/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --topic DEMO-TOPIC-01 --partitions 10 --replication-factor 1
 * # 情况二，如果 `DEMO-TOPIC-01` Topic 未创建，则进行修改 Partition 大小：
 * $ bin/kafka-topics.sh --zookeeper 127.0.0.1:2181 -alter --partitions 10 --topic DEMO-TOPIC-01
 */
@SpringBootApplication
@EnableBinding(DemoSource.class)
public class ProducerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ProducerApplication.class, args);
	}

}
