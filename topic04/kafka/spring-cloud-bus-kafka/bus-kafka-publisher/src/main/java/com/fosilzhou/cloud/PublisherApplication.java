package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublisherApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(PublisherApplication.class, args);
	}

}
