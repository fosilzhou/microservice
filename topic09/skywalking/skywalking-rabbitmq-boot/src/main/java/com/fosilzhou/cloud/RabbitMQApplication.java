package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 插件：
 * apm-rabbitmq-5.x-plugin
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-rabbitmq -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class RabbitMQApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(RabbitMQApplication.class, args);
	}

}
