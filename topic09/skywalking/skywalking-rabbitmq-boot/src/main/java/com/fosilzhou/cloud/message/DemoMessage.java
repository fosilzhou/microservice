package com.fosilzhou.cloud.message;

import java.io.Serializable;

public class DemoMessage implements Serializable {

    public static final String QUEUE = "QUEUE_DEMO_";

    public static final String EXCHANGE = "EXCHANGE_DEMO_";

    public static final String ROUTING_KEY = "ROUTING_KEY_";

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
