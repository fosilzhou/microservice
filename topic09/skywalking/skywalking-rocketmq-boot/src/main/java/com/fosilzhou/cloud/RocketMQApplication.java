package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 插件：
 * apm-rocketmq-3.x-plugin
 * apm-rocketmq-4.x-plugin
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-rocketmq -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class RocketMQApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(RocketMQApplication.class, args);
	}

}
