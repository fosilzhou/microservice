package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 插件：
 * apm-spring-cloud-gateway-2.x-plugin
 * apm-webflux-5.x-plugin
 *
 * # 复制插件到 plugins 目录
 * $ cp optional-plugins/apm-spring-cloud-gateway-2.x-plugin-6.6.0.jar plugins/
 * $ cp optional-plugins/apm-spring-webflux-5.x-plugin-6.6.0.jar plugins/
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-gateway -Dskywalking.collector.backend_service=10.10.0.235:11800
 *
 * 先启动 `skywalking-springmvc` 和 `skywalking-feign` 关联服务
 */
@SpringBootApplication
public class GatewayApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(GatewayApplication.class, args);
	}

}
