package com.fosilzhou.cloud.entity;

import org.springframework.data.annotation.Id;

public class User {
    @Id
    private String id;

    private String name;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public User() {
    }

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
