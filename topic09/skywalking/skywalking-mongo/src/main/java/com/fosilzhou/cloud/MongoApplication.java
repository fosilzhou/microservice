package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 插件：
 * apm-mongodb-2.x-plugin
 * apm-mongodb-3.x-plugin
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-mongo -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class MongoApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MongoApplication.class, args);
	}

}
