package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	private MongoTemplate mongoTemplate;

	@GetMapping("/user/{key}")
	public R<User> getById(@PathVariable("key")String id) {
		User user = mongoTemplate.findOne(new Query(Criteria.where("_id").is(id)), User.class);
		if(user != null) {
			return new R<User>(200, "查询成功！", user);
		}else {
			user = new User();
			user.setId(id);
			user.setName("心随梦飞");
			mongoTemplate.insert(user);
			return new R<User>(500, "没有记录！");
		}
	}
}
