package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.producer.DemoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	private DemoProducer producer;


	@GetMapping("/send/{id}")
	public void send(@PathVariable("id")Integer id) {
		producer.syncSend(id);
	}
}