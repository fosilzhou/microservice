package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 插件：
 * apm-springmvc-annotation-3.x-plugin
 * apm-springmvc-annotation-4.x-plugin
 * apm-springmvc-annotation-5.x-plugin
 * apm-springmvc-annotation-commons
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-springmvc -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class SpringMvcApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringMvcApplication.class, args);
	}

}
