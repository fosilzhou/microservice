package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

	@GetMapping("/user/{id}")
	public R<User> getById(@PathVariable("id")Integer id) {
		User user = new User();
		user.setId(id);
		user.setName("心随梦飞");
		if(id == 2){
			throw new RuntimeException("查无此人异常!");
		}
		return new R<User>(200, "查询成功！", user);
	}

	@GetMapping("/info")
	public Map<String, String> getById(HttpServletRequest request) {

		Map<String, String> resultMap = new HashMap<>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()){
			System.out.println(headerNames.nextElement());
		}
		String userAgent = request.getHeader("user-agent");

		Browser browser = UserAgent.parseUserAgentString(request.getHeader("User-Agent")).getBrowser();
		Version version = browser.getVersion(request.getHeader("User-Agent"));
		System.out.println(browser.getName() +"-->"+ version.getVersion());
		System.out.println(userAgent);
		return null;
	}

}
