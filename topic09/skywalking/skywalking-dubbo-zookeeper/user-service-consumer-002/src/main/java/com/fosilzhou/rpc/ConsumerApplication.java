package com.fosilzhou.rpc;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

/**
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-user-service-consumer-002 -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class ConsumerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ConsumerApplication.class, args);
	}

	@Component
	public class UserServiceTest implements CommandLineRunner {

		private final Logger logger = LoggerFactory.getLogger(getClass());

		@Reference(version = "${dubbo.consumer.UserService.version}")
		private UserService userService;

		@Override
		public void run(String... args) throws Exception {
			User user = userService.getById(1);
			logger.info("[run][发起一次 Dubbo RPC 请求，获得用户名为({})", user.getName());
		}

	}
}
