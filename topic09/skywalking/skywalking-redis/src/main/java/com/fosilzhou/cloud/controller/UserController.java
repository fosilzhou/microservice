package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	private StringRedisTemplate redisTemplate;

	@GetMapping("/user/{key}")
	public R<String> getById(@PathVariable("key")String key) {
		String result = redisTemplate.opsForValue().get(key);
		if(result != null) {
			return new R<String>(200, "查询成功！", result);
		}else {
			redisTemplate.opsForValue().set(key,"心随梦飞");
			return new R<String>(500, "没有记录！");
		}
	}
}
