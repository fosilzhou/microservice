package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 插件：
 * apm-redisson-3.x-plugin
 * apm-jedis-2.x-plugin
 * apm-lettuce-5.x-plugin
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-redis -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class RedisApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(RedisApplication.class, args);
	}

}
