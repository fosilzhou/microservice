package com.fosilzhou.rpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-user-service-provider-003 -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class ProviderApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ProviderApplication.class, args);
	}

}
