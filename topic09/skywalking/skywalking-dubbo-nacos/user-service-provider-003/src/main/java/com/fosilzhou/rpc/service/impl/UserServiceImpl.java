package com.fosilzhou.rpc.service.impl;

import javax.annotation.Resource;

import com.fosilzhou.rpc.api.UserService;
import com.fosilzhou.rpc.entity.User;
import com.fosilzhou.rpc.mapper.UserMapper;
import org.apache.dubbo.config.annotation.Service;


@Service(protocol = "dubbo", version = "1.0.0")
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;

	@Override
	public User getById(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}
	
	
}
