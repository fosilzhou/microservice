package com.fosilzhou.cloud.feign;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "skywalking-springmvc", url = "http://127.0.0.1:18080")
public interface UserServiceFeignClient {

    @GetMapping("/user/{id}")
    R<User> getById(@PathVariable("id")Integer id);

}