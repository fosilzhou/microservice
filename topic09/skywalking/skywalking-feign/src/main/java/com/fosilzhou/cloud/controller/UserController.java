package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;
import com.fosilzhou.cloud.feign.UserServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	private UserServiceFeignClient userServiceFeignClient;


	@GetMapping("/user/{id}")
	public R<User> getById(@PathVariable("id")Integer id) {
		return userServiceFeignClient.getById(id);
	}
}
