package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 插件：
 * apm-feign-default-http-9.x-plugin
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-feign -Dskywalking.collector.backend_service=10.10.0.235:11800
 *
 * 先启动 `skywalking-springmvc` 关联服务
 */
@SpringBootApplication
@EnableFeignClients
public class FeignApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(FeignApplication.class, args);
	}

}
