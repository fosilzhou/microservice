package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 插件：
 * apm-mysql-5.x-plugin
 * apm-mysql-6.x-plugin
 * apm-mysql-8.x-plugin
 * apm-mysql-commons
 * apm-jdbc-commons
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-mysql -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication
public class MySQLApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MySQLApplication.class, args);
	}

}
