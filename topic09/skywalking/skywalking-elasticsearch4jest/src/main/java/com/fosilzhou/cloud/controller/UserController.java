package com.fosilzhou.cloud.controller;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;
import com.fosilzhou.cloud.repository.ESUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class UserController {

	@Autowired
	ESUserRepository userRepository;


	@GetMapping("/user/{id}")
	public R<User> getById(@PathVariable("id")Integer id) {
		Optional<User> optional = userRepository.findById(id);
		if(optional.isPresent()){
			return new R<User>(200, "查询成功！", optional.get());
		}else{
			User user = new User();
			user.setId(id);
			user.setName("心随梦飞");
			userRepository.save(user);
			return new R<User>(500, "没有记录！");
		}
	}
}