package com.fosilzhou.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;

/**
 * 插件：
 * apm-httpClient-4.x-plugin
 *
 * apm-elasticsearch-5.x-plugin （有问题）暂时只支持 transport-client 5.2.x-5.6.x 版本。如果使用 Elasticsearch TCP 协议的客户端，不能使用 6.X 开始的版本
 * apm-elasticsearch-6.x-plugin （有问题）暂时只支持 Elasticsearch Rest 协议的客户端
 * 一般不会直接使用 Elasticsearch 原生的 TCP 协议和 Rest 协议的客户端，
 * 而是采用 Spring Data Elasticsearch 库。该库的主流版本是基于 Elasticsearch TCP 协议的客户端，并且使用 transport-client 的 6.X 开始的版本。
 * 因此，在我们使用 Spring Data Elasticsearch 主流版本的情况下，SkyWalking 暂时无法实现对 Elasticsearch 的链路追踪
 *
 * Jest 是基于 Elasticsearch Rest 协议的第三方客户端，其内部采用 HttpClient 发起 HTTP 请求。
 * 因此，我们可以采用 SkyWalking 的 httpClient-4.x-plugin 插件，间接实现对 Elasticsearch 的链路追踪。
 *
 * VM options:
 * -javaagent:E:/fosilzhou/microservice/soft/skywalking-6.5.0/agent/skywalking-agent.jar -Dskywalking.agent.service_name=skywalking-elasticsearch4jest -Dskywalking.collector.backend_service=10.10.0.235:11800
 */
@SpringBootApplication(exclude = {ElasticsearchAutoConfiguration.class, ElasticsearchDataAutoConfiguration.class})
public class Elasticsearch4JestApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Elasticsearch4JestApplication.class, args);
	}

}
