package com.fosilzhou.cloud.repository;

import com.fosilzhou.cloud.entity.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ESUserRepository extends ElasticsearchRepository<User, Integer> {
}
