package com.fosilzhou.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 操作说明
 * http://www.easyswoole.com/wstool.html
 * 用户登录：
 * ws://127.0.0.1:8080/?accessToken=路敏
 * ws://127.0.0.1:8080/?accessToken=金红
 * ws://127.0.0.1:8080/?accessToken=书辰
 * 发送单聊消息
 * {type: "SEND_TO_ONE_REQUEST",body: {toUser: "金红",msgId: "100001",content: "老婆，我爱你!"}}
 * 发送群聊消息
 * {type: "SEND_TO_ALL_REQUEST",body: {msgId: "100002",content: "嗨，大家好！"}}
 *
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
