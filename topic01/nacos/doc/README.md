# Nacos注册中心

## Nacos简介

## 注册中心原理

在使用注册中心时，一共有三种角色：服务提供者（Service Provider）、服务消费者（Service Consumer）、注册中心（Registry）。

三个角色交互如下图所示：![注册中心原理](assets/01.png)

① Provider：

- 启动时，向 Registry **注册**自己为一个服务（Service）的实例（Instance）。
- 同时，定期向 Registry 发送**心跳**，告诉自己还存活。
- 关闭时，向 Registry **取消注册**。

② Consumer：

- 启动时，向 Registry **订阅**使用到的服务，并缓存服务的实例列表在内存中。
- 后续，Consumer 向对应服务的 Provider 发起**调用**时，从内存中的该服务的实例列表选择一个，进行远程调用。
- 关闭时，向 Registry **取消订阅**。

③ Registry：

- Provider 超过一定时间未收到**心跳**时，从服务的实例列表移除。
- 服务的实例列表发生变化（新增或者移除）时，通知订阅该服务的 Consumer，从而让 Consumer 能够刷新本地缓存。

当然，不同的注册中心可能在实现原理上会略有差异。例如说，[Eureka](https://github.com/Netflix/eureka/) 注册中心，并不提供通知功能，而是 Eureka Client 自己定期轮询，实现本地缓存的更新。

另外，Provider 和 Consumer 是角色上的定义，一个服务**同时**即可以是 Provider 也可以作为 Consumer。例如说，优惠劵服务可以给订单服务提供接口，同时又调用用户服务提供的接口。

## 快速入门

### 服务注册中心

### 服务提供者

### 服务消费者