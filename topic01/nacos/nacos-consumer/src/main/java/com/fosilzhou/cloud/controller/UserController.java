package com.fosilzhou.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;

/**
 * postForObject、getForObject返回对象为响应体
 * postForEntity、getForEntity返回对象为ResponseEntity对象，包含响应体、响应头和响应状态等信息
 */
@RestController
public class UserController {

	private static final String url = "http://nacos-provider";
	
	@Autowired
	private RestTemplate restTemplate;

	@PostMapping("/user/create")
	public R<User> createObject(@RequestBody User user) {
		return restTemplate.postForObject(url+"/user/create", user, R.class);
	}

	@GetMapping("/user/{id}")
	public R<User> getByIdObject(@PathVariable("id") Integer id) {
		return restTemplate.getForObject(url+"/user/"+id, R.class);
	}

	@PostMapping("/user/entity/create")
	public R<User> createEntity(@RequestBody User user) {
		ResponseEntity<R> entity = restTemplate.postForEntity(url + "/user/create", user, R.class);
		if(entity.getStatusCode().is2xxSuccessful()){
			return entity.getBody();
		}else {
			return new R(500, "操作失败");
		}
	}

	@GetMapping("/user/entity/{id}")
	public R<User> getByIdEntity(@PathVariable("id") Integer id) {
		ResponseEntity<R> entity = restTemplate.getForEntity(url+"/user/"+id, R.class);
		if(entity.getStatusCode().is2xxSuccessful()){
			return entity.getBody();
		}else {
			return new R(500, "操作失败");
		}
	}
}
