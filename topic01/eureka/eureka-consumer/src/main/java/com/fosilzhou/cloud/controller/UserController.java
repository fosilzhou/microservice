package com.fosilzhou.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fosilzhou.cloud.entity.R;
import com.fosilzhou.cloud.entity.User;

@RestController
public class UserController {

	private static final String url = "http://EUREKA-PROVIDER";
	
	@Autowired
	private RestTemplate restTemplate;

	@PostMapping("/user/create")
	public R<User> create(@RequestBody User user) {
		return restTemplate.postForObject(url+"/user/create", user, R.class);
	}

	@GetMapping("/user/{id}")
	public R<User> getById(@PathVariable("id") Integer id) {
		return restTemplate.getForObject(url+"/user/"+id, R.class);
	}
}
