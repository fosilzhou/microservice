
[文档](https://github.com/alibaba/Sentinel/wiki)

## 流控规则

![1591712376086](assets/1591712376086.png)

- 资源名：唯一的名称，默认为请求路径
- 针对来源：Sentinel可以针对调用者进行限流，填写微服务名，默认default（不区分来源）
- 阈值类型/单机阈值
  - QPS：每秒请求数量，当调用该api的QPS达到阈值时，就进行限流
  - 线程数：没调用该api的线程数达到阈值时，就进行限流
- 是否集群
- 流控模式
  - 直接：api达到限制条件时，直接限流
  - 关联：当关联的资源达到阈值时，就限流自己
  - 链路：只记录指定链路上的流量（指定资源从入口资源进来的流量，如果达到阈值就进行限流）
- 流控效果
  - 快速失败：直接失败，抛出异常
  - Warm Up：根据coldFactor的值，从阈值/coldFactor，经过预热时长，才达到设置的QPS
  - 排队等待：让请求以均匀的速度通过，对应的是漏桶算法



### 1、QPS-直接-快速失败

每秒最多接受3个请求，若超过3个请求，就直接快速失败，重定向到默认错误页面

![1591711947021](assets/1591711947021.png)

```java
@RestController
public class FlowLimitController {
    @GetMapping("/test1")
    public String test1() {
        return "test1...";
    }
}
```



### 2、线程数-直接-快速失败

每秒最多接受3个客户端请求，若超过3个，就直接快速失败，重定向到默认错误页面

![1591713487915](assets/1591713487915.png)

```java
@RestController
public class FlowLimitController {
    @GetMapping("/test1")
    public String test1() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(500);
        return "test1...";
    }
}
```



### 3、QPS-关联-快速失败

每秒最多接受3个/test2请求，若超过3个/test2请求，就限流/test1请求为快速失败，重定向到默认错误页面

![1591714515865](assets/1591714515865.png)

```java
@RestController
public class FlowLimitController {
    @GetMapping("/test1")
    public String test1() {
        return "test1...";
    }
    @GetMapping("/test2")
    public String test2(){
        return "test2...";
    }
}
```

**操作：**压测/test2请求，访问/test1看效果

使用场景：在电商系统中，当支付请求出现高峰时，对下单请求进行限流

### 4、QPS-链路-快速失败



### 5、QPS-直接-预热

WarmUp配置：默认coldFactor为3，即请求QPS从（threshold/3）开始，经过多少秒预热时长逐渐升至设定的QPS阈值

**案例：**阈值设置为10，预热时长设置为5秒。

系统初始化的阈值为10/3约等于3，即刚开始QPS阈值为3，然后经过5秒后阈值才慢慢升至到QPS阈值为10

![1591715703610](assets/1591715703610.png)

```java
@RestController
public class FlowLimitController {
    @GetMapping("/test3")
    public String test3(){
        return "test3...";
    }
}
```

使用场景：秒杀系统在开启瞬间，会有很多流量上来，很可能把系统打死，预热方式就是为了保护系统，让流量慢慢涨上来，慢慢把阈值增长到设置的阈值。（原因：主要是系统缓存需要预热）

### 6、QPS-直接-匀速排队

匀速排队：该方式会严格控制请求通过的间隔时间，也就是让请求均匀的速度通过，对应的是漏桶算法。

**案例：**每秒只接受1个QPS请求，多余的请求在后面排队等待

![1591716569732](assets/1591716569732.png)

```java
@RestController
@Slf4j
public class FlowLimitController {
    @GetMapping("/test3")
    public String test3(){
        log.info(Thread.currentThread().getName());
        return "test3...";
    }
}
```

操作：压测/test3请求，看日志输出的时间间隔

使用场景：多用于限时抢购活动


## 降级规则

![1591717552603](assets/1591717552603.png)

### 1、RT（平均响应时间）

当 1s 内持续进入 N （可配置，默认值为5）个请求，对应时刻的平均响应时间（秒级）均超过阈值（`count`，以 ms 为单位），那么在接下的时间窗口（`DegradeRule` 中的 `timeWindow`，以 s 为单位）之内，对这个方法的调用都会自动地熔断（抛出 `DegradeException`）。注意 Sentinel 默认统计的 RT 上限是 4900 ms，**超出此阈值的都会算作 4900 ms**，若需要变更此上限可以通过启动配置项 `-Dcsp.sentinel.statistic.max.rt=xxx` 来配置。  

**案例：** 每秒持续进入大于5个请求 && 平均响应时间（RT）大于200毫秒，则在未来1秒内断路器打开（保险丝跳闸），微服务不可用；当访问量降下来后断路器关闭（保险丝恢复），微服务恢复可用

![1591717739168](assets/1591717739168.png)

```java
@RestController
@Slf4j
public class FlowLimitController {
    @GetMapping("/test3")
    public String test3() throws InterruptedException {
        log.info("测试RT");
        TimeUnit.SECONDS.sleep(1);//响应时间设置为1秒
        return "test3...";
    }
}
```



### 2、异常比例

当资源的每秒请求量 >= N（可配置，默认值为5），并且每秒异常总数占通过量的比值超过阈值（`DegradeRule` 中的 `count`）之后，资源进入降级状态，即在接下的时间窗口（`DegradeRule` 中的 `timeWindow`，以 s 为单位）之内，对这个方法的调用都会自动地返回。异常比率的阈值范围是 `[0.0, 1.0]`，代表 0% - 100%

**案例：** 每秒持续进入大于5个请求 && 异常比例（秒级统计）超过阈值20%，则在未来1秒内断路器打开（保险丝跳闸），微服务不可用；当访问量降下来后断路器关闭（保险丝恢复），微服务恢复可用

![1591720024759](assets/1591720024759.png)

```java
@RestController
@Slf4j
public class FlowLimitController {
    @GetMapping("/test3")
    public String test3() throws InterruptedException {
        log.info("测试异常比例");
        int a = 1/0;
        return "test3...";
    }
}
```

### 3、异常数

当资源近 1 分钟的异常数目超过阈值之后会进行熔断。注意由于统计时间窗口是分钟级别的，若 `timeWindow` 小于 60s，则结束熔断状态后仍可能再进入熔断状态。 

**案例：每分钟QPS异常数超过设置大的阈值5，则断路器打开（保险丝跳闸），微服务不可用；当访问量降下来后断路器关闭（保险丝恢复），微服务恢复可用

![1591796281742](assets/1591796281742.png)

```java
@RestController
@Slf4j
public class FlowLimitController {
    @GetMapping("/test5")
    public String test5() throws InterruptedException {
        log.info("测试异常数");
        int a = 1/0;
        return "test5...";
    }
}
```



## 热点规则

### 1、参数索引限流

**案例：**请求中带了方法中第0个参数（即参数p1） && 每10秒访问的QPS阈值超过设置的1，则触发断路器打开

![1591798165177](assets/1591798165177.png)

```java
@RestController
public class FlowLimitController {
    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey", blockHandler = "dealTestHotKey")
    public String testHotKey(@RequestParam(value = "p1", required = false) String p1,
                             @RequestParam(value = "p2", required = false) String p2) {
        return "testHotKey...";
    }
    public String dealTestHotKey(String p1, String p2, BlockException ex) {
        return "dealTestHotKey o(╥﹏╥)o";
    }
}
```

```http
√ http://localhost:90/testHotKey?p1=111&p2=222
√ http://localhost:90/testHotKey?p1=111
× http://localhost:90/testHotKey?p2=222
```

### 2、参数指定值限流

**案例：** 当参数p1=111时，每10秒访问的QPS阈值超过3就开启限流；当参数p1=222时，每10秒访问的QPS阈值超过6就开启限流；当参数p1=333时，每10秒访问的QPS阈值超过9就开启限流；当参数p1为其他值时，每10秒访问的QPS阈值超过5就开启限流

![1591798544108](assets/1591798544108.png)


```http
http://localhost:90/testHotKey?p1=111&p2=999
http://localhost:90/testHotKey?p1=222&p2=999
http://localhost:90/testHotKey?p2=333&p2=999
http://localhost:90/testHotKey?p2=666&p2=999
```

## 系统规则



应用场景：一般用于网关的限流配置



## SentinelResource配置

```java
@RestController
public class RateLimitController {
    @GetMapping("/rate/limit")
    @SentinelResource(value = "rateLimit", blockHandler = "handleException")
    public R rateLimit() {
        return new R(200, "请求成功");
    }
    public R handleException(BlockException ex) {
        return new R(500, "服务不可用："+ex.getClass().getCanonicalName());
    }
}
```

### 按资源名称限流

![1591800796384](assets/1591800796384.png)

### 按URL地址限流

![1591800764953](assets/1591800764953.png)

### 统一限流逻辑处理

```java
public class CustomerBlockHandler {

    public R handleAjaxException(BlockException ex) {
        return new R(500, "全局Ajax请求统一限流处理："+ex.getClass().getCanonicalName());
    }

    public R handlePageException(BlockException ex) {
        return new R(500, "全局页面跳转统一限流处理："+ex.getClass().getCanonicalName());
    }
}

@RestController
public class RateLimitController {

    @GetMapping("/rate/limit/ajax")
    @SentinelResource(value = "customerBlockHandler",
            blockHandlerClass = CustomerBlockHandler.class,
            blockHandler = "handleAjaxException")
    public R rateLimitAjax() {
        return new R(200, "Ajax请求成功");
    }
}
```



## 规则持久化

