## 一、定位CPU使用率过高问题

### 1、查看所有线程信息

使用`thread`命令查看所有线程信息，同时会列出每个线程的 CPU 使用率

![1603849341931](assets/1603849341931.png)

### 2、查看CPU使用率高的线程信息

2.1、使用`thread 线程ID` 查看CPU消耗搞的线程信息，可以看到使用较高的方法和行数

![1603849721523](assets/1603849721523.png)

2.2、也可以使用`thread -n [显示线程个数]` 排序列出CPU使用率前N个线程

![1603850034155](assets/1603850034155.png)

### 3、定位CPU使用率高的代码

![1603850744914](assets/1603850744914.png)

## 二、定位线程死锁问题

### 1、查看死锁线程

线程状态为`BLOCKED` 的线程可能为出现死锁

1.1、使用`thread`命令查看所有线程信息

![1603852496534](assets/1603852496534.png)

1.2、使用`thread | grep BLOCKED`命令查看死锁线程信息

![1603852274378](assets/1603852274378.png)

### 2、检查死锁信息

使用`thread -b`命令检查死锁信息

![1603852840236](assets/1603852840236.png)

## 三、反编译线上代码

### 1、反编译类

使用`jad 完整包名.类名`反编译整个class类

![1603854350529](assets/1603854350529.png)

### 2、反编译类中的方法

使用`jad 完整包名.类名 方法名` 反编译class类中的某个方法

![1603854446155](assets/1603854446155.png)

### 3、只显示反编译源码

使用`jad --source-only 完整包名.类名 方法名` 只反编译class类中的某个方法的源码

![1603854650398](assets/1603854650398.png)

### 4、查看类的属性信息

使用`sc -d -f 完整包名.类名`反编译整个class类

![1603854878939](assets/1603854878939.png)

### 5、查看类的方法信息

使用`sm 完整包名.类名`反编译整个class类

![1603854972996](assets/1603854972996.png)

### 6、查看静态变量的值

使用`ognl '@完整包名.类名@静态变量名' --classLoaderClass [ClassLoader类名]`查看静态变量值（ClassLoader默认是SystemClassLoader）

![1603855864167](assets/1603855864167.png)

### 7、静态变量集合API相关操作

使用`ognl '@完整包名.类名@静态变量名.方法名(参数值)' --classLoaderClass [ClassLoader类名]`执行API方法

![1603856547359](assets/1603856547359.png)

## 四、程序BUG追踪

### 1、运行慢、耗时长问题追踪

使用`trace 完整包名.类名 方法名`命令跟踪统计方法执行耗时

![1603863481315](assets/1603863481315.png)

### 2、监控统计方法执行情况

使用`monitor -c 5 完整包名.类名 方法名`命令监控统计方法执行情况（`-c 5` 表示每5秒钟统计一次）

![1603866278492](assets/1603866278492.png)

## 五、观察方法执行信息

### 1、观察方法参数和返回值

使用`watch 完整包名.类名 方法名 'params, returnObj'`命令观察方法参数和返回值

> 内置核心变量：<https://arthas.aliyun.com/doc/advice-class.html>
>
> watch详细使用说明：<https://arthas.aliyun.com/doc/watch.html>

```shell
watch com.fosilzhou.arthas.controller.UserController get '{params[0],returnObj}'
watch com.fosilzhou.arthas.controller.UserController get '{params[0],returnObj.size}'
watch com.fosilzhou.arthas.controller.UserController get '{params[0],returnObj.toString()}'
```

![1603868410949](assets/1603868410949.png)

### 2、观察方法调用链路

使用`stack 完整包名.类名 方法名`命令观察方法调用链路

![1603868839982](assets/1603868839982.png)

### 3、方法调用时空隧道

> tt命令详细说明：<https://arthas.aliyun.com/doc/tt.html>

#### 3.1、记录方法调用信息

使用`tt -t 完整包名.类名 方法名`命令记录方法调用信息

![1603869249554](assets/1603869249554.png)

#### 3.2、查看方法调用信息

使用`tt -l 完整包名.类名 方法名`命令查看`3.1记录的方法调用信息`

![1603869466875](assets/1603869466875.png)

#### 3.3、查看单条记录详细信息

使用`tt -i INDEX`命令查看单条方法调用的详细信息（`INDEX`为`tt -l`命令的查询结果的INDEX列信息）

![正常调用](assets/1603869648894.png)

![异常调用](assets/1603869594801.png)

#### 3.4、重新发起调用请求

使用`tt -i INDEX -p`命令重新发起调用请求（`INDEX`为`tt -l`命令的查询结果的INDEX列信息）

![1603869794403](assets/1603869794403.png)



## 附注

测试完整代码：<https://gitee.com/fosilzhou/microservice/tree/master/topic00/arthas-demo>