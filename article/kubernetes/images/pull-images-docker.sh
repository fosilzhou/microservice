#!/bin/bash

for imagepath in $(cat ./imagepath.txt)
do
  imagename=$(echo $imagepath | awk -F '/' '{print $NF}')
  
  docker pull fosilzhou/$imagename
  docker tag fosilzhou/$imagename $imagepath
  docker rmi fosilzhou/$imagename

done
