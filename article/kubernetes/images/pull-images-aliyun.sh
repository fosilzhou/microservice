#!/bin/bash

for imagepath in $(cat ./imagepath.txt)
do
  imagename=$(echo $imagepath | awk -F '/' '{print $NF}')
  
  docker pull registry.cn-shenzhen.aliyuncs.com/fosilzhou/$imagename
  docker tag registry.cn-shenzhen.aliyuncs.com/fosilzhou/$imagename $imagepath 
  docker rmi registry.cn-shenzhen.aliyuncs.com/fosilzhou/$imagename

done
