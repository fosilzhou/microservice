## 控制器之Deployment(简称：deploy)

### 简介

Deployment是用于部署应用的对象。是Kubernetes中最常用的一个对象，它为ReplicaSet和Pod的创建提供了一种声明式的定义方法，无需手动创建ReplicaSet和Pod对象（使用Deployment而不直接创建ReplicaSet是因为Deployment对象拥有许多ReplicaSet没有的特性，例如滚动升级和回滚）

### 资源清单格式

```yaml
apiVersion: apps/v1
kind: Deployment
metadata: 
  name: myapp-deploy
  namespace: default
spec: 
  replicas: 2
  selector:
    matchLabels:  
      app: myapp
      release: canary
  template:
    metadata:
      name: myapp
      labels: 
        app: myapp
        release: canary
        environment: zlm
    spec: 
      containers: 
      - name: myapp
        image: ikubernetes/myapp:v1
        ports: 
        - name: http
          containerPort: 80

```

### 常用命令

```shell
## 根据yaml创建或更新控制器
kubectl apply -f deploy-demo.yaml
## 查看deploy控制器信息列表
kubectl get deploy -o wide
## 查看rs控制器信息列表
kubectl get rs -o wide
## 查看pods信息列表
kubectl get pods -o wide
## 更新deploy信息（集群数由2改为3）
vim deploy-demo.yaml  ##集群数由2改为3
kubectl apply -f deploy-demo.yaml ##执行此命令后集群数立即改为3
## 查看deploy详细信息
kubectl describe deploy myapp-deploy
## 动态监控pods变化
kubectl get pods -l app=myapp -w
## 更新deploy信息（镜像版本由v1改为v2）
vim deploy-demo.yaml  ##镜像版本由v1改为v2
kubectl apply -f deploy-demo.yaml
## 查看滚动历史记录
kubectl rollout history deployment myapp-deploy
## 通过打补丁方式修改清单配置信息（集群数由3改为5）
kubectl patch deployment myapp-deploy -p '{"spec":{"replicas":5}}'
## 通过打补丁方式修改更新策略
kubectl patch deployment myapp-deploy -p '{"spec":{"strategy":{"rollingUpdate":{"maxSurge":1,"maxUnavailable":0}}}}'
## 实现金丝雀发布（仅给一台或一小部分机器更新新版本）
kubectl set image deployment myapp-deploy myapp=ikubernetes/myapp:v3 && kubectl rollout pause deployment myapp-deploy
## 查看发布状态信息
kubectl rollout status deployment myapp-deploy
## 继续更新所有节点
kubectl rollout resume deployment myapp-deploy
## 回滚到指定的版本（回滚到第1个版本）
kubectl rollout undo deployment myapp-deploy --to-revision=1
```


