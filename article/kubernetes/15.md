### ConifgMap存储解析

configmap是让配置文件从镜像中解耦，让镜像的可移植性和可复制性。许多应用程序会从配置文件、命令行参数或环境变量中读取配置信息。这些配置信息需要与docker image解耦，总不能每修改一个配置就重做一个image吧？ConfigMap API给我们提供了向容器中注入配置信息的机制，ConfigMap可以被用来保存单个属性，也可以用来保存整个配置文件或者JSON二进制大对象。

ConfigMap API资源用来保存key-value pair配置数据，这个数据可以在pods里使用，或者被用来为像controller一样的系统组件存储配置数据。虽然ConfigMap跟Secrets类似，但是ConfigMap更方便的处理不含敏感信息的字符串。 注意：ConfigMaps不是属性配置文件的替代品。ConfigMaps只是作为多个properties文件的引用。可以把它理解为Linux系统中的/etc目录，专门用来存储配置文件的目录。下面举个例子，使用ConfigMap配置来创建Kuberntes Volumes，ConfigMap中的每个data项都会成为一个新文件。

ConfigMap主要用于向Pod注入非敏感数据，使用时，用户将数据直接存储在ConfigMap对象当中，然后Pod通过使用ConfigMap卷进行引用，实现容器的配置文件集中定义和管理。

### ConfigMap创建方式

#### 1、通过--from-literal

每个--from-literal对应一个信息条目

```shell
[root@foresee701 ~]# kubectl create configmap nginx-config --from-literal=nginx_port=80 --from-literal=server_name=myapp.zlmlive.com
configmap/nginx-config created
[root@foresee701 ~]# kubectl get cm
NAME           DATA   AGE
nginx-config   2      14s
[root@foresee701 ~]# kubectl describe cm nginx-config
Name:         nginx-config
Namespace:    default
Labels:       <none>
Annotations:  <none>

Data
====
nginx_port:
----
80
server_name:
----
myapp.zlmlive.com
Events:  <none>
```

#### 2、通过--from-file

```shell
[root@foresee701 ~]# vim www.conf 
server {
    server_name myapp.zlmlive.com;
    listen 80;
    root /data/web/html;
}
[root@foresee701 ~]# kubectl create configmap nginx-www --from-file=./www.conf
configmap/nginx-www created
[root@foresee701 ~]# kubectl get cm nginx-www -o yaml
apiVersion: v1
data:
  www.conf: |
    server {
        server_name myapp.zlmlive.com;
        listen 80;
        root /data/web/html;
    }
kind: ConfigMap
metadata:
  creationTimestamp: "2019-06-11T06:31:43Z"
  name: nginx-www
  namespace: default
  resourceVersion: "3567771"
  selfLink: /api/v1/namespaces/default/configmaps/nginx-www
  uid: 946413ed-8c12-11e9-88b7-000c29087acc
```

### ConfigMap使用方式

#### 1、环境变量方式注入

```yaml
[root@foresee701 yaml]# vim pod-configmap01.yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-configmap01
  namespace: default
spec:
  containers:
  - name: myapp
    image: ikubernetes/myapp:v1
    ports:
    - name: http
      containerPort: 80 
    env:
    - name: NGINX_SERVER_PORT
      valueFrom:
        configMapKeyRef:
          name: nginx-config
          key: nginx_port
    - name: NGINX_SERVER_NAME
      valueFrom:
        configMapKeyRef:
          name: nginx-config
          key: server_name
[root@foresee701 yaml]# kubectl apply -f pod-configmap01.yaml 
pod/pod-configmap01 created
[root@foresee701 yaml]# kubectl exec -it pod-configmap01 -- /bin/sh
/ # echo $NGINX_SERVER_PORT
80
[root@foresee701 yaml]# kubectl edit cm nginx-config ##nginx_port由80改为8080
configmap/nginx-config edited
[root@foresee701 yaml]# kubectl exec -it pod-configmap01 -- /bin/sh
/ # echo $NGINX_SERVER_PORT
80
```

修改端口，可以发现使用环境变化注入pod中的端口不会根据配置的更改而变化

#### 2、存储卷方式挂载

Volume形式的ConfigMap支持动态更新

```yaml
[root@foresee701 yaml]# vim pod-configmap02.yaml 
apiVersion: v1
kind: Pod
metadata:
  name: pod-configmap02
  namespace: default
spec:
  containers:
  - name: myapp
    image: ikubernetes/myapp:v1
    ports:
    - name: http
      containerPort: 80
    volumeMounts: 
    - name: nginxconf
      mountPath: /etc/nginx/config.d/
      readOnly: true
  volumes:
  - name: nginxconf
    configMap:
      name: nginx-config
[root@foresee701 yaml]# kubectl apply -f pod-configmap02.yaml 
pod/pod-configmap02 created
[root@foresee701 yaml]# kubectl exec -it pod-configmap02 -- /bin/sh
/ # cd /etc/nginx/config.d
/etc/nginx/config.d # cat nginx_port
8080
[root@foresee701 ~]# kubectl edit cm nginx-config ##nginx_port由8080改为8090
configmap/nginx-config edited
[root@foresee701 yaml]# kubectl exec -it pod-configmap02 -- /bin/sh
/ # cd /etc/nginx/config.d
/etc/nginx/config.d # cat nginx_port
8090

[root@foresee701 yaml]# kubectl delete -f pod-configmap02.yaml
[root@foresee701 yaml]# kubectl delete -f pod-configmap01.yaml
```

### 简单案例

实现nginx配置文件动态加载

```yaml
[root@foresee701 yaml]# vim pod-configmap-nginx.yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-configmap-nginx
  namespace: default
spec:
  containers:
  - name: myapp
    image: ikubernetes/myapp:v1
    ports:
    - name: http
      containerPort: 80
    volumeMounts: 
    - name: nginxconf
      mountPath: /etc/nginx/conf.d/
      readOnly: true
  volumes:
  - name: nginxconf
    configMap:
      name: nginx-www
[root@foresee701 yaml]# kubectl apply -f pod-configmap-nginx.yaml 
pod/pod-configmap-nginx created
[root@foresee701 yaml]# kubectl exec -it pod-configmap-nginx -- /bin/sh
/ # nginx -T
  ....
# configuration file /etc/nginx/conf.d/www.conf:
server {
    server_name myapp.zlmlive.com;
    listen 80;
    root /data/web/html;
}

/ # mkdir -p /data/web/html/
/ # vi /data/web/html/index.html
<h1> Nginx Server Configured By ConfigMap... </h1>
/ # 

[root@foresee701 ~]# kubectl get pods -o wide
NAME                           READY   STATUS    RESTARTS   AGE     IP            NODE         NOMINATED NODE   READINESS GATES
pod-configmap-nginx            1/1     Running   0          7m46s   10.244.2.53   foresee704   <none>           <none>
[root@foresee701 ~]# vim /etc/hosts
### k8s hosts
10.244.2.53 myapp.zlmlive.com
[root@foresee701 ~]# curl myapp.zlmlive.com
<h1> Nginx Server Configured By ConfigMap。 </h1>

[root@foresee701 ~]# kubectl edit cm nginx-www  ##nginx_port由80改为8080
[root@foresee701 ~]# kubectl exec -it pod-configmap-nginx -- /bin/sh
/ # nginx -T
  ....
# configuration file /etc/nginx/conf.d/www.conf:
server {
    server_name myapp.zlmlive.com;
    listen 8080;
    root /data/web/html;
}

/ # nginx -s reload  ## 手工重载nginx配置信息
2019/06/11 07:28:33 [notice] 19#19: signal process started
/etc/nginx/conf.d # ls -l   ##configMap挂载是以link方式链接文件
total 0
lrwxrwxrwx    1 root     root     15 Jun 11 07:15 www.conf -> ..data/www.conf

[root@foresee701 ~]# curl myapp.zlmlive.com
curl: (7) Failed connect to myapp.zlmlive.com:80; Connection refused
[root@foresee701 ~]# curl myapp.zlmlive.com:8080
<h1> Nginx Server Configured By ConfigMap。 </h1>

[root@foresee701 ~]# kubectl explain pods.spec.volumes.configMap.items ##挂载configMap中指定几项配置
```

