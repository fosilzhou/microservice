# Kubernetes网络通信

## 网络通信

- 容器间通信：同一个Pod类的多个容器间通信，lo
- Pod间通信：Pod IP <==>Pod IP
- Pod与Service通信：Pod IP <==> ClusterIP
- Service与集群外部客户端通信

## CNI（容器网络插件）

- flannel
- calico
- canel：flannel + calico
- kube-router

## 网络解决方案

- 虚拟网桥
- 多路复用:MacVLAN
- 硬件交换：SR-IOV

> **kubelet网络配置文件位置：/etc/cni/net.d/**

## flannel

支持多种后端

- VxLAN
  - vxlan
  - Directruting：直接路由通信
- host-gw：Host Gateway
- udp：

### flannel配置参数

- Network：flannel使用的CIDR格式的网络地址，用于为Pod配置网络功能
  - 10.244.0.0/16 --> 16位掩码（支持255个节点即255台机器集群）
    - master:   10.244.0.0/24
    - node01:  10.244.1.0/24
    - ......
    - node255: 10.244.255.0/24
  - 10.0.0.0/8 --> 8位掩码（）
    - 10.0.0.0/24
    - .....
    - 10.255.255.0/24
- SubnetLen：把Network切分子网供各节点使用时，使用多长的掩码进行切分，默认为24位
- SubnetMin：10.244.10.0/24
- SubnetMax：10.244.100.0/24
- Backend：vxlan(默认)、host-gw、udp



### 常用命令

```shell
[root@foresee701 ~]# kubectl get daemonset -n kube-system 
NAME                      DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR                     AGE
kube-flannel-ds-amd64     3         3         3       3            3           beta.kubernetes.io/arch=amd64     34d
kube-flannel-ds-arm       0         0         0       0            0           beta.kubernetes.io/arch=arm       34d
kube-flannel-ds-arm64     0         0         0       0            0           beta.kubernetes.io/arch=arm64     34d
kube-flannel-ds-ppc64le   0         0         0       0            0           beta.kubernetes.io/arch=ppc64le   34d
kube-flannel-ds-s390x     0         0         0       0            0           beta.kubernetes.io/arch=s390x     34d
kube-proxy                3         3         3       3            3           <none>                            34d

[root@foresee701 ~]# kubectl get pods -n kube-system -o wide 
NAME                                    READY   STATUS    RESTARTS   AGE    IP            NODE      
kube-flannel-ds-amd64-crkm6             1/1     Running   0          34d    10.10.0.181   foresee702
kube-flannel-ds-amd64-kzlhw             1/1     Running   0          33d    10.10.0.53    foresee704
kube-flannel-ds-amd64-l252t             1/1     Running   0          34d    10.10.0.167   foresee701

[root@foresee701 ~]# kubectl get configmap kube-flannel-cfg -o json -n kube-system
{
    "apiVersion": "v1",
    "data": {
        "cni-conf.json": "{\n  \"name\": \"cbr0\",\n  \"plugins\": [\n    {\n      \"type\": \"flannel\",\n      \"delegate\": {\n        \"hairpinMode\": true,\n        \"isDefaultGateway\": true\n      }\n    },\n    {\n      \"type\": \"portmap\",\n      \"capabilities\": {\n        \"portMappings\": true\n      }\n    }\n  ]\n}\n",
        "net-conf.json": "{\n  \"Network\": \"10.244.0.0/16\",\n  \"Backend\": {\n    \"Type\": \"vxlan\"\n  }\n}\n"
    },
    "kind": "ConfigMap",
    "metadata": {
        "annotations": {
            "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"v1\",\"data\":{\"cni-conf.json\":\"{\\n  \\\"name\\\": \\\"cbr0\\\",\\n  \\\"plugins\\\": [\\n    {\\n      \\\"type\\\": \\\"flannel\\\",\\n      \\\"delegate\\\": {\\n        \\\"hairpinMode\\\": true,\\n        \\\"isDefaultGateway\\\": true\\n      }\\n    },\\n    {\\n      \\\"type\\\": \\\"portmap\\\",\\n      \\\"capabilities\\\": {\\n        \\\"portMappings\\\": true\\n      }\\n    }\\n  ]\\n}\\n\",\"net-conf.json\":\"{\\n  \\\"Network\\\": \\\"10.244.0.0/16\\\",\\n  \\\"Backend\\\": {\\n    \\\"Type\\\": \\\"vxlan\\\"\\n  }\\n}\\n\"},\"kind\":\"ConfigMap\",\"metadata\":{\"annotations\":{},\"labels\":{\"app\":\"flannel\",\"tier\":\"node\"},\"name\":\"kube-flannel-cfg\",\"namespace\":\"kube-system\"}}\n"
        },
        "creationTimestamp": "2019-05-14T08:20:50Z",
        "labels": {
            "app": "flannel",
            "tier": "node"
        },
        "name": "kube-flannel-cfg",
        "namespace": "kube-system",
        "resourceVersion": "2457",
        "selfLink": "/api/v1/namespaces/kube-system/configmaps/kube-flannel-cfg",
        "uid": "2f15135c-7621-11e9-b9ec-000c29087acc"
    }
}
```

