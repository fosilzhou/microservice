## 下载docker仓库镜像
docker pull mirrorgooglecontainers/defaultbackend-amd64:1.5
## 修改镜像标签
docker tag mirrorgooglecontainers/defaultbackend-amd64:1.5 k8s.gcr.io/defaultbackend-amd64:1.5
## 删除原来镜像
docker rmi mirrorgooglecontainers/defaultbackend-amd64:1.5
