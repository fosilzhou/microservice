docker pull mirrorgooglecontainers/metrics-server-amd64:v0.3.3
docker pull mirrorgooglecontainers/addon-resizer:1.8.5
docker tag mirrorgooglecontainers/metrics-server-amd64:v0.3.3 k8s.gcr.io/metrics-server-amd64:v0.3.3
docker tag mirrorgooglecontainers/addon-resizer:1.8.5 k8s.gcr.io/addon-resizer:1.8.5
docker rmi mirrorgooglecontainers/metrics-server-amd64:v0.3.3
docker rmi mirrorgooglecontainers/addon-resizer:1.8.5