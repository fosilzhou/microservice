# Dockerfile指令

## FROM指令

From指令是最重要的且必须为Dockerfile文件开篇的第一个非注释行，用于为映像文件构建过程指定基准镜像，后续的指令运行于此基准镜像所提供的的运行环境。

实际中，基准镜像可以是任何空镜像文件，默认情况下，docker build会在docker主机上查找指定的镜像文件，在其不存在时，这会从Docker Hub Registry上拉取所需的镜像文件。

语法：

- FROM `<repository>[:<tag>]`或
- FROM `<repository>@<digest>`
  - repository：指定作为基础镜像名称
  - tag：基础镜像的标签

## LABEL指令

语法：

​	LABEL `<key>=<value> <key>=<value>...`

## COPY指令

用于将Docker主机上的文件复制到创建新的映射文件

语法：

- COPY `<src> <dest>`或
- COPY `["<src>","<dest>"]`
  - src：复制的源文件或目录，支持使用通配符
  - dest：目标路径，即正常创建的镜像的文件系统路径，强烈建议使用绝对路径
  - 注意：如果路径中有空格字符时，使用第二种格式
- 文件复制准则
  - src必须在buld上下文中的路径，不能是其父目录中的文件
  - 如果src是目录，这其内部文件或子目录会被递归复制，但是src目录不会被复制
  - 如果指定了多个src，或src使用了通配符，则dest必须是一个目录，且必须以/结尾
  - 如果dest不存在，它将会自动创建，包括其父目录（即多级目录）

```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL author="fosilzhou <fosilzhou@qq.com>"  dockerInfo="演示Dockerfile示例"
COPY index.html /data/web/html/
COPY yum.repos.d /etc/yum.repos.d/
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# vim index.html
<h1>BusyBox httpd Server...</h1>
[root@foresee703 ~]# cp -r /etc/yum.repos.d/ ./
[root@foresee703 ~]# docker build -t tinyhttpd:v0.1 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm tinyhttpd:v0.1 cat /data/web/html/index.html
[root@foresee703 ~]# docker run --name tinyweb01 --rm tinyhttpd:v0.1 ls /etc/yum.repos.d/
```

说明：每个镜像指令生成一个镜像层，层越多，联合挂在的效率越差；

## ADD指令

ADD指令类似于COPY指令，ADD指令支持使用tar文件和URL路径

语法：

- ADD `<src> <dest>`
- ADD `["<src>", "<dest>"]`

操作准则：

- 同COPY指令
- 如果src为URL且dest不以/结尾，则src指定的文件将被下载并直接创建为dest；如果dest以/结尾，则文件名为URL指定的文件将被下载并保存为dest/filename
- 如果src为本地系统上压缩格式为tar的文件，它将会被解压为目录，其行为类似于tar -x命令；如果是URL获取的tar文件将不会被解压
- 如果src有多个，或其间接或直接使用通配符，则dest必须以/结尾的目录路径；如果不以/结尾，则视为普通文件，src的内容直接写入到dest

```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL author="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
COPY index.html /data/web/html/
COPY yum.repos.d /etc/yum.repos.d/
ADD http://nginx.org/download/nginx-1.15.2.tar.gz /usr/local/src/
ADD nginx-1.15.2.tar.gz /usr/local/src/
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# wget http://nginx.org/download/nginx-1.15.2.tar.gz
[root@foresee703 ~]# docker build -t tinyhttpd:v0.2 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm tinyhttpd:v0.2 ls /usr/local/src
```

## WORKDIR指令

用于为Dockerfile中所有RUN、CMD、ENTRYPOINT、COPY和ADD指令设置工作目录（类似于cd命令）

语法：

WORKDIR `<dirpath>`

- 在Dockerfile中，WORKDIR指令可以出现多次，其路径可以为相对路径，不过其是相对前一个WORKDIR指令指定的路径
- WORKDIR可以调用由ENV指令定义的变量

```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL author="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
COPY index.html /data/web/html/
COPY yum.repos.d /etc/yum.repos.d/
WORKDIR /usr/local/src/
# ADD http://nginx.org/download/nginx-1.15.2.tar.gz /usr/local/src/
ADD nginx-1.15.2.tar.gz ./
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# docker build -t tinyhttpd:v0.3 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm tinyhttpd:v0.3 ls /usr/local/src
```

## VOLUME指令

用于在镜像中创建一个挂载点目录，以挂载Docker host上的卷或其他容器上的卷

语法：

- VOLUME` <mountpoint>`
- VOLUME `["<mountpoint>"]`

如果挂载点目录路径下存在次文件，docker run命令会在卷挂载完成后将此前所有文件复制到新挂载的卷中

```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL author="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
COPY index.html /data/web/html/
COPY yum.repos.d /etc/yum.repos.d/
WORKDIR /usr/local/src/
# ADD http://nginx.org/download/nginx-1.15.2.tar.gz /usr/local/src/
ADD nginx-1.15.2.tar.gz ./
VOLUME /data/mysql
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# docker build -t tinyhttpd:v0.4 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm tinyhttpd:v0.4 mount | grep data
```

## EXPOSE指令

用于为容器打开指定要监听的端口以实现与外部通信

语法：

EXPOSE `<port>[/<protocol>][<port>[/<protocol>]]`

- protocol用于指定传输层协议，可为tcp或udp二选一，默认为tcp协议
- EXPOSE 指令可一次指定多个端口；例如：EXPOSE 11211/udp 11211/tcp

```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL author="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
COPY index.html /data/web/html/
COPY yum.repos.d /etc/yum.repos.d/
WORKDIR /usr/local/src/
# ADD http://nginx.org/download/nginx-1.15.2.tar.gz /usr/local/src/
ADD nginx-1.15.2.tar.gz ./
VOLUME /data/mysql
EXPOSE 80/tcp
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# docker build -t tinyhttpd:v0.5 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm tinyhttpd:v0.5 /bin/httpd -f -h /data/web/html/
[root@localhost ~]# curl `docker inspect --format='{{.NetworkSettings.IPAddress}}' tinyweb01`
<h1>BusyBox httpd Server...</h1>
[root@foresee703 ~]# docker port tinyweb01
[root@foresee703 ~]# docker kill tinyweb01
tinyweb01
[root@foresee703 ~]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.5 /bin/httpd -f -h /data/web/html/
[root@localhost ~]# docker port tinyweb01
80/tcp -> 0.0.0.0:32768
[root@localhost ~]# curl `docker inspect --format='{{.NetworkSettings.IPAddress}}' tinyweb01`
<h1>BusyBox httpd Server...</h1>
[root@localhost ~]# curl 127.0.0.1:32768
<h1>BusyBox httpd Server...</h1>
```

## ENV指令

用于为镜像定义所需的环境变量，并可被Dockerfile文件中位于其后的其他指令（如：ENV、ADD、COPY）所调用

调用格式为`$variable_name`或`${variable_name}`

语法

- ENV `<key> <value>`
- ENV `<key>=<value> ...`

第一种格式：key之后所有内容均会被视作其value的组成部分

第二种格式：可一次设置多个变量，每个变量为一个key=value键值对，如果value中包含空格可以使用反斜杠(`\`)转义，可以对value加引号标识；另外反斜杠也可用于续行

```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL author="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
# ENV DOC_ROOT /data/web/html/
ENV DOC_ROOT=/data/web/html/ \
    JAVA_HOME=/usr/local/jdk \
    WEB_SERVER_PACKAGE="nginx-1.15.2"
# COPY index.html $DOC_ROOT
COPY index.html ${DOC_ROOT:-/data/web/html/}
COPY yum.repos.d /etc/yum.repos.d/
WORKDIR /usr/local/src/
# ADD http://nginx.org/download/nginx-1.15.2.tar.gz /usr/local/src/
ADD ${WEB_SERVER_PACKAGE}.tar.gz ./
VOLUME /data/mysql
EXPOSE 80/tcp
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# docker build -t tinyhttpd:v0.6 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.6 printenv
[root@foresee703 ~]# docker run --name tinyweb01 --rm -P -e JAVA_HOME=/usr/jdk tinyhttpd:v0.6 printenv
```

## RUN指令(难点)

用于指定docker build过程中运行的程序，可以是任何命令

语法：

- RUN `<commond>`

- RUN `["<executable>","参数1","参数2"...]`


```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL author="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
# ENV DOC_ROOT /data/web/html/
ENV DOC_ROOT=/data/web/html/ \
    JAVA_HOME=/usr/local/jdk \
    WEB_SERVER_PACKAGE="nginx-1.15.2"
# COPY index.html $DOC_ROOT
COPY index.html ${DOC_ROOT:-/data/web/html/}
COPY yum.repos.d /etc/yum.repos.d/
WORKDIR /usr/local/src/
ADD http://nginx.org/download/${WEB_SERVER_PACKAGE}.tar.gz /usr/local/src/
# ADD ${WEB_SERVER_PACKAGE}.tar.gz ./
VOLUME /data/mysql
EXPOSE 80/tcp
RUN cd /usr/local/src && \
    tar -xf ${WEB_SERVER_PACKAGE}.tar.gz
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# docker build -t tinyhttpd:v0.7 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.7 ls /usr/local/src
```

## CMD指令(难点)

类似于RUN命令，CMD指令也可以运行任何命令或应用程序，不过二者运行时间点不同

- RUN指令运行于镜像文件构建时
- CMD指令运行于镜像文件启动容器时
- CMD指令首要目的在于启动容器默认要运行的程序，其结束后，容器也将终止
- CMD指定的命令可以被docker run命令行选项覆盖
- Dockerfile可以存在多个CMD指令，但只有最后一个有效

语法：

- CMD `<commond>`
- CMD `["<executable>","参数1","参数2"...]`
- CMD `["参数1","参数2"...]`

第三种格式用于为ENTRYPOINT指令提供默认参数

```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL maintainer="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
ENV DOC_ROOT=/data/web/html/
RUN mkdir -p ${DOC_ROOT} && \
    echo '<h1>Busybox httpd Server.</h1>' > ${DOC_ROOT}/index.html
CMD /bin/httpd -f -h ${DOC_ROOT}
# CMD ["/bin/httpd", "-f" "-h ${DOC_ROOT}"]
# CMD ["/bin/sh","-c","/bin/httpd", "-f" "-h ${DOC_ROOT}"]
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# docker build -t tinyhttpd:v0.8 ./
[root@localhost docker]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.8
[root@localhost ~]# curl `docker inspect --format='{{.NetworkSettings.IPAddress}}' tinyweb01`
<h1>Busybox httpd Server.</h1>
[root@foresee703 ~]# docker kill tinyweb01

[root@foresee703 ~]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.8 cat /data/web/html/index.html

######## CMD ["/bin/httpd", "-f" "-h ${DOC_ROOT}"]
[root@localhost docker]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.8
/bin/sh: [/bin/httpd,: not found
[root@foresee703 ~]# docker rm tinyweb01
```

## ENTRYPOINT指令(难点)


```dockerfile
# Description 演示Dockerfile示例
FROM busybox:latest
LABEL maintainer="fosilzhou <fosilzhou@qq.com>" dockerInfo="演示Dockerfile示例"
ENV DOC_ROOT=/data/web/html/
RUN mkdir -p ${DOC_ROOT} && \
    echo '<h1>Busybox httpd Server.</h1>' > ${DOC_ROOT}/index.html
# CMD /bin/httpd -f -h ${DOC_ROOT}
ENTRYPOINT /bin/httpd -f -h ${DOC_ROOT}

## 或者
CMD ["/bin/httpd", "-f", "-h ${DOC_ROOT}"]
ENTRYPOINT ["/bin/sh", "-c"]
```

```shell
[root@foresee703 ~]# vim Dockerfile
[root@foresee703 ~]# docker build -t tinyhttpd:v0.9 ./
[root@foresee703 ~]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.9
[root@foresee703 ~]# docker run --name tinyweb01 --rm -P tinyhttpd:v0.9 "ls /usr/local/src"
[root@foresee703 ~]# docker rm tinyweb01
[root@foresee703 ~]# docker kill tinyweb01
```