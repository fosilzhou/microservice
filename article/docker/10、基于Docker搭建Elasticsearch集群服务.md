## 环境准备

```shell
[root@zlmlive ~]# cat /etc/security/limits.conf
## 添加以下几行配置
* soft nofile 65536
* hard nofile 131072
* soft nproc 4096
* hard nproc 409
[root@zlmlive ~]# cat /etc/security/limits.d/20-nproc.conf 
## 添加以下这行配置
*          soft    nproc     4096
[root@zlmlive ~]# cat /etc/sysctl.conf
## 添加以下这行配置
vm.max_map_count=655360
[root@zlmlive ~]# sysctl -p
```



## 定制集群镜像

```shell
## 拉取官方镜像
[root@zlmlive ~]# docker pull elasticsearch:6.8.2
## 运行官方镜像
[root@zlmlive ~]# docker run \
-p 9200:9200 \
-p 9300:9300 \
-v /root/es/node1/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /root/es/node1/data:/usr/share/elasticsearch/data \
-v /root/es/node1/logs:/usr/share/elasticsearch/logs \
elasticsearch:6.8.2
## 进入容器执行相关命令
[root@zlmlive ~]# docker exec -it f03d96635f5b /bin/sh
## 在容器中安装ik分词插件
[root@f03d96635f5b elasticsearch]# elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v6.8.2/elasticsearch-analysis-ik-6.8.2.zip
## 在容器中安装pinyin分词插件
[root@f03d96635f5b elasticsearch]# elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-pinyin/releases/download/v6.8.2/elasticsearch-analysis-pinyin-6.8.2.zip
## 打包定制镜像
[root@zlmlive ~]# docker commit f03d96635f5b fosilzhou/elasticsearch-ik:6.8.2
## 将定制镜像提交到阿里云镜像仓库托管
#### 登录阿里云镜像仓库
[root@foresee706 es]# docker login --username=zhoulumin1988@foxmail.com registry.cn-shenzhen.aliyuncs.com
Password: chowmin3410
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
#### 修改镜像标签
[root@foresee706 es]# docker tag fosilzhou/elasticsearch-ik:6.8.2 registry.cn-shenzhen.aliyuncs.com/fosilzhou/elasticsearch-ik:6.8.2
[root@foresee706 es]# docker images | grep fosilzhou
fosilzhou/elasticsearch-ik                                     6.8.2               14c1efb01e0c        4 hours ago         910MB
registry.cn-shenzhen.aliyuncs.com/fosilzhou/elasticsearch-ik   6.8.2               14c1efb01e0c        4 hours ago         910MB
#### 提交定制镜像
[root@foresee706 es]# docker push registry.cn-shenzhen.aliyuncs.com/fosilzhou/elasticsearch-ik:6.8.2
The push refers to repository [registry.cn-shenzhen.aliyuncs.com/fosilzhou/elasticsearch-ik]
4e6af78787dc: Pushed 
39fe7344bbb9: Pushed 
c8de5a3d9a5c: Pushed 
e0c527a0fe16: Pushed 
94af43b3abdc: Pushed 
8454b765a166: Pushed 
e6d691ec9385: Pushed 
d69483a6face: Pushed 
6.8.2: digest: sha256:a236c819055a72caf0c106c6c3802aaa975db0c53e97e95c9ac4bd3545b214d8 size: 2002

## 使用定制镜像
#### 登录阿里云镜像仓库
[root@foresee706 es]# docker login --username=zhoulumin1988@foxmail.com registry.cn-shenzhen.aliyuncs.com
#### 拉取镜像
[root@foresee706 es]# docker pull registry.cn-shenzhen.aliyuncs.com/fosilzhou/elasticsearch-ik:6.8.2
#### 修改镜像标签
[root@foresee706 es]# docker tag registry.cn-shenzhen.aliyuncs.com/fosilzhou/elasticsearch-ik:6.8.2 fosilzhou/elasticsearch-ik:6.8.2
```

## Docker方式部署集群

### 配置文件

```yaml
[root@zlmlive es]# cat node1/config/elasticsearch.yml 
cluster.name: elasticsearch-cluster
node.name: elasticsearch-node1
node.master: true
node.data: true

network.host: 0.0.0.0
http.port: 9200
transport.tcp.port: 9300

http.cors.enabled: true
http.cors.allow-origin: "*"

discovery.zen.ping.unicast.hosts: ["10.10.0.62:9300","10.10.0.62:9301","10.10.0.62:9302"]
discovery.zen.minimum_master_nodes: 2

[root@zlmlive es]# cat node2/config/elasticsearch.yml 
cluster.name: elasticsearch-cluster
node.name: elasticsearch-node2
node.master: true
node.data: true

network.host: 0.0.0.0
http.port: 9201
transport.tcp.port: 9301

http.cors.enabled: true
http.cors.allow-origin: "*"

discovery.zen.ping.unicast.hosts: ["10.10.0.62:9300","10.10.0.62:9301","10.10.0.62:9302"]
discovery.zen.minimum_master_nodes: 2

[root@zlmlive es]# cat node3/config/elasticsearch.yml 
cluster.name: elasticsearch-cluster
node.name: elasticsearch-node3
node.master: true
node.data: true

network.host: 0.0.0.0
http.port: 9202
transport.tcp.port: 9302

http.cors.enabled: true
http.cors.allow-origin: "*"

discovery.zen.ping.unicast.hosts: ["10.10.0.62:9300","10.10.0.62:9301","10.10.0.62:9302"]
discovery.zen.minimum_master_nodes: 2
```

**说明：**Docker所在的宿主机IP地址为`10.10.0.62`

### 执行命令

```shell
[root@zlmlive ~]# mkdir -p /root/es/node1/config
[root@zlmlive ~]# mkdir -p /root/es/node1/data
[root@zlmlive ~]# mkdir -p /root/es/node1/logs
[root@zlmlive ~]# mkdir -p /root/es/node2/data
[root@zlmlive ~]# mkdir -p /root/es/node2/logs
[root@zlmlive ~]# mkdir -p /root/es/node2/config
[root@zlmlive ~]# mkdir -p /root/es/node3/config
[root@zlmlive ~]# mkdir -p /root/es/node3/data
[root@zlmlive ~]# mkdir -p /root/es/node3/logs
[root@zlmlive ~]# mkdir -p /root/es/analysis-ik/custom

[root@zlmlive ~]# docker run \
-e ES_JAVA_OPTS="-Xms256m -Xmx256m" -d \
-p 9200:9200 \
-p 9300:9300 \
-v /root/es/node1/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /root/es/node1/data:/usr/share/elasticsearch/data \
-v /root/es/node1/logs:/usr/share/elasticsearch/logs \
-v /root/es/node1/analysis-ik/custom:/usr/share/elasticsearch/config/analysis-ik/custom \
-v /root/es/node1/analysis-ik/IKAnalyzer.cfg.xml:/usr/share/elasticsearch/config/analysis-ik/IKAnalyzer.cfg.xml \
--name elasticsearch-node01 fosilzhou/elasticsearch-ik:6.8.2
[root@zlmlive ~]# docker run \
-e ES_JAVA_OPTS="-Xms256m -Xmx256m" -d \
-p 9201:9201 \
-p 9301:9301 \
-v /root/es/node1/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /root/es/node1/data:/usr/share/elasticsearch/data \
-v /root/es/node1/logs:/usr/share/elasticsearch/logs \
-v /root/es/node1/analysis-ik/custom:/usr/share/elasticsearch/config/analysis-ik/custom \
-v /root/es/node1/analysis-ik/IKAnalyzer.cfg.xml:/usr/share/elasticsearch/config/analysis-ik/IKAnalyzer.cfg.xml \
--name elasticsearch-node02 fosilzhou/elasticsearch-ik:6.8.2
[root@zlmlive ~]# docker run \
-e ES_JAVA_OPTS="-Xms256m -Xmx256m" -d \
-p 9202:9202 \
-p 9302:9302 \
-v /root/es/node1/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /root/es/node1/data:/usr/share/elasticsearch/data \
-v /root/es/node1/logs:/usr/share/elasticsearch/logs \
-v /root/es/node1/analysis-ik/custom:/usr/share/elasticsearch/config/analysis-ik/custom \
-v /root/es/node1/analysis-ik/IKAnalyzer.cfg.xml:/usr/share/elasticsearch/config/analysis-ik/IKAnalyzer.cfg.xml \
--name elasticsearch-node03 fosilzhou/elasticsearch-ik:6.8.2
```

### 验证环境

访问地址：<http://10.10.0.62:9200/_cat/nodes?pretty>

![1566369195292](assets/1566369195292.png)

访问地址：<http://10.10.0.62:9200/_cat/nodes?v>

![1566369569549](assets/1566369569549.png)

访问地址：<http://10.10.0.62:9200/_cat/health?v>

![1566369603178](assets/1566369603178.png)

#### 部署控制台

```shell
[root@zlmlive ~]# docker pull mobz/elasticsearch-head:5
[root@zlmlive ~]# docker run -d -p 9100:9100 --name elasticsearch-manager  mobz/elasticsearch-head:5
```

访问地址：<<http://10.10.0.62:9100/>>

![1566369500598](assets/1566369500598.png)

## Rancher方式部署集群

### 添加应用

![1566370217705](D:\repo\blog\容器技术\docker\assets\1566370217705.png)

### 配置文件

#### docker-compose.yml

```yaml
version: '2'
services:
  elasticsearch-node02:
    image: fosilzhou/elasticsearch-ik:6.8.2
    environment:
      ES_JAVA_OPTS: -Xms256m -Xmx256m
    stdin_open: true
    volumes:
    - /root/es/node2/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    - /root/es/node2/data:/usr/share/elasticsearch/data
    - /root/es/node2/logs:/usr/share/elasticsearch/logs
    - /root/es/analysis-ik/custom:/usr/share/elasticsearch/config/analysis-ik/custom
    - /root/es/analysis-ik/IKAnalyzer.cfg.xml:/usr/share/elasticsearch/config/analysis-ik/IKAnalyzer.cfg.xml
    tty: true
    ports:
    - 9201:9201/tcp
    - 9301:9301/tcp
  elasticsearch-node03:
    image: fosilzhou/elasticsearch-ik:6.8.2
    environment:
      ES_JAVA_OPTS: -Xms256m -Xmx256m
    stdin_open: true
    volumes:
    - /root/es/node3/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    - /root/es/node3/data:/usr/share/elasticsearch/data
    - /root/es/node3/logs:/usr/share/elasticsearch/logs
    - /root/es/analysis-ik/custom:/usr/share/elasticsearch/config/analysis-ik/custom
    - /root/es/analysis-ik/IKAnalyzer.cfg.xml:/usr/share/elasticsearch/config/analysis-ik/IKAnalyzer.cfg.xml
    tty: true
    ports:
    - 9202:9202/tcp
    - 9302:9302/tcp
  elasticsearch-manager:
    image: mobz/elasticsearch-head:5
    stdin_open: true
    tty: true
    ports:
    - 9100:9100/tcp
    labels:
      io.rancher.container.pull_image: always
  elasticsearch-node01:
    image: fosilzhou/elasticsearch-ik:6.8.2
    environment:
      ES_JAVA_OPTS: -Xms256m -Xmx256m
    stdin_open: true
    volumes:
    - /root/es/node1/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    - /root/es/node1/data:/usr/share/elasticsearch/data
    - /root/es/node1/logs:/usr/share/elasticsearch/logs
    - /root/es/analysis-ik/custom:/usr/share/elasticsearch/config/analysis-ik/custom
    - /root/es/analysis-ik/IKAnalyzer.cfg.xml:/usr/share/elasticsearch/config/analysis-ik/IKAnalyzer.cfg.xml
    tty: true
    ports:
    - 9200:9200/tcp
    - 9300:9300/tcp
```

#### rancher-compose.yml

```shell
version: '2'
services:
  elasticsearch-node02:
    scale: 1
    start_on_create: true
  elasticsearch-node03:
    scale: 1
    start_on_create: true
  elasticsearch-manager:
    scale: 1
    start_on_create: true
  elasticsearch-node01:
    scale: 1
    start_on_create: true

```



