#  Docker私有仓库之Nexus

## 搭建私有仓库

下载地址：<https://help.sonatype.com/repomanager3/download/download-archives---repository-manager-3>

安装Nexus服务

```shell
## 下载nexus
[root@localhost ~]# wget https://sonatype-download.global.ssl.fastly.net/nexus/3/nexus-3.18.0-01-unix.tar.gz
## 解压nexus
[root@localhost ~]# tar -zxvf nexus-3.13.0-01-unix.tar.gz -C /usr/local/nexus/
## 创建nexus管理账号
[root@localhost ~]# useradd nexus
## 修改文件权限
[root@localhost nexus]# chmod -R 755 *
[root@localhost nexus]# chown -R nexus:nexus *
## 设置nexus管理账号
[root@localhost bin]# vim nexus.rc 
run_as_user="nexus"

## 启动nexus服务
[root@localhost bin]# ./nexus start
```

访问地址：http://192.168.221.128:8081/ 默认账号：admin/admin123

![1562678693460](assets/1562678693460.png)

## 创建Docker私有仓库

点击`Create repository`按钮，创建仓库。Nexus支持多种仓库类型，例如：maven、npm、docker等。本文创建一个docker仓库。一般来说，对于特定的仓库类型（例如docker），细分了三类，分别是proxy、hosted、group，含义如下：

- hosted，本地代理仓库，通常我们会部署自己的构件到这一类型的仓库，可以push和pull。
- proxy，代理的远程仓库，它们被用来代理远程的公共仓库，如maven中央仓库，只能pull。
- group，仓库组，用来合并多个hosted/proxy仓库，通常我们配置maven依赖仓库组，只能pull

![1562679506854](assets/1562679506854.png)

创建hosted类型的仓库

![1562680096675](assets/1562680096675.png)

配置Docker私有仓库

```shell
### (centos7配置)
[root@localhost ~]# cat /etc/docker/daemon.json
{
  "registry-mirrors": ["https://f78f35gg.mirror.aliyuncs.com"],
  "insecure-registries": ["192.168.221.128:8082"]
}
[root@localhost ~]# systemctl restart docker

### (centos6配置)
[root@localhost ~]# vim /etc/sysconfig/docker
other_args="--insecure-registry 192.168.221.128:8082"

[root@localhost ~]# service docker restart
```

## 镜像上传到私有仓库

```shell
[root@localhost bin]# docker tag centos:7 192.168.221.128:8082/centos:7
[root@localhost bin]# docker login 192.168.221.128:8082
Username: admin
Password: admin123  ##<==此账号为nexus登录账号 
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
[root@localhost bin]# docker push 192.168.221.128:8082/centos:7
The push refers to repository [192.168.221.128:8082/centos]
d69483a6face: Pushed 
7: digest: sha256:ca58fe458b8d94bc6e3072f1cfbd334855858e05e1fd633aa07cf7f82b048e66 size: 529
```



## 使用Nexus镜像搭建私服

```shell
[root@localhost ~]# docker run -d -p 8081:8081  --name nexus sonatype/nexus3
```





参考资料：<http://www.itmuch.com/docker/11-docker-nexus>

