## 准备资源

- [wordpress-4.7.4-zh_CN.tar.gz](https://cn.wordpress.org/wordpress-4.7.4-zh_CN.tar.gz)
- start.sh
- wp-config.php


## 编写Dockerfile

```dockerfile
FROM centos:6
MAINTAINER fosilzhou <fosilzhou@qq.com>
RUN yum install -y httpd php php-gd php-mysql mysql mysql-server
ENV MYSQL_ROOT_PASSWORD 123456
RUN echo "<?php phpinfo()?>" /var/www/html/index.php
ADD start.sh /start.sh
RUN chmod +x /start.sh
ADD wordpress-4.7.4-zh_CN.tar.gz /var/www/html
VOLUME ["/var/lib/mysql"]
EXPOSE 80 3306
CMD /start.sh
```

**start.sh**

```shell
service httpd start
service mysqld start
mysqladmin -uroot password $MYSQL_ROOT_PASSWORD
tail -f
```

## 执行命令

```shell
## 构建命令
docker build -t wordpress:v1 ./
## 运行命令
docker run -itd --name wordpress -p 88:80 wordpress:v1
```

访问地址<http://192.168.221.128:88/wordpress> 配置数据库信息

![1562947775300](assets/1562947775300.png)

![1562947838154](assets/1562947838154.png)

## 编写wp-config.php

```php
<?php
/**
 * WordPress基础配置文件。
 *
 * 这个文件被安装程序用于自动生成wp-config.php配置文件，
 * 您可以不使用网站，您需要手动复制这个文件，
 * 并重命名为“wp-config.php”，然后填入相关信息。
 *
 * 本文件包含以下配置选项：
 *
 * * MySQL设置
 * * 密钥
 * * 数据库表名前缀
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/zh-cn:%E7%BC%96%E8%BE%91_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL 设置 - 具体信息来自您正在使用的主机 ** //
/** WordPress数据库的名称 */
define('DB_NAME', 'test');

/** MySQL数据库用户名 */
define('DB_USER', 'root');

/** MySQL数据库密码 */
define('DB_PASSWORD', '123456');

/** MySQL主机 */
define('DB_HOST', 'localhost');

/** 创建数据表时默认的文字编码 */
define('DB_CHARSET', 'utf8');

/** 数据库整理类型。如不确定请勿更改 */
define('DB_COLLATE', '');

/**#@+
 * 身份认证密钥与盐。
 *
 * 修改为任意独一无二的字串！
 * 或者直接访问{@link https://api.wordpress.org/secret-key/1.1/salt/
 * WordPress.org密钥生成服务}
 * 任何修改都会导致所有cookies失效，所有用户将必须重新登录。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cC9R2X*_4vj,nLB,<=3uenAo_IAmn.AZX>6F.e1;o+sS_ebjb(h.#ZAjxHjhc0-}');
define('SECURE_AUTH_KEY',  '$;@*k^Wc5_PS(mZ /),Z{zEVQ?K[A9lmkPYPZ3fe)J;1y}@80dFOiyAht]oD#!g!');
define('LOGGED_IN_KEY',    'P17hw!$2):=z2u(|X|cD}L1Pn}$(G26LTM/c+vw+8D(NkP>%$ 8sXjbL3=Y?6cHm');
define('NONCE_KEY',        'DLetnk$m}Y$>wD{JGZhDFhh9Ho1L-|qPy2!NRGO(,uPd,5=;dcL=W7hG.6&%vyE(');
define('AUTH_SALT',        'Hgq2=NeG)VuH-k-c)xIr-)454`%-jc~t){W9^p7)26 L6(,)JV`sIT#3y[AK+1gg');
define('SECURE_AUTH_SALT', 'OT%A6XDTN9!Fj6AJDy_<>Ktu0MLS` ivrE#Y#/~1{|N%(k.Gv&nu@sa={~dLi4}h');
define('LOGGED_IN_SALT',   '7|fp5hfe6HLSuN[rc;+-2ep+kWJ&}bl[$4XWKI6F~ uSuaJn.BL.&G(6#Wi-gz(`');
define('NONCE_SALT',       '(?Igq;Jr$g7`NL`9~j0gg@e6aUx&rGNs&NY{pfue@@u/zsoxKOsx$u:l5*z80l>*');

/**#@-*/

/**
 * WordPress数据表前缀。
 *
 * 如果您有在同一数据库内安装多个WordPress的需求，请为每个WordPress设置
 * 不同的数据表前缀。前缀名只能为数字、字母加下划线。
 */
$table_prefix  = 'wp_';

/**
 * 开发者专用：WordPress调试模式。
 *
 * 将这个值改为true，WordPress将显示所有用于开发的提示。
 * 强烈建议插件开发者在开发环境中启用WP_DEBUG。
 *
 * 要获取其他能用于调试的信息，请访问Codex。
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/**
 * zh_CN本地化设置：启用ICP备案号显示
 *
 * 可在设置→常规中修改。
 * 如需禁用，请移除或注释掉本行。
 */
define('WP_ZH_CN_ICP_NUM', true);

/* 好了！请不要再继续编辑。请保存本文件。使用愉快！ */

/** WordPress目录的绝对路径。 */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** 设置WordPress变量和包含文件。 */
require_once(ABSPATH . 'wp-settings.php');
```

## 修改Dockerfile

```dockerfile
FROM centos:6
MAINTAINER fosilzhou <fosilzhou@qq.com>
RUN yum install -y httpd php php-gd php-mysql mysql mysql-server
ENV MYSQL_ROOT_PASSWORD 123456
RUN echo "<?php phpinfo()?>" /var/www/html/index.php
ADD start.sh /start.sh
RUN chmod +x /start.sh
ADD wordpress-4.7.4-zh_CN.tar.gz /var/www/html
COPY wp-config.php /var/www/html/wordpress ## 添加此行指令
VOLUME ["/var/lib/mysql"]
EXPOSE 80 3306
CMD /start.sh
```

## 执行命令

```shell
## 构建命令
docker build -t wordpress:v1 ./
## 运行命令
docker run -itd --name wordpress -p 88:80 wordpress:v1
```

再次访问地址<http://192.168.221.128:88/wordpress> 配置博客相关信息

![1562948347222](assets/1562948347222.png)

![1562948448193](assets/1562948448193.png)