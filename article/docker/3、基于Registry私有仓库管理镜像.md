# Docker私有仓库

## 搭建私有仓库

拉取私有仓库镜像

```shell
[root@localhost ~]# docker pull registry
```

启动私有仓库容器

```shell
[root@localhost ~]# docker run -di --name=registry -p 5000:5000 registry
```

访问地址：<http://192.168.221.148:5000/v2/_catalog> 看到以下内容表示搭建成功

![1562596138181](assets/1562596138181.png)

设置Docker信任私服地址

```shell
### (centos7配置)
[root@localhost ~]# cat /etc/docker/daemon.json
{
  "registry-mirrors": ["https://f78f35gg.mirror.aliyuncs.com"],
  "insecure-registries": ["192.168.221.128:5000"]
}
[root@localhost ~]# systemctl restart docker

### (centos6配置)
[root@localhost ~]# vim /etc/sysconfig/docker
other_args="--insecure-registry 192.168.221.148:5000"

[root@localhost ~]# service docker restart
```

如果设置未生效或未设置时，会出现以下异常：

> [root@localhost ~]# docker push 192.168.221.148:5000/tinyhttpd:v0.1
> 
> Error response from daemon: invalid registry endpoint https://192.168.221.148:5000/v0/: unable to ping registry endpoint https://192.168.221.148:5000/v0/
> 
> v2 ping attempt failed with error: Get https://192.168.221.148:5000/v2/: tls: oversized record received with length 20527
> 
> v1 ping attempt failed with error: Get https://192.168.221.148:5000/v1/_ping: tls: oversized record received with length 20527. 
> 
> If this private registry supports only HTTP or HTTPS with an unknown CA certificate, please add `--insecure-registry 192.168.221.148:5000` to the daemon's arguments. In the case of HTTPS, if you have access to the registry's CA certificate, no need for the flag; simply place the CA certificate at /etc/docker/certs.d/192.168.221.148:5000/ca.crt

## 镜像上传到私有仓库

修改镜像标签

```shell
[root@localhost ~]# docker tag centos:7 192.168.221.128:5000/centos:7
```

上传镜像

```shell
[root@localhost ~]# docker push 192.168.221.128:5000/centos:7
The push refers to repository [192.168.221.128:5000/centos]
d69483a6face: Pushed 
7: digest: sha256:ca58fe458b8d94bc6e3072f1cfbd334855858e05e1fd633aa07cf7f82b048e66 size: 529
```

私有仓库管理

```shell
## 上传镜像
[root@localhost ~]# docker push 192.168.221.128:5000/centos:7
## 下载镜像
[root@localhost ~]# docker pull 192.168.221.128:5000/centos:7

## 列出所有镜像
[root@localhost ~]# curl http://192.168.221.128:5000/v2/_catalog
{"repositories":["centos","spring-boot-docker"]}
## 列出镜像标签
[root@localhost ~]# curl http://192.168.221.128:5000/v2/centos/tags/list
{"name":"centos","tags":["7"]}
[root@localhost ~]# curl http://192.168.221.128:5000/v2/spring-boot-docker/tags/list
{"name":"spring-boot-docker","tags":["0.0.1-SNAPSHOT"]}
```





参考资料：<https://www.jianshu.com/p/07041223df66>