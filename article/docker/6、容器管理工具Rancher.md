# 容器管理工具

## 简介

Rancher是一款开源企业级全栈话容器部署及管理平台。Rancher为容器提供基础架构服务、CNI兼容的网络服务、存储服务、主机管理、负载均衡、防火墙等等；Rancher让上述服务跨越公有云、私有云、虚拟机、物理机环境运行，实现一键式应用部署和管理

## 部署

```shell
[root@localhost ~]# docker pull rancher/server
[root@localhost ~]# docker run -di --name=rancher -p 9000:8080 rancher/server
```



访问地址：<http://192.168.221.128:9000>

![1562770676614](assets/1562770676614.png)

## 初始化

### 添加环境

Default -> 环境管理

![1562771180902](assets/1562771180902.png)

![1562771105517](assets/1562771105517.png)

### 添加主机

基础架构 -> 主机 -> 添加主机

![1562771365758](assets/1562771365758.png)

![1562771586983](assets/1562771586983.png)

在Docker宿主机上执行命令第5步的命令，等此命令执行完成后点【关闭】按钮

```shell
[root@localhost ~]# docker run --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.11 http://192.168.221.128:9000/v1/scripts/9F842E3B677A930B4098:1546214400000:Bzr7qPGEUPDGOnCA5tdUMcHDv8
Unable to find image 'rancher/agent:v1.2.11' locally
v1.2.11: Pulling from rancher/agent
b3e1c725a85f: Pull complete 
6a710864a9fc: Pull complete 
d0ac3b234321: Pull complete 
87f567b5cf58: Pull complete 
063e24b217c4: Pull complete 
d0a3f58caef0: Pull complete 
16914729cfd3: Pull complete 
bbad862633b9: Pull complete 
3cf9849d7f3c: Pull complete 
Digest: sha256:0fba3fb10108f7821596dc5ad4bfa30e93426d034cd3471f6ccd3afb5f87a963
Status: Downloaded newer image for rancher/agent:v1.2.11

INFO: Running Agent Registration Process, CATTLE_URL=http://192.168.221.128:9000/v1
INFO: Attempting to connect to: http://192.168.221.128:9000/v1
INFO: http://192.168.221.128:9000/v1 is accessible
INFO: Configured Host Registration URL info: CATTLE_URL=http://192.168.221.128:9000/v1 ENV_URL=http://192.168.221.128:9000/v1
INFO: Inspecting host capabilities
INFO: Boot2Docker: false
INFO: Host writable: true
INFO: Token: xxxxxxxx
INFO: Running registration
INFO: Printing Environment
INFO: ENV: CATTLE_ACCESS_KEY=0EDA33860F9F813F4FC3
INFO: ENV: CATTLE_HOME=/var/lib/cattle
INFO: ENV: CATTLE_REGISTRATION_ACCESS_KEY=registrationToken
INFO: ENV: CATTLE_REGISTRATION_SECRET_KEY=xxxxxxx
INFO: ENV: CATTLE_SECRET_KEY=xxxxxxx
INFO: ENV: CATTLE_URL=http://192.168.221.128:9000/v1
INFO: ENV: DETECTED_CATTLE_AGENT_IP=172.17.0.1
INFO: ENV: RANCHER_AGENT_IMAGE=rancher/agent:v1.2.11
INFO: Launched Rancher Agent: f86bcda4d29fce658c9f9cb805e74b42c0502c076d5c76d6d89c2ed34fea912d
```

### 添加应用

应用 -> 用户 -> 添加应用

![1562772187149](assets/1562772187149.png)

![1562772356609](assets/1562772356609.png)

## 应用部署

### MySQL部署

![1562772694487](assets/1562772694487.png)

![1562772746697](assets/1562772746697.png)

以上操作相当于执行以下命令

```shell
[root@localhost ~]# docker run -di --name=mysql -p 33306:3306 -e MYSQL_ROOT_PASSWORD=123456 centos/mysql-57-centos7
```

![1562773115034](assets/1562773115034.png)

测试数据库链接

![1562773228350](assets/1562773228350.png)

### RabbitMQ部署

镜像：rabbitmq:management   端口:4369、5671、5672、15671、15672、25672

![1562773764684](assets/1562773764684.png)

![1562773807707](assets/1562773807707.png)

![1562774527301](assets/1562774527301.png)

访问：<http://192.168.221.128:15672/>

![1562774265993](assets/1562774265993.png)


### Redis部署

![1562774699470](assets/1562774699470.png)

![1562774853346](assets/1562774853346.png)

## 微服务部署

以spring-boot-docker项目为例

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.zlmlive.boot</groupId>
	<artifactId>spring-boot-docker</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>spring-boot-docker</name>
	<description>Demo project for Spring Boot</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.14.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>0.4.13</version>
				<configuration>
			<imageName>192.168.221.128:5000/${project.artifactId}:${project.version}</imageName>
					<baseImage>java:8</baseImage>
					<entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
					<resources>
						<resource>
							<targetPath>/</targetPath>
							<directory>${project.build.directory}</directory>
							<include>${project.build.finalName}.jar</include>
						</resource>
					</resources>
					<dockerHost>http://192.168.221.128:2375</dockerHost>
					<!-- 设置覆盖构建相同标签名称的镜像 -->
					<forceTags>true</forceTags>
				</configuration>
			</plugin>
		</plugins>
	</build>

</project>
```

执行以下命令构建

```shell
D:\repo\boot\spring-boot-1.5.14\spring-boot-docker>mvn clean package docker:build -Dmaven.test.skip=true -DpushImage
```
查询构建结果
```shell
[root@localhost ~]# docker images
REPOSITORY                                TAG                 IMAGE ID            CREATED             SIZE
192.168.221.128:5000/spring-boot-docker   0.0.1-SNAPSHOT      4bc6346e1fd6        2 minutes ago       658MB
centos                                    7                   9f38484d220f        3 months ago        202MB
java                                      8                   d23bdf5b1b1b        2 years ago         643MB
[root@localhost ~]#
```

![1562777271092](assets/1562777271092.png)

![1562777295709](assets/1562777295709.png)

测试访问：<http://192.168.221.128:9001/index>

![1562777329167](assets/1562777329167.png)

## 扩容与缩容

创建以spring-boot-docker服务为例，但不要设置端口映射

![1562834205100](assets/1562834205100.png)

API -> WebHooks ->添加接收器

![1562834597790](assets/1562834597790.png)

![1562834739700](assets/1562834739700.png)

使用POST方式发送`触发地址`请求

```shell
curl -d http://192.168.221.128:9000/v1-webhooks/endpoint?key=YZvDda5fS71eCIIdWxEruP9SrRLE3fZfXG61eUyh&projectId=1a5
```

![1562835341234](assets/1562835341234.png)

```shell
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE                                                    COMMAND                  CREATED              STATUS              PORTS                              NAMES
024c87b9eef6        192.168.221.128:5000/spring-boot-docker:0.0.1-SNAPSHOT   "/.r/r java -jar /sp…"   About a minute ago   Up About a minute                                      r-microservice-demo-spring-boot-docker-3-a29e0d62
23a23cd00d26        192.168.221.128:5000/spring-boot-docker:0.0.1-SNAPSHOT   "/.r/r java -jar /sp…"   About a minute ago   Up About a minute                                      r-microservice-demo-spring-boot-docker-2-e7fc468d
d45e9dabb5f4        192.168.221.128:5000/spring-boot-docker:0.0.1-SNAPSHOT   "/.r/r java -jar /sp…"   18 minutes ago       Up 18 minutes                                          r-microservice-demo-spring-boot-docker-1-f9df1991
```

## 负载均衡器

![1562835609569](assets/1562835609569.png)

![1562835755535](assets/1562835755535.png)

简单压测

```shell
[root@localhost ~]# for a in `seq 1 5000`; do curl http://192.168.221.128:9001/index; done
```

压测CPU、内存、网络、存储统计图

![1562837985994](assets/1562837985994.png)