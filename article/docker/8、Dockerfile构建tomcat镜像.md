## 准备资源

- [jdk-8u171-linux-x64.tar.gz](http://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html)
- [apache-tomcat-9.0.8.tar.gz](https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.8/bin/apache-tomcat-9.0.8.tar.gz)
- README.md <touch README.md>


## 编写Dockerfile

```dockerfile
FROM centos:7
MAINTAINER fosilzhou <fosilzhou@qq.com>
## 把宿主机当前上下文的app.war拷贝到容器/usr/local/路径下
COPY README.md /usr/local/README.txt
# 把jdk和tomcat添加到容器中
ADD jdk-8u171-linux-x64.tar.gz /usr/local/
ADD apache-tomcat-9.0.8.tar.gz /usr/local/
# 安装vim编辑器
RUN yum -y install vim
# 设置工作访问路径的WORKDIR,即登录落脚点
ENV WORK_PATH /usr/local
WORKDIR $WORK_PATH
# 配置java和tomcat环境变量
ENV JAVA_HOME=/usr/local/jdk1.8.0_171
ENV JRE_HOME=$JAVA_HOME/jre
ENV CLASSPATH=$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
ENV CATALINA_HOME=/usr/local/apache-tomcat-9.0.8
ENV CATALINA_BASE=/usr/local/apache-tomcat-9.0.8
ENV PATH=$PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin
# 容器运行时监听的端口
EXPOSE 8080
# 启动时运行tomcat
# 写法一
# ENTRYPOINT["/usr/local/apache-tomcat-9.0.8/bin/startup.sh"]
# 写法二
# CMD["/usr/local/apache-tomcat-9.0.8/bin/startup.sh"]
# 写法三
CMD /usr/local/apache-tomcat-9.0.8/bin/startup.sh \
    && tail -F /usr/local/apache-tomcat-9.0.8/logs/catalina.out
```

## 执行命令

```shell
## 构建命令
docker build -t=tomcat:9 ./
## 运行命令
#### 简单run命令
docker run -d -p 9080:8080 --name tomcat9 tomcat:9
#### 完整run命令
docker run -d -p 9080:8080 --name tomcat9 -v /apps/tomcat/webapps/web-demo:/usr/local/apache-tomcat-9.0.8/webapps/web-demo -v /apps/tomcat/logs:/usr/local/apache-tomcat-9.0.8/logs tomcat:9
## 测试命令
docker exec tomcat9 ls -l /usr/local/apache-tomcat-9.0.8/webapps/web-demo
```

测试访问地址：http://192.168.221.128:9080/

![1562937759927](D:\repo\blog\容器技术\docker\assets\1562937759927.png)

> **说明：**
>
> - 将部署的`/usr/local/apache-tomcat-9.0.8/webapps/web-demo`应用路径映射到宿主机的`/apps/tomcat/webapps/web-demo`路径下；发布时就不用将Web应用拷贝到容器中的`/usr/local/apache-tomcat-9.0.8/webapps/web-demo`下，而是将Web应用直接拷贝到宿主机的`/apps/tomcat/webapps/web-demo`这个路径即可
> - 将容器中tomcat的日志路径`/usr/local/apache-tomcat-9.0.8/logs`映射到宿主机的`/apps/tomcat/logs`路径下；运维人员就可以直接通过`tail -f /apps/tomcat/logs/catalina.out`查询tomcat运行日志
> - -privileged=true：提高写权限
> - -p 9080:8080：将容器中的8080端口映射到宿主机的9080端口

创建web-demo测试应用

```shell
[root@localhost tomcat]# cat webapps/web-demo/WEB-INF/web.xml 
<web-app xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
                      http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0"
         metadata-complete="true">
[root@localhost tomcat]# cat webapps/web-demo/index.jsp 
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title></title>
</head>
<body>
	<%="Apache Tomcat/9.0.8"%>
	<%System.out.println("Apache Tomcat/9.0.8");%>
</body>
</html>
```

