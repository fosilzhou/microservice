# DockerMaven自动构建微服务

## 开启Docker远程访问配置

修改宿主机Docker配置，让其可以远程访问

```shell
[root@localhost ~]# vim /lib/systemd/system/docker.service
```

其中ExecStart=后添加配置 `-H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock`

![1562747278700](assets/1562747278700.png)

## 刷新配置，重启服务

```shell
## 重启服务
[root@localhost ~]# systemctl daemon-reload
[root@localhost ~]# systemctl restart docker
## 启动私有仓库
[root@localhost ~]# docker start registry
```

## 添加POM配置

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
        <plugin>
            <groupId>com.spotify</groupId>
            <artifactId>docker-maven-plugin</artifactId>
            <version>0.4.13</version>
            <configuration>
                <imageName>192.168.221.128:5000/${project.artifactId}:${project.version}</imageName>
                <baseImage>java:8</baseImage>
                <entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
                <resources>
                    <resource>
                        <targetPath>/</targetPath>
                        <directory>${project.build.directory}</directory>
                        <include>${project.build.finalName}.jar</include>
                    </resource>
                </resources>
                <dockerHost>http://192.168.221.128:2375</dockerHost>
                <!-- 设置覆盖构建相同标签名称的镜像 -->
                <forceTags>true</forceTags>
            </configuration>
        </plugin>
    </plugins>
</build>
```

说明：

dockerHost地址对应docker开启远程访问(`-H tcp://0.0.0.0:2375`)的地址一致



以上POM配置会自动生成Dockerfile

```dockerfile
FROM java:8
ADD ${project.build.finalName}.jar /
ENTRYPOINT ["java", "-jar", "/${project.build.finalName}.jar"]
```



## 执行自动构建命令

```shell
mvn clean package docker:build -Dmaven.test.skip=true -DpushImage
mvn clean package docker:build -Dmaven.test.skip=true -DpushImageTags 
      -DdockerImageTags=latest -DdockerImageTags=0.0.1
```



## 基于Dockerfile的POM配置

编写Dockerfile文件

```dockerfile
FROM java:8
VOLUME /tmp
ADD spring-boot-docker02-0.0.1-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE 9000
ENV JAVA_OPTS=""
# ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]
ENTRYPOINT ["java", "$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]
```

添加POM配置

```xml
<build>
	<plugins>
		<plugin>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-maven-plugin</artifactId>
		</plugin>
		<plugin>
			<groupId>com.spotify</groupId>
			<artifactId>docker-maven-plugin</artifactId>
			<version>0.4.13</version>
			<configuration>
				<imageName>192.168.221.128:5000/${project.artifactId}:${project.version}</imageName>
				<dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
				<resources>
					<resource>
						<targetPath>/</targetPath>
						<directory>${project.build.directory}</directory>
						<include>${project.build.finalName}.jar</include>
					</resource>
				</resources>
				<dockerHost>http://192.168.221.128:2375</dockerHost>
				<!-- 设置覆盖构建相同标签名称的镜像 -->
				<forceTags>true</forceTags>
			</configuration>
		</plugin>
	</plugins>
</build>
```

## 带认证的私服的POM配置

```xml
<build>
	<plugins>
		<plugin>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-maven-plugin</artifactId>
		</plugin>
		<plugin>
			<groupId>com.spotify</groupId>
			<artifactId>docker-maven-plugin</artifactId>
			<version>0.4.13</version>
			<configuration>
				<imageName>192.168.221.128:8082/${project.artifactId}:${project.version}</imageName>
				<baseImage>java:8</baseImage>
				<entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
				<resources>
					<resource>
						<targetPath>/</targetPath>
						<directory>${project.build.directory}</directory>
						<include>${project.build.finalName}.jar</include>
					</resource>
				</resources>
				<!-- 与maven配置文件settings.xml中配置的server.id一致，用于推送镜像 -->
				<serverId>docker-repo</serverId>
				<registryUrl>http://192.168.221.128:8082</registryUrl>
				<dockerHost>http://192.168.221.128:2375</dockerHost>
				<!-- 设置覆盖构建相同标签名称的镜像 -->
				<forceTags>true</forceTags>
			</configuration>
		</plugin>
	</plugins>
</build>

--- settings.xml配置
<servers>
	<server>
		<id>docker-repo</id>
		<username>admin</username>
		<password>admin123</password>
		<configuration>
			<email>996977899@qq.com</email>
		</configuration>
	</server>
</servers>
```

## 将docker:build绑定到mvn package命令上

```xml
<build>
	<plugins>
		<plugin>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-maven-plugin</artifactId>
		</plugin>
		<plugin>
			<groupId>com.spotify</groupId>
			<artifactId>docker-maven-plugin</artifactId>
			<version>0.4.13</version>
			
			<!-- docker插件绑定在某个phase执行（这里绑定在package上）：这样只需执行mvn package, 就会自动执行mvn docker:build -->
			<executions>
				<execution>
					<id>build-image</id>
					<phase>package</phase>
					<goals>
						<goal>build</goal>
					</goals>
				</execution>
			</executions>

			<configuration>
				<imageName>192.168.221.128:5000/${project.artifactId}:${project.version}</imageName>
				<dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
				<resources>
					<resource>
						<targetPath>/</targetPath>
						<directory>${project.build.directory}</directory>
						<include>${project.build.finalName}.jar</include>
					</resource>
				</resources>
				<dockerHost>http://192.168.221.128:2375</dockerHost>
				<!-- 设置覆盖构建相同标签名称的镜像 -->
				<forceTags>true</forceTags>
			</configuration>
		</plugin>
	</plugins>
</build>
```



## 启动容器

```shell
[root@localhost ~]# docker run -di --name=app -p 8080:8080 192.168.221.128:5000/spring-boot-docker:0.0.1-SNAPSHOT
```

