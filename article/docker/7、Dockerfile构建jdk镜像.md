## 准备资源

- jdk-8u171-linux-x64.tar.gz

## 编写Dockerfile

```dockerfile
## 依赖镜像名称和ID
FROM centos:7
## 指定镜像创建者
MAINTAINER fosilzhou <fosilzhou@qq.com>
## 切换工作目录
WORKDIR /usr
RUN mkdir /usr/local/java
## 把Java安装包添加到容器中
ADD jdk-8u171-linux-x64.tar.gz /usr/local/java

## 配置Java环境变量
ENV JAVA_HOME=/usr/local/java/jdk1.8.0_171
ENV JRE_HOME=$JAVA_HOME/jre
ENV CLASSPATH=$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
ENV PATH=$JAVA_HOME/bin:$PATH
```

## 执行命令

```shell
## 构建命令
[root@localhost ~]# docker build -t=jdk:8 ./
## 运行并测试命令
[root@localhost ~]# docker run --name jdk8 jdk:8 printenv
[root@localhost ~]# docker run --name jdk8 jdk:8 java -version
```

