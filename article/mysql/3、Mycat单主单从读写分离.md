# Mycat单主单从读写分离

## 简介


上一章讲到MySql主从复制，通过Mycat，能够实现读写分离，即master主服务器实现写操作(insert,update,delete等)，salve从服务器实现读操作(select等)；主服务器一旦有写入操作，从服务器通过读取binlog，来实现数据同步；Mycat也时时发送心跳包来检测mysql服务器是否可用；

![](./assets/mycat10.png)

## 部署规划

| 类型      | 分配IP     |
| --------- | ---------- |
| MySQL主库 | 172.20.0.2 |
| MySQL备库 | 172.20.0.3 |
| Mycat     | 172.20.0.4 |

## MySQL部署

### 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3306:3306 --name master -d -v /home/docker/master/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.2 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3307:3306 --name slave -d -v /home/docker/slave/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.3 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```

### 主库配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=2
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=db_mycat
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
```

### 备库配置

```mysql
[mysqld]
server-id=3
relay-log=mysql-relay
```

### 主库命令

```shell
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      154 | db_mycat     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set

mysql> CREATE USER 'slave1'@'172.20.0.3' IDENTIFIED BY '123456';
Query OK, 0 rows affected

mysql> GRANT REPLICATION SLAVE ON *.* TO 'slave1'@'172.20.0.3';
Query OK, 0 rows affected

mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected

mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      761 | db_mycat     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set
```

### 备库命令

```shell
mysql> CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave1', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=761;
Query OK, 0 rows affected

mysql> START SLAVE;
Query OK, 0 rows affected

mysql> SHOW SLAVE STATUS;
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Slave_IO_State                   | Master_Host | Master_User | Master_Port | Connect_Retry | Master_Log_File  | Read_Master_Log_Pos | Relay_Log_File     | Relay_Log_Pos | Relay_Master_Log_File | Slave_IO_Running | Slave_SQL_Running | Replicate_Do_DB | Replicate_Ignore_DB | Replicate_Do_Table | Replicate_Ignore_Table | Replicate_Wild_Do_Table | Replicate_Wild_Ignore_Table | Last_Errno | Last_Error | Skip_Counter | Exec_Master_Log_Pos | Relay_Log_Space | Until_Condition | Until_Log_File | Until_Log_Pos | Master_SSL_Allowed | Master_SSL_CA_File | Master_SSL_CA_Path | Master_SSL_Cert | Master_SSL_Cipher | Master_SSL_Key | Seconds_Behind_Master | Master_SSL_Verify_Server_Cert | Last_IO_Errno | Last_IO_Error | Last_SQL_Errno | Last_SQL_Error | Replicate_Ignore_Server_Ids | Master_Server_Id | Master_UUID                          | Master_Info_File           | SQL_Delay | SQL_Remaining_Delay | Slave_SQL_Running_State                                | Master_Retry_Count | Master_Bind | Last_IO_Error_Timestamp | Last_SQL_Error_Timestamp | Master_SSL_Crl | Master_SSL_Crlpath | Retrieved_Gtid_Set | Executed_Gtid_Set | Auto_Position | Replicate_Rewrite_DB | Channel_Name | Master_TLS_Version |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Waiting for master to send event | 172.20.0.2  | slave1      |        3306 |            60 | mysql-bin.000001 |                 761 | mysql-relay.000002 |           320 | mysql-bin.000001      | Yes              | Yes               |                 |                     |                    |                        |                         |                             |          0 |            |            0 |                 761 |             523 | None            |                |             0 | No                 |                    |                    |                 |                   |                |                     0 | No                            |             0 |               |              0 |                |                             |                2 | 8460233e-6cdb-11ea-86de-0242ac140002 | /var/lib/mysql/master.info |         0 | NULL                | Slave has read all relay log; waiting for more updates |              86400 |             |                         |                          |                |                    |                    |                   |             0 |                      |              |                    |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
1 row in set
```

## Mycat部署

![](assets/mycat06.png)



### 配置文件

#### schema.xml

```xml
<?xml version="1.0"?>
<!DOCTYPE mycat:schema SYSTEM "schema.dtd">
<!--
	主配置标签
-->
<mycat:schema xmlns:mycat="http://io.mycat/">
	<!--
		逻辑库配置标签
		name: 逻辑库名
		sqlMaxLimit: 限制每次查询数据所返回的最大行数(server.xml中的limit是整个mycat系统的默认值，这里则是当前逻辑库的默认值，默认先看schema.xml的限制数)
		checkSQLschema: 检查发给Mycat的SQL是否包含库名
	-->
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000" dataNode="dn1">
		<!--
			逻辑表配置标签
			name: 逻辑表名，配置多个表名用逗号分隔
			splitTableNames: 多个表名将此属性设置为true
			primaryKey: 物理表的真实主键
			dataNode: 表数据存储的实际物理节点名称(多个用逗号隔开，顺序一旦定义就不能修改，否则会出现混乱)
			rule: 在rule.xml中的<tableRule>中定义
		-->
		<!--
		<table name="travelrecord,address" dataNode="dn1,dn2,dn3" rule="auto-sharding-long" splitTableNames ="true"/>
		-->
	</schema>
	<!--
		dataNode数据节点配置标签
		name: 数据节点名称
		dataHost: 数据库所在主机组的名字
		database: 定义物理数据库名
	-->
	<dataNode name="dn1" dataHost="localhost1" database="db_mycat" />
	<!--
		dataHost数据库主机配置标签
		name: 数据库服务器的名称
		maxCon: Mycat连接mysql的最大连接数
		minCon: Mycat连接mysql的最小连接数
		balance: Mycat读写分离、读的负载均衡的工作方式
				0:不开启读写分离机制
				1:全部的readHost与stand by writeHost参与select语句的负载均衡(双主双从使用该配置)
				2:所有的readHost与writeHost都参与select语句的负载均衡
				3:所有的readHost参与select语句的负载均衡(单主单从使用该配置)（1.4版本新增）
		writeType: 
				0:所有写操作都是由writeHost标签配置的第一个写主机来执行，只有当第一个挂掉之后，写请求才会到第二个上（切换记录在配置文件dnindex.properties中）
				1:随机发送到写请求的writeHost上定义的写主机执行（1.5以后版本废弃）
		dbType: 使用的数据库类型（mysql、mongodb等）
		dbDriver: 所使用数据库的驱动程序
				natice：mysql原生的通讯协议
		switchType: 写数据库如何进行高可用切换
				 1: 当每一个writeHost不可访问的时候都会切换到第二个writeHost写服务器
				-1: 关闭自动切换功能
				 2: 基于MySQL主从同步的状态决定是否切换，心跳语句为 show slave status
				 3: 基于MySQL galary cluster 的切换机制（适合集群）（1.4.1版本新增）
		slaveThreshold: 配置真实MySQL与MyCat的心跳间隔
	-->
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<!--
			select user()是返回当前连接mysql的用户
		-->
		<heartbeat>select user()</heartbeat>
		<!--
			writeHost通常定义master服务器，readHost通常定义slave服务器。readHost必须镶嵌在writeHost内
			host: MySQL实例名称
			url: MySQL数据库连接的IP地址和端口
			user: MySQL数据库用户名
			password: MySQL数据库密码(不支持加密)
		-->
		<writeHost host="hostM1" url="172.20.0.2:3306" user="root" password="123456">
			<readHost host="hostS1" url="172.20.0.3:3316" user="root" password="123456"/>
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

### 启动命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

