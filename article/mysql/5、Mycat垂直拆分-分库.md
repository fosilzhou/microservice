# Mycat垂直分库

## 简介

![](./assets/mycat08.png)

单个数据库负载是有限的，在应对高并发时候，达到极限时候，容易崩掉，以及当某个表数据量很大的时候，执行速度缓慢，影响应用用户体验；

这时候，mycat提供了对数据库表垂直的库拆分，不同的业务模块的表，可以放不同的数据库，通过mycat，能屏蔽拆分细节，也就是连接mycat操作，我们依然可以看做是一个库；

同时了mycat支持了把单个数据量巨大的表依据某个字段规则拆分成多个表分散存到多个数据库当中去，来减少单表压力；

在设计时，通常把同一个业务模块的表放同一个库，**依据业务模块来垂直分库；** 

垂直分库原则：

**1、根据业务模块来划分库，不搞跨库join操作，避坑；**

**2、公共表，比如数据字典表，系统属性表等，采用全局表；**

**3、有些核心表，比如用户表，部门表，权限表等，业务模块偶尔用到的时候，可以通过API方式查询，无需划分到具体业务模块里去；**

![](assets/mycat09.png)

## 部署规划

| 类型     | 分配IP | 角色      | 分配表               |
| --------- | ---------- | ------------- | ------------------ |
| Mycat     | 172.20.0.4 |  |  |
| MySQL用户库 | 172.20.0.7 | db_mall_user  | t_user             |
| MySQL订单库 | 172.20.0.8 | db_mall_order | t_order、t_product |

##  MySQL部署


### 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3307:3306 --name db_mall_user -d --net mycatnet --ip 172.20.0.7 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3308:3306 --name db_mall_order -d --net mycatnet --ip 172.20.0.8 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```



##  Mycat部署

###  配置文件

####  schema.xml

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2" />
		<table name="t_product" primaryKey="id" dataNode="dn2" />
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="db_mall_user" />
	<dataNode name="dn2" dataHost="localhost2" database="db_mall_order" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```



### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql

-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 商品表t_product
CREATE TABLE `t_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `p_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `u_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 数据字典表t_datadic:
CREATE TABLE `t_datadic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(20) DEFAULT NULL COMMENT '数据字典名称',
  `value` varchar(20) DEFAULT NULL COMMENT '数据字典值',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert  into `t_user`(`id`,`username`,`password`) values (1,'java1234','123456'),(2,'jack','123'),(3,'marry','456');

insert  into `t_product`(`id`,`name`,`price`,`description`,`stock`) values (1,'耐克鞋2020款001A','998','dfs',50),(2,'阿迪达斯休闲衣服2020Add','388','dfs',20);

insert  into `t_order`(`id`,`p_id`,`num`,`u_id`,`order_date`) values (1,1,1,2,'2020-03-16 22:10:25'),(2,2,3,3,'2020-03-16 22:10:34');

select * from t_order o left join t_product p on o.p_id=p.id
```


