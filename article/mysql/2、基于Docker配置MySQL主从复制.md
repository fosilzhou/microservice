## 基于Docker配置MySQL主从复制

### 主从原理

主从同步过程中主服务器有一个工作线程I/O dump thread，从服务器有两个工作线程I/O thread和SQL thread。

主库把外界接收的SQL请求记录到自己的binlog日志中，从库的I/O thread去请求主库的binlog日志，并将binlog日志写到中继日志中，然后从库重做中继日志的SQL语句。主库通过I/O dump thread给从库I/O thread传送binlog日志

![](assets/mycat04.png)
![](assets/mycat05.png)

### 主从部署

```shell
# 启动容器
docker run -p 3306:3306 -d -e MYSQL_ROOT_PASSWORD=123456 镜像ID

docker exec -it 容器ID /bin/bash

# 拷贝文件到宿主机
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/master/
docker cp 容器ID:/var/log/ /home/docker/master/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/slave/
docker cp 容器ID:/var/log/ /home/docker/slave/

# 启动容器
docker run -p 3306:3306 --name master -d -v /home/docker/master/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/master/log/:/var/log -e MYSQL_ROOT_PASSWORD=123456 镜像ID
docker run -p 3307:3306 --name slave -d -v /home/docker/slave/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/slave/log/:/var/log -e MYSQL_ROOT_PASSWORD=123456 镜像ID
```

### 配置固定IP

```shell
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3306:3306 --name master -d -v /home/docker/master/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/master/log/:/var/log --net mycatnet --ip 172.20.0.2 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3307:3306 --name slave -d -v /home/docker/slave/mysql.conf.d/:/etc/mysql/mysql.conf.d/ -v /home/docker/slave/log/:/var/log --net mycatnet --ip 172.20.0.3 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

## 现在启动两个Mysql容器，并且固定分配IP为172.20.0.2和172.20.0.3
## 注意：这里必须用172.20.0.2开始分配，因为172.20.0.1是网关；

#查看容器IP地址信息
docker inspect 容器id

# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```

### 主从配置

主库配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=2
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=db_mycat
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
```

主库命令

(Navicat工具：选择数据库弹出菜单-->命令列界面，执行以下命令)

```shell
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      154 | db_mycat     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set

mysql> CREATE USER 'slave1'@'172.20.0.3' IDENTIFIED BY '123456';
Query OK, 0 rows affected

mysql> GRANT REPLICATION SLAVE ON *.* TO 'slave1'@'172.20.0.3';
Query OK, 0 rows affected

mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected

mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                 | Executed_Gtid_Set |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
| mysql-bin.000001 |      761 | db_mycat     | mysql,information_schema,performation_schema,sys |                   |
+------------------+----------+--------------+--------------------------------------------------+-------------------+
1 row in set
```

从库配置

```mysql
[mysqld]
server-id=3
relay-log=mysql-relay
```

从库命令

(Navicat工具：选择数据库弹出菜单-->命令列界面，执行以下命令)

```shell
mysql> CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave1', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=761;
Query OK, 0 rows affected

mysql> START SLAVE;
Query OK, 0 rows affected

mysql> SHOW SLAVE STATUS;
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Slave_IO_State                   | Master_Host | Master_User | Master_Port | Connect_Retry | Master_Log_File  | Read_Master_Log_Pos | Relay_Log_File     | Relay_Log_Pos | Relay_Master_Log_File | Slave_IO_Running | Slave_SQL_Running | Replicate_Do_DB | Replicate_Ignore_DB | Replicate_Do_Table | Replicate_Ignore_Table | Replicate_Wild_Do_Table | Replicate_Wild_Ignore_Table | Last_Errno | Last_Error | Skip_Counter | Exec_Master_Log_Pos | Relay_Log_Space | Until_Condition | Until_Log_File | Until_Log_Pos | Master_SSL_Allowed | Master_SSL_CA_File | Master_SSL_CA_Path | Master_SSL_Cert | Master_SSL_Cipher | Master_SSL_Key | Seconds_Behind_Master | Master_SSL_Verify_Server_Cert | Last_IO_Errno | Last_IO_Error | Last_SQL_Errno | Last_SQL_Error | Replicate_Ignore_Server_Ids | Master_Server_Id | Master_UUID                          | Master_Info_File           | SQL_Delay | SQL_Remaining_Delay | Slave_SQL_Running_State                                | Master_Retry_Count | Master_Bind | Last_IO_Error_Timestamp | Last_SQL_Error_Timestamp | Master_SSL_Crl | Master_SSL_Crlpath | Retrieved_Gtid_Set | Executed_Gtid_Set | Auto_Position | Replicate_Rewrite_DB | Channel_Name | Master_TLS_Version |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
| Waiting for master to send event | 172.20.0.2  | slave1      |        3306 |            60 | mysql-bin.000001 |                 761 | mysql-relay.000002 |           320 | mysql-bin.000001      | Yes              | Yes               |                 |                     |                    |                        |                         |                             |          0 |            |            0 |                 761 |             523 | None            |                |             0 | No                 |                    |                    |                 |                   |                |                     0 | No                            |             0 |               |              0 |                |                             |                2 | 8460233e-6cdb-11ea-86de-0242ac140002 | /var/lib/mysql/master.info |         0 | NULL                | Slave has read all relay log; waiting for more updates |              86400 |             |                         |                          |                |                    |                    |                   |             0 |                      |              |                    |
+----------------------------------+-------------+-------------+-------------+---------------+------------------+---------------------+--------------------+---------------+-----------------------+------------------+-------------------+-----------------+---------------------+--------------------+------------------------+-------------------------+-----------------------------+------------+------------+--------------+---------------------+-----------------+-----------------+----------------+---------------+--------------------+--------------------+--------------------+-----------------+-------------------+----------------+-----------------------+-------------------------------+---------------+---------------+----------------+----------------+-----------------------------+------------------+--------------------------------------+----------------------------+-----------+---------------------+--------------------------------------------------------+--------------------+-------------+-------------------------+--------------------------+----------------+--------------------+--------------------+-------------------+---------------+----------------------+--------------+--------------------+
1 row in set
```



**binlog_format详解**

mysql复制主要有三种方式：基于SQL语句的复制(statement-based replication, SBR)，基于行的复制(row-based replication, RBR)，混合模式复制(mixed-based replication, MBR)。对应的，binlog的格式也有三种：STATEMENT，ROW，MIXED。

① STATEMENT模式（SBR）
每一条会修改数据的sql语句会记录到binlog中。优点是并不需要记录每一条sql语句和每一行的数据变化，减少了binlog日志量，节约IO，提高性能。缺点是在某些情况下会导致master-slave中的数据不一致(如sleep()函数， last_insert_id()，以及user-defined functions(udf)等会出现问题)

② ROW模式（RBR）
不记录每条sql语句的上下文信息，仅需记录哪条数据被修改了，修改成什么样了。而且不会出现某些特定情况下的存储过程、或function、或trigger的调用和触发无法被正确复制的问题。缺点是会产生大量的日志，尤其是alter table的时候会让日志暴涨。

③ MIXED模式（MBR）
以上两种模式的混合使用，一般的复制使用STATEMENT模式保存binlog，对于STATEMENT模式无法复制的操作使用ROW模式保存binlog，MySQL会根据执行的SQL语句选择日志保存方式。

### 主从测试

```sql
CREATE DATABASE db_mycat;
```
### 常用命令
```shell
## 启动从库（START SLAVE）
同时启动I/O 线程和SQL线程。
I/O线程从主库读取binlog，并存储到relaylog中继日志文件中。
SQL线程读取中继日志，解析后，在从库重放。

## 停止从库（STOP SLAVE）
完成停止I/O线程和SQL线程的操作

## 查询从库服务器状态（SHOW SLAVE STATUS）

## 查询主库服务器状态（SHOW MASTER STATUS）

## 重置主库（RESET MASTER）
删除所有index file 中记录的所有binlog 文件，将日志索引文件清空，创建一个新的日志文件。
这个命令通常仅仅用于第一次用于搭建主从关系的时的主库
```

### 重做主从

```shell
## 1、停止从库
STOP SLAVE;

## 2、调整配置，删除相关库、表等对象
# 将主库的 binlog_format 值由 MIXED 改为 STATEMENT 

## 3、重置主库
RESET MASTER;

## 4、查看binlog位置
SHOW MASTER STATUS;

## 5、调整binlog同步位置
CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave1', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=761;

## 6、启动从库
START SLAVE;
```
