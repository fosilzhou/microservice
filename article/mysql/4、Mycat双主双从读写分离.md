# Mycat双主双从读写分离

## 简介

上一章我们搭建了Mycat实现一主一从读写分离，现在来搭建一个双主双从高可用的读写分离，抗风险能力强，企业级架构必须是双主双从；

![](assets/mycat07.png)

## 部署规划

| 类型       | 分配IP     |
| ---------- | ---------- |
| Mycat      | 172.20.0.4 |
| MySQL主库1 | 172.20.0.2 |
| MySQL备库1 | 172.20.0.3 |
| MySQL主库2 | 172.20.0.5 |
| MySQL备库2 | 172.20.0.6 |

M1 S1 主从复制  M2  S2 主从复制；  M1,M2 互为主从复制 实现数据同步；无论哪个挂掉，Mycat可以自动切换，依然系统可用； 

## MySQL部署

### 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 拷贝文件到宿主机
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/master1/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/slave1/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/master2/
docker cp 容器ID:/etc/mysql/mysql.conf.d/ /home/docker/slave2/
# 在容器中通过（--net mycatnet --ip 172.20.0.2）指定IP地址
docker run -p 3302:3306 --name master1 -d -v /home/docker/master1/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.2 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3303:3306 --name slave1 -d -v /home/docker/slave1/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.3 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3305:3306 --name master2 -d -v /home/docker/master2/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.5 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3306:3306 --name slave2 -d -v /home/docker/slave2/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.6 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm mycatnet
# 删除所有容器
docker rm -f $(docker ps -qa)
```

### 配置文件

#### 主库M1配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=2
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=db_mycat
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
# 在作为从数据库的时候，有写入操作也要更新二进制日志文件
log-slave-updates
#表示自增长字段每次递增的量，指自增字段的起始值，其默认值是1，取值范围是1 .. 65535
auto-increment-increment=2 
# 表示自增长字段从哪个数开始，指字段一次递增多少，他的取值范围是1 .. 65535
auto-increment-offset=1
```

#### 主库M2配置

```mysql
[mysqld]
# 主服务器ID 必须唯一
server-id=5
# 开启及设置二进制日志文件名称
log_bin=mysql-bin
# 要同步的数据库
binlog-do-db=db_mycat
# 不需要同步的数据库
binlog-ignore-db=mysql    
binlog_ignore_db=information_schema
binlog_ignore_db=performation_schema
binlog_ignore_db=sys
# 设置logbin格式
binlog_format= MIXED # binlog日志格式，mysql默认采用statement，建议使用mixed
# 在作为从数据库的时候，有写入操作也要更新二进制日志文件
log-slave-updates
#表示自增长字段每次递增的量，指自增字段的起始值，其默认值是1，取值范围是1 .. 65535
auto-increment-increment=2 
# 表示自增长字段从哪个数开始，指字段一次递增多少，他的取值范围是1 .. 65535
auto-increment-offset=2
```

#### 备库S1配置

```mysql
[mysqld]
server-id=3
relay-log=mysql-relay
```


#### 备库S2配置

```mysql
[mysqld]
server-id=6
relay-log=mysql-relay
```

### MySQL执行命令

```shell
#### 配置M1与M2,S1主从复制
## M1执行命令
CREATE USER 'slave'@'172.20.0.%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'slave'@'172.20.0.%';
FLUSH PRIVILEGES;
RESET MASTER;
SHOW MASTER STATUS;

## S1执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;

## M2执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.2', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;


#### 配置M2与M1,S2主从复制
## M2执行命令
CREATE USER 'slave'@'172.20.0.%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'slave'@'172.20.0.%';
FLUSH PRIVILEGES;
RESET MASTER;
SHOW MASTER STATUS;

## S2执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.5', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;

## M1执行命令
CHANGE MASTER TO MASTER_HOST='172.20.0.5', MASTER_USER='slave', MASTER_PASSWORD='123456', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=154;
START SLAVE;
SHOW SLAVE STATUS;
```

### MySQL测试脚本

```mysql

CREATE TABLE `t_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

## 先M1执行两次观察结果
## 再M2执行两次观察结果
INSERT INTO t_test (hostname) VALUES (@@hostname);
```



## Mycat部署

![](assets/mycat06.png)



### 配置文件

#### schema.xml

```xml
<?xml version="1.0"?>
<!DOCTYPE mycat:schema SYSTEM "schema.dtd">
<!--
	主配置标签
-->
<mycat:schema xmlns:mycat="http://io.mycat/">
	<!--
		逻辑库配置标签
		name: 逻辑库名
		sqlMaxLimit: 限制每次查询数据所返回的最大行数(server.xml中的limit是整个mycat系统的默认值，这里则是当前逻辑库的默认值，默认先看schema.xml的限制数)
		checkSQLschema: 检查发给Mycat的SQL是否包含库名
	-->
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000" dataNode="dn1">
		<!--
			逻辑表配置标签
			name: 逻辑表名，配置多个表名用逗号分隔
			splitTableNames: 多个表名将此属性设置为true
			primaryKey: 物理表的真实主键
			dataNode: 表数据存储的实际物理节点名称(多个用逗号隔开，顺序一旦定义就不能修改，否则会出现混乱)
			rule: 在rule.xml中的<tableRule>中定义
		-->
		<!--
		<table name="travelrecord,address" dataNode="dn1,dn2,dn3" rule="auto-sharding-long" splitTableNames ="true"/>
		-->
	</schema>
	<!--
		dataNode数据节点配置标签
		name: 数据节点名称
		dataHost: 数据库所在主机组的名字
		database: 定义物理数据库名
	-->
	<dataNode name="dn1" dataHost="localhost1" database="db_mycat" />
	<!--
		dataHost数据库主机配置标签
		name: 数据库服务器的名称
		maxCon: Mycat连接mysql的最大连接数
		minCon: Mycat连接mysql的最小连接数
		balance: Mycat读写分离、读的负载均衡的工作方式
				0:不开启读写分离机制
				1:全部的readHost与stand by writeHost参与select语句的负载均衡(双主双从使用该配置)
				2:所有的readHost与writeHost都参与select语句的负载均衡
				3:所有的readHost参与select语句的负载均衡(单主单从使用该配置)（1.4版本新增）
		writeType: 
				0:所有写操作都是由writeHost标签配置的第一个写主机来执行，只有当第一个挂掉之后，写请求才会到第二个上（切换记录在配置文件dnindex.properties中）
				1:随机发送到写请求的writeHost上定义的写主机执行（1.5以后版本废弃）
		dbType: 使用的数据库类型（mysql、mongodb等）
		dbDriver: 所使用数据库的驱动程序
				natice：mysql原生的通讯协议
		switchType: 写数据库如何进行高可用切换
				 1: 当每一个writeHost不可访问的时候都会切换到第二个writeHost写服务器
				-1: 关闭自动切换功能
				 2: 基于MySQL主从同步的状态决定是否切换，心跳语句为 show slave status
				 3: 基于MySQL galary cluster 的切换机制（适合集群）（1.4.1版本新增）
		slaveThreshold: 配置真实MySQL与MyCat的心跳间隔
	-->
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="1" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<!--
			select user()是返回当前连接mysql的用户
		-->
		<heartbeat>select user()</heartbeat>
		<!--
			writeHost通常定义master服务器，readHost通常定义slave服务器。readHost必须镶嵌在writeHost内
			host: MySQL实例名称
			url: MySQL数据库连接的IP地址和端口
			user: MySQL数据库用户名
			password: MySQL数据库密码(不支持加密)
		-->
		<writeHost host="hostM1" url="172.20.0.2:3306" user="root" password="123456">
			<readHost host="hostS1" url="172.20.0.3:3316" user="root" password="123456"/>
		</writeHost>
		<writeHost host="hostM2" url="172.20.0.5:3306" user="root" password="123456">
			<readHost host="hostS2" url="172.20.0.6:3316" user="root" password="123456"/>
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

### 启动命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```mysql
## M1、S1、M2、S2、Mycat启动完，执行测试脚本
## M1自增ID为奇数增长，M2自增ID为偶数增长
## 观察自增ID是否为奇数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
## 停止M1数据服务，执行测试脚本
## 观察自增ID是否改为偶数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
## 开启M1数据服务，执行测试脚本
## 观察自增ID是否仍为偶数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
## 停止M2数据服务，执行测试脚本
## 观察自增ID是否改为奇数递增
INSERT INTO t_test (hostname) VALUES (@@hostname);
```

