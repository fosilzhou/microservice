## Mycat简介

Mycat 是一个开源的分布式数据库系统，但是由于真正的数据库需要存储引擎，而 Mycat 并没有存 储引擎，所以并不是完全意义的分布式数据库系统

MyCat是目前最流行的**基于Java语言编写的**数据库中间件，也可以理解为是数据库代理。在架构体系中是位于数据库和应用层之间的一个组件，并且对于应用层是透明的，即数据库 感受不到mycat的存在，认为是直接连接的mysql数据库（实际上是连接的mycat,mycat实现了mysql的原生协议）

MyCat是基于**阿里开源的Cobar**产品而研发，Cobar的稳定性、可靠性、优秀的架构和性能以及众多成熟的使用案例使得MyCat变得非常的强大。

mycat的三大功能：**分库分表、读写分离、主从切换**

![](assets/mycat01.png)

## Mycat部署

**编写Dockerfile**

```dockerfile
MAINTAINER fosilzhou<fosilzhou@foxmail.com>
 
LABEL name="Min Chow MyCat Image" \
    build-date="20200504"
    

ADD jdk-8u144-linux-x64.tar.gz /usr/local/
ADD Mycat-server-1.6.7.4-release-20200105164103-linux.tar.gz /usr/local/
 
ENV WORKPATH /usr/local/mycat/
WORKDIR $WORKPATH
 
ENV JAVA_HOME /usr/local/jdk1.8.0_144
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib:$CATALINA_HOME/bin

EXPOSE 8066
CMD /usr/local/mycat/bin/mycat console

```

```shell
# Mycat启动方式
bin目录 ./mycat console # 控制台方式启动
bin目录 ./mycat start # 后台启动

# 构建命令
docker build -f MycatDockerfile -t fosilzhou/mycat:1.0 .

# 启动容器
docker run -p 8066:8066 镜像ID

# 进入容器
docker exec -it 容器ID /bin/bash

# 拷贝文件到宿主机
docker cp 容器ID:/usr/local/mycat/conf/ /home/docker/mycat/
docker cp 容器ID:/usr/local/mycat/logs/ /home/docker/mycat/

# 启动容器
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ 镜像ID
```

## Mycat配置

Mycat的conf目录下有三个重要配置文件：

- schema.xml：定义逻辑库，表，分片节点等信息
- rule.xml：定义分片规则
- server.xml：定义用户以及系统相关变量

## Mycat连接

- 端口：8066
- 账号：root
- 密码：123456
- 逻辑库：TESTDB

![](assets/mycat02.png)

![](assets\mycat03.png)

**逻辑库，相对于Mysql这类的真实数据库而言，就是真实数据库的代理；**

**逻辑库可以对应后端多个物理数据库，逻辑库中并不保存数据；**