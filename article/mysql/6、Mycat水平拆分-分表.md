# Mycat水平分表之取模分片

## 简介

![](./assets/mycat08.png)

可以根据业务来垂直划分库，来减少服务器负载，但是假如单个表的数据量很大的时候，比如订单表，上了千万数据量，就查询就歇菜了，

这时候我们需要进行水平分表，把单个表的数据根据分表策略分到多个数据库，来减少单表压力，这就是水平分表；

## 分片规则

- 分片枚举：sharding-by-intfile

- 主键范围约定：auto-sharding-long（此分片适用于，提前规划好分片字段某个范围属于哪个分片）

- 一致性hash：sharding-by-murmur

- 字符串hash解析：sharding-by-stringhash

- 按日期（天）分片：sharding-by-date

- 按单月小时拆分：sharding-by-hour

- 自然月分片：sharding-by-month

- 取模：mod-long（此规则为对分片字段求摸运算）

- 取模范围约束：sharding-by-pattern（此种规则是取模运算与范围约束的结合，主要为了后续数据迁移做准备，即可以自主决定取模后数据的节点分布）


## 部署规划

| 类型     | 分配IP | 角色      | 分配表               |
| --------- | ---------- | ------------- | ------------------ |
| Mycat     | 172.20.0.4 |  |  |
| MySQL用户库 | 172.20.0.7 | db_mall_user  | t_user             |
| MySQL订单库 | 172.20.0.8 | db_mall_order | t_order、t_product |
| MySQL订单库 | 172.20.0.9 | db_mall_order | t_order |

##  MySQL部署


### 部署命令

```shell
docker pull mysql:5.7
# Docker默认网络模式是bridge桥接网络模式
docker network ls
# 创建自定义网络模式
docker network create --subnet=172.20.0.0/16 mycatnet
# 在容器中通过（--net extnetwork --ip 172.20.0.2）指定IP地址
docker run -p 3307:3306 --name db_mall_user -d -v /home/docker/mysql/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.7 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3308:3306 --name db_mall_order01 -d -v /home/docker/mysql/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.8 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e
docker run -p 3309:3306 --name db_mall_order02 -d -v /home/docker/mysql/mysql.conf.d/:/etc/mysql/mysql.conf.d/ --net mycatnet --ip 172.20.0.9 -e MYSQL_ROOT_PASSWORD=123456 84164b03fa2e

#查看容器IP地址信息
docker inspect 容器id
# 删除网络
docker network rm extnetwork
# 删除所有容器
docker rm -f $(docker ps -qa)
```


##  取模分片

###  配置文件

####  schema.xml

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order-rule"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="db_mall_user" />
	<dataNode name="dn2" dataHost="localhost2" database="db_mall_order" />
	<dataNode name="dn3" dataHost="localhost3" database="db_mall_order" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

#### rule.xml

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="order-rule">
       <rule>
          <columns>user_id</columns>
          <algorithm>mod-long</algorithm>
       </rule>
	</tableRule>
  
	<!-- 取模算法函数 -->
	<function name="mod-long" class="io.mycat.route.function.PartitionByMod">
		<!-- count：分片节点数 -->
		<property name="count">2</property>
	</function>
</mycat:rule>
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 172.20.0.7建 db_mall_user 数据库；
-- 172.20.0.8 和172.20.0.9 建 db_mall_order 数据库
-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `product_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (1,1,1,1001,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (2,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (3,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (4,1,1,1004,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (5,1,1,1005,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (6,1,1,1005,now());
```

## ER表分片

### 配置文件

#### schema.xml

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order-rule">
            <!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="db_mall_user" />
	<dataNode name="dn2" dataHost="localhost2" database="db_mall_order" />
	<dataNode name="dn3" dataHost="localhost3" database="db_mall_order" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 172.20.0.7建 db_mall_user 数据库；
-- 172.20.0.8 和172.20.0.9 建 db_mall_order 数据库
CREATE DATABASE IF NOT EXISTS db_mall_user DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
CREATE DATABASE IF NOT EXISTS db_mall_order DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `product_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 订单详情表t_order_detail：
CREATE TABLE `t_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `detail` varchar(200) DEFAULT NULL COMMENT '订单详情',
  `order_id` int(11) DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (1,1,1,1001,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (2,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (3,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (4,1,1,1004,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (5,1,1,1005,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (6,1,1,1005,now());

insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (1,1,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (2,2,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (3,3,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (4,4,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (5,5,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (6,6,'心随梦飞');

select * from T_ORDER;
select * from T_ORDER_DETAIL;
select * from T_ORDER inner o join T_ORDER_DETAIL d on o.id=d.order_id;
```

### 测试异常
异常信息：`ERROR 1064 (HY000): can't find (root) parent sharding node for sql:INSERT INTO t_order_detail...`
出现以上异常原因有两种情况：

- 原因一：此类ER表的插入操作不能作为一个事务进行数据提交，如果父子表在一个事务中进行提交，显然在事务没有提交前子表是无法查询附表的数据的，因此就无法确定分片节点，如果是ER关系的表在插入数据时不能再同一个事务中提交数据，需要分开提交

- 原因二：表名字大小写的问题导致。在my.cnf配置文件，在[mysqld]节点下加上如下配置`lower_case_table_names = 1`

## 全局表分片

### 配置文件

#### schema.xml

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order-rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="db_mall_user" />
	<dataNode name="dn2" dataHost="localhost2" database="db_mall_order" />
	<dataNode name="dn3" dataHost="localhost3" database="db_mall_order" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```



### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 172.20.0.7建 db_mall_user 数据库；
-- 172.20.0.8 和172.20.0.9 建 db_mall_order 数据库
-- 用户表t_user:
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 订单表t_order：
CREATE TABLE `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `product_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `order_date` datetime DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 订单详情表t_order_detail：
CREATE TABLE `t_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `detail` varchar(200) DEFAULT NULL COMMENT '订单详情',
  `order_id` int(11) DEFAULT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 数据字典表t_datadic:
CREATE TABLE `t_datadic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(20) DEFAULT NULL COMMENT '数据字典名称',
  `value` varchar(20) DEFAULT NULL COMMENT '数据字典值',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 显示表
show tables;

insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (1,1,1,1001,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (2,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (3,1,1,1002,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (4,1,1,1004,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (5,1,1,1005,now());
insert into `T_ORDER`(`id`,`product_id`,`num`,`user_id`,`order_date`) values (6,1,1,1005,now());

insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (1,1,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (2,2,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (3,3,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (4,4,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (5,5,'心随梦飞');
insert into `T_ORDER_DETAIL`(`id`,`order_id`,`detail`) values (6,6,'心随梦飞');

select * from T_ORDER;
select * from T_ORDER_DETAIL;
select * from T_ORDER inner o join T_ORDER_DETAIL d on o.id=d.order_id;

insert into `T_DATADIC`(`id`,`name`,`value`,`remark`) values (1,'gender','男','性别');
insert into `T_DATADIC`(`id`,`name`,`value`,`remark`) values (2,'gender','女','性别');
```



## 枚举分片

### 配置文件

#### schema.xml

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<!-- 取模分表 -->
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order—rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
		<!-- 枚举分表 -->
		<table name="t_product" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_intfile"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="db_mall_user" />
	<dataNode name="dn2" dataHost="localhost2" database="db_mall_order" />
	<dataNode name="dn3" dataHost="localhost3" database="db_mall_order" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

#### rule.xml

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="sharding_by_intfile">
		<rule>
			<!-- 分片字段 -->
			<columns>region</columns>
			<!-- 分片算法函数 -->
			<algorithm>hash-int</algorithm>
		</rule>
	</tableRule>
  
	<!-- 枚举分表算法函数 -->
	<function name="hash-int" class="io.mycat.route.function.PartitionByFileMap">
		<!-- 标识配置文件名称 -->
		<property name="mapFile">partition-hash-int.txt</property>
		<!-- 分片字段类型：0为int类型，非0为字符串 -->
		<property name="type">1</property>
		<!-- 默认节点：小于0表示不设置默认节点，大于等于0设置默认节点（设置了默认节点如果遇到不能识别的枚举值，就会路由到默认节点；如果没有设置默认节点，则会报错） -->
		<property name="defaultNode">0</property>
	</function>
</mycat:rule>
```

#### partition-hash-int.txt

```
## 区域编码=节点编号
36=0
44=1
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 商品表t_product
CREATE TABLE `t_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `description` text COMMENT '描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存量',
  `region` varchar(4) DEFAULT NULL COMMENT '生产地',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert into `T_PRODUCT`(`id`,`name`,`price`,`description`,`stock`,`region`) values (1,'余干辣椒', 6.25,'余干辣椒皮嫩爽口非常辣',100,'36');
insert into `T_PRODUCT`(`id`,`name`,`price`,`description`,`stock`,`region`) values (2,'龙眼',7.85,' 营养丰富,有壮阳益气、补益心脾、养血安神、润肤美容等多种功效,可治疗贫血、心悸、失眠、健忘、神经衰弱及病后、产后身体虚弱等症',230,'44');
insert into `T_PRODUCT`(`id`,`name`,`price`,`description`,`stock`,`region`) values (3,'荔枝',9.0,'果肉产鲜时半透明凝脂状，味香美，但不耐储藏',300,'44');
```



## 范围分片

### 配置文件

#### schema.xml

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<!-- 取模分表 -->
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order—rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
		<!-- 枚举分表 -->
		<table name="t_product" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_intfile"/>
		<!-- 范围分表 -->
		<table name="t_payment" primaryKey="id" dataNode="dn2,dn3" rule="auto_sharding_long"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="db_mall_user" />
	<dataNode name="dn2" dataHost="localhost2" database="db_mall_order" />
	<dataNode name="dn3" dataHost="localhost3" database="db_mall_order" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

#### rule.xml

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="auto_sharding_long">
		<rule>
			<!-- 分片字段 -->
			<columns>order_id</columns>
			<!-- 分片算法函数 -->
			<algorithm>rang-long</algorithm>
		</rule>
	</tableRule>
  
	<!-- 范围分表算法函数 -->
	<function name="rang-long" class="io.mycat.route.function.AutoPartitionByLong">
		<!-- 标识配置文件名称 -->
		<property name="mapFile">autopartition-long.txt</property>
		<!-- 默认节点：小于0表示不设置默认节点，大于等于0设置默认节点（设置了默认节点如果遇到不能识别的枚举值，就会路由到默认节点；如果没有设置默认节点，则会报错） -->
		<property name="defaultNode">0</property>
	</function>
</mycat:rule>
```

#### autopartition-long.txt

```
1-100=0
101-200=1
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 支付表 t_payment
CREATE TABLE `t_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `order_id` int(11) DEFAULT NULL COMMENT '订单编号',
  `status` int(4) DEFAULT NULL COMMENT '支付状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (1, 27, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (2, 121, 0);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (3, 725, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (4, 99, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (5, 100, 1);
insert into `T_PAYMENT`(`id`,`order_id`,`status`) values (6, 101, 1);
```


## 日期分片

### 配置文件

#### schema.xml

```xml
<mycat:schema xmlns:mycat="http://io.mycat/">
	<schema name="TESTDB" checkSQLschema="true" sqlMaxLimit="1000">
		<table name="t_user" primaryKey="id" dataNode="dn1" />
		<!-- 取模分表 -->
		<table name="t_order" primaryKey="id" dataNode="dn2,dn3" rule="order—rule">
			<!-- ER表 -->
			<childTable name="t_order_detail" primaryKey="id" joinKey="order_id" parentKey="id"/>
		</table>
		<!-- 全局表 -->
		<table name="t_datadic" primaryKey="id" dataNode="dn1,dn2,dn3" type="global"/>
		<!-- 枚举分表 -->
		<table name="t_product" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_intfile"/>
		<!-- 范围分表 -->
		<table name="t_payment" primaryKey="id" dataNode="dn2,dn3" rule="auto_sharding_long"/>
		<!-- 日期分表 -->
		<table name="t_login_log" primaryKey="id" dataNode="dn2,dn3" rule="sharding_by_date"/>
	</schema>
	
	<dataNode name="dn1" dataHost="localhost1" database="db_mall_user" />
	<dataNode name="dn2" dataHost="localhost2" database="db_mall_order" />
	<dataNode name="dn3" dataHost="localhost3" database="db_mall_order" />
	
	<dataHost name="localhost1" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM1" url="172.20.0.7:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost2" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM2" url="172.20.0.8:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	<dataHost name="localhost3" maxCon="1000" minCon="10" balance="3" writeType="0" dbType="mysql" dbDriver="native" switchType="1"  slaveThreshold="100">
		<heartbeat>select user()</heartbeat>
		<writeHost host="hostM3" url="172.20.0.9:3306" user="root" password="123456">
		</writeHost>
	</dataHost>
	
</mycat:schema>
```

#### rule.xml

```xml
<mycat:rule xmlns:mycat="http://io.mycat/">
	<tableRule name="sharding_by_date">
		<rule>
			<!-- 分片字段 -->
			<columns>login_date</columns>
			<!-- 分片算法函数 -->
			<algorithm>partbyday</algorithm>
		</rule>
	</tableRule>
  
	<!-- 按天分表算法函数 -->
	<function name="partbyday" class="io.mycat.route.function.PartitionByDate">
		<!-- 日期格式 -->
		<property name="dateFormat">yyyy-MM-dd</property>
		<!-- 开始日期 -->
		<property name="sBeginDate">2020-04-01</property>
		<!-- 结束日期：代表数据达到结束日期后循环从开始分片插入 -->
		<property name="sEndDate">2014-04-06</property>
		<!-- 分区天数：默认从开始日期算起，分隔3天为一个分区 -->
		<property name="sPartionDay">3</property>
	</function>
</mycat:rule>
```

### 部署命令

```shell
docker run -p 8066:8066 -it -v /home/docker/mycat/conf/:/usr/local/mycat/conf/ -v /home/docker/mycat/logs/:/usr/local/mycat/logs/ --net mycatnet --ip 172.20.0.4 3e5fcff9d051
```

### 测试脚本

```sql
-- 登录日志表 t_login_log
CREATE TABLE `t_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `login_date` date DEFAULT NULL COMMENT '登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (1, 19880627, '2020-04-01');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (2, 19880725, '2020-04-02');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (3, 20180121, '2020-04-03');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (4, 19880627, '2020-04-04');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (5, 19880725, '2020-04-05');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (6, 20180121, '2020-04-06');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (7, 19880627, '2020-04-07');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (8, 19880725, '2020-04-08');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (9, 20180121, '2020-04-09');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (10, 19880627, '2020-04-10');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (11, 19880725, '2020-04-11');
insert into `T_LOGIN_LOG`(`id`,`user_id`,`login_date`) values (12, 20180121, '2020-04-12');
```



## 全局自增ID

第一步：导入dbseq.sql脚本到`dn2`库

第二步：修改sequence_db_conf.properties文件 

```properties
#sequence stored in datanode
GLOBAL=dn2
COMPANY=dn2
CUSTOMER=dn2
ORDERS=dn2
```

第三步：修改server.xml配置文件的 sequnceHandlerType配置 ，将值改为`1`

- 0：本地方式
- 1：数据库方式
- 2：时间戳方式

第四步：重启Mycat

第五步：测试

```sql
DELETE FROM T_ORDER;

insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1001,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1002,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1002,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1004,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1005,now());
insert into `T_ORDER`(`id`,`p_id`,`num`,`user_id`,`order_date`) values ('next value for MYCATSEQ_GLOBAL',1,1,1005,now());
```

