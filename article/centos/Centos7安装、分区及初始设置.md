

## Centos7安装

### 进入主引导界面

![1562124733863](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171554.png)

菜单有三个选项
- 安装CentOS
- 试用CentOS
- 故障排除

进入第一项- 安装Centos【上下键选中并回车】

### 选择安装语言

![1562125281476](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171555.png)

### 系统的基本配置

![1562125544393](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171556.png)

#### 配置网络连接

![1562126071699](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171557.png)

#### 配置系统时区

![1562126149376](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171558.png)

#### 选择安装包

![1562126235018](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171559.png)

#### KDUMP配置

![1562124733864](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171600.png)

#### 配置磁盘分区

##### 选择手动分区

![1562126506304](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171601.png)

![1562126622458](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171602.png)



![1562130121071](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171603.png)

![1562130179309](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171604.png)

### 设置账号

![1562130250497](https://gitee.com/fosilzhou/imgs/raw/master/blogs/20201201171605.png)

## 磁盘分区方案

> **必须分区**
>
> - **/boot分区：**
>   - 作用：引导分区，包含了系统启动的必要内核文件，即使根分区顺坏也能正常引导启动 一般这些文件所占空间在200M以下
>   - 分区建议：分区的时候可选100M-500M之间,如果空间足够用，建议分300-500M。避免由于长期使用的冗余文件塞满这个分区
>   - 分区格式：建议ext4，按需求更改
>- **/分区（根分区）：**
> 	- 作用：所有的文件都从这里开始，你可以比喻为Windows的C盘，但其实有区别。如果你有大量的数据在根目录下（比如FTP等）可以划分大一点的空间
>   	- 分区建议：建议15G以上。看需求，根分区和home分区的大小就类似C盘和D盘的空间分布一样，主要占空间在哪儿就在那里分大容量
>   	- 分区格式：建议ext4，按需求更改
>- **/swap分区：**
> 	- 作用：类似于Windows的虚拟内存，在内存不够用时占用硬盘的虚拟内存来进行临时数据的存放，而对于linux就是swap分区
> 	- 分区建议：建议是物理内存大小的2倍，比如你电脑是4G的物理内存，swap分区可以是8G
> 	- 分区格式：swap格式

> **可选分区**
>
> - **/home分区**
> 	- 作用：存放用户数据，HOME的结构一般是 HOME/userName/userFile，如果不分则默认在/目录下
> 	- 分区建议：如果用户数据多可以将此分区适当增大，请参考“根分区”分区建议；一般硬盘的主要容量几乎都在Home分区和根分区下
> 	- 分区格式：建议ext4，按需求更改
> - **/var分区**
>  - 作用：用于log日志的文件的存放，如果不分则默认在/目录下
>   - 分区建议：如果你安装的linux是用于服务器或者经常做日志分析，请划分var分区，避免日志文件不断膨胀塞满导致根分区而引发问题
>   - 分区格式：建议ext4，按需求更改
> 

## Centos7配置

### 配置SELinux

```shell
[root@localhost ~]# setenforce 0
[root@localhost ~]# sed -i 's#^SELINUX=enforcing#SELINUX=disabled#g' /etc/selinux/config
[root@localhost ~]# getenforce
Permissive
```

### 配置防火墙

```shell
[root@localhost ~]# servcie iptables start 	#临时关闭
[root@localhost ~]# chkconfig iptables off 	#永久关闭

[root@localhost ~]# systemctl disable firewalld  #永久关闭
[root@localhost ~]# systemctl stop firewalld  #临时关闭

[root@localhost ~]# systemctl disable NetworkManager
[root@localhost ~]# systemctl stop NetworkManager
```

### 配置国内YUM源

```shell
[root@localhost ~]# mv CentOS-Base.repo CentOS-Base.repo.backup
## CentOS基础源
[root@localhost ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
## Docker源
[root@localhost ~]# wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
## K8S源
[root@localhost ~]# vim /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes Repo
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
enabled=1
[root@localhost ~]# wget https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
[root@localhost ~]# rpm --import rpm-package-key.gpg
[root@localhost ~]# yum clean all
[root@localhost ~]# yum makecache
[root@localhost ~]# yum -y update
```

### 时间同步配置

- 国内：
  - 中国国家授时中心：210.72.145.44
  - NTP服务器(上海) ：ntp.api.bz
  
- 国外：

  - 美国：time.nist.gov
  - 复旦：ntp.fudan.edu.cn
  - 微软公司授时主机(美国) ：time.windows.com
  - 台警大授时中心(台湾)：asia.pool.ntp.org

```shell
[root@localhost ~]# yum install -y ntpdate
[root@localhost ~]# ntpdate -u ntp.api.bz
[root@localhost ~]# ntpdate -u 210.72.145.44
```

### 配置主机名称

```shell
[root@localhost ~]# vim /etc/hostname
master
```

### 配置hosts

```shell
[root@localhost ~]# vim /etc/hosts
10.10.0.202 microservices701 master
10.10.0.170 microservices702 node01
10.10.0.224 microservices703 node02
10.10.0.156 microservices704 node03
```

### 更改默认启动模式

```shell
## 获取当前启动模式
systemctl get-default
## 修改为图形界面启动
systemctl set-default graphical.target
## 修改为命令行启动
systemctl set-default multi-user.target
```











安装完最小化 RHEL/CentOS 7 后需要做的 30 件事情

https://linux.cn/article-5341-1.html

https://linux.cn/article-5342-1.html

https://linux.cn/article-5351-1.html

https://linux.cn/article-5366-1.html

https://linux.cn/article-5367-1.html