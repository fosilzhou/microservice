# 构建企业光盘源



# 构建企业YUM源

## 搭建YUM源服务器

### 创建yum本地源

```shell
### 创建yun源存放的文件夹
[root@localhost ~]# mkdir -p /var/www/html/centos/
### 安装createrepo命令
[root@localhost ~]# yum install createrepo* -y
### 切换到目录
[root@localhost ~]# cd /var/www/html
### 创建本地源
[root@localhost html]# createrepo centos/
### 切换到目录
[root@localhost html]# cd centos/repodata/

[root@localhost repodata]# ll
total 32008
-rw-r--r--. 1 root root 3632808 Jun  2 14:23 110b3045fb0e29d64a65df3374a3cd944112ca9f8547c0aed0954984ed94b399-primary.xml.gz
-rw-r--r--. 1 root root 8495665 Jun  2 14:24 16ff9ca9d68646dbe6a180e72d3f37c9335f0b981d2d02a22fd4f70a26f6bd07-filelists.sqlite.bz2
-rw-r--r--. 1 root root 8446731 Jun  2 14:23 58111e995557613118b280687952907f68a41d984b64b4fe57eed1500d648f15-filelists.xml.gz
-rw-r--r--. 1 root root 1737451 Jun  2 14:23 7fd79adc2fbf15086de552e644d7cf524dba3c088344dc57ab6ea4d0b92cb8c3-other.xml.gz
-rw-r--r--. 1 root root 7588907 Jun  2 14:24 b2b08a0d681e8ed5bd967da2aa71662a3a4dbe83c6ffc21415943f52d20df8a4-primary.sqlite.bz2
-rw-r--r--. 1 root root 2856112 Jun  2 14:23 f150d16d85919991bc676fafa506f77e3d640c83bca335e27ed3f2ec7a1cf975-other.sqlite.bz2
-rw-r--r--. 1 root root    3012 Jun  2 14:24 repomd.xml
```

### 发布yum本地源

```shell
### 安装httpd web服务
[root@localhost ~]# yum install httpd httpd-devel -y
### 创建apache用户和组
[root@localhost ~]# useradd apache -g apache
### 重启httpd服务
[root@localhost ~]# systemctl restart httpd.service
### 临时关闭SeLinux应用级安全策略
[root@localhost ~]# setenforce 0
### 查看httpd进程是否启动
[root@localhost ~]# ps -ef |grep httpd
```

### 同步外网yum源

```shell
[root@localhost yum.repos.d]# cd /etc/yum.repos.d/
[root@localhost yum.repos.d]# mv CentOS-Base.repo CentOS-Base.repo.backup
### CentOS基础源
[root@localhost yum.repos.d]# wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
### Docker源
[root@localhost yum.repos.d]# wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
### 清空本地源缓存
[root@localhost ~]# yum clean all
### 安装yum源相关工具命令
[root@localhost ~]# yum install yum-utils createrepo –y
### 查看yum源信息
[root@localhost ~]# yum repolist
### 同步外网base软件包
[root@localhost ~]# reposync -r base -p /var/www/html/centos/
### 同步外网updates软件包
[root@localhost ~]# reposync -r updates -p /var/www/html/centos/
### 同步外网所有软件包
[root@localhost ~]# reposync -p /var/www/html/centos/
### 执行createrepo更新本地yum仓库
[root@localhost ~]# createrepo /var/www/html/centos/
```

## 客户端使用yum源

### 编写本地yum配置文件

```properties
# vim /etc/yum.repos.d/http.repo
[base]
name="CentOS7 HTTP YUM"
baseurl=http://192.168.1.115/centos/
gpgcheck=0
enabled=1
[updates]
name="CentOS7 HTTP YUM"
baseurl=http://192.168.1.115/centos
gpgcheck=0
enabled=1
```

### 测试本地yum源使用

```shell
### 清空本地源缓存
[root@localhost ~]# yum clean all
### 安装ntpdate软件
[root@localhost ~]# yum install ntpdate -y
```

## 扩展yum源软件包

### 下载yum源软件包



### 更新yum源仓库

