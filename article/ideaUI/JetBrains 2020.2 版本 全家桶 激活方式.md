## 一、下载ideaUI

本教程支持IDEA2020.2.3以下所有版本，都可以激活

百度网盘：

阿里网盘：

## 二、开始激活

**1、** 下载完成后，双击 `ideaIU-2020.2.exe`,打开安装软件；

> PS: 确保电脑没有安装老版本软件，如有请卸载。

![img](assets/1.png)

**2、** 安装目录选择；

![img](assets/2.png)

**3、** 按自己电脑配置勾选：

![img](assets/3.png)

**4、** 点击 `next`, 安心等待其安装完成：

![img](assets/4.png)

**5、** 安装完成后，勾选 `Run IntelliJ IDEA`，点击 `finish` 运行软件:

![img](assets/5.png)

**6、** 会先弹出一个注册框，勾选 `Evaluate for free`, 点击 `Evaluate`:

![img](assets/6.png)

**补丁，拖不进去，请重启 IDEA ，新建一个Java 项目，写一个空的 main 方法，再次试试**

![img](assets/7.png)

**补丁，拖不进去，请重启 IDEA ，新建一个Java 项目，写一个空的 main 方法，再次试试**

**注意：激活补丁，拖进去之后，激活补丁的位置不要更换，不要删除。否则激活之后还会失效，**

**注意：激活补丁，拖进去，等于是程序，帮你去写一个激活补丁的位置，你换补丁jar位置，或者，删除补丁jar，就找不到激活补丁了**

**8、** 拖入补丁后会弹框，点击 `restart` 重启 **idea**：

![img](assets/8.png)

**9、** 配置助手会提示您，需要使用哪种激活方式，这里我们选择默认的 `Activation Code`，通过注册码来激活，点击`为IDEA安装`：

![img](assets/9.png)

![img](assets/10.png)

**10、** 点击`是`，重启 **IDEA** 即可

## 三、验证激活

验证步骤如下：

**1、** 进入 **IDEA** 界面后，点击 `Help` -> `Register` 查看：

![img](assets/11.png)

**2、** 可以看到，已经成功激活至 2089 年，为确保不失效，请勿随意更新

![img](assets/12.png)