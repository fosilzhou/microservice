## MongoDB安装

```shell
## MongoDB安装

### 下载软件
[root@node03 ~]# wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-rhel70-3.6.14.tgz
### 解压缩到指定位置
[root@node03 ~]# tar -zxvf mongodb-linux-x86_64-rhel70-3.6.14.tgz -C /usr/local/
### 修改文件夹命名
[root@node03 local]# mv mongodb-linux-x86_64-rhel70-3.6.14 mongodb-3.6.14
### 创建超链接
[root@node03 local]# ln -s /usr/local/mongodb-3.6.14 /usr/local/mongodb
### 配置环境变量
[root@node03 ~]# echo 'export PATH=/usr/local/mongodb/bin:$PATH' >> /etc/profile
[root@node03 ~]# source /etc/profile

```

## 单机模式配置

```shell
## 单机模式配置
[root@node03 simple]# mkdir data
[root@node03 simple]# mkdir logs
[root@node03 simple]# mkdir conf
[root@node03 simple]# cat conf/mongodb.conf 
# 数据存储的位置
dbpath=./data
# 所有的输出位置
logpath=./logs/mongo.log
# 设置绑定IP
bind_ip=10.10.0.156,127.0.0.1
# 设置端口号
port=27017
# 设置守护进程
fork=true
[root@node03 simple]# mongod --config conf/mongodb.conf
### 强烈说明使用 kill -15 关闭mongodb进程服务，不能使用 kill -9 关闭服务
### 否则会出现 ERROR: child process failed, exited with error number 100 错误提示
[root@node03 simple]# kill -15 进程ID

## 测试数据
show dbs;
use zlmlive;
db.users.insert({name:"心随梦飞",birthday:"1988-07-25"});
db.users.insert({name:"清水涧",birthday:"1988-06-27"});

```

