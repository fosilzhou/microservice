## MongoDB安装

```shell
## MongoDB安装

### 下载软件
[root@node03 ~]# wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-rhel70-3.6.14.tgz
### 解压缩到指定位置
[root@node03 ~]# tar -zxvf mongodb-linux-x86_64-rhel70-3.6.14.tgz -C /usr/local/
### 修改文件夹命名
[root@node03 local]# mv mongodb-linux-x86_64-rhel70-3.6.14 mongodb-3.6.14
### 创建超链接
[root@node03 local]# ln -s /usr/local/mongodb-3.6.14 /usr/local/mongodb
### 配置环境变量
[root@node03 ~]# echo 'export PATH=/usr/local/mongodb/bin:$PATH' >> /etc/profile
[root@node03 ~]# source /etc/profile

```

## 主从复制模式配置

```shell
## 主从复制模式配置
一、部署规划
    主服务器{port:27100}
    从服务器{port:27101}

二、部署步骤
[root@node03 masterSlave]# vim master/conf/mongodb.conf
# 数据存储的位置
dbpath=./master/data
# 所有的输出位置
logpath=./master/logs/mongo.log
# 设置绑定IP
bind_ip=10.10.0.156,127.0.0.1
# 设置端口号
port=27100
# 设置守护进程
fork=true

##主服务器标识
master=true

[root@node03 masterSlave]# vim slave/conf/mongodb.conf
# 数据存储的位置
dbpath=./master/data
# 所有的输出位置
logpath=./master/logs/mongo.log
# 设置绑定IP
bind_ip=10.10.0.156,127.0.0.1
# 设置端口号
port=27101
# 设置守护进程
fork=true

# 从服务器标识
slave=true
# 关联到主数据库
source = 127.0.0.1:27100

## 启动mongo主库
[root@node03 masterSlave]#  mongod --config master/conf/mongo.conf 
about to fork child process, waiting until server is ready for connections.
forked process: 129327
child process started successfully, parent exiting
## 启动mongo备库
[root@node03 masterSlave]#  mongod --config slave/conf/mongo.conf 
about to fork child process, waiting until server is ready for connections.
forked process: 129655
child process started successfully, parent exiting

### 测试数据
[root@node03 ~]# mongo 127.0.0.1:27100
> use zlmlive;
switched to db zlmlive
> db.users.insert({name:"心随梦飞",birthday:"1988-07-25"});
WriteResult({ "nInserted" : 1 })
> db.users.insert({name:"清水涧",birthday:"1988-06-27"});
WriteResult({ "nInserted" : 1 })
> db.users.find()
{ "_id" : ObjectId("5d80c655686552d016236028"), "name" : "心随梦飞", "birthday" : "1988-07-25" }
{ "_id" : ObjectId("5d80c656686552d016236029"), "name" : "清水涧", "birthday" : "1988-06-27" }
>
[root@node03 ~]# mongo 127.0.0.1:27101
> use zlmlive
switched to db zlmlive
> db.users.find()
Error: error: {
        "ok" : 0,
        "errmsg" : "not master and slaveOk=false",
        "code" : 13435,
        "codeName" : "NotMasterNoSlaveOk"
}
>


三、主从复制的其他设置项
    --only：从节点-->指定复制某个数据库,默认是复制全部数据库
    --slavedelay：从节点-->设置主数据库同步数据的延迟(单位是秒)
    --fastsync：从节点-->以主数据库的节点快照为节点启动从数据库
    --autoresync：从节点-->如果不同步则从新同步数据库
    --oplogSize：主节点-->设置oplog的大小(主节点操作记录存储到local的oplog中)

四、利用shell动态删除从服务器
    通过控制台连接到需要删除的从服务器
    切换到local库：use local
    查看local中的文档：show collections
    查看关联的主服务器信息：db.sources.find()
    删除主服务器关联：db.sources.remove({"host":"127.0.0.1:27101"})

五、利用shell动态添加(挂接)从服务器
    通过控制台连接到准备要设置的从服务器（127.0.0.1:27102）
    切换到local库：use local
    查看关联的主服务器信息：db.sources.find()
    添加主服务器关联：db.sources.insert({"host":"127.0.0.1:27101"})

六、查看主服务器关联哪些从服务器
    通过控制台连接到主服务器
    切换到local库：use local
    查看local中的文档：show collections
    查看关联的从服务器信息：db.slaves.find()
```

