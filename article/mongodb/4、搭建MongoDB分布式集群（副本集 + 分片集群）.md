## 1、部署规划

![img](assets/242916-20180313222747434-1293437958.png) 

**说明：**整个集群由两个分片组成的分布式集群，每个分片都是一个PSS(Primary-Secondary-Secondary)模式的数据副本集； Config副本集采用PSS(Primary-Secondary-Secondary)模式 

- 端口通讯

当前集群中存在shard、config、mongos共12个进程节点

| 实例类型             | 节点01 | 节点02 | 节点03 |
| -------------------- | ------ | ------ | ------ |
| 路由服务器（mongos） | 25001  | 25002  | 25003  |
| 配置服务器（config） | 26001  | 26002  | 26003  |
| 分片服务器（shard1） | 27001  | 27002  | 27003  |
| 分片服务器（shard2） | 27004  | 27005  | 27006  |

- 内部鉴权

节点间鉴权采用keyfile方式实现鉴权，mongos与分片之间、副本集节点之间共享同一套keyfile文件。 [官方说明](https://www.cnblogs.com/littleatp/p/8563273.html) 

- 账户设置

管理员账户：admin/admin@123，具有集群及所有库的管理权限
应用账号：appuser/appuser@123，具有appdb的owner权限 

- 初始化权限

keyfile方式默认会开启鉴权，而针对初始化安装的场景，Mongodb提供了[localhost-exception机制](https://docs.mongodb.com/manual/core/security-users/#localhost-exception)
可以在首次安装时通过本机创建用户、角色，以及副本集初始操作。 

## 2、准备工作

```shell
### 下载软件
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-rhel70-3.6.14.tgz
### 解压缩到指定位置
tar -zxvf mongodb-linux-x86_64-rhel70-3.6.14.tgz -C /usr/local/
### 修改文件夹命名
mv mongodb-linux-x86_64-rhel70-3.6.14 mongodb-3.6.14
### 创建超链接
ln -s /usr/local/mongodb-3.6.14 /usr/local/mongodb
### 配置环境变量
echo 'export PATH=/usr/local/mongodb/bin:$PATH' >> /etc/profile
source /etc/profile
```

## 3、创建节点文件夹

```shell
WORK_DIR=/usr/local/mongodb

mkdir -p $WORK_DIR/nodes/config/n1/data
mkdir -p $WORK_DIR/nodes/config/n2/data
mkdir -p $WORK_DIR/nodes/config/n3/data

mkdir -p $WORK_DIR/nodes/shard1/n1/data
mkdir -p $WORK_DIR/nodes/shard1/n2/data
mkdir -p $WORK_DIR/nodes/shard1/n3/data

mkdir -p $WORK_DIR/nodes/shard2/n1/data
mkdir -p $WORK_DIR/nodes/shard2/n2/data
mkdir -p $WORK_DIR/nodes/shard2/n3/data

mkdir -p $WORK_DIR/nodes/mongos/n1
mkdir -p $WORK_DIR/nodes/mongos/n2
mkdir -p $WORK_DIR/nodes/mongos/n3
```

## 4、创建keyfile密钥文件

```shell
#### mongo.key 采用随机算法生成，用作节点内部通讯的密钥文件
cd $WORK_DIR
mkdir keyfile
openssl rand -base64 756 > mongo.key
chmod 400 mongo.key
mv mongo.key keyfile
```

## 5、创建配置文件

```shell
cd $WORK_DIR
mkdir conf
```

mongod实例共享的配置文件

```yaml
##vim mongo_node.conf

storage:
  engine: wiredTiger
  directoryPerDB: true
  journal:
    enabled: true
systemLog:
  destination: file
  logAppend: true
operationProfiling:
  slowOpThresholdMs: 10000
replication:
  oplogSizeMB: 10240
processManagement:
  fork: true
security:
  authorization: "enabled"
```

路由配置文件
```yaml
## vim mongos.conf

systemLog:
  destination: file
  logAppend: true
processManagement:
  fork: true
```

## 6、启动Config副本集实例

```shell
WORK_DIR=/usr/local/mongodb
KEYFILE=$WORK_DIR/keyfile/mongo.key
CONFFILE=$WORK_DIR/conf/mongo_node.conf
MONGOD=$WORK_DIR/bin/mongod

echo "启动Config副本集"

$MONGOD --port 26001 --configsvr --replSet configReplSet --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/config/n1/data --pidfilepath $WORK_DIR/nodes/config/n1/db.pid --logpath $WORK_DIR/nodes/config/n1/db.log --config $CONFFILE
$MONGOD --port 26002 --configsvr --replSet configReplSet --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/config/n2/data --pidfilepath $WORK_DIR/nodes/config/n2/db.pid --logpath $WORK_DIR/nodes/config/n2/db.log --config $CONFFILE
$MONGOD --port 26003 --configsvr --replSet configReplSet --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/config/n3/data --pidfilepath $WORK_DIR/nodes/config/n3/db.pid --logpath $WORK_DIR/nodes/config/n3/db.log --config $CONFFILE
```

## 7、执行Config副本集初始化

```shell
#### 其中configsvr:true指明这是一个用于分片集群的Config副本集

./bin/mongo --port 26001 --host 127.0.0.1
> cfg={
    _id:"configReplSet", 
    configsvr: true,
    members:[
        {_id:0, host:'127.0.0.1:26001'},
        {_id:1, host:'127.0.0.1:26002'}, 
        {_id:2, host:'127.0.0.1:26003'}
    ]};
> rs.initiate(cfg);
```

## 8、启动分片实例

```shell

WORK_DIR=/usr/local/mongodb
KEYFILE=$WORK_DIR/keyfile/mongo.key
CONFFILE=$WORK_DIR/conf/mongo_node.conf
MONGOD=$WORK_DIR/bin/mongod

echo "启动分片1的副本集"

$MONGOD --port 27001 --shardsvr --replSet shard1 --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/shard1/n1/data --pidfilepath $WORK_DIR/nodes/shard1/n1/db.pid --logpath $WORK_DIR/nodes/shard1/n1/db.log --config $CONFFILE
$MONGOD --port 27002 --shardsvr --replSet shard1 --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/shard1/n2/data --pidfilepath $WORK_DIR/nodes/shard1/n2/db.pid --logpath $WORK_DIR/nodes/shard1/n2/db.log --config $CONFFILE
$MONGOD --port 27003 --shardsvr --replSet shard1 --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/shard1/n3/data --pidfilepath $WORK_DIR/nodes/shard1/n3/db.pid --logpath $WORK_DIR/nodes/shard1/n3/db.log --config $CONFFILE

echo "启动分片2的副本集"

$MONGOD --port 27004 --shardsvr --replSet shard2 --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/shard2/n1/data --pidfilepath $WORK_DIR/nodes/shard2/n1/db.pid --logpath $WORK_DIR/nodes/shard2/n1/db.log --config $CONFFILE
$MONGOD --port 27005 --shardsvr --replSet shard2 --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/shard2/n2/data --pidfilepath $WORK_DIR/nodes/shard2/n2/db.pid --logpath $WORK_DIR/nodes/shard2/n2/db.log --config $CONFFILE
$MONGOD --port 27006 --shardsvr --replSet shard2 --keyFile $KEYFILE --dbpath $WORK_DIR/nodes/shard2/n3/data --pidfilepath $WORK_DIR/nodes/shard2/n3/db.pid --logpath $WORK_DIR/nodes/shard2/n3/db.log --config $CONFFILE
```

## 9、执行分片副本集初始化

```shell
./bin/mongo --port 27001 --host 127.0.0.1
> cfg={
    _id:"shard1", 
    members:[
        {_id:0, host:'127.0.0.1:27001'},
        {_id:1, host:'127.0.0.1:27002'},
        {_id:2, host:'127.0.0.1:27003'}
    ]};
> rs.initiate(cfg);

./bin/mongo --port 27004 --host 127.0.0.1
> cfg={
    _id:"shard2", 
    members:[
        {_id:0, host:'127.0.0.1:27004'},
        {_id:1, host:'127.0.0.1:27005'},
        {_id:2, host:'127.0.0.1:27006'}
    ]};
> rs.initiate(cfg);
```


## 10、启动路由实例

```shell
WORK_DIR=/usr/local/mongodb
KEYFILE=$WORK_DIR/keyfile/mongo.key
CONFFILE=$WORK_DIR/conf/mongos.conf
MONGOS=$WORK_DIR/bin/mongos

echo "启动路由服务"

$MONGOS --port=25001 --configdb configReplSet/127.0.0.1:26001,127.0.0.1:26002,127.0.0.1:26003 --keyFile $KEYFILE --pidfilepath $WORK_DIR/nodes/mongos/n1/db.pid --logpath $WORK_DIR/nodes/mongos/n1/db.log --config $CONFFILE
$MONGOS --port 25002 --configdb configReplSet/127.0.0.1:26001,127.0.0.1:26002,127.0.0.1:26003 --keyFile $KEYFILE --pidfilepath $WORK_DIR/nodes/mongos/n2/db.pid --logpath $WORK_DIR/nodes/mongos/n2/db.log --config $CONFFILE
$MONGOS --port 25003 --configdb configReplSet/127.0.0.1:26001,127.0.0.1:26002,127.0.0.1:26003 --keyFile $KEYFILE --pidfilepath $WORK_DIR/nodes/mongos/n3/db.pid --logpath $WORK_DIR/nodes/mongos/n3/db.log --config $CONFFILE

```
## 11、执行路由初始化

```shell
./bin/mongo --port 25001 --host 127.0.0.1

> sh.addShard("shard1/127.0.0.1:27001")
> sh.addShard("shard2/127.0.0.1:27004")
```

## 12、初始化用户

连接到其中一个mongos实例，添加管理员用户

```shell
./bin/mongo --port 25001 --host 127.0.0.1
mongos> use admin
mongos> db.createUser({
    user:'admin',pwd:'admin@123',
    roles:[
        {role:'clusterAdmin',db:'admin'},
        {role:'userAdminAnyDatabase',db:'admin'},
        {role:'dbAdminAnyDatabase',db:'admin'},
        {role:'readWriteAnyDatabase',db:'admin'}
]})

## admin用户具有集群管理权限、所有数据库的操作权限

mongos> use admin
mongos> db.auth('admin','admin@123')
```

## 13、检查集群状态

```shell
mongos> sh.status()
--- Sharding Status --- 
  sharding version: {
        "_id" : 1,
        "minCompatibleVersion" : 5,
        "currentVersion" : 6,
        "clusterId" : ObjectId("5d8dc9833896b5471346bc65")
  }
  shards:
        {  "_id" : "shard1",  "host" : "shard1/127.0.0.1:27001,127.0.0.1:27002,127.0.0.1:27003",  "state" : 1 }
        {  "_id" : "shard2",  "host" : "shard2/127.0.0.1:27004,127.0.0.1:27005,127.0.0.1:27006",  "state" : 1 }
  active mongoses:
        "3.6.14" : 3
  autosplit:
        Currently enabled: yes
  balancer:
        Currently enabled:  yes
        Currently running:  no
        Failed balancer rounds in last 5 attempts:  0
        Migration Results for the last 24 hours: 
                No recent migrations
  databases:
        {  "_id" : "config",  "primary" : "config",  "partitioned" : true }
                config.system.sessions
                        shard key: { "_id" : 1 }
                        unique: false
                        balancing: true
                        chunks:
                                shard1  1
                        { "_id" : { "$minKey" : 1 } } -->> { "_id" : { "$maxKey" : 1 } } on : shard1 Timestamp(1, 0) 



```

## 14、数据验证操作

```shell
## 创建业务库用户appdb并为数据库实例appdb启动分片
mongos> use appdb
mongos> db.createUser({user:'appuser',pwd:'appuser@123',roles:[{role:'dbOwner',db:'appdb'}]})
mongos> sh.enableSharding("appdb")

## 创建数据集合，并为其执行分片初始化
mongos> use appdb
mongos> db.createCollection("users")
mongos> db.users.ensureIndex({userId:1})
mongos> sh.shardCollection("appdb.users", {userId:"hashed"}, false, { numInitialChunks: 4} )

## 往集合添加10万条记录，观察chunks的分布情况
mongos> use appdb
mongos> var cnt = 0;
mongos> for(var i=0; i<1000; i++){
    var dl = [];
    for(var j=0; j<100; j++){
        dl.push({
                "userId" : "USER-" + i + "-" + j,
                "username" : "chowmin",
                "password" : "123456",
                "nickname" : "心随梦飞",
                "realname" : "周路敏",
                "email" : "zhoulumin@foresee.com.cn",
                "address" : {
                      "province" : "江西",
                      "city" : "上饶",
                      "county" : "万年",
                      "detail" : "东方翰府"
                },
                "createTime" : new Date()
            });
      }
      cnt += dl.length;
      db.users.insertMany(dl);
      print("insert ", cnt);
}

## 查看数据分布情况
mongos> db.users.getShardDistribution()

Shard shard1 at shard1/127.0.0.1:27001,127.0.0.1:27002,127.0.0.1:27003
 data : 13.99MiB docs : 50298 chunks : 2
 estimated data per chunk : 6.99MiB
 estimated docs per chunk : 25149

Shard shard2 at shard2/127.0.0.1:27004,127.0.0.1:27005,127.0.0.1:27006
 data : 13.83MiB docs : 49702 chunks : 2
 estimated data per chunk : 6.91MiB
 estimated docs per chunk : 24851

Totals
 data : 27.82MiB docs : 100000 chunks : 4
 Shard shard1 contains 50.29% data, 50.29% docs in cluster, avg obj size on shard : 291B
 Shard shard2 contains 49.7% data, 49.7% docs in cluster, avg obj size on shard : 291B

```

通过mongostat工具查看mongodb运行状态

```shell
[root@node03 ~]# mongostat -h 127.0.0.1:25001 -uadmin -padmin@123 --authenticationDatabase admin
insert query update delete getmore command flushes mapped vsize   res faults qrw arw net_in net_out conn                time
    *0    *0     *0     *0       0     2|0       0     0B  303M 16.0M      0 0|0 0|0   296b   21.6k    2 Sep 27 17:06:31.111
    *0    *0     *0     *0       0     2|0       0     0B  303M 16.0M      0 0|0 0|0   286b   20.9k    2 Sep 27 17:06:32.109
    *0    *0     *0     *0       0     1|0       0     0B  303M 16.0M      0 0|0 0|0   285b   20.8k    2 Sep 27 17:06:33.111
    *0    *0     *0     *0       0     2|0       0     0B  303M 16.0M      0 0|0 0|0   286b   20.8k    2 Sep 27 17:06:34.109
    *0    *0     *0     *0       0     1|0       0     0B  303M 16.0M      0 0|0 0|0   285b   20.8k    2 Sep 27 17:06:35.110
    *0    *0     *0     *0       0     1|0       0     0B  303M 16.0M      0 0|0 0|0   285b   20.8k    2 Sep 27 17:06:36.110
    *0    *0     *0     *0       0     1|0       0     0B  303M 16.0M      0 0|0 0|0   285b   20.8k    2 Sep 27 17:06:37.111
    *0    *0     *0     *0       0     1|0       0     0B  303M 16.0M      0 0|0 0|0   285b   20.8k    2 Sep 27 17:06:38.111
    *0    *0     *0     *0       0     2|0       0     0B  303M 16.0M      0 0|0 0|0   286b   20.8k    2 Sep 27 17:06:39.110
    *0    *0     *0     *0       0     2|0       0     0B  303M 16.0M      0 0|0 0|0   286b   20.8k    2 Sep 27 17:06:40.108

```

- inserts:当前mongodb插入数量
- query:当前mongodb的查询数量，数量以每秒为单位
- update:当前mongodb的更新数量
- delete:当前mongodb的删除数量
- getmore:在进行mongodb查询时，每次并不是返回所有的数据，比如要一次查询一百万条，每次只会返回一定量的数据，当每次find的时候，getmore用来获取以后的数据
- command:执行命令的数量
- flushes:在mongodb写入数据，查询数据时，我们看到的数据是在内存中，实际上并不是在内存中，有些是在硬盘上的，每个一段时间，mongodb会把内存数据刷到硬盘上，flushes就是看mongodb隔多久往磁盘上刷一次
- mapped,vsize,res：mongodb所占据到磁盘空间大小和申请的内存大小
- faults：如果数据没有加塞到内存中，需要到硬盘上读取
- locked:锁的情况
- ids miss：表明当前查询没有使用索引的情况
- qr|qw：在写入或读取数据时，并不是来个请求就处理，而是放到队列中，如果请求比较多，或者mongodb处理比较慢，这样qr,qw比较高，一般到qr,qw比较高时，比如几百上千，mongodb的性能会出现明显的下降
- ar|aw：当前活跃的客户端的数目
- netIn,netOut：mongodb使用网卡的输入流量
- conn:连接到mongodb到连接数量


