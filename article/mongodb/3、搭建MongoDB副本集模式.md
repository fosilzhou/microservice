## MongoDB安装

```shell
## MongoDB安装

### 下载软件
[root@node03 ~]# wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-rhel70-3.6.14.tgz
### 解压缩到指定位置
[root@node03 ~]# tar -zxvf mongodb-linux-x86_64-rhel70-3.6.14.tgz -C /usr/local/
### 修改文件夹命名
[root@node03 local]# mv mongodb-linux-x86_64-rhel70-3.6.14 mongodb-3.6.14
### 创建超链接
[root@node03 local]# ln -s /usr/local/mongodb-3.6.14 /usr/local/mongodb
### 配置环境变量
[root@node03 ~]# echo 'export PATH=/usr/local/mongodb/bin:$PATH' >> /etc/profile
[root@node03 ~]# source /etc/profile

```

## 副本集模式配置

```shell
# 副本集模式配置
一、部署规划
    副本服务器01{port:27201}
    副本服务器02{port:27202}
    副本服务器03{port:27203}

二、部署步骤

[root@node03 mongodb]# vim replicaSet/node01/conf/mongodb.conf 
# 数据存储的位置
dbpath=./replicaSet/node01/data
# 所有的输出位置
logpath=./replicaSet/node01/logs/mongo.log
# 设置绑定IP
bind_ip=10.10.0.156,127.0.0.1
# 设置端口号
port=27201
# 设置守护进程
fork=true

# 设置副本集名称
replSet=zlmlive
##设定同伴副本
replSet=child/127.0.0.1:27202

[root@node03 mongodb]# vim replicaSet/node02/conf/mongodb.conf 
# 数据存储的位置
dbpath=./replicaSet/node02/data
# 所有的输出位置
logpath=./replicaSet/node02/logs/mongo.log
# 设置绑定IP
bind_ip=10.10.0.156,127.0.0.1
# 设置端口号
port=27202
# 设置守护进程
fork=true

# 设置副本集名称
replSet=zlmlive
##设定同伴副本
replSet=child/127.0.0.1:27203

[root@node03 mongodb]# vim replicaSet/node03/conf/mongodb.conf 
# 数据存储的位置
dbpath=./replicaSet/node03/data
# 所有的输出位置
logpath=./replicaSet/node03/logs/mongo.log
# 设置绑定IP
bind_ip=10.10.0.156,127.0.0.1
# 设置端口号
port=27203
# 设置守护进程
fork=true

# 设置副本集名称
replSet=zlmlive
##设定同伴副本
replSet=child/127.0.0.1:27201

## 启动服务
[root@node03 mongodb]# mongod --config replicaSet/node01/conf/mongodb.conf
[root@node03 mongodb]# mongod --config replicaSet/node02/conf/mongodb.conf
[root@node03 mongodb]# mongod --config replicaSet/node03/conf/mongodb.conf

三、初始化副本集
	在其中一台副本服务器中执行初始化操作
	use admin;
	db.runCommand({"replSetInitiate":
	   {
		  "_id":'child',
		   "members":[{
				"_id":1,
			"host":"127.0.0.1:27201"
			},{
			"_id":2,
			"host":"127.0.0.1:27202"
			},{
			"_id":3,
			"host":"127.0.0.1:27203"
			}]
		}
	});

# 说明：备份副本服务器不能执行任何查询操作的（控制台标识：SECONDARY）；活跃副本服务器才能执行查询操作（控制台标识：PRIMARY）

四、在活跃服务器中查看副本集状态
	rs.status();

五、在活跃服务器中执行以下脚本
    show dbs;
    use zlmlive;
    db.users.insert({name:"心随梦飞",birthday:"1988-07-25"});
    db.users.insert({name:"清水涧",birthday:"1988-06-27"});

六、验证备份服务中的数据
	先关闭活跃副本服务器，使备份副本服务器变为活跃服务器再查询数据
	show dbs;
	use zlmlive;
	db.users.find();

七、节点和初始化高级参数
	standard：常规节点-->参与投票有可能成为活跃节点
	passive：副本节点-->参与投票,但是不能成为活跃节点
	arbiter：仲裁节点-->只是参与投票不复制节点也不能成为活跃节点

八、高级参数
	Priority：0到1000之间 ,0代表是副本节点 ,1到1000是常规节点
	arbiterOnly：true仲裁节点
	用法：
	members:[{
		"_id":1,
		"host":"127.0.0.1:27201",
		"arbiterOnly":true
	}]

九、Oplog（在主从复制中使用）
	他是被存储在本地数据库local中的,他的每一个文档保证这一个节点操作，如果想故障恢复可以更彻底oplog可已经尽量设置大一些用来保存更多的操作信息
	改变oplog大小：主库 --master --oplogSize 1024

```

